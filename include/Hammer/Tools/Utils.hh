///
/// @file  Utils.hh
/// @brief Hammer utility functions
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_TOOLS_UTILS
#define HAMMER_TOOLS_UTILS

#include <exception>
#include <map>
#include <unordered_map>
#include <string>
#include <complex>
#include <algorithm>
#include <iterator>
#include <vector>

#include <boost/functional/hash.hpp>

#include "Hammer/Config/HammerConfig.hh"

#define UNUSED(x) ((void)(x))

namespace Hammer {

    /// @brief
    /// @return
    inline std::string version() {
        return HAMMER_NAME " ver. " HAMMER_VERSION;
    }

    template<typename KeyType, typename ValueType>
    ValueType getOrDefault(const std::map<KeyType, ValueType>& data, KeyType key, ValueType fallback) {
        auto it = data.find(key);
        return (it == data.end()) ? fallback : it->second;
    }

    template <typename KeyType, typename ValueType>
    ValueType getOrDefault(const std::unordered_map<KeyType, ValueType>& data, KeyType key, ValueType fallback) {
        auto it = data.find(key);
        return (it == data.end()) ? fallback : it->second;
    }

    template <typename KeyType, typename ValueType>
    auto const& getOrThrow(const std::map<KeyType, ValueType>& data, KeyType key, std::exception error) {
        auto it = data.find(key);
        if(it == data.end()) {
            throw error;
        }
        return it->second;
    }

    template<typename KeyType, typename ValueType>
    auto& getOrThrow(std::map<KeyType, ValueType>& data, KeyType key, std::exception error) {
        auto it = data.find(key);
        if(it == data.end()) {
            throw error;
        }
        return it->second;
    }

    template <typename _InputIterator, typename _OutputIterator, typename _UnaryOperation>
    _OutputIterator transform_n(_InputIterator __first, size_t __n, _OutputIterator __result, _UnaryOperation __op) {
        return std::generate_n(__result, __n, [&__first, &__op]() -> decltype(auto) { return __op(*__first++); });
    }

    template <typename T>
    struct reversion_wrapper {
        T& iterable;
    };

    template <typename T>
    auto begin(reversion_wrapper<T> w) {
        return std::rbegin(w.iterable);
    }

    template <typename T>
    auto end(reversion_wrapper<T> w) {
        return std::rend(w.iterable);
    }

    template <typename T>
    reversion_wrapper<T> reverse_range(T&& iterable) {
        return {iterable};
    }

    template <typename T>
    std::ostream& operator<<(std::ostream& out, const std::vector<T>& v) {
        if (!v.empty()) {
            out << '[';
            std::copy(v.begin(), v.end(), std::ostream_iterator<T>(out, ", "));
            out << "\b\b]";
        }
        return out;
    }


    template<typename T>
    inline void combine_hash(std::size_t&, T const&) {
        throw std::bad_typeid{};
    }

    template<>
    inline void combine_hash(std::size_t& seed, size_t const& value) {
        seed ^= value + 0x9e3779b9 + (seed<<6) + (seed>>2);
    }

    template<>
    inline void combine_hash(std::size_t& seed, int const& value) {
        combine_hash(seed, static_cast<size_t const&>(value)); 
    }

    template<typename K, typename V>
    using UMap = std::unordered_map<K, V, boost::hash<K>>;

} // namespace Hammer

#endif
