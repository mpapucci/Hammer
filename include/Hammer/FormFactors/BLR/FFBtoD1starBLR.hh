///
/// @file  FFBtoD1starBLR.hh
/// @brief \f$ B \rightarrow D_1^* \f$ BLR form factors
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_BDSSD1STARBLR
#define HAMMER_FF_BDSSD1STARBLR

#include "Hammer/FormFactors/BLR/FFBLRBase.hh"

namespace Hammer {

    class FFBtoD1starBLR : public FFBLRBase {

    public:
        FFBtoD1starBLR();

        FFBtoD1starBLR(const FFBtoD1starBLR& other) = default;
        FFBtoD1starBLR& operator=(const FFBtoD1starBLR& other) = delete;
        FFBtoD1starBLR(FFBtoD1starBLR&& other) = delete;
        FFBtoD1starBLR& operator=(FFBtoD1starBLR&& other) = delete;
        virtual ~FFBtoD1starBLR() override = default;

    public:

        virtual std::unique_ptr<FormFactorBase> clone(const std::string& label) override;

    protected:
        virtual void evalAtPSPoint(const std::vector<double>& point, const std::vector<double>& masses = {}) override;

        virtual void defineSettings() override;
    private:
        mutable bool initialized;
    };

} // namespace Hammer

#endif
