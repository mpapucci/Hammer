#include "gtest/gtest.h"
#include "Hammer/FormFactors/ISGW2/FFBtoD0starISGW2.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoD0starISGW2: public FFBtoD0starISGW2 {

    public:

        TestableFFBtoD0starISGW2() : FFBtoD0starISGW2() { }

        using FFBtoD0starISGW2::evalAtPSPoint;
        using FFBtoD0starISGW2::initSettings;

    };

    TEST(FFBtoD0starISGW2Test, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pBmes{6.0740724185305135e+00, 2.2041876529821600e-01, 2.6809423870568733e-01, 2.9832753503876139e+00};
        FourMomentum pD0starmes{2.7206523538400131e+00, 3.5605736720613768e-01, 1.1540824843894137e+00, 7.9961099454269813e-01};
        FourMomentum kNuTau{1.1647033125190303e+00, -2.9539536290726093e-01, -6.1270578819281141e-01, 9.4544539920648796e-01};
        FourMomentum pTau{2.1887167521714703e+00, 1.5975676099933930e-01, -2.7328245749091501e-01, 1.2382189566384276e+00};

        //Evaluate the FF class
        TestableFFBtoD0starISGW2 ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        //MeV momenta
        ff.eval(Particle(1000*pBmes, -511), {Particle(1000*pD0starmes,10411), Particle(1000*kNuTau,-16), Particle(1000*pTau,15)}, {});

        // Compare to direct EvtGen evaluation (also in GeV)
        double uppum = -0.352846;
        double upmum = 0.759315;
        double mb = pBmes.mass(); //GeV
        double mx = pD0starmes.mass(); //GeV
        double gppgm = mb/sqrt(mb*mx)*uppum;
        double gpmgm = mx/sqrt(mb*mx)*upmum;
        double gp = (gppgm + gpmgm)/2.0;
        double gm = (gppgm - gpmgm)/2.0;
        Tensor ffEval{"ffEval", MD::makeVector({4}, {FF_BDSSD0STAR},
                                               {0, gp, gm, 0})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 4; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        }
    }


}
