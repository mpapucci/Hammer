///
/// @file  FFLbtoLcstar12LSPRVar.hh
/// @brief \f$ \Lambda_b \rightarrow Lambda_c^*(2595) \f$ LSPRVar form factors
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_LBLCSTAR12LSPRVAR
#define HAMMER_FF_LBLCSTAR12LSPRVAR

#include "Hammer/FormFactors/LSPR/FFLSPRVarBase.hh"

namespace Hammer {

    class FFLbtoLcstar12LSPRVar : public FFLSPRVarBase {

    public:
        FFLbtoLcstar12LSPRVar();

        FFLbtoLcstar12LSPRVar(const FFLbtoLcstar12LSPRVar& other) = default;
        FFLbtoLcstar12LSPRVar& operator=(const FFLbtoLcstar12LSPRVar& other) = delete;
        FFLbtoLcstar12LSPRVar(FFLbtoLcstar12LSPRVar&& other) = delete;
        FFLbtoLcstar12LSPRVar& operator=(FFLbtoLcstar12LSPRVar&& other) = delete;
        virtual ~FFLbtoLcstar12LSPRVar() override = default;

    public:
        virtual void eval(const Particle& parent, const ParticleList& daughters,
                          const ParticleList& references) override;

        virtual std::unique_ptr<FormFactorBase> clone(const std::string& label) override;

    protected:
        virtual void evalAtPSPoint(const std::vector<double>& point, const std::vector<double>& masses = {}) override;

        virtual void defineSettings() override;
    private:
        mutable bool initialized;
    };

} // namespace Hammer

#endif
