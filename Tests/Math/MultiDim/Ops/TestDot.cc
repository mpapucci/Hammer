#include "Hammer/Math/MultiDim/SparseContainer.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Math/MultiDim/OuterContainer.hh"
#include "Hammer/Math/MultiDim/ScalarContainer.hh"
#include "Hammer/Math/MultiDim/Operations.hh"
#include "Hammer/Math/MultiDim/Ops/Dot.hh"
#include "Hammer/Exceptions.hh"

#include "gtest/gtest.h"

using namespace std;

namespace Hammer {

    namespace MultiDimensional {

        TEST(DotTest, SparseSparse) {
            auto t1 = makeEmptySparse({3, 2, 1}, {FF_BD, WILSON_BCENU, INTEGRATION_INDEX});
            t1->element({2, 1, 0}) = 0.1;
            t1->element({1, 0, 0}) = 0.2i;
            auto t2 = makeEmptySparse({2}, {WILSON_BCENU});
            t2->element({0}) = 0.3;
            t2->element({1}) = 0.4;
            auto res = calcDot(std::move(t1), *t2, {{1,0}});
            EXPECT_DOUBLE_EQ(res->element({0,0}).real(), 0.0);
            EXPECT_DOUBLE_EQ(res->element({1,0}).real(), 0.0);
            EXPECT_DOUBLE_EQ(res->element({2,0}).real(), 0.1*0.4);
            EXPECT_DOUBLE_EQ(res->element({0,0}).imag(), 0.0);
            EXPECT_DOUBLE_EQ(res->element({1,0}).imag(), 0.2*0.3);
            EXPECT_DOUBLE_EQ(res->element({2,0}).imag(), 0.0);
        }

        TEST(DotTest, SparseSparse2) {
            auto t1 = makeEmptySparse({3, 2, 4, 1}, {FF_BD, WILSON_BCENU, SPIN_DSTAR, INTEGRATION_INDEX});
            t1->element({2, 0, 3, 0}) = 0.2i;
            t1->element({2, 1, 2, 0}) = 0.1;
            t1->element({1, 0, 1, 0}) = 0.6i;
            t1->element({1, 1, 3, 0}) = 0.05;
            auto t2 = makeEmptySparse({3, 2, 7, 4}, {FF_BD_HC, WILSON_BCENU, FF_BDSTAR, SPIN_DSTAR});
            t2->element({2, 0, 5, 3}) = 0.3;
            t2->element({2, 1, 5, 2}) = 0.4;
            t2->element({2, 0, 5, 1}) = 0.8;
            t2->element({2, 1, 5, 3}) = 0.3;
            auto res = calcDot(std::move(t1), *t2, {{1, 1}, {2, 3}});
            EXPECT_EQ(res->labels().size(), 4);
            EXPECT_EQ(res->labels()[0], FF_BD);
            EXPECT_EQ(res->labels()[1], INTEGRATION_INDEX);
            EXPECT_EQ(res->labels()[2], FF_BD_HC);
            EXPECT_EQ(res->labels()[3], FF_BDSTAR);
            EXPECT_EQ(res->dims()[0], 3);
            EXPECT_EQ(res->dims()[1], 1);
            EXPECT_EQ(res->dims()[2], 3);
            EXPECT_EQ(res->dims()[3], 7);
            // for (IndexType i1 = 0; i1 < 3; ++i1) {
            //     for (IndexType i2 = 0; i2 < 1; ++i2) {
            //         for (IndexType i3 = 0; i3 < 3; ++i3) {
            //             for (IndexType i4 = 0; i4 < 7; ++i4) {
            //                 cout << i1 << ", " << i2 << ", " << i3 << ", " << i4 << " -> " << res->element({i1,i2,i3,i4}) << endl;
            //             }
            //         }
            //     }
            // }
            EXPECT_DOUBLE_EQ(res->element({0, 0, 0, 0}).real(), 0.0);
            EXPECT_DOUBLE_EQ(res->element({0, 0, 0, 0}).imag(), 0.0);
            EXPECT_DOUBLE_EQ(res->element({2, 0, 2, 5}).real(), 0.1 * 0.4);
            EXPECT_DOUBLE_EQ(res->element({2, 0, 2, 5}).imag(), 0.2 * 0.3);
            EXPECT_DOUBLE_EQ(res->element({1, 0, 2, 5}).real(), 0.05 * 0.3);
            EXPECT_DOUBLE_EQ(res->element({1, 0, 2, 5}).imag(), 0.6 * 0.8);
        }

        TEST(DotTest, VectorVector) {
            auto t1 = makeEmptyVector({3, 2, 1}, {FF_BD, WILSON_BCENU, INTEGRATION_INDEX});
            t1->element({2, 1, 0}) = 0.1;
            t1->element({1, 0, 0}) = 0.2i;
            auto t2 = makeEmptyVector({2}, {WILSON_BCENU});
            t2->element({0}) = 0.3;
            t2->element({1}) = 0.4;
            auto res = calcDot(std::move(t1), *t2, {{1, 0}});
            EXPECT_DOUBLE_EQ(res->element({0, 0}).real(), 0.0);
            EXPECT_DOUBLE_EQ(res->element({1, 0}).real(), 0.0);
            EXPECT_DOUBLE_EQ(res->element({2, 0}).real(), 0.1 * 0.4);
            EXPECT_DOUBLE_EQ(res->element({0, 0}).imag(), 0.0);
            EXPECT_DOUBLE_EQ(res->element({1, 0}).imag(), 0.2 * 0.3);
            EXPECT_DOUBLE_EQ(res->element({2, 0}).imag(), 0.0);
        }

        TEST(DotTest, VectorVector2) {
            auto t1 = makeEmptyVector({3, 2, 4, 1}, {FF_BD, WILSON_BCENU, SPIN_DSTAR, INTEGRATION_INDEX});
            t1->element({2, 0, 3, 0}) = 0.2i;
            t1->element({2, 1, 2, 0}) = 0.1;
            t1->element({1, 0, 1, 0}) = 0.6i;
            t1->element({1, 1, 3, 0}) = 0.05;
            auto t2 = makeEmptyVector({3, 2, 7, 4}, {FF_BD_HC, WILSON_BCENU, FF_BDSTAR, SPIN_DSTAR});
            t2->element({2, 0, 5, 3}) = 0.3;
            t2->element({2, 1, 5, 2}) = 0.4;
            t2->element({2, 0, 5, 1}) = 0.8;
            t2->element({2, 1, 5, 3}) = 0.3;
            auto res = calcDot(std::move(t1), *t2, {{1, 1}, {2, 3}});
            EXPECT_EQ(res->labels().size(), 4);
            EXPECT_EQ(res->labels()[0], FF_BD);
            EXPECT_EQ(res->labels()[1], INTEGRATION_INDEX);
            EXPECT_EQ(res->labels()[2], FF_BD_HC);
            EXPECT_EQ(res->labels()[3], FF_BDSTAR);
            EXPECT_EQ(res->dims()[0], 3);
            EXPECT_EQ(res->dims()[1], 1);
            EXPECT_EQ(res->dims()[2], 3);
            EXPECT_EQ(res->dims()[3], 7);
            // for (IndexType i1 = 0; i1 < 3; ++i1) {
            //     for (IndexType i2 = 0; i2 < 1; ++i2) {
            //         for (IndexType i3 = 0; i3 < 3; ++i3) {
            //             for (IndexType i4 = 0; i4 < 7; ++i4) {
            //                 cout << i1 << ", " << i2 << ", " << i3 << ", " << i4 << " -> " <<
            //                 res->element({i1,i2,i3,i4}) << endl;
            //             }
            //         }
            //     }
            // }
            EXPECT_DOUBLE_EQ(res->element({0, 0, 0, 0}).real(), 0.0);
            EXPECT_DOUBLE_EQ(res->element({0, 0, 0, 0}).imag(), 0.0);
            EXPECT_DOUBLE_EQ(res->element({2, 0, 2, 5}).real(), 0.1 * 0.4);
            EXPECT_DOUBLE_EQ(res->element({2, 0, 2, 5}).imag(), 0.2 * 0.3);
            EXPECT_DOUBLE_EQ(res->element({1, 0, 2, 5}).real(), 0.05 * 0.3);
            EXPECT_DOUBLE_EQ(res->element({1, 0, 2, 5}).imag(), 0.6 * 0.8);
        }

        TEST(DotTest, OuterOuter2) {
            auto v1 = makeVector({2}, {WILSON_BCENU}, {1.i,2.});
            auto v2 = makeVector({2}, {WILSON_BCENU_HC}, {-2.,1.});
            auto v3 = makeVector({3,2}, {FF_BD, WILSON_BCENU}, {-2., 1., 1.i, 5., 3., 1.});
            auto v4 = makeVector({3,2}, {FF_BD_HC, WILSON_BCENU_HC}, {3., -2.i, 4., 1., -2., 1.});

            vector<TensorData> vec1;
            vec1.push_back(std::move(v1));
            vec1.push_back(std::move(v2));
            vector<TensorData> vec2;
            vec2.push_back(std::move(v3));
            vec2.push_back(std::move(v4));

            auto o1 = combineTensors(vec1);
            auto o2 = combineTensors(vec2);

            auto idxs = o1->getSameLabelPairs(*o2, {WILSON_BCENU, WILSON_BCENU_HC, FF_BD, FF_BD_HC});

            auto res = calcDot(std::move(o1), *o2, idxs);

            auto resEvalSingle1 = makeVector({3}, {FF_BD}, {1.i * (-2.) + 2.*1., 1.i * 1.i + 2.*5., 1.i * 3. + 2.*1.});
            auto resEvalSingle2 =
                makeVector({3}, {FF_BD_HC}, {-2. * 3. + 1. * (-2.i), -2. * 4. + 1. * 1., -2. * (-2.) + 1. * 1.});
            vector<TensorData> vec3;
            vec3.push_back(std::move(resEvalSingle1));
            vec3.push_back(std::move(resEvalSingle2));
            auto resEvalOuter = combineTensors(vec3);
            auto resEval = makeVector({3,3}, {FF_BD, FF_BD_HC}, {-16. + 8.*1i,
                                                                                  -14. + 14.*1i,
                                                                                  10. - 10.*1i,
                                                                                  -54. - 18.*1i,
                                                                                  -63.,
                                                                                  45.,
                                                                                  -6. - 22.*1i,
                                                                                  -14. - 21.*1i,
                                                                                  10. + 15.*1i});

            for (IndexType idx1 = 0; idx1 < 3; ++idx1) {
                for (IndexType idx2 = 0; idx2 < 3; ++idx2) {
                    EXPECT_DOUBLE_EQ(const_cast<const IContainer*>(res.get())->element({idx1, idx2}).real(),
                                     const_cast<const IContainer*>(resEvalOuter.get())->element({idx1, idx2}).real());
                    EXPECT_DOUBLE_EQ(const_cast<const IContainer*>(res.get())->element({idx1, idx2}).imag(),
                                     const_cast<const IContainer*>(resEvalOuter.get())->element({idx1, idx2}).imag());
                    EXPECT_DOUBLE_EQ(resEval->element({idx1, idx2}).real(),
                                     const_cast<const IContainer*>(resEvalOuter.get())->element({idx1, idx2}).real());
                    EXPECT_DOUBLE_EQ(resEval->element({idx1, idx2}).imag(),
                                     const_cast<const IContainer*>(resEvalOuter.get())->element({idx1, idx2}).imag());
                }
            }

        }

        TEST(DotTest, OuterOuter) {
            auto v1 = makeVector({3}, {WILSON_BCENU}, {1.i, 2., 3.});
            auto v2 = makeVector({3}, {WILSON_BCENU_HC}, {-2., 1., 2.});
            auto v3 = makeVector({2, 3}, {FF_BD, WILSON_BCENU}, {-2., 1., 1.i, 5., 3., 1.});
            auto v4 = makeVector({2, 3}, {FF_BD_HC, WILSON_BCENU_HC}, {3., -2.i, 4., 1., -2., 1.});

            vector<TensorData> vec1;
            vec1.push_back(std::move(v1));
            vec1.push_back(std::move(v2));
            vector<TensorData> vec2;
            vec2.push_back(std::move(v3));
            vec2.push_back(std::move(v4));

            auto o1 = combineTensors(vec1);
            auto o2 = combineTensors(vec2);

            auto idxs = o1->getSameLabelPairs(*o2, {WILSON_BCENU, WILSON_BCENU_HC, FF_BD, FF_BD_HC});

            auto res = calcDot(std::move(o1), *o2, idxs);

            auto resEvalSingle1 =
                makeVector({2}, {FF_BD}, {1.i * (-2.) + 2.*1. + 3.*(1.i), 1.i*5. + 2.*3. +3.*1.});
            auto resEvalSingle2 =
                makeVector({2}, {FF_BD_HC}, {-2. * 3. + 1. * (-2.i) + 2. * 4., -2. * 1. + 1. * (-2.) + 2. * 1.});
            vector<TensorData> vec3;
            vec3.push_back(std::move(resEvalSingle1));
            vec3.push_back(std::move(resEvalSingle2));
            auto resEvalOuter = combineTensors(vec3);

            for (IndexType idx1 = 0; idx1 < 2; ++idx1) {
                for (IndexType idx2 = 0; idx2 < 2; ++idx2) {
                    EXPECT_NEAR(const_cast<const IContainer*>(res.get())->element({idx1, idx2}).real(),
                                     const_cast<const IContainer*>(resEvalOuter.get())->element({idx1, idx2}).real(), 1.e-10);
                    EXPECT_NEAR(const_cast<const IContainer*>(res.get())->element({idx1, idx2}).imag(),
                                     const_cast<const IContainer*>(resEvalOuter.get())->element({idx1, idx2}).imag(), 1.e-10);
                }
            }
        }

        TEST(DotTest, Chunkify) {
            vector<IndexList> v1{{2, 3}, {2, 3, 7}, {9}};
            vector<LabelsList> v2{
                {SPIN_TAUM, SPIN_DSTAR}, {SPIN_TAUM_HC, SPIN_DSTAR_HC, WILSON_BCMUNU}, {WILSON_BCENU}};
            vector<IndexList> v3{{2, 1}, {5, 3, 9}, {11}};
            vector<LabelsList> v4{
                {SPIN_TAUM_HC, SPIN_DSSD1}, {SPIN_DSTAR_HC, SPIN_DSTAR, WILSON_BCENU}, {INTEGRATION_INDEX}};
            BlockIndexing b1{v1, v2};
            BlockIndexing b2{v3, v4};
            Ops::Dot dotter{{{2,0}, {1,3}, {5,4}}};
            auto res = dotter.partitionContractions(b1,b2);
            // 1 with 0, 0 with 1, 2 with 1 => {{},{2}},{{1},{0}},{{0,2},{1}}
            EXPECT_EQ(res.size(), 3);
            // 0
            EXPECT_EQ(get<0>(res[0]).size(), 0);
            EXPECT_EQ(get<1>(res[0]).size(), 1);
            EXPECT_EQ(get<1>(res[0])[0], 2);
            EXPECT_EQ(get<2>(res[0]).size(), 0);
            // 1
            EXPECT_EQ(get<0>(res[1]).size(), 1);
            EXPECT_EQ(get<0>(res[1])[0], 1);
            EXPECT_EQ(get<1>(res[1]).size(), 1);
            EXPECT_EQ(get<1>(res[1])[0], 0);
            EXPECT_EQ(get<2>(res[1]).size(), 1);
            EXPECT_EQ(get<2>(res[1])[0].first, 2);
            EXPECT_EQ(get<2>(res[1])[0].second, 0);
            // 2
            EXPECT_EQ(get<0>(res[2]).size(), 2);
            EXPECT_EQ(get<0>(res[2])[0], 0);
            EXPECT_EQ(get<0>(res[2])[1], 2);
            EXPECT_EQ(get<1>(res[2]).size(), 1);
            EXPECT_EQ(get<1>(res[2])[0], 1);
            EXPECT_EQ(get<2>(res[2]).size(), 2);
            EXPECT_EQ(get<2>(res[2])[0].first, 1);
            EXPECT_EQ(get<2>(res[2])[0].second, 3);
            EXPECT_EQ(get<2>(res[2])[1].first, 5);
            EXPECT_EQ(get<2>(res[2])[1].second, 4);
            Ops::Dot dotter2{{{3, 1}, {0, 2}, {4, 5}}};
            res = dotter2.partitionContractions(b2, b1);
            // 1 with 0, 0 with 1, 1 with 2 => {{2},{}},{{1},{0,2}},{{0},{1}}
            EXPECT_EQ(res.size(), 3);
            // 0
            EXPECT_EQ(get<0>(res[0]).size(), 1);
            EXPECT_EQ(get<0>(res[0])[0], 2);
            EXPECT_EQ(get<1>(res[0]).size(), 0);
            EXPECT_EQ(get<2>(res[0]).size(), 0);
            // 1
            EXPECT_EQ(get<0>(res[1]).size(), 1);
            EXPECT_EQ(get<0>(res[1])[0], 1);
            EXPECT_EQ(get<1>(res[1]).size(), 2);
            EXPECT_EQ(get<1>(res[1])[0], 0);
            EXPECT_EQ(get<1>(res[1])[1], 2);
            EXPECT_EQ(get<2>(res[1]).size(), 2);
            EXPECT_EQ(get<2>(res[1])[0].first, 3);
            EXPECT_EQ(get<2>(res[1])[0].second, 1);
            EXPECT_EQ(get<2>(res[1])[1].first, 4);
            EXPECT_EQ(get<2>(res[1])[1].second, 5);
            // 2
            EXPECT_EQ(get<0>(res[2]).size(), 1);
            EXPECT_EQ(get<0>(res[2])[0], 0);
            EXPECT_EQ(get<1>(res[2]).size(), 1);
            EXPECT_EQ(get<1>(res[2])[0], 1);
            EXPECT_EQ(get<2>(res[2]).size(), 1);
            EXPECT_EQ(get<2>(res[2])[0].first, 0);
            EXPECT_EQ(get<2>(res[2])[0].second, 2);
        }

    } // namespace MultiDimensional

} // namespace Hammer
