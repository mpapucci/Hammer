///
/// @file  FFBtoD1BLRVar.hh
/// @brief \f$ B \rightarrow D_1 \f$ BLR form factors with variations
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_BDSSD1BLR_VAR
#define HAMMER_FF_BDSSD1BLR_VAR

#include "Hammer/FormFactors/BLR/FFBLRBase.hh"

namespace Hammer {

    class FFBtoD1BLRVar : public FFBLRBase {

    public:
        FFBtoD1BLRVar();

        FFBtoD1BLRVar(const FFBtoD1BLRVar& other) = default;
        FFBtoD1BLRVar& operator=(const FFBtoD1BLRVar& other) = delete;
        FFBtoD1BLRVar(FFBtoD1BLRVar&& other) = delete;
        FFBtoD1BLRVar& operator=(FFBtoD1BLRVar&& other) = delete;
        virtual ~FFBtoD1BLRVar() override = default;

    public:

        virtual std::unique_ptr<FormFactorBase> clone(const std::string& label) override;

    protected:
        virtual void evalAtPSPoint(const std::vector<double>& point, const std::vector<double>& masses = {}) override;

        virtual void defineSettings() override;
    private:
        mutable bool initialized;
    };

} // namespace Hammer

#endif
