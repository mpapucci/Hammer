///
/// @file  FFBtoDstarBLPR.hh
/// @brief \f$ B \rightarrow D^* \f$ BLPR form factors
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_BDSTARBLPR
#define HAMMER_FF_BDSTARBLPR

#include "Hammer/FormFactors/BLPR/FFBLPRBase.hh"

namespace Hammer {

    class FFBtoDstarBLPR : public FFBLPRBase {

    public:
        FFBtoDstarBLPR();

        FFBtoDstarBLPR(const FFBtoDstarBLPR& other) = default;
        FFBtoDstarBLPR& operator=(const FFBtoDstarBLPR& other) = delete;
        FFBtoDstarBLPR(FFBtoDstarBLPR&& other) = delete;
        FFBtoDstarBLPR& operator=(FFBtoDstarBLPR&& other) = delete;
        virtual ~FFBtoDstarBLPR() override = default;

    public:

        virtual std::unique_ptr<FormFactorBase> clone(const std::string& label) override;

    protected:
        virtual void evalAtPSPoint(const std::vector<double>& point, const std::vector<double>& masses = {}) override;

        virtual void defineSettings() override;
    private:
        mutable bool initialized;
    };

} // namespace Hammer

#endif
