///
/// @file  OuterSquare.hh
/// @brief Tensor outer square algorithm
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_MATH_MULTIDIM_OPS_OUTERSQUARE
#define HAMMER_MATH_MULTIDIM_OPS_OUTERSQUARE

namespace Hammer {


    namespace MultiDimensional {

        class IContainer;
        class VectorContainer;
        class SparseContainer;
        class OuterContainer;

        namespace Ops {

            class OuterSquare final {
            public:
                OuterSquare() {}

                IContainer* operator()(OuterContainer& first);

                IContainer* operator()(IContainer& first);

                IContainer* error(IContainer&);
            };

        } // namespace Ops

    } // namespace MultiDimensional

} // namespace Hammer


#endif
