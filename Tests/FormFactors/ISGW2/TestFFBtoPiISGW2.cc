#include "gtest/gtest.h"
#include "Hammer/FormFactors/ISGW2/FFBtoPiISGW2.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoPiISGW2: public FFBtoPiISGW2 {

    public:

        TestableFFBtoPiISGW2() : FFBtoPiISGW2() { }

        using FFBtoPiISGW2::evalAtPSPoint;
        using FFBtoPiISGW2::initSettings;

    };

    TEST(FFBtoPiISGW2Test, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs (q^2 = 19.8958 GeV^2)
        FourMomentum pBmes{5474.7423876404975e+00, 2844.8361598961946e-01, 1776.6650823948621e-01, 1409.3767837987914e+00};
        FourMomentum pPimes{7809.7394236330053e-01, -4349.7440268391885e-01, 5980.1941366927713e-01, 2088.3130366317992e-01};
        FourMomentum kNuMu{2952.7404598550526e+00, 2231.1045907528642e+00, -6669.6014486150135e-01, 3727.2426960535188e-01};
        FourMomentum pMu{1741.0279854221444e+00, -1511.6465720793257e+00, 2466.0723943171059e-01, 8278.2121053025981e-01 };

        //Evaluate the FF class
        TestableFFBtoPiISGW2 ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pBmes, -511), {Particle(pPimes,211), Particle(kNuMu,-16), Particle(pMu,15)}, {});

        // Compare to direct evaluation
        // Fs, Fz, Fp, Ft
        Tensor ffEval{"ffEval", MD::makeVector({4}, {FF_BPI},
                        {0.,
                         0.417168, 
                         1.22849,
                         0.})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 1; idx1 < 3; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1.,1e-4);
        }

    }


}
