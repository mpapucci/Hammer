#include "gtest/gtest.h"
#include "Hammer/FormFactors/BGL/FFBtoDBGLVar.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoDBGLVar: public FFBtoDBGLVar {

    public:

        TestableFFBtoDBGLVar() : FFBtoDBGLVar() { }

        using FFBtoDBGLVar::evalAtPSPoint;
        using FFBtoDBGLVar::initSettings;

    };

    TEST(FFBtoDBGLVarTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pBmes{5280., 0., 0., 0.};
        FourMomentum pDmes{2381.3371929065643, 0., 0., -1475.671313782346};
        FourMomentum pTau{2034.777874233763, 553.6990600712329, -92.91177555543321, 819.1030156888944};
        FourMomentum kNuTau{863.8849328596731, -553.6990600712329, 92.91177555543321, 656.5682980934517};

        //Evaluate the FF class
        TestableFFBtoDBGLVar ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pBmes, 511), {Particle(pDmes,-411), Particle(kNuTau,16), Particle(pTau,-15)}, {});

        //Compare to direct evaluation
        Tensor ffEval{"ffEval", MD::makeVector({4, 9}, {FF_BD, FF_BD_VAR},
                                               {0.,
                                                0.,
                                                0.,
                                                0.,
                                                0.,
                                                0.,
                                                0.,
                                                0.,
                                                0.,
                                                0.7972552474671366,
                                                0.,
                                                0.,
                                                0.,
                                                0.,
                                                10.982399458702659,
                                                0.352544571387107,
                                                0.011316987265112734,
                                                0.00036328513088375883,
                                                0.9061166367377584,
                                                62.58166863722641,
                                                2.0089259755451656,
                                                0.06448827049682451,
                                                0.0020701295529533103,
                                                0.,
                                                0.,
                                                0.,
                                                0.,
                                                0.,
                                                0.,
                                                0.,
                                                0.,
                                                0.,
                                                0.,
                                                0.,
                                                0.,
                                                0.})};


        auto& t = ff.getTensor();
        for (IndexType idx1 = 1; idx1 < 3; ++idx1) {
            for (IndexType idx2 = 0; idx2 < 9; ++idx2) {
                double comRe = compareVals(t.element({idx1,idx2}).real(),ffEval.element({idx1,idx2}).real());
                EXPECT_NEAR(comRe, 1., 1e-4);
            }
        }

    }


}
