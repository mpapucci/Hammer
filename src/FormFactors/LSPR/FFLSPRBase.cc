///
/// @file  FFLSPRBase.cc
/// @brief Hammer base class for LSPR form factors
//

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#include "Hammer/FormFactors/LSPR/FFLSPRBase.hh"

using namespace std;

namespace Hammer {

    FFLSPRBase::FFLSPRBase() {
        setGroup("LSPR");
    }

    void FFLSPRBase::addRefs() const {
    }

    void FFLSPRBase::defineSettings() {

        //1S scheme: mb1S = 4710 MeV, delta mb-mc = 3400 MeV, alpha_s = 26/100
        addSetting<double>("as",0.26);
        addSetting<double>("mb1S",4.710); //GeV
        addSetting<double>("dmbmc",3.400);  //GeV

        addSetting<double>("s1",0.97457);
        addSetting<double>("sp1",-1.7743);
        addSetting<double>("s11",0.90412); //GeV
        addSetting<double>("s1p1",0.0); //GeV
        addSetting<double>("pc1",0.17871); //GeV
        addSetting<double>("pb1",0.0); //GeV

        addSetting<double>("barLambda",0.81); //GeV
        addSetting<double>("barLambdap",1.10); //GeV

    }

}
