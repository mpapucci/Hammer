#include "Hammer/Math/MultiDim/SparseContainer.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Math/MultiDim/OuterContainer.hh"
#include "Hammer/Math/MultiDim/ScalarContainer.hh"
#include "Hammer/Math/MultiDim/Operations.hh"
#include "Hammer/Exceptions.hh"

#include "gtest/gtest.h"

using namespace std;

namespace Hammer {

    namespace MultiDimensional {

        TEST(TraceTest, SparseAll) {
            auto t1 = makeEmptySparse({2, 2, 2, 2}, {SPIN_TAUP, SPIN_MUP, SPIN_TAUP_HC, SPIN_MUP_HC});
            t1->element({1,0,0,0}) = 0.1;
            t1->element({1,0,1,0}) = 0.2;
            t1->element({0,0,0,0}) = 0.1i;
            t1->element({0,1,1,1}) = 0.2;
            t1->element({0,0,1,1}) = 0.2;
            t1->element({0,1,0,1}) = 0.2;
            t1->element({1,1,1,1}) = 0.5;
            auto pos = t1->getSpinLabelPairs();
            EXPECT_EQ(pos.size(), 2);
            EXPECT_EQ(pos[0].first, 0);
            EXPECT_EQ(pos[1].first, 1);
            EXPECT_EQ(pos[0].second, 2);
            EXPECT_EQ(pos[1].second, 3);
            auto res = calcTrace(std::move(t1), pos);
            IContainer* tmp = res.get();
            EXPECT_NE(tmp, nullptr);
            EXPECT_EQ(dynamic_cast<VectorContainer*>(tmp), nullptr);
            EXPECT_EQ(dynamic_cast<SparseContainer*>(tmp), nullptr);
            EXPECT_NE(dynamic_cast<ScalarContainer*>(tmp), nullptr);
            EXPECT_EQ(res->rank(),0);
            EXPECT_DOUBLE_EQ(res->element().real(), 0.2+0.2+0.5);
            EXPECT_DOUBLE_EQ(res->element().imag(), 0.1);
        }

        TEST(TraceTest, Sparse) {
            auto t1 = makeEmptySparse({2, 2, 11, 2, 2}, {SPIN_TAUP, SPIN_MUP, INTEGRATION_INDEX, SPIN_TAUP_HC, SPIN_MUP_HC});
            t1->element({1, 0, 4, 0, 0}) = 0.1;
            t1->element({1, 0, 4, 1, 0}) = 0.2;
            t1->element({0, 0, 4, 0, 0}) = 0.1i;
            t1->element({0, 1, 4, 1, 1}) = 0.2;
            t1->element({0, 0, 4, 1, 1}) = 0.2;
            t1->element({0, 1, 4, 0, 1}) = 0.2;
            t1->element({1, 1, 4, 1, 1}) = 0.5;
            auto pos = t1->getSpinLabelPairs();
            EXPECT_EQ(pos.size(), 2);
            EXPECT_EQ(pos[0].first, 0);
            EXPECT_EQ(pos[1].first, 1);
            EXPECT_EQ(pos[0].second, 3);
            EXPECT_EQ(pos[1].second, 4);
            auto res = calcTrace(std::move(t1), pos);
            IContainer* tmp = res.get();
            EXPECT_NE(tmp, nullptr);
            EXPECT_EQ(dynamic_cast<VectorContainer*>(tmp), nullptr);
            EXPECT_NE(dynamic_cast<SparseContainer*>(tmp), nullptr);
            EXPECT_EQ(dynamic_cast<ScalarContainer*>(tmp), nullptr);
            EXPECT_EQ(res->rank(), 1);
            EXPECT_DOUBLE_EQ(res->element({4}).real(), 0.2 + 0.2 + 0.5);
            EXPECT_DOUBLE_EQ(res->element({4}).imag(), 0.1);
        }

        TEST(TraceTest, Sparse2) {
            auto t1 = makeEmptySparse({2, 2, 11, 2, 2, 11},
                                      {SPIN_TAUP, SPIN_MUP, WILSON_BCENU, SPIN_TAUP_HC, SPIN_MUP_HC, WILSON_BCENU_HC});
            t1->element({1, 0, 4, 0, 0, 4}) = 0.1;
            t1->element({1, 0, 4, 1, 0, 4}) = 0.2;
            t1->element({0, 0, 4, 0, 0, 4}) = 0.1i;
            t1->element({0, 1, 4, 1, 1, 4}) = 0.2;
            t1->element({0, 0, 4, 1, 1, 4}) = 0.2;
            t1->element({0, 1, 3, 0, 1, 2}) = 0.2;
            t1->element({1, 1, 2, 1, 1, 2}) = 0.5;
            auto pos = t1->getSpinLabelPairs();
            EXPECT_EQ(pos.size(), 2);
            EXPECT_EQ(pos[0].first, 0);
            EXPECT_EQ(pos[1].first, 1);
            EXPECT_EQ(pos[0].second, 3);
            EXPECT_EQ(pos[1].second, 4);
            auto res = calcTrace(std::move(t1), pos);
            IContainer* tmp = res.get();
            EXPECT_NE(tmp, nullptr);
            EXPECT_EQ(dynamic_cast<VectorContainer*>(tmp), nullptr);
            EXPECT_NE(dynamic_cast<SparseContainer*>(tmp), nullptr);
            EXPECT_EQ(dynamic_cast<ScalarContainer*>(tmp), nullptr);
            EXPECT_EQ(res->rank(), 2);
            EXPECT_DOUBLE_EQ(res->element({4,4}).real(), 0.2);
            EXPECT_DOUBLE_EQ(res->element({4,4}).imag(), 0.1);
            EXPECT_DOUBLE_EQ(res->element({3,2}).real(), 0.2);
            EXPECT_DOUBLE_EQ(res->element({2,2}).real(), 0.5);
        }

        TEST(TraceTest, VectorAll) {
            auto t1 = makeEmptyVector({2, 2, 2, 2}, {SPIN_TAUP, SPIN_MUP, SPIN_TAUP_HC, SPIN_MUP_HC});
            t1->element({1, 0, 0, 0}) = 0.1;
            t1->element({1, 0, 1, 0}) = 0.2;
            t1->element({0, 0, 0, 0}) = 0.1i;
            t1->element({0, 1, 1, 1}) = 0.2;
            t1->element({0, 0, 1, 1}) = 0.2;
            t1->element({0, 1, 0, 1}) = 0.2;
            t1->element({1, 1, 1, 1}) = 0.5;
            auto pos = t1->getSpinLabelPairs();
            EXPECT_EQ(pos.size(), 2);
            EXPECT_EQ(pos[0].first, 0);
            EXPECT_EQ(pos[1].first, 1);
            EXPECT_EQ(pos[0].second, 2);
            EXPECT_EQ(pos[1].second, 3);
            auto res = calcTrace(std::move(t1), pos);
            IContainer* tmp = res.get();
            EXPECT_NE(tmp, nullptr);
            EXPECT_EQ(dynamic_cast<VectorContainer*>(tmp), nullptr);
            EXPECT_EQ(dynamic_cast<SparseContainer*>(tmp), nullptr);
            EXPECT_NE(dynamic_cast<ScalarContainer*>(tmp), nullptr);
            EXPECT_EQ(res->rank(), 0);
            EXPECT_DOUBLE_EQ(res->element().real(), 0.2 + 0.2 + 0.5);
            EXPECT_DOUBLE_EQ(res->element().imag(), 0.1);
        }

        TEST(TraceTest, Vector) {
            auto t1 =
                makeEmptyVector({2, 2, 11, 2, 2}, {SPIN_TAUP, SPIN_MUP, INTEGRATION_INDEX, SPIN_TAUP_HC, SPIN_MUP_HC});
            t1->element({1, 0, 4, 0, 0}) = 0.1;
            t1->element({1, 0, 4, 1, 0}) = 0.2;
            t1->element({0, 0, 4, 0, 0}) = 0.1i;
            t1->element({0, 1, 4, 1, 1}) = 0.2;
            t1->element({0, 0, 4, 1, 1}) = 0.2;
            t1->element({0, 1, 4, 0, 1}) = 0.2;
            t1->element({1, 1, 4, 1, 1}) = 0.5;
            auto pos = t1->getSpinLabelPairs();
            EXPECT_EQ(pos.size(), 2);
            EXPECT_EQ(pos[0].first, 0);
            EXPECT_EQ(pos[1].first, 1);
            EXPECT_EQ(pos[0].second, 3);
            EXPECT_EQ(pos[1].second, 4);
            auto res = calcTrace(std::move(t1), pos);
            IContainer* tmp = res.get();
            EXPECT_NE(tmp, nullptr);
            EXPECT_NE(dynamic_cast<VectorContainer*>(tmp), nullptr);
            EXPECT_EQ(dynamic_cast<SparseContainer*>(tmp), nullptr);
            EXPECT_EQ(dynamic_cast<ScalarContainer*>(tmp), nullptr);
            EXPECT_EQ(res->rank(), 1);
            EXPECT_DOUBLE_EQ(res->element({4}).real(), 0.2 + 0.2 + 0.5);
            EXPECT_DOUBLE_EQ(res->element({4}).imag(), 0.1);
        }

        TEST(TraceTest, Vector2) {
            auto t1 = makeEmptyVector({2, 2, 11, 2, 2, 11},
                                      {SPIN_TAUP, SPIN_MUP, WILSON_BCENU, SPIN_TAUP_HC, SPIN_MUP_HC, WILSON_BCENU_HC});
            t1->element({1, 0, 4, 0, 0, 4}) = 0.1;
            t1->element({1, 0, 4, 1, 0, 4}) = 0.2;
            t1->element({0, 0, 4, 0, 0, 4}) = 0.1i;
            t1->element({0, 1, 4, 1, 1, 4}) = 0.2;
            t1->element({0, 0, 4, 1, 1, 4}) = 0.2;
            t1->element({0, 1, 3, 0, 1, 2}) = 0.2;
            t1->element({1, 1, 2, 1, 1, 2}) = 0.5;
            auto pos = t1->getSpinLabelPairs();
            EXPECT_EQ(pos.size(), 2);
            EXPECT_EQ(pos[0].first, 0);
            EXPECT_EQ(pos[1].first, 1);
            EXPECT_EQ(pos[0].second, 3);
            EXPECT_EQ(pos[1].second, 4);
            auto res = calcTrace(std::move(t1), pos);
            IContainer* tmp = res.get();
            EXPECT_NE(tmp, nullptr);
            EXPECT_NE(dynamic_cast<VectorContainer*>(tmp), nullptr);
            EXPECT_EQ(dynamic_cast<SparseContainer*>(tmp), nullptr);
            EXPECT_EQ(dynamic_cast<ScalarContainer*>(tmp), nullptr);
            EXPECT_EQ(res->rank(), 2);
            EXPECT_DOUBLE_EQ(res->element({4, 4}).real(), 0.2);
            EXPECT_DOUBLE_EQ(res->element({4, 4}).imag(), 0.1);
            EXPECT_DOUBLE_EQ(res->element({3, 2}).real(), 0.2);
            EXPECT_DOUBLE_EQ(res->element({2, 2}).real(), 0.5);
        }

    } // namespace MultiDimensional

} // namespace Hammer
