///
/// @file  SequentialIndexing.hh
/// @brief Non-sparse tensor indexer
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_MATH_MULTIDIM_SEQUENTIALINDEXING
#define HAMMER_MATH_MULTIDIM_SEQUENTIALINDEXING

#include <algorithm>

#include "Hammer/Math/MultiDimensional.fhh"

namespace Hammer {

    namespace MultiDimensional {

        class SequentialIndexing {
        public:
            SequentialIndexing();

            SequentialIndexing(IndexList dimensions);

            SequentialIndexing(const SequentialIndexing&) = default;
            SequentialIndexing(SequentialIndexing&&) = default;
            SequentialIndexing& operator=(const SequentialIndexing&) = default;
            SequentialIndexing& operator=(SequentialIndexing&&) = default;

            ~SequentialIndexing() noexcept = default;

        public:
            using StrideMap = std::vector<std::tuple<PositionType, long, bool>>;

            /// @brief  rank of the tensor
            /// @return the number of components
            size_t rank() const;

            /// @brief  dimension of a specific component
            /// @param[in] index   the component index
            /// @return  the dimension
            IndexType dim(IndexType index) const;

            /// @brief  get the dimensions of all the indices at once
            /// @return  the list of dimensions
            const IndexList& dims() const;

            /// @brief  stride of a specific component, i.e. the product of the dimension of
            ///         the indices to right of the given one (row-major convention).
            ///         The last index has stride equal to 1.
            /// @param[in] index   the component index
            /// @return  the stride
            PositionType stride(IndexType index) const;

            /// @brief  list of strides, i.e. the product of the dimension of
            ///         the indices to right of the given one (row-major convention).
            ///         The last index has stride equal to 1.
            /// @return  the strides
            const PositionList& strides() const;

            /// @brief  the number of elements (product of all the dimensions)
            PositionType numValues() const;

            /// @brief convert the indices into the position indicizing
            ///        a sparse tensor container organized as row-major
            /// @param[in] indices the element indices
            /// @return the absolute position
            PositionType indicesToPos(const IndexList& indices) const;

            PositionType indicesToPos(IndexList::const_iterator first, IndexList::const_iterator last) const;

            /// @brief  check that the indices are within range for each component
            /// @param[in] indices the element indices
            /// @return true if all indices are OK
            bool checkValidIndices(const IndexList& indices) const;

            bool checkValidIndices(IndexList::const_iterator first, IndexList::const_iterator last) const;

            /// @brief convert the absolute position (in row-major convention)
            ///        into the list of indices
            /// @param[in] position  the absolute position
            /// @param[out] result  the indices
            void posToIndices(PositionType position, IndexList& result) const;

            /// @brief extract the value of the i-th index from an absolute position
            /// @param[in] position  the absolute position
            /// @param[in] indexPosition  the index coordinate i
            /// @return the index value
            IndexType ithIndexInPos(PositionType position, IndexType indexPosition) const;

            PositionType build2ndPosition(PositionType reducedPosition, PositionType innerPosition,
                                      const PositionPairList& conversion) const;

            PositionPair splitPosition(PositionType position, const StrideMap& conversion) const;

            /// @brief extends an absolute position from a rank N-1 tensor to the corresponding
            ///        position for this rank N tensor given the missing index and its value.
            ///        Used in AddAt
            /// @param position the sub-tensor position
            /// @param indexPosition the missing index coordinate
            /// @param indexValue  the missing index value
            /// @return the corresponding absolute position
            PositionType extendPosition(PositionType position, IndexType indexPosition, IndexType indexValue) const;

            /// @brief Get the Inner Outer Strides object
            ///
            /// @param positions
            /// @param secondStrides
            /// @param flipSecond
            /// @return
            StrideMap getInnerOuterStrides(const IndexPairList& positions, const PositionList& secondStrides,
                                           bool flipSecond = false) const;
            // StrideMap getInnerOuterStrides(const IndexBoolPairList& positions) const;

            /// @brief Get the Outer Strides2nd object
            ///
            /// @param positions
            /// @return
            PositionPairList getOuterStrides2nd(const IndexPairList& positions) const;

            PositionType reducedNumValues(const IndexPairList& indices) const;

        public:
            template<typename BasicIndexing>
            bool isSameShape(const BasicIndexing& other) const;

            bool isSameShape(const IndexList& indices) const;

        protected:
            StrideMap buildStrideMap(const std::map<IndexType, long> innerMap) const;

        private:
            void calcPadding();

            IndexList _dimensions;            ///< the dimensions of each tensor index
            PositionList _strides;            ///< the strides for each tensor index (necessary to convert coordinates to position in `_data`)
            PositionType _maxIndex;
        };


        template <typename BasicIndexing>
        bool SequentialIndexing::isSameShape(const BasicIndexing& other) const {
            return isSameShape(other.dims());
        }

    } // namespace MultiDimensional

} // namespace Hammer

#endif
