#include "gtest/gtest.h"
#include "Hammer/FormFactors/BLR/FFBtoD1BLR.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoD1BLR: public FFBtoD1BLR {

    public:

        TestableFFBtoD1BLR() : FFBtoD1BLR() { }

        using FFBtoD1BLR::evalAtPSPoint;
        using FFBtoD1BLR::initSettings; 

    };

    TEST(FFBtoD1BLRTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pBmes{5280., 0., 0., 0.};
        FourMomentum pD1mes{3171.36751893939, 0, 0, -2048.49479867141};
        FourMomentum kNuTau{50.13801061207, -24.8428481565870, -43.029075211928, -6.71950072636};
        FourMomentum pTau{2058.49447044853, 24.8428481565870, 43.029075211928, 2055.21429939777};

        //Evaluate the FF class
        TestableFFBtoD1BLR ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pBmes, 511), {Particle(pD1mes,-10413), Particle(kNuTau,14), Particle(pTau,-13)}, {});

        // Compare to direct evaluation
        Tensor ffEval{"ffEval", MD::makeVector({8}, {FF_BDSSD1},
                                               {-0.739944645300832, -0.843405995077826, -1.13030094119333, 0.905362519914457, -0.643238103665703, 0.0249154323981526, -0.660647522031832, -0.203766349305352})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 8; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        }
    }


}
