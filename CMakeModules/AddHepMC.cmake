set(HepMCVersion 02_06_09)
set(HepMCSHA256 e589a823b5e35cdf9ad30f361fd0fcb97ea0cd38534aa5f0e147656e6dcc9fd3)

include(ExternalProject)
include(GNUInstallDirs)
file(DOWNLOAD https://gitlab.cern.ch/hepmc/HepMC/-/archive/HEPMC_${HepMCVersion}/HepMC-HEPMC_${HepMCVersion}.tar.gz
    ${PROJECT_BINARY_DIR}/ThirdParty/tmp/HepMC-${HepMCVersion}.tar.gz
    STATUS Status
    SHOW_PROGRESS
    EXPECTED_HASH SHA256=${HepMCSHA256}
)
if(NOT EXISTS ${PROJECT_BINARY_DIR}/ThirdParty/HepMC)
    execute_process(COMMAND ${CMAKE_COMMAND} -E tar xfz ${PROJECT_BINARY_DIR}/ThirdParty/tmp/HepMC-${HepMCVersion}.tar.gz
                    WORKING_DIRECTORY ${PROJECT_BINARY_DIR}/ThirdParty/tmp
                    RESULT_VARIABLE Result
    )
    if(NOT Result EQUAL "0")
        message(FATAL_ERROR "Failed extracting HepMC to ${PROJECT_BINARY_DIR}/ThirdParty/HepMC")
    endif()
    execute_process(COMMAND ${CMAKE_COMMAND} -E rename ${PROJECT_BINARY_DIR}/ThirdParty/tmp/HepMC-HEPMC_${HepMCVersion} ${PROJECT_BINARY_DIR}/ThirdParty/HepMC)
    execute_process(COMMAND sed -i "s/lib/${CMAKE_INSTALL_LIBDIR}/" ${PROJECT_BINARY_DIR}/ThirdParty/HepMC/src/CMakeLists.txt)
endif()
ExternalProject_Add(HepMC_EP
    SOURCE_DIR ${PROJECT_BINARY_DIR}/ThirdParty/HepMC
    BINARY_DIR ${PROJECT_BINARY_DIR}/ThirdParty/HepMC-build
    INSTALL_DIR ${CMAKE_INSTALL_PREFIX}
    CMAKE_ARGS -Dmomentum:STRING=GEV -Dlength:STRING=MM -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR> -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER} -DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS} -DCMAKE_CXX_FLAGS=-fPIC
    LOG_DOWNLOAD ON
    LOG_CONFIGURE ON
    LOG_BUILD ON
    LOG_INSTALL ON)
ExternalProject_Get_Property(HepMC_EP install_dir)
ExternalProject_Get_Property(HepMC_EP source_dir)
add_library(ThirdParty::HepMC INTERFACE IMPORTED)
add_dependencies(ThirdParty::HepMC HepMC_EP)
set_property(TARGET ThirdParty::HepMC PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${source_dir})
set_property(TARGET ThirdParty::HepMC PROPERTY INTERFACE_LINK_LIBRARIES ${install_dir}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}HepMC${CMAKE_SHARED_LIBRARY_SUFFIX})
set(BUILT_EXTERNAL_DEPENDENCIES ON CACHE BOOL "" FORCE)
