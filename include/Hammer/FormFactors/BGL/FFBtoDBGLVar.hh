///
/// @file  FFBtoDBGLVar.hh
/// @brief \f$ B \rightarrow D \f$ BGL form factors with variations
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_BDBGLVAR
#define HAMMER_FF_BDBGLVAR

#include "Hammer/FormFactors/BGL/FFBGLBase.hh"

namespace Hammer {

    class FFBtoDBGLVar : public FFBGLBase {

    public:
        FFBtoDBGLVar();

        FFBtoDBGLVar(const FFBtoDBGLVar& other) = default;
        FFBtoDBGLVar& operator=(const FFBtoDBGLVar& other) = delete;
        FFBtoDBGLVar(FFBtoDBGLVar&& other) = delete;
        FFBtoDBGLVar& operator=(FFBtoDBGLVar&& other) = delete;
        virtual ~FFBtoDBGLVar() override = default;

    public:

        virtual std::unique_ptr<FormFactorBase> clone(const std::string& label) override;

    protected:
        virtual void evalAtPSPoint(const std::vector<double>& point, const std::vector<double>& masses = {}) override;

        virtual void defineSettings() override;
    private:
        mutable bool initialized;
    };

} // namespace Hammer

#endif
