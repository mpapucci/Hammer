#include "Hammer/Tools/Utils.hh"
#include "Hammer/Tools/ParticleData.hh"
#include "Hammer/Tools/Pdg.hh"
#include "gtest/gtest.h"

using namespace std;
namespace Hammer {

    class TestableParticleData : public ParticleData {
    public:
        TestableParticleData() : ParticleData() {
        }
        void eval(const Particle&, const ParticleList&, const ParticleList&)  override {
        }
        using ParticleData::addProcessSignature;
        using ParticleData::_signatures;
        using ParticleData::_signatureIndex;
    };

    TEST(ParticleDataTest, SetSignature) {
        TestableParticleData p1;
        p1.addProcessSignature(PID::MUON, {PID::NU_MU, PID::NU_EBAR, PID::ELECTRON});
        TestableParticleData p2;
        p2.addProcessSignature(PID::K0L, {PID::PI0, PID::PI0}, {PID::GAMMA, PID::GAMMA, PID::EMINUS, PID::EPLUS});
        EXPECT_EQ(p1._signatures[0].parent, PID::MUON);
        EXPECT_EQ(p1._signatures[0].daughters.size(), 3);
        EXPECT_EQ(p2._signatures[0].parent, PID::K0L);
        EXPECT_EQ(p2._signatures[0].daughters.size(), 2 + 4);
        EXPECT_EQ(p1._signatures[0].daughters[0], PID::NU_MU);
        EXPECT_EQ(p1._signatures[0].daughters[2], PID::ELECTRON);
        EXPECT_EQ(p2._signatures[0].daughters[0], PID::PI0);
        EXPECT_EQ(p2._signatures[0].daughters[1], PID::PI0);
        EXPECT_EQ(p2._signatures[0].daughters[2], PID::GAMMA);
        EXPECT_EQ(p2._signatures[0].daughters[5], PID::EPLUS);
    }

    TEST(ParticleDataTest, CalcId) {
        TestableParticleData p1;
        p1.addProcessSignature(PID::MUON, {PID::NU_MU, PID::NU_EBAR, PID::ELECTRON});
        HashId seed = 0ul;
        combine_hash(seed, static_cast<int>(PID::MUON));
        combine_hash(seed, static_cast<int>(PID::NU_MU));
        combine_hash(seed, static_cast<int>(PID::NU_EBAR));
        combine_hash(seed, static_cast<int>(PID::ELECTRON));
        EXPECT_EQ(p1._signatures[0].id, seed);
    }

    TEST(ParticleDataTest, HadronicId) {
        TestableParticleData p1;
        p1.addProcessSignature(PID::BZERO, {PID::DSTARMINUS, PID::NU_MU, PID::ANTIMUON}, {-PID::D0, PID::PIMINUS});
        HashId seed = 0ul;
        combine_hash(seed, static_cast<int>(PID::BZERO));
        combine_hash(seed, static_cast<int>(PID::DSTARMINUS));
        EXPECT_EQ(p1._signatures[0].hadronicId, seed);
    }


} // namespace Hammer
