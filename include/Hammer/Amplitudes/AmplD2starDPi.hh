///
/// @file  AmplD2starDPi.hh
/// @brief \f$ D_1 \rightarrow D \pi \f$ amplitude
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_AMPL_D2STARDPI
#define HAMMER_AMPL_D2STARDPI

#include "Hammer/AmplitudeBase.hh"

namespace Hammer {

    class AmplD2starDPi : public AmplitudeBase {

    public:
        AmplD2starDPi();

        virtual ~AmplD2starDPi() override {
        }

    public:
        virtual void eval(const Particle& parent, const ParticleList& daughters,
                          const ParticleList& references) override;
    
    protected:
        virtual void defineSettings() override;
        
    };     
    
} // namespace Hammer

#endif
