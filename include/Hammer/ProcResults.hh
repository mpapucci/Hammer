///
/// @file  ProcResults.hh
/// @brief Container for process-related results of weight calculation
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_PROCESSRESULTS_HH
#define HAMMER_PROCESSRESULTS_HH

#include <utility>
#include <vector>
#include <string>

#include "Hammer/IndexTypes.hh"
#include "Hammer/Math/Tensor.hh"
#include "Hammer/Tools/SettingsConsumer.hh"
#include "Hammer/Tools/HammerSerial.fhh"

namespace Hammer {

    class Log;

    /// @brief Decay process class
    ///
    /// Contains the amplitudes, weights and info associated to a decay, ...
    ///
    /// @ingroup Core
    class ProcResults : public SettingsConsumer {

    public:
        ProcResults();

        ProcResults(const Serial::FBProcData* msgreader);

        ProcResults(const ProcResults& other) = delete;
        ProcResults& operator=(const ProcResults& other) = delete;
        ProcResults(ProcResults&& other) = delete;
        ProcResults& operator=(ProcResults&& other) = delete;

        ~ProcResults() noexcept override = default;

    public:

        /// @brief
        /// @param[in] what
        /// @return
        const Tensor& processAmplitude(WTerm what = WTerm::NUMERATOR) const;

        /// @brief
        /// @param[in] what
        /// @return
        Tensor& processAmplitude(WTerm what = WTerm::NUMERATOR);

        /// @brief
        /// @param[in] what
        /// @return
        const Tensor& processAmplitudeSquared(WTerm what = WTerm::NUMERATOR) const;

        /// @brief
        /// @param[in] what
        /// @return
        Tensor& processAmplitudeSquared(WTerm what = WTerm::NUMERATOR);

        std::vector<std::reference_wrapper<const Tensor>> processFormFactors(const std::string& schemeName) const;

        std::vector<std::reference_wrapper<Tensor>> processFormFactors(const std::string& schemeName);

        void appendFormFactor(const std::string& schemeName, const Tensor& formfact);

        void appendFormFactor(const std::string& schemeName, Tensor&& formfact);

        void clearFormFactors();

        bool haveFormFactors() const;

        /// @brief
        /// @param[in] schemeName
        /// @return
        const Tensor& processWeight(const std::string& schemeName) const;

        /// @brief
        /// @param[in] schemeName
        /// @return
        Tensor& processWeight(const std::string& schemeName);

        void setProcessWeight(const std::string& schemeName, const Tensor& weight);

        void setProcessWeight(const std::string& schemeName, Tensor&& weight);

        void clearWeights();

        bool haveWeights() const;

        std::vector<std::string> availableSchemes() const;

    public:

        /// @brief
        /// @param[in] msgwriter
        /// @param[in] msg
        void write(flatbuffers::FlatBufferBuilder* msgwriter, flatbuffers::Offset<Serial::FBProcData>* msg) const;

        /// @brief
        /// @param[in] msgreader
        /// @param[in] merge
        bool read(const Serial::FBProcData* msgreader, bool merge);

    protected:
        /// @brief purely virtual function for a class to define new settings
        virtual void defineSettings() override;

        /// @brief logging facility
        /// @return   stream to be used for logging
        Log& getLog() const;

    private:


        SchemeDict<Tensor> _processWeights;
        NumDenPair<Tensor> _processSquaredAmplitude;
        NumDenPair<Tensor> _processAmplitude;
        SchemeDict<std::vector<Tensor>> _processFormFactors;

    };

} // namespace Hammer

#endif
