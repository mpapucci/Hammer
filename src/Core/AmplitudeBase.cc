///
/// @file  AmplitudeBase.cc
/// @brief Hammer base amplitude class
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#include "Hammer/AmplitudeBase.hh"
#include "Hammer/Tools/ParticleUtils.hh"
#include "Hammer/IndexLabels.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Math/MultiDim/ScalarContainer.hh"

using namespace std;

namespace Hammer {

    namespace MD = MultiDimensional;

    AmplitudeBase::AmplitudeBase() : _WCNames{}, _WCPrefix{""}, _WCLabel{NONE}, _tensorList{}, _multiplicity{1ul} {
    }

    vector<complex<double>> AmplitudeBase::getWCVectorFromDict(const map<string, complex<double>>& wcDict) const {
        vector<complex<double>> result(_WCNames.size());
        for(size_t pos = 0ul; pos < _WCNames.size(); ++pos) {
            auto it = wcDict.find(_WCNames[pos]);
            if(it != wcDict.end()) {
                result[pos] = it->second;
            }
        }
        return result;
    }

    vector<complex<double>> AmplitudeBase::getWCVectorFromSettings(WTerm what) const {
        WTerm oldwhat = const_cast<AmplitudeBase*>(this)->setWeightTerm(what);
        vector<complex<double>> result(_WCNames.size());
        for(size_t pos = 0ul; pos < _WCNames.size(); ++pos) {
            result[pos] = *getSetting<complex<double>>(_WCPrefix,_WCNames[pos]);
        }
        const_cast<AmplitudeBase*>(this)->setWeightTerm(oldwhat);
        return result;
    }

    void AmplitudeBase::updateWCSettings(const vector<complex<double>>& values, WTerm what) {
        this->updateVectorOfSettings(values, _WCNames, _WCPrefix, what);
    }

    void AmplitudeBase::updateWCSettings(const map<string, complex<double>>& values, WTerm what) {
        this->updateVectorOfSettings(values, _WCPrefix, what);
    }

    void AmplitudeBase::updateWCTensor(vector<complex<double>> values, MD::SharedTensorData& data) const {
        ASSERT(values.size() == _WCNames.size());
        preProcessWCValues(values);
        if(!data || data->rank() == 0) {
            data = MD::SharedTensorData{MD::makeVector({static_cast<uint16_t>(values.size())}, {_WCLabel}, values).release()};
        }
        else {
            for(IndexType i = 0; i<values.size(); ++i) {
                data->element({i}) = values[i];
            }
        }
    }

    pair<string, IndexLabel> AmplitudeBase::getWCInfo() const {
        return make_pair(_WCPrefix, _WCLabel);
    }

    void AmplitudeBase::preProcessWCValues(vector<complex<double>>&) const {
        return;
    }

    void AmplitudeBase::init() {
        // initSettings();
        ///@todo anything else?
    }

    Tensor& AmplitudeBase::getTensor() {
        return _tensorList[_signatureIndex];
    }

    const Tensor& AmplitudeBase::getTensor() const {
        return _tensorList[_signatureIndex];
    }

    bool AmplitudeBase::setSignatureIndex(size_t idx) {
        bool res = ParticleData::setSignatureIndex(idx);
        if(res) {
            this->updateWilsonCeffLabelPrefix();
            return true;
        }
        MSG_WARNING("Unable to update the signature index");
        return false;
    }

    void AmplitudeBase::updateWilsonCeffLabelPrefix() {
        _WCLabel = NONE;
        _WCPrefix = "None";
    }

    size_t AmplitudeBase::multiplicityFactor() const {
        return _multiplicity;
    }

    Log& AmplitudeBase::getLog() const {
        return Log::getLog("Hammer.AmplitudeBase");
    }

    void AmplitudeBase::addTensor(Tensor&& tensor) {
        _tensorList.push_back(std::move(tensor));
    }


} // namespace Hammer
