///
/// @file  SparseContainer.hh
/// @brief Sparse tensor data container
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_MATH_MULTIDIM_SPARSECONTAINER
#define HAMMER_MATH_MULTIDIM_SPARSECONTAINER

#include <complex>
#include <map>
#include <memory>
#include <set>
#include <type_traits>
#include <utility>
#include <vector>

#include "Hammer/Math/MultiDim/ISingleContainer.hh"
#include "Hammer/Math/MultiDim/AlignedIndexing.hh"
#include "Hammer/Math/MultiDim/LabeledIndexing.hh"

namespace Hammer {

    class Log;

    namespace Serial {

        struct FBSingleTensor;

    }

    namespace MultiDimensional {

        namespace Ops {

            class Sum;
            class Multiply;
            class Divide;
            class Trace;
            class Dot;
            class AddAt;
            class Convert;

        } // namespace Ops

        class SparseContainer final : public ISingleContainer {
        private:
            using DataType = std::map<PositionType, ElementType>;

        public:
            SparseContainer(const IndexList& dimensions, const LabelsList& labels);
            SparseContainer(LabeledIndexing<AlignedIndexing> indexing);
            SparseContainer(const Serial::FBSingleTensor* input);

            ElementType value(const IndexList& indices) const;
            ElementType value(IndexList::const_iterator first, IndexList::const_iterator last) const;
            void setValue(const IndexList& indices, ElementType value = 0.);
            void setValue(IndexList::const_iterator first, IndexList::const_iterator last, ElementType value = 0.);

            using iterator = DataType::iterator;
            using const_iterator = DataType::const_iterator;


            iterator begin();
            const_iterator begin() const;

            iterator end();
            const_iterator end() const;

            reference operator[](PositionType pos);

            iterator erase(const_iterator first, const_iterator last);

        public:
            size_t rank() const override;
            IndexList dims() const override;
            LabelsList labels() const override;
            size_t numValues() const override;
            size_t dataSize() const override;
            size_t entrySize() const override;
            IndexType labelToIndex(IndexLabel label) const override;

            IndexPairList getSameLabelPairs(const IContainer& other,
                                                    const UniqueLabelsList& indices) const override;
            IndexPairList getSpinLabelPairs() const override;

            bool isSameShape(const IContainer& other) const override;
            bool canAddAt(const IContainer& subContainer, IndexLabel coord, IndexType position) const override;

            reference element(const IndexList& coords = {}) override;
            ElementType element(const IndexList& coords = {}) const override;

            reference element(IndexList::const_iterator start, IndexList::const_iterator end) override;
            ElementType element(IndexList::const_iterator start, IndexList::const_iterator end) const override;

            bool compare(const IContainer& other) const override;
            TensorData clone() const override;
            void clear() override;

            IContainer& operator*=(double value) override;
            IContainer& operator*=(const ElementType value) override;

            IContainer& conjugate() override;

            SerialType write(flatbuffers::FlatBufferBuilder* msgwriter) const override;

            NonZeroIt firstNonZero() const override;
            NonZeroIt endNonZero() const override;

            bool hasNaNs() const override;

        protected:
            /// @brief logging facility
            /// @return   stream to be used for logging
            Log& getLog() const;

        private:

            class ItAligned : public ItBase {
            public:
                ItAligned(DataType::const_iterator it);

                IContainer::ElementType value() const override;
                PositionType position() const override;
                void next(int n = 1) override;
                bool isSame(const ItBase& other) const override;
                bool isAligned() const override;
                ptrdiff_t distanceFrom(const ItBase& other) const override;

            private:
                friend class SparseContainer;
                DataType::const_iterator _it;
            };

            // all these classes are friends to be able to use getIndexing (not necessary, but cleaner)
            friend class Ops::Sum;
            friend class Ops::Multiply;
            friend class Ops::Divide;
            friend class Ops::Trace;
            friend class Ops::Dot;
            friend class Ops::AddAt;
            friend class Ops::Convert;

            const LabeledIndexing<AlignedIndexing>& getIndexing() const;

        private:

            mutable DataType _data;
            LabeledIndexing<AlignedIndexing> _indexing;

        };

        TensorData makeEmptySparse(const IndexList& dimensions, const LabelsList& labels);
        TensorData makeEmptySparse(LabeledIndexing<AlignedIndexing> indexing);

    } // namespace MultiDimensional

} // namespace Hammer


#endif
