///////////////////////
//// Weights demo ////
///////////////////////
#include "HepMCParser.hh"
#include "Hammer/Hammer.hh"

using namespace std;

int main() {

    ////////////////////////////////////////////////////////
    //// Initialize the sample and store tensor weights ////
    ////////////////////////////////////////////////////////

    auto begin = std::chrono::system_clock::now();
    auto fin = unique_ptr<ifstream>(new ifstream("./data/BCLepNu.hepmc", std::ios::in));
    auto io = unique_ptr<HepMC::IO_GenEvent>(new HepMC::IO_GenEvent(*fin));
    if (io->rdstate() != 0) {
        return EXIT_FAILURE;
    }
    //Outfile
    ofstream outFile("./DemoWeights-Card.dat",ios::binary);
    Hammer::Hammer ham{};
    //Read inputs from cards
    ham.readCards("./data/Processes.yml", "./data/Options.yml");
    ham.addTotalSumOfWeights(); //adds "Total Sum of Weights" histo with auto bin filling
    ham.initRun();
    ham.saveOptionCard("Opts01-Card.yml", false);
    //Saves the FF scheme definitions & other run information
    outFile << ham.saveRunHeader();
    //Loop over events. Can be more than one process per event if there are two taus, or zero!
    HepMC::GenEvent ge;
    size_t savedEvents = 0ul;
    for(size_t i =0; i < 10000; ++i) {
        if (io->rdstate() == 0 && io->fill_next_event(&ge)) {
            if ((i + 1) % 1000 == 0) {
                cout<< "processing event " << i + 1 << endl;
            }
            // look for decay processes starting with B mesons
            auto processes = parseGenEvent(ge, {521, -521, 511, -511});
            // can also use Hammer PDG constants for increased readability
            // auto processes = parseGenEvent(ge, {PID::BPLUS, PID::BMINUS, PID::BZERO, -PID::BZERO});
            if(processes.size() > 0) {
                bool hasProcesses = false;
                ham.initEvent();
                for(auto& elem : processes) {
                    auto procId = ham.addProcess(elem); // addProcess return an unique Id of that specific process
                                                        // that can be used to retrieve process-specific information later
                    if (procId != 0){ // Make sure process isn't forbidden
                        hasProcesses = true;
                    }
                }
                if(hasProcesses) { // Process & store events with at least one tau
                    ++savedEvents;
                    ham.processEvent();
                    outFile <<  ham.saveEventWeights();
                }
            }
        }
    }
    // save the total rates (optional)
    outFile << ham.saveRates();
    // save the sum of weights of processed events (the ones for which processEvent() has been called)
    outFile << ham.saveHistogram("Total Sum of Weights");
    outFile.close();
    auto init = std::chrono::system_clock::now();
    auto durinit = init - begin;
    typedef std::chrono::duration<float> float_seconds;
    auto secsinit = std::chrono::duration_cast<float_seconds>(durinit);
    cout << "Init Time: " << secsinit.count() << endl;
    cout << "Events Stored: " << savedEvents << endl;


} // int main()
