///
/// @file  Convert.hh
/// @brief Tensor storage type conversion algorithm
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_MATH_MULTIDIM_OPS_Convert
#define HAMMER_MATH_MULTIDIM_OPS_Convert

#include "Hammer/Math/MultiDimensional.fhh"

#include <complex>

namespace Hammer {


    namespace MultiDimensional {

        class IContainer;
        class VectorContainer;
        class SparseContainer;
        class OuterContainer;

        namespace Ops {

            class Convert final {
            public:
                Convert(bool destinationSparse = true);

                IContainer* error(IContainer&);

                IContainer* operator()(VectorContainer& first);
                IContainer* operator()(SparseContainer& first);
                IContainer* operator()(OuterContainer& first);
                IContainer* operator()(IContainer& first);

            private:

                IContainer* toSparse(VectorContainer& first);
                IContainer* toSparse(OuterContainer& first);

                IContainer* toVector(SparseContainer& first);
                IContainer* toVector(OuterContainer& first);

            private:

                bool _destIsSparse;

            };


        } // namespace Ops

    } // namespace MultiDimensional

} // namespace Hammer


#endif
