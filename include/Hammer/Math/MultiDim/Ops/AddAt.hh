///
/// @file  AddAt.hh
/// @brief Sub-tensor block insertion algorithm
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_MATH_MULTIDIM_OPS_ADDAT
#define HAMMER_MATH_MULTIDIM_OPS_ADDAT

#include "Hammer/Math/MultiDimensional.fhh"

namespace Hammer {


    namespace MultiDimensional {

        class IContainer;
        class VectorContainer;
        class SparseContainer;
        class OuterContainer;

        namespace Ops {

            class AddAt final {
            public:
                AddAt(IndexType position, IndexType coord);

                IContainer* operator()(VectorContainer& first, const VectorContainer& second);
                IContainer* operator()(SparseContainer& first, const SparseContainer& second);
                IContainer* operator()(VectorContainer& first, const SparseContainer& second);
                IContainer* operator()(SparseContainer& first, const VectorContainer& second);
                IContainer* operator()(SparseContainer& first, const OuterContainer& second);
                IContainer* operator()(VectorContainer& first, const OuterContainer& second);
                IContainer* operator()(OuterContainer& first, const IContainer& second);

                IContainer* operator()(IContainer& first, const IContainer& second);

                IContainer* error(IContainer&, const IContainer&);

            private:
                IndexType _position;
                IndexType _coord;
            };

        } // namespace Ops

    } // namespace MultiDimensional

} // namespace Hammer


#endif
