///
/// @file  AmplBToQLepNuBase.cc
/// @brief \f$ b -> c \tau\nu \f$ base amplitude
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#include "Hammer/Amplitudes/AmplBToQLepNuBase.hh"
#include "Hammer/Math/MultiDim/IContainer.hh"
#include "Hammer/IndexLabels.hh"

using namespace std;

namespace Hammer {

    AmplBToQLepNuBase::AmplBToQLepNuBase() {
        _perms = {6,9,5,4,7,3,2};
        _flips = {1.,-1.,-1.,-1.,-1.,1.,1.,1.,1.,-1.,-1.};
    }

    void AmplBToQLepNuBase::updateWilsonCeffLabelPrefix() {
        _WCLabel = NONE;
        _WCPrefix = "None";
        _multiplicity = 1ul;
        for(auto elem: getTensor().labels()) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wswitch-enum"
            switch(elem) {
            case WILSON_BCTAUNU: {
                _WCLabel = WILSON_BCTAUNU;
                _WCPrefix = "BtoCTauNu";
                break;
            }
            case WILSON_BCMUNU: {
                _WCLabel = WILSON_BCMUNU;
                _WCPrefix = "BtoCMuNu";
                break;
            }
            case WILSON_BCENU: {
                _WCLabel = WILSON_BCENU;
                _WCPrefix = "BtoCENu";
                break;
            }
            case WILSON_BUTAUNU: {
                _WCLabel = WILSON_BUTAUNU;
                _WCPrefix = "BtoUTauNu";
                break;
            }
            case WILSON_BUMUNU: {
                _WCLabel = WILSON_BUMUNU;
                _WCPrefix = "BtoUMuNu";
                break;
            }
            case WILSON_BUENU: {
                _WCLabel = WILSON_BUENU;
                _WCPrefix = "BtoUENu";
                break;
            }
            default:
                break;
            }
#pragma clang diagnostic pop
        }
    }



    void AmplBToQLepNuBase::defineSettings() {
        //_WCNames = {"SM",  "S_aRbL", "S_aRbR", "S_aLbL", "S_aLbR", "V_aRbL", "V_aRbR", "V_aLbL", "V_aLbR", "T_aRbL", "T_aLbR"};
        _WCNames = {"SM", "S_qLlL", "S_qRlL", "V_qLlL", "V_qRlL", "T_qLlL", "S_qLlR", "S_qRlR", "V_qLlR", "V_qRlR", "T_qRlR"};
        setPath(_WCPrefix);
        addSetting<complex<double>>("SM", 1.0);
        for (auto elem : _WCNames) {
            if (elem != "SM") {
                addSetting<complex<double>>(elem, 0.);
            }
        }
    }

    void AmplBToQLepNuBase::preProcessWCValues(vector<complex<double>>& data) const {
        //getWC order: conj( {"SM", "S_qLlL", "S_qRlL", "V_qLlL", "V_qRlL", "T_qLlL", "S_qLlR", "S_qRlR", "V_qLlR", "V_qRlR", "T_qRlR"} )
        //order for amplitudes: {SM, aSR bSL, aSR bSR, aSL bSL, aSL bSR, aVR bVL, aVR bVR, aVL bVL,  aVL bVR, aTR bTL, aTL bTR}
        auto it = _perms.begin();
        MultiDimensional::IContainer::ElementType tmp = data[*it];
        IndexType previous = *it;
        ++it;
        for (; it != _perms.end(); ++it) {
            data[previous] = data[*it];
            previous = *it;
        }
        data[previous] = tmp;
        for(IndexType i=0; i < _flips.size(); ++i) {
            data[i] = _flips[i]*conj(data[i]);
        }
        // auto temp = ext.data();
        // ext.data() = {conj(temp[0]), -conj(temp[1]), -conj(temp[6]), -conj(temp[2]), -conj(temp[7]), conj(temp[4]),
        //               conj(temp[9]), conj(temp[3]),  conj(temp[8]),  -conj(temp[5]), -conj(temp[10])};
    }

} // namespace Hammer
