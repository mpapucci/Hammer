#include "gtest/gtest.h"
#include "Hammer/FormFactors/BLRSXP/FFLbtoLcBLRSXPVar.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFLbtoLcBLRSXPVar: public FFLbtoLcBLRSXPVar {

    public:

        TestableFFLbtoLcBLRSXPVar() : FFLbtoLcBLRSXPVar() { }

        using FFLbtoLcBLRSXPVar::initSettings;
        using FFLbtoLcBLRSXPVar::evalAtPSPoint;
    };

    TEST(FFLbtoLcBLRSXPVarTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pLbmes{5620., 0., 0., 0.};
        FourMomentum pLcmes{2514.6000000000004,380.2511160960518,-878.708483534587,-425.0854616098454};
        FourMomentum pTau{2456.13415321897,-581.6942711749232,1418.910848905593,723.6624190525239};
        FourMomentum kNuTau{649.2658467810297,201.44315507887137,-540.2023653710064,-298.5769574426786};

        //Evaluate the FF class
        TestableFFLbtoLcBLRSXPVar ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pLbmes, -5122), {Particle(pLcmes, -4122), Particle(kNuTau, 16), Particle(pTau, -15)}, {});

        //Compare to direct evaluation
        Tensor ffEval{"ffEval", MD::makeVector({12, 5}, {FF_LBLC, FF_LBLC_VAR},
                                               {0.744050262639,
                                                0.0929528712770,
                                                0.00464764356385,
                                                -0.00231727489216,
                                                -0.000115863744608,
                                                1.18511593540,
                                                0.148054418529,
                                                0.00740272092646,
                                                -0.0286460043510,
                                                -0.00143230021755,
                                                1.21407897674,
                                                0.151672719589,
                                                0.00758363597944,
                                                -0.0338884913088,
                                                -0.00169442456544,
                                                -0.301188694543,
                                                -0.0376269660260,
                                                -0.00188134830130,
                                                0.0321109773817,
                                                0.00160554886909,
                                                -0.0826568041477,
                                                -0.0103261670104,
                                                -0.000516308350518,
                                                0.000617671220448,
                                                0.0000308835610224,
                                                0.763812946280,
                                                0.0954217880705,
                                                0.00477108940352,
                                                0.00292521206564,
                                                0.000146260603282,
                                                -0.364472049319,
                                                -0.0455328425856,
                                                -0.00227664212928,
                                                -0.113589309497,
                                                -0.00567946547487,
                                                0.104855753419,
                                                0.0130994421205,
                                                0.000654972106027,
                                                0.145082615659,
                                                0.00725413078293,
                                                0.830515836674,
                                                0.103754861111,
                                                0.00518774305557,
                                                0.00816769902344,
                                                0.000408384951172,
                                                -0.352357783478,
                                                -0.0440194289764,
                                                -0.00220097144882,
                                                -0.113589309497,
                                                -0.00567946547487,
                                                0.1066387974459,
                                                0.01332219462832,
                                                0.000666109731416,
                                                0.145082615659,
                                                0.00725413078293,
                                                0.00453549559603,
                                                0.000566611369533,
                                                0.0000283305684767,
                                                0.145700286879,
                                                0.00728501434396})};


        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 12; ++idx1) {
            for (IndexType idx2 = 0; idx2 < 5; ++idx2) {
                double comRe = compareVals(t.element({idx1,idx2}).real(),ffEval.element({idx1,idx2}).real());
                EXPECT_NEAR(comRe, 1., 2e-4);
            }
        }
    }


}
