#include "gtest/gtest.h"
#include "Hammer/FormFactors/BLR/FFBtoD0starBLR.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoD0starBLR: public FFBtoD0starBLR {

    public:

        TestableFFBtoD0starBLR() : FFBtoD0starBLR() { }

        using FFBtoD0starBLR::evalAtPSPoint;
        using FFBtoD0starBLR::initSettings; 

    };

    TEST(FFBtoD0starBLRTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pBmes{5280., 0., 0., 0.};
        FourMomentum pD0starmes{3117.27272727273, 0, 0, -2104.13622567512};
        FourMomentum kNuTau{49.97993502618, -24.8428481565870, -43.029075211928, -5.41530120089};
        FourMomentum pTau{2112.74733770109, 24.8428481565870, 43.029075211928, 2109.55152687601};

        //Evaluate the FF class
        TestableFFBtoD0starBLR ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pBmes, 511), {Particle(pD0starmes,-10411), Particle(kNuTau,14), Particle(pTau,-13)}, {});

        // Compare to direct evaluation
        Tensor ffEval{"ffEval", MD::makeVector({4}, {FF_BDSSD0STAR},
                                               {0.495949833064184, -0.131663927760578, 0.720459193477596, 0.877081840967645})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 4; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        }
    }


}
