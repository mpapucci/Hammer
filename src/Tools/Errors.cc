///
/// @file  Errors.cc
/// @brief Hammer errors reporting helper functions
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#include <iostream>

#include "Hammer/Exceptions.hh"

using namespace std;

namespace Assert {

    void HandleAssert(const char *message, const char *condition, const char *fileName, long lineNumber) {
        cerr << "Assert Failed: \"" << message << "\"" << endl;
        cerr << "Condition: " << condition << endl;
        cerr << "File: " << fileName << endl;
        cerr << "Line: " << lineNumber << endl;
        cerr << "Application now terminating";
    }

} // namespace Assert
