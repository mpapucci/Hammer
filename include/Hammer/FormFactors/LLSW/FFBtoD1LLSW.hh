///
/// @file  FFBtoD1LLSW.hh
/// @brief \f$ B \rightarrow D_1 \f$ LLSW form factors
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_BDSSD1LLSW
#define HAMMER_FF_BDSSD1LLSW

#include "Hammer/FormFactors/LLSW/FFLLSWBase.hh"

namespace Hammer {

    class FFBtoD1LLSW : public FFLLSWBase {

    public:
        FFBtoD1LLSW();

        FFBtoD1LLSW(const FFBtoD1LLSW& other) = default;
        FFBtoD1LLSW& operator=(const FFBtoD1LLSW& other) = delete;
        FFBtoD1LLSW(FFBtoD1LLSW&& other) = delete;
        FFBtoD1LLSW& operator=(FFBtoD1LLSW&& other) = delete;
        virtual ~FFBtoD1LLSW() override = default;

    public:

        virtual std::unique_ptr<FormFactorBase> clone(const std::string& label) override;

    protected:
        virtual void evalAtPSPoint(const std::vector<double>& point, const std::vector<double>& masses = {}) override;

        virtual void defineSettings() override;
    private:
        mutable bool initialized;
    };

} // namespace Hammer

#endif
