///
/// @file  BinnedIndexing.hh
/// @brief Binned tensor (histogram) indexer
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_MATH_MULTIDIM_BinnedIndexing
#define HAMMER_MATH_MULTIDIM_BinnedIndexing

#include "Hammer/Math/MultiDimensional.fhh"

namespace Hammer {

    namespace MultiDimensional {


        using BinValue = std::vector<double>;
        using BinEdgeList = std::vector<std::vector<double>>;
        using BinRange = std::pair<double, double>;
        using BinRangeList = std::vector<BinRange>;

        template<class BasicIndexing>
        class BinnedIndexing : public BasicIndexing {

        public:
            BinnedIndexing();

            BinnedIndexing(IndexList dimensions, BinEdgeList edges, bool hasUnderOverFlow);
            BinnedIndexing(IndexList dimensions, BinRangeList ranges, bool hasUnderOverFlow);
            BinnedIndexing(BinEdgeList edges, bool hasUnderOverFlow);

            BinnedIndexing(const BinnedIndexing&) = default;
            BinnedIndexing(BinnedIndexing&&) = default;
            BinnedIndexing& operator=(const BinnedIndexing&) = default;
            BinnedIndexing& operator=(BinnedIndexing&&) = default;

            ~BinnedIndexing() noexcept = default;

        public:
            using BasicIndexing::dim;

            /// @brief   get the labels of all the indices at once
            /// @return  the list of labels
            const BinEdgeList& edges() const;

            const BinValue& edge(IndexType pos) const;

            IndexList valueToPos(const BinValue& point) const;
            bool isValid(const BinValue& point) const;

            BinRangeList binEdges(IndexList position) const;
            BinRange binEdge(IndexType position, IndexType coord) const;

            bool isSameBinShape(const BinEdgeList& otherEdges, const IndexList& otherIndices, bool otherUnderOverFlow) const;
            bool isSameBinShape(const BinRangeList& otherRanges, const IndexList& otherIndices, bool otherUnderOverFlow) const;

            template<typename S>
            bool isSameBinShape(const BinnedIndexing<S>& other) const;

            bool hasUnderOverFlow() const;

            typename BasicIndexing::StrideMap getBinStrides(const UniqueIndexList& positions) const;

        private:

            bool isValid() const;
            void fillEdges(const BinRangeList& ranges);

            static IndexList processEdges(const BinEdgeList& edges, bool hasUnderOverFlow);
            static IndexList processRanges(const IndexList& dimensions, bool hasUnderOverFlow);

        private:
            BinEdgeList _edges; ///< the labels of each tensor index
            bool _hasUnderOverFlow;

        };

    }

} // namespace Hammer

#include "Hammer/Math/MultiDim/BinnedIndexingDefs.hh"

#endif
