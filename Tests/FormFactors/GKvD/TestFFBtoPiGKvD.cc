#include "gtest/gtest.h"
#include "Hammer/FormFactors/GKvD/FFBtoPiGKvD.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoPiGKvD: public FFBtoPiGKvD {

    public:

        TestableFFBtoPiGKvD() : FFBtoPiGKvD() { }

        using FFBtoPiGKvD::evalAtPSPoint;
        using FFBtoPiGKvD::initSettings;

    };

    TEST(FFBtoPiGKvDTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs (q^2 = 19.8958 GeV^2)
        FourMomentum pBmes{28783.8884892086, 0, 0, 28295.4737822005};
        FourMomentum pPimes{139., 0, 0, 0};
        FourMomentum kNuMu{483.601218826, 143.20931144041, 440.75294019035, 138.1957947273};
        FourMomentum pMu{28161.2872703827, -143.20931144041, -440.75294019035, 28157.2779874733};

        //Evaluate the FF class
        TestableFFBtoPiGKvD ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pBmes, 511), {Particle(pPimes,-211), Particle(kNuMu,14), Particle(pMu,-13)}, {});

        // Compare to direct evaluation
        // Fs, Fz, Fp, Ft
        Tensor ffEval{"ffEval", MD::makeVector({4}, {FF_BPI},
                        {3109.8859405624,
                         0.5263315492142,
                         1.3230756246809,
                         0.00016462135373268})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 4; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        }

    }


}
