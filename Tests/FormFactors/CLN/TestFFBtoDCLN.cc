#include "gtest/gtest.h"
#include "Hammer/FormFactors/CLN/FFBtoDCLN.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoDCLN: public FFBtoDCLN {

    public:

        TestableFFBtoDCLN() : FFBtoDCLN() { }

        using FFBtoDCLN::initSettings;
        using FFBtoDCLN::evalAtPSPoint;
    };

    TEST(FFBtoDCLNTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pBmes{5280., 0., 0., 0.};
        FourMomentum pDmes{2381.3371929065643, 0., 0., -1475.671313782346};
        FourMomentum pTau{2034.777874233763, 553.6990600712329, -92.91177555543321, 819.1030156888944};
        FourMomentum kNuTau{863.8849328596731, -553.6990600712329, 92.91177555543321, 656.5682980934517};

        //Evaluate the FF class
        TestableFFBtoDCLN ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pBmes, 511), {Particle(pDmes,-411), Particle(kNuTau,16), Particle(pTau,-15)}, {});

        // Compare to direct evaluation
        // Fs, Fz, Fp, Ft
        Tensor ffEval{"ffEval", MD::makeVector({4}, {FF_BD}, {5135.76, 0.793366, 0.911495, 0.})};


        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 3; ++idx1) {
            // std::cout<<t.element({idx1}).real()<<std::endl;
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        }

    }


}
