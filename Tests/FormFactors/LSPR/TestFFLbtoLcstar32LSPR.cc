#include "gtest/gtest.h"
#include "Hammer/FormFactors/LSPR/FFLbtoLcstar32LSPR.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFLbtoLcstar32LSPR: public FFLbtoLcstar32LSPR {

    public:

        TestableFFLbtoLcstar32LSPR() : FFLbtoLcstar32LSPR() { }

        using FFLbtoLcstar32LSPR::initSettings;
        using FFLbtoLcstar32LSPR::evalAtPSPoint;
    };

    TEST(FFLbtoLcstar32LSPRTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        // Momentum decs
        FourMomentum pLbmes{5620., 0., 0., 0.};
        FourMomentum pLcmes{3067.17304270463, 0, 0, -1586.48210638947};
        FourMomentum kNuTau{103.858273281499, -16.4797557212763, -28.5437742055744, -98.4896519157352};
        FourMomentum pTau{2448.96868401388, 16.4797557212763, 28.5437742055744, 1684.97175830520};

        //Evaluate the FF class
        TestableFFLbtoLcstar32LSPR ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pLbmes, -5122), {Particle(pLcmes, -4124), Particle(kNuTau, 16), Particle(pTau, -15)}, {});

        //Compare to direct evaluation
        Tensor ffEval{"ffEval", MD::makeVector({16}, {FF_LBLCSTAR32},
                                                    {0.720401386256984,
                                                     1.24417777068813,
                                                     1.26182697446594,
                                                     -0.470732504336548,
                                                     -0.0170396347149046,
                                                     0.0207258015212351,
                                                     0.738050590034792,
                                                     -0.511494766669524,
                                                     0.0281547899021519,
                                                     -0.0207258015212351,
                                                     0.781633411513197,
                                                     -0.496146403812827,
                                                     0.0276299806183191,
                                                     -0.0207258015212351,
                                                     0,
                                                     0})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 16; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        }
    }


}
