set (Hammer_FormFactors_TEST_LIBS FormFactors)

new_test (FFBtoRhoBSZ "${Hammer_FormFactors_TEST_LIBS}")
new_test (FFBtoOmegaBSZ "${Hammer_FormFactors_TEST_LIBS}")
new_test (FFBtoRhoBSZVar "${Hammer_FormFactors_TEST_LIBS}")
new_test (FFBtoOmegaBSZVar "${Hammer_FormFactors_TEST_LIBS}")
