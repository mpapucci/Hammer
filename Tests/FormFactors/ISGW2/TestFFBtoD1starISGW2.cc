#include "gtest/gtest.h"
#include "Hammer/FormFactors/ISGW2/FFBtoD1starISGW2.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoD1starISGW2: public FFBtoD1starISGW2 {

    public:

        TestableFFBtoD1starISGW2() : FFBtoD1starISGW2() { }

        using FFBtoD1starISGW2::evalAtPSPoint;
        using FFBtoD1starISGW2::initSettings;

    };

    TEST(FFBtoD1starISGW2Test, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pBmes{6.0740724185305135e+00, 2.2041876529821600e-01, 2.6809423870568733e-01, 2.9832753503876139e+00};
        FourMomentum pD1starmes{2.8324053852172666e+00, 3.4694469299009156e-01, 1.1061032382391849e+00, 8.8195903786572871e-01};
        FourMomentum kNuTau{1.0425915030586572e+00, -2.6601336043581686e-01, -5.4778975227448157e-01, 8.4626256068835759e-01};
        FourMomentum pTau{2.1990755302545901e+00, 1.3948743274394138e-01, -2.9021924725901632e-01, 1.2550537518335279e+00};

        //Evaluate the FF class
        TestableFFBtoD1starISGW2 ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        set.changeSetting<bool>("BtoD**1*ISGW2", "SmearQ2", false);
        //MeV momenta
        ff.eval(Particle(1000*pBmes, -511), {Particle(1000*pD1starmes,20413), Particle(1000*kNuTau,-16), Particle(1000*pTau,15)}, {});

        // Compare to direct EvtGen evaluation (also in GeV)
        double ql = 0.0665166;
        double ll = 0.205976;
        double cppcm = 0.00451374;
        double cpmcm = -0.154666;
        double mb = pBmes.mass(); //GeV
        double mx = pD1starmes.mass(); //GeV
        double sqmbmx = sqrt(mb*mx);
        double Gv1 = ll/sqmbmx;
        double Gv2 = mb*mb*cppcm/sqmbmx;
        double Gv3 = cpmcm*sqmbmx;
        double Ga = 2*ql*sqmbmx;
        Tensor ffEval{"ffEval", MD::makeVector({8}, {FF_BDSSD1STAR},
                                               {0, Gv1, Gv2, Gv3, Ga, 0, 0, 0})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 8; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        }
    }


}
