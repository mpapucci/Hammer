#include "Hammer/Tools/Pdg.hh"
#include "Hammer/Tools/ParticleUtils.hh"
#include "gtest/gtest.h"
#include <boost/functional/hash.hpp>
#include <algorithm>

#include <iostream>

using namespace std;
namespace Hammer {

    // class TestablePID : public PID {
    // public:
    //     TestablePID() : PID() {
    //     }
    //     using PID::isHadron;
    //     using PID::isMeson;
    //     using PID::isBaryon;
    //     using PID::isDiQuark;
    //     using PID::isPentaquark;
    //     using PID::isLepton;
    //     using PID::isNeutrino;
    //     using PID::fundamentalID;
    //     using PID::extraBits;
    //     using PID::abspid;
    //     using PID::digit;
    //     using PID::location;
    // };

    TEST(PIDTest, getMass) {
        auto& pdg = PID::instance();
        EXPECT_NEAR(pdg.getMass(PID::ANTIMUON),0.106,0.001);
        EXPECT_NEAR(pdg.getMass("Mu-"),0.106,0.001);
    }

    TEST(PIDTest, toPdg) {
        auto& pdg = PID::instance();
        EXPECT_EQ(pdg.toPdgCode("Mu-"),13);
        EXPECT_EQ(pdg.toPdgCode("Ell"),0);
        auto codes = pdg.toPdgList("Ell");
        EXPECT_EQ(codes.size(), 4);
        sort(codes.begin(),codes.end());
        EXPECT_EQ(codes[0], -13);
        EXPECT_EQ(codes[3], 13);
        EXPECT_EQ(codes[1], -11);
        EXPECT_EQ(codes[2], 11);
    }

    TEST(PIDTest, getCharges) {
        auto& pdg = PID::instance();
        EXPECT_EQ(pdg.getThreeCharge(PID::MUON),-3);
        EXPECT_EQ(pdg.getThreeCharge({PID::MUON,PID::PIPLUS,PID::NEUTRON}),0);
        EXPECT_EQ(pdg.getLeptonNumber(PID::MUON),1);
        EXPECT_EQ(pdg.getLeptonNumber({PID::MUON,PID::PIPLUS,PID::NEUTRON}),1);
        EXPECT_EQ(pdg.getLeptonNumber({PID::MUON,PID::NU_EBAR}),0);
        EXPECT_EQ(pdg.getBaryonNumber(PID::MUON),0);
        EXPECT_EQ(pdg.getBaryonNumber({PID::MUON,PID::PIPLUS,PID::NEUTRON}),1);
        EXPECT_EQ(pdg.getBaryonNumber({PID::NEUTRON,PID::ANTIPROTON}),0);
        EXPECT_EQ(get<0>(pdg.getLeptonFlavorNumber(PID::MUON)),0);
        EXPECT_EQ(get<1>(pdg.getLeptonFlavorNumber(PID::MUON)),1);
        EXPECT_EQ(get<2>(pdg.getLeptonFlavorNumber(PID::MUON)),0);
        EXPECT_EQ(get<0>(pdg.getLeptonFlavorNumber({PID::MUON,PID::NU_EBAR})),-1);
        EXPECT_EQ(get<1>(pdg.getLeptonFlavorNumber({PID::MUON,PID::NU_EBAR})),1);
        EXPECT_EQ(get<2>(pdg.getLeptonFlavorNumber({PID::MUON,PID::NU_EBAR})),0);
    }

    TEST(PIDTest, getValidVertices) {
        auto& pdg = PID::instance();
        auto list1 = pdg.expandToValidVertices("B+D0barNutTau+");
        auto list2 = pdg.expandToValidVertices("BDTauNu");
        auto list3 = pdg.expandToValidVertices("TauEllNuNu");
        auto list4 = pdg.expandToValidVertices("TauEllNutNu");
        auto list5 = pdg.expandToValidVertexUIDs("BD*", true);
        auto list6 = pdg.expandToValidVertexUIDs("TauPiPiPi", true);
        EXPECT_EQ(list1.size(), 1);
        EXPECT_EQ(list2.size(), 8);
        EXPECT_EQ(list3.size(), 4);
        EXPECT_EQ(list4.size(), 2);
        EXPECT_EQ(list5.size(), 14);
        EXPECT_EQ(list6.size(), 4);

        struct less_than_hash {
            inline bool operator() (const pair<PdgId, vector<PdgId>>& vertex1, const pair<PdgId, vector<PdgId>>& vertex2 )
            {
                return (processID(vertex1.first, combineDaughters(vertex1.second))  < processID(vertex2.first, combineDaughters(vertex2.second)));
            }

        };

        sort(list1.begin(), list1.end(), less_than_hash());
        sort(list2.begin(), list2.end(), less_than_hash());
        sort(list3.begin(), list3.end(), less_than_hash());
        sort(list4.begin(), list4.end(), less_than_hash());
        sort(list5.begin(), list5.end());
        sort(list6.begin(), list6.end());

        vector<pair<PdgId, vector<PdgId>>> list1Exp = {{521, {-421,16,-15}}};//, {-521, {421,-16,15}}};
        sort(list1Exp.begin(), list1Exp.end(), less_than_hash());
        for (size_t idx = 0; idx < list1.size(); ++idx) {
            auto parent = get<0>(list1[idx]);
            EXPECT_EQ(parent, get<0>(list1Exp[idx]));
            auto daughters = get<1>(list1[idx]);
            for (size_t idx1 = 0; idx1 < daughters.size(); ++idx1) {
                EXPECT_EQ(daughters[idx1],get<1>(list1Exp[idx])[idx1]);
            }
        }

        vector<pair<PdgId, vector<PdgId>>> list2Exp = {{511, {411,-16,15}}, {-511, {-411,16,-15}}, {511, {-411,16,-15}}, {-511, {411,-16,15}}, {-521, {421,-16,15}}, {521, {-421,16,-15}}, {-521, {-421,-16,15}}, {521, {421,16,-15}} };
        sort(list2Exp.begin(), list2Exp.end(), less_than_hash());
        for (size_t idx = 0; idx < list2.size(); ++idx) {
            auto parent = get<0>(list2[idx]);
            EXPECT_EQ(parent, get<0>(list2Exp[idx]));
            auto daughters = get<1>(list2[idx]);
            for (size_t idx1 = 0; idx1 < daughters.size(); ++idx1) {
                EXPECT_EQ(daughters[idx1],get<1>(list2Exp[idx])[idx1]);
            }
        }

        vector<pair<PdgId, vector<PdgId>>> list3Exp = {{15, {16,-14,13}}, {-15, {-16,14,-13}},{15, {16,-12,11}}, {-15, {-16,12,-11}}};
        sort(list3Exp.begin(), list3Exp.end(), less_than_hash());
        for (size_t idx = 0; idx < list3.size(); ++idx) {
            auto parent = get<0>(list3[idx]);
            EXPECT_EQ(parent, get<0>(list3Exp[idx]));
            auto daughters = get<1>(list3[idx]);
            for (size_t idx1 = 0; idx1 < daughters.size(); ++idx1) {
                EXPECT_EQ(daughters[idx1],get<1>(list3Exp[idx])[idx1]);
            }
        }

//        vector<pair<PdgId, vector<PdgId>>> list4Exp = {{15, {16,-14,13}}, {-15, {-16,14,-13}},{15, {16,-12,11}}, {-15, {-16,12,-11}}};
        vector<pair<PdgId, vector<PdgId>>> list4Exp = {{15, {16,-14,13}},{15, {16,-12,11}}};
        sort(list4Exp.begin(), list4Exp.end(), less_than_hash());
        for (size_t idx = 0; idx < list4.size(); ++idx) {
            auto parent = get<0>(list4[idx]);
            EXPECT_EQ(parent, get<0>(list4Exp[idx]));
            auto daughters = get<1>(list4[idx]);
            for (size_t idx1 = 0; idx1 < daughters.size(); ++idx1) {
                EXPECT_EQ(daughters[idx1],get<1>(list4Exp[idx])[idx1]);
            }
        }

        vector<pair<PdgId, vector<PdgId>>> list5Exp = {{511, {423}}, {-511, {-423}}, {511, {-423}}, {-511, {423}}, {-521, {-413}}, {521, {413}}, {511, {-413}}, {-511, {413}}, {511, {413}}, {-511, {-413}}, {-521, {423}}, {521, {-423}}, {-521, {-423}}, {521, {423}}};
        sort(list5Exp.begin(), list5Exp.end(), less_than_hash());
        for (size_t idx = 0; idx < list5Exp.size(); ++idx) {
            auto daughters = get<1>(list5Exp[idx]);
            EXPECT_EQ(processID(get<0>(list5Exp[idx]),daughters) ,  list5[idx] );
        }

        vector<pair<PdgId, vector<PdgId>>> list6Exp = {{15, {211,-211,-211}}, {-15, {211,211,-211}}, {15, {-211,111,111}}, {-15, {211,111,111}}};
        sort(list6Exp.begin(), list6Exp.end(), less_than_hash());
        for (size_t idx = 0; idx < list6Exp.size(); ++idx) {
            auto daughters = get<1>(list6Exp[idx]);
            EXPECT_EQ(processID(get<0>(list6Exp[idx]),daughters) ,  list6[idx] );
        }

    }



} // namespace Hammer
