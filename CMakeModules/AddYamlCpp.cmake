set(YamlCppVersion 0.6.2)
set(YamlCppSHA256 e4d8560e163c3d875fd5d9e5542b5fd5bec810febdcba61481fe5fc4e6b1fd05)

include(ExternalProject)
include(GNUInstallDirs)
file(DOWNLOAD https://github.com/jbeder/yaml-cpp/archive/yaml-cpp-${YamlCppVersion}.tar.gz
    ${PROJECT_BINARY_DIR}/ThirdParty/tmp/yaml-cpp-${YamlCppVersion}.tar.gz
    STATUS Status
    SHOW_PROGRESS
    EXPECTED_HASH SHA256=${YamlCppSHA256}
)
if(NOT EXISTS ${PROJECT_BINARY_DIR}/ThirdParty/YamlCpp)
    execute_process(COMMAND ${CMAKE_COMMAND} -E tar xfz ${PROJECT_BINARY_DIR}/ThirdParty/tmp/yaml-cpp-${YamlCppVersion}.tar.gz
                    WORKING_DIRECTORY ${PROJECT_BINARY_DIR}/ThirdParty/tmp
                    RESULT_VARIABLE Result
    )
    if(NOT Result EQUAL "0")
        message(FATAL_ERROR "Failed extracting YamlCpp to ${PROJECT_BINARY_DIR}/ThirdParty/YamlCpp")
    endif()
    execute_process(COMMAND ${CMAKE_COMMAND} -E rename ${PROJECT_BINARY_DIR}/ThirdParty/tmp/yaml-cpp-yaml-cpp-${YamlCppVersion} ${PROJECT_BINARY_DIR}/ThirdParty/YamlCpp)
endif()
get_property(bdir TARGET Boost::headers PROPERTY INTERFACE_INCLUDE_DIRECTORIES)
string(SUBSTRING "${CMAKE_INSTALL_LIBDIR}" 3 -1 LIBSUFFIX_FIX)
ExternalProject_Add(Yaml_EP
    SOURCE_DIR ${PROJECT_BINARY_DIR}/ThirdParty/YamlCpp
    INSTALL_DIR ${CMAKE_INSTALL_PREFIX}
    CMAKE_ARGS -DLIB_SUFFIX=${LIBSUFFIX_FIX} -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR> -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER} -DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS} -DBOOST_INCLUDEDIR=${bdir} -DCMAKE_CXX_FLAGS=-fPIC
    BUILD_IN_SOURCE 1
    LOG_DOWNLOAD ON
    LOG_CONFIGURE ON
    LOG_BUILD ON
    LOG_INSTALL ON)
add_dependencies(Yaml_EP Boost::headers)
ExternalProject_Get_Property(Yaml_EP install_dir)
ExternalProject_Get_Property(Yaml_EP source_dir)
add_library(ThirdParty::YamlCpp INTERFACE IMPORTED)
add_dependencies(ThirdParty::YamlCpp Yaml_EP)
set_property(TARGET ThirdParty::YamlCpp PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${source_dir}/include)
set_property(TARGET ThirdParty::YamlCpp PROPERTY INTERFACE_LINK_LIBRARIES ${install_dir}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}yaml-cpp${CMAKE_SHARED_LIBRARY_SUFFIX})
set(BUILT_EXTERNAL_DEPENDENCIES ON CACHE BOOL "" FORCE)
