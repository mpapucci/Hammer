///
/// @file  AmplTauEllNuNu.hh
/// @brief \f$ \tau-> \ell\nu\nu \f$ amplitude
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_AMPL_TAUELLNUNU
#define HAMMER_AMPL_TAUELLNUNU

#include "Hammer/AmplitudeBase.hh"

namespace Hammer {

    class AmplTauEllNuNu : public AmplitudeBase {

    public:
        AmplTauEllNuNu();

        virtual ~AmplTauEllNuNu() override {
        }

    public:
        virtual void eval(const Particle& parent, const ParticleList& daughters,
                          const ParticleList& references) override;

    protected:
        virtual void defineSettings() override;
    };

} // namespace Hammer

#endif
