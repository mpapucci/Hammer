from math import floor, sqrt
from sys import path
# This import has side effects required for the kwarg projection='3d' in the call to fig.add_subplot
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import pyplot
from numpy import meshgrid, linspace, array
from io import FileIO

path.append("@CONFIG_PY_DIR@")
from hammer.hammerlib import Hammer, IOBuffer, RecordType


HAMMER = Hammer()
BUFFER = IOBuffer()
with open("./DemoHistosPY.dat", 'rb', buffering=0) as fin:
    HAMMER.init_run()
    if BUFFER.load(fin) and HAMMER.load_run_header(BUFFER):
        if BUFFER.load(fin):
            while BUFFER.kind == RecordType.HISTOGRAM or BUFFER.kind == RecordType.HISTOGRAM_DEFINITION:
                if BUFFER.kind == RecordType.HISTOGRAM_DEFINITION:
                    name = HAMMER.load_histogram_definition(BUFFER)
                else:
                    info = HAMMER.load_histogram(BUFFER)
                if not BUFFER.load(fin):
                     break
            cols = ['r', 'b', 'g', 'm', 'k']
            bins = HAMMER.get_histogram_bin_edges(b"pEllVsQ2:D*")
            bins_q2 = array([((bins[1][i]+bins[1][i+1])/2.) for i in range(0, len(bins[1])-1)])
            bins_pl = array([((bins[0][i]+bins[0][i+1])/2.) for i in range(0, len(bins[0])-1)])
            X, Y = meshgrid(bins_q2, bins_pl)
            fD = pyplot.figure()
            axD = fD.add_subplot(111, projection='3d')
            axD.set_xlabel('Q^2')
            axD.set_ylabel('p_l')
            axD.set_zlabel('Weights')
            fDs = pyplot.figure()
            axDs = fDs.add_subplot(111, projection='3d')
            axDs.set_xlabel('Q^2')
            axDs.set_ylabel('p_l')
            axDs.set_zlabel('Weights')
            for idw in range(0, 5):
                val = idw * 0.2
                HAMMER.set_wilson_coefficients(b"BtoCTauNu", {b"S_qLlL": val*1j, b"T_qLlL": val/4.})
                histo_D = HAMMER.get_histogram(b"pEllVsQ2:D", b"Scheme1")
                Z = array([elem.sum_wi for elem in histo_D]).reshape([len(bins_pl), len(bins_q2)])
                histo_Ds = HAMMER.get_histogram(b"pEllVsQ2:D*", b"Scheme1")
                Zs = array([elem.sum_wi for elem in histo_Ds]).reshape(
                    [len(bins_pl), len(bins_q2)])
                axD.plot_surface(X, Y, Z, color=cols[idw])
                axDs.plot_surface(X, Y, Zs, color=cols[idw])
            fD.show()
            fD.savefig("PlotD.pdf", bbox_inches='tight')
            fDs.show()
            fDs.savefig("PlotDs.pdf", bbox_inches='tight')
