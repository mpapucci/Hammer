//Validation analysis for Tau to 3 Pi Nu
#include "HepMCParser.hh"
#include "Hammer/Hammer.hh"
#include <iomanip>

using namespace std;

inline void PrintHistogram(Hammer::IOHistogram& histo){
    cout << "{";
    double totwgt = 0.0;
    for(size_t idW = 0; idW < histo.size(); ++idW) {
        totwgt += histo[idW].sumWi;
    }
    for(size_t idW = 0; idW < histo.size() - 1; ++idW) {
        cout << setprecision(5) << histo[idW].sumWi/totwgt << ", ";
    }
    cout << setprecision(5) << histo[histo.size() - 1].sumWi/totwgt << "}";
}

inline double dot(const array<double, 3>& a, const array<double, 3>& b) {
        return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
    }

int main() {
    //Container for histos
    vector<vector<Hammer::IOHistogram>> histocollection{};
    Hammer::Hammer ham{};
    //Do not calculate rates
    ham.setOptions("ProcessCalc: {Rates: false}");
    //Runs and observables
    vector<string> datasets{"Full", "PS", "PSRaw"};
    vector<string> obs{"PNu", "PPi", "mPiPi", "CosThetaPiNu", "CosThetaPiPi", "CosThetaPiD"};
    vector<uint16_t> dims{20, 20, 20, 10, 10, 10};
    //Included decays
    ham.includeDecay(string("TauPiPiPiNu"));
    ham.addFFScheme("Scheme", {{"BD", "ISGW2"},{"TauPiPiPi", "RCT"}});
    ham.setFFInputScheme({{"BD", "ISGW2"}, {"TauPiPiPi", "RCT"}});
    set<string> purePS{"TauPiPiPiNu"};
    for (auto& elemd : datasets) {
        auto elemf = elemd;
        for(size_t idx = 0; idx < obs.size(); ++idx){
            ham.addHistogram(obs[idx] + elemd, vector<uint16_t>{dims[idx]}, false);
        }
        ham.clearPurePSVertices(Hammer::WTerm::NUMERATOR);
        ham.clearPurePSVertices(Hammer::WTerm::DENOMINATOR);
        if (elemd == "PS"){
            cout << "Setting input tau decay to phase space" << endl;
            ham.addPurePSVertices(purePS, Hammer::WTerm::DENOMINATOR); //PS in denominator
            //ham.setFFInputScheme({{"BD", "ISGW2"}});
        } else if (elemd == "PSRaw") {
            cout << "Setting input and output tau decay to phase space" << endl;
            ham.addPurePSVertices(purePS, Hammer::WTerm::DENOMINATOR); //PS in denominator
            ham.addPurePSVertices(purePS, Hammer::WTerm::NUMERATOR); //PS in denominator
            elemf = "PS";
            //ham.setFFInputScheme({{"BD", "ISGW2"}});
        }
        ham.setUnits("GeV");
        ham.initRun();
        auto fin = unique_ptr<ifstream>(new ifstream("./data/BTau3PiNu-" + elemf + ".hepmc", std::ios::in));
        auto io = unique_ptr<HepMC::IO_GenEvent>(new HepMC::IO_GenEvent(*fin));
        if (io->rdstate() != 0) {
            return EXIT_FAILURE;
        }
        HepMC::GenEvent ge;
        for(size_t i = 0; i < 60000; ++i) {
            if (io->rdstate() == 0 && io->fill_next_event(&ge)) {
                ham.initEvent();
                auto processes = parseGenEvent(ge, {511,-511,521,-521});
                bool first = true;
                for(auto& elem : processes) {
                    auto procId = ham.addProcess(elem);
                    if (procId != 0 && first){//Process exists and is first one
                        auto BVer = elem.getParticlesByVertex("BDTauNu"); //Tau decay vertex momenta
                        auto pD = *find_if(BVer.second.begin(), BVer.second.end(), [&](Hammer::Particle& p)-> bool {return (abs(p.pdgId()) > 400);});
                        auto TVer = elem.getParticlesByVertex("TauPiPiPiNu"); //Tau decay vertex momenta
                        auto pNu = *find_if(TVer.second.begin(), TVer.second.end(), [&](Hammer::Particle& p)-> bool {return (abs(p.pdgId()) == 16);});
                        Hammer::Particle pPi;
                        Hammer::Particle pPiS;
                        if (pNu.pdgId() == 16){
                            pPi = *find_if(TVer.second.begin(), TVer.second.end(), [&](Hammer::Particle& p)-> bool {return (p.pdgId() == 211);});
                            pPiS = *find_if(TVer.second.begin(), TVer.second.end(), [&](Hammer::Particle& p)-> bool {return (p.pdgId() == -211);});
                        } else if (pNu.pdgId() == -16) {
                            pPi = *find_if(TVer.second.begin(), TVer.second.end(), [&](Hammer::Particle& p)-> bool {return (p.pdgId() == -211);});
                            pPiS = *find_if(TVer.second.begin(), TVer.second.end(), [&](Hammer::Particle& p)-> bool {return (p.pdgId() == 211);});
                        }
                        auto PNubin = static_cast<uint16_t>(floor(20. * (pNu.p().p() - 0.0) / (5.0 - 0.0)));
                        auto PPibin = static_cast<uint16_t>(floor(20. * (pPi.p().p() - 0.0) / (5.0 - 0.0)));
                        double mPiPi = (pPi.p() + pPiS.p()).mass();
                        auto mPiPibin = static_cast<uint16_t>(floor(20. * (mPiPi + 0.0) / (2.0 - 0.0)));
                        double CosThNuPi = dot(pNu.p().pVec(),pPi.p().pVec())/(pNu.p().p() * pPi.p().p());
                        auto CosThNuPibin = static_cast<uint16_t>(floor(10. * (CosThNuPi + 1.0) / (1.0 + 1.0)));
                        double CosThPiPi = dot(pPi.p().pVec(),pPiS.p().pVec())/(pPi.p().p() * pPiS.p().p());
                        auto CosThPiPibin = static_cast<uint16_t>(floor(10. * (CosThPiPi + 1.0) / (1.0 + 1.0)));
                        double CosThPiD = dot(pPi.p().pVec(),pD.p().pVec())/(pPi.p().p() * pD.p().p());
                        auto CosThPiDbin = static_cast<uint16_t>(floor(10. * (CosThPiD + 1.0) / (1.0 + 1.0)));
                        vector<uint16_t> indices{PNubin, PPibin, mPiPibin, CosThNuPibin, CosThPiPibin, CosThPiDbin};
                        for(size_t idx = 0; idx < obs.size(); ++idx){
                            ham.setEventHistogramBin(obs[idx]+elemd, {indices[idx]});
                        }
                        first = false;
                    } else {
                       ham.removeProcess(procId);
                    }
                }
                ham.processEvent();
                if ((i + 1) % 15000 == 0) {
                    cout << "processed tau"+elemd + " event " << i + 1 << endl;
//                    cout << ham.getWeight("Scheme") << endl;
                }
            }
        }
        vector<Hammer::IOHistogram> histos{};
        for (size_t idx = 0; idx < obs.size(); ++idx) {
            //WCs set to SM by default
            histos.push_back(ham.getHistogram(obs[idx]+elemd, "Scheme"));
        }
        histocollection.push_back(histos);
    }

    //Print histograms by observable
    for(size_t idx1 = 0; idx1 < obs.size(); ++idx1){
        cout << obs[idx1] << " = {";
        for(size_t idx2 = 0; idx2 < datasets.size() - 1; ++idx2){
            PrintHistogram(histocollection[idx2][idx1]);
            cout << ",";
        }
        PrintHistogram(histocollection[datasets.size() - 1][idx1]);
        cout << "};" << endl << endl;
    }

} // int main()
