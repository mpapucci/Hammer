#include "Hammer/Rates/RateTau3PiNu.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
#include "gtest/gtest.h"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {


    namespace MD = MultiDimensional;

    class TestableRateTau3PiNu : public RateTau3PiNu {

    public:
        TestableRateTau3PiNu() : RateTau3PiNu() {
        }

        using RateTau3PiNu::evalAtPSPoint;
    };

    TEST(RateTau3PiNuTest, evalAtPSPoint) {

        //Using PDG values Mt = 1.77686 GeV, Mp = 0.139571 GeV
        double Sqp = 1.181003641;
        double s1 = 0.2490221257;
        double s2 = 0.1498536045;

        // Evaluate at FF point
        TestableRateTau3PiNu rateP2s1s2;
        Tensor rate = rateP2s1s2.evalAtPSPoint({Sqp,s1,s2});

        // Compare to direct evaluation
        Tensor rateEval{"rateEval", MD::makeVector({3, 3}, {FF_TAU3PI, FF_TAU3PI_HC},
                                                    {5.348170407e-8,
                                                     -6.348410606e-8,
                                                     0.,
                                                     -6.348410606e-8,
                                                     9.966642831e-8,
                                                     0.,
                                                     0.,
                                                     0.,
                                                     7.423868365e-7})};
        double RateNorm = pow(GFermi, 2.);
        rateEval *= RateNorm;

        // Check TEs.
        for (IndexType idx1 = 0; idx1 < 3; ++idx1) {
            for (IndexType idx2 = 0; idx2 < 3; ++idx2) {
                double comRe = compareVals(rate.element({idx1, idx2}).real(), rateEval.element({idx1, idx2}).real());
                double comIm = compareVals(rate.element({idx1, idx2}).imag(), rateEval.element({idx1, idx2}).imag());
                EXPECT_NEAR(comRe, 1., 1e-4);
                EXPECT_NEAR(comIm, 1., 1e-4);
            }
        }
    }

} // namespace Hammer
