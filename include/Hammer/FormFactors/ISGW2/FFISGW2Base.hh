///
/// @file  FFISGW2Base.hh
/// @brief Hammer base class for ISGW2 form factors
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_ISGW2_BASE
#define HAMMER_FF_ISGW2_BASE

#include "Hammer/FormFactorBase.hh"

namespace Hammer {

    /// @brief Base class for ISGW2 form factors
    /// implementation matched to EvtGen
    ///
    /// Provides the rate contents as tensor...
    ///
    /// @ingroup FormFactors
    class FFISGW2Base : public FF1to1Base {

    public:
        FFISGW2Base();

        FFISGW2Base(const FFISGW2Base& other) = default;
        FFISGW2Base& operator=(const FFISGW2Base& other) = delete;
        FFISGW2Base(FFISGW2Base&& other) = delete;
        FFISGW2Base& operator=(FFISGW2Base&& other) = delete;
        virtual ~FFISGW2Base() override = default;

    protected:
        /// @brief
        /// @param[in] z
        /// @return
        double GetGammaji(double z) const;

        /// @brief
        /// @param[in] mq1
        /// @param[in] mq2
        /// @return
        double Getas(double mq1, double mq2) const;

        /// @brief
        /// @param[in] m
        /// @return
        double Getas(double m) const;

        /// @brief
        /// @param[in] point
        /// @param[in] masses
        /// @return
        virtual void evalAtPSPoint(const std::vector<double>& point, const std::vector<double>& masses = {}) override = 0;

        /// @brief
         virtual void defineSettings() override = 0;
        
    protected:
        virtual void addRefs() const override; 
        
    protected:
        double msb;
        double msd;
        double bb2;
        double mbb;
        double nf;
        double cf;
        double msq;
        double bx2;
        double mbx;
        double nfp;
    };

} // namespace Hammer

#endif
