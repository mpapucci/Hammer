###############################################################################
# Find a python module
#
# This sets the following variables:
# PY<MODULE_NAME>_FOUND - True if the package named <MODULE_NAME> was found.
# PY<MODULE_NAME> containts the path
#

# Find if a Python module is installed
# based on function found at http://www.cmake.org/pipermail/cmake/2011-January/041666.html
# To use do: find_python_module(PyQt4 REQUIRED)

function(find_python_module module_name)
	# A module's location is usually a directory, but for binary modules
	# it's a .so file.
	string(TOLOWER ${module_name} module_lower)
	execute_process(COMMAND "${Python3_EXECUTABLE}" "-c"
		"import re, ${module_lower}; print(re.compile('/__init__.py.*').sub('',${module_lower}.__file__))"
		RESULT_VARIABLE _${module_name}_status
		OUTPUT_VARIABLE _${module_name}_location
		ERROR_QUIET
		OUTPUT_STRIP_TRAILING_WHITESPACE)
	if(NOT _${module_name}_status)
		set(Python3_${module_name}_PATH ${_${module_name}_location} CACHE STRING
			"Location of Python module ${module_name}")
	endif()
	find_package_handle_standard_args(Python3_${module_name} REQUIRED_VARS Python3_${module_name}_PATH)
	set(Python3_${module_name}_FOUND ${Python3_${module_name}_FOUND} PARENT_SCOPE)
	mark_as_advanced(_${module_name}_status _${module_name}_location)
endfunction()

function(find_cython_module module_name)
	file(WRITE "${PROJECT_BINARY_DIR}/tmp_${module_name}.pyx" "cimport ${module_name}\n")
	# A module's location is usually a directory, but for binary modules
	# it's a .so file.
	execute_process(COMMAND "${Cython_EXECUTABLE}" "-3" "--cplus"
		"${PROJECT_BINARY_DIR}/tmp_${module_name}.pyx"
		RESULT_VARIABLE _${module_name}_status
		OUTPUT_VARIABLE _${module_name}_output
		ERROR_QUIET
		OUTPUT_STRIP_TRAILING_WHITESPACE)
	file(REMOVE "${PROJECT_BINARY_DIR}/tmp_${module_name}.pyx" "${PROJECT_BINARY_DIR}/tmp_${module_name}.cpp")
	if(NOT _${module_name}_status)
		set(Python3_${module_name}_FOUND TRUE)
	endif()
	set(Python3_${module_name}_FOUND ${Python3_${module_name}_FOUND} PARENT_SCOPE)
endfunction()
