///
/// @file  FFLbtoLcstar32PCR.hh
/// @brief \f$ \Lambda_b \rightarrow Lambda_c^*(2625) \f$ PCR form factors
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_LBLCSTAR32PCR
#define HAMMER_FF_LBLCSTAR32PCR

#include "Hammer/FormFactors/PCR/FFPCRBase.hh"

namespace Hammer {

    class FFLbtoLcstar32PCR : public FFPCRBase {

    public:
        FFLbtoLcstar32PCR();

        FFLbtoLcstar32PCR(const FFLbtoLcstar32PCR& other) = default;
        FFLbtoLcstar32PCR& operator=(const FFLbtoLcstar32PCR& other) = delete;
        FFLbtoLcstar32PCR(FFLbtoLcstar32PCR&& other) = delete;
        FFLbtoLcstar32PCR& operator=(FFLbtoLcstar32PCR&& other) = delete;
        virtual ~FFLbtoLcstar32PCR() override = default;

    public:
        virtual void eval(const Particle& parent, const ParticleList& daughters,
                          const ParticleList& references) override;

        virtual std::unique_ptr<FormFactorBase> clone(const std::string& label) override;

    protected:
        virtual void evalAtPSPoint(const std::vector<double>& point, const std::vector<double>& masses = {}) override;

        virtual void defineSettings() override;
    private:
        mutable bool initialized;
    };

} // namespace Hammer

#endif
