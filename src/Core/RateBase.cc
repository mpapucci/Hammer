///
/// @file  RateBase.cc
/// @brief Hammer base rate class
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#include "Hammer/RateBase.hh"
#include "Hammer/Math/Integrator.hh"
#include "Hammer/Math/MultiDim/IContainer.hh"
#include <string>

using namespace std;

namespace Hammer {

    RateBase::RateBase() : _integ{}, _PSRanges{}, _nPoints{
        _integ.getPointsNumber()}, _tensorList{} {
    }

    // RateBase::RateBase(const RateBase& other)
    //     : ParticleData(other),
    //       SettingsConsumer(other),
    //       _integ(other._integ),
    //       _PSRanges(other._PSRanges),
    //       _nPoints(other._nPoints) {
    //     for (auto& elem : other._tensorList) {
    //         addTensor(Tensor{elem});
    //     }
    // }

    // RateBase& RateBase::operator=(const RateBase& other) {
    //     ParticleData::operator=(other);
    //     SettingsConsumer::operator=(other);
    //     _integ = other._integ;
    //     _PSRanges = other._PSRanges;
    //     _nPoints = other._nPoints;
    //     _tensorList.clear();
    //     for (auto& elem : other._tensorList) {
    //         addTensor(Tensor{elem});
    //     }
    //     return *this;
    // }


    // RateBase::~RateBase() {
    //     _PSRanges.clear();
    //     _tensorList.clear();
    // }

    void RateBase::init() {
        // initSettings();
        ///@todo anything else?
    }

    Tensor& RateBase::getTensor() {
        return _tensorList[_signatureIndex];
    }

    const string RateBase::getPdgIdString() {
        auto sig = _signatures[_signatureIndex];
        string ds;
        for(auto& daughter : sig.daughters){
            ds = ds + " " + to_string(daughter);
        }
        return to_string(sig.parent) + " ->" + ds;
    }

    const pair<PdgId, vector<PdgId>> RateBase::getPdgIds() {
        auto sig = _signatures[_signatureIndex];
        return make_pair(sig.parent, sig.daughters);
    }
    
    const Tensor& RateBase::getTensor() const {
        return _tensorList[_signatureIndex];
    }

    const IntegrationBoundaries& RateBase::getIntegrationBoundaries() const {
        return _PSRanges[_signatureIndex];
    }

    void RateBase::addIntegrationBoundaries(const IntegrationBoundaries& boundaries) {
        // _PSRanges.push_back({std::pow(qMin, 2.), std::pow(qMax, 2.)});
        _PSRanges.push_back(boundaries);
    }

    void RateBase::eval(const Particle&, const ParticleList&, const ParticleList&) {
    }

    void RateBase::defineSettings() {
    }

    const EvaluationGrid& RateBase::getEvaluationPoints() const {
        return _integ.getEvaluationPoints();
    }

    void RateBase::calcTensor() {
        auto& boundaries = getIntegrationBoundaries();
        if(boundaries.size() > 0) {
            _integ.applyRange(boundaries);
            const EvaluationGrid& points = _integ.getEvaluationPoints();
            const EvaluationWeights& weights = _integ.getEvaluationWeights();
            Tensor& t = getTensor();
            MSG_INFO("Integrating rate " + getPdgIdString());
            t.clearData();
            for (IndexType i = 0; i < points.size(); ++i) {
                t.addAt(evalAtPSPoint(points[i])*weights[i], INTEGRATION_INDEX, i);
            }
        }
        else {
            Tensor& t = getTensor();
            t = evalAtPSPoint({});
        }
    }
    
    void RateBase::addTensor(Tensor&& tensor) {
        _tensorList.push_back(std::move(tensor));
    }

    Log& RateBase::getLog() const {
        return Log::getLog("Hammer.RateBase");
    }

} // namespace Hammer
