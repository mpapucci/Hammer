#include "gtest/gtest.h"
#include "Hammer/FormFactors/BLR/FFBtoD1starBLR.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoD1starBLR: public FFBtoD1starBLR {

    public:

        TestableFFBtoD1starBLR() : FFBtoD1starBLR() { }

        using FFBtoD1starBLR::evalAtPSPoint;
        using FFBtoD1starBLR::initSettings; 

    };

    TEST(FFBtoD1starBLRTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pBmes{5280., 0., 0., 0.};
        FourMomentum pD1starmes{3174.12206439394, 0, 0, -2045.65927751237};
        FourMomentum kNuTau{50.14709776263, -24.8428481565870, -43.029075211928, -6.78697251279};
        FourMomentum pTau{2055.73083784344, 24.8428481565870, 43.029075211928, 2052.44625002516};

        //Evaluate the FF class
        TestableFFBtoD1starBLR ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pBmes, 511), {Particle(pD1starmes,-20413), Particle(kNuTau,14), Particle(pTau,-13)}, {});

        // Compare to direct evaluation
        Tensor ffEval{"ffEval", MD::makeVector({8}, {FF_BDSSD1STAR},
                                               {0.719582925659466, 0.370603518630512, 0.365643880996805, -1.18565830953627, 0.782229873159499, -0.668353566591224, 0.862587515435831, 0.392418134101618})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 8; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        }
    }


}
