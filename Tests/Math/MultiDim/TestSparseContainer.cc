#include "Hammer/Math/MultiDim/SparseContainer.hh"
#include "Hammer/Math/MultiDim/ScalarContainer.hh"

#include "gtest/gtest.h"

using namespace std;

namespace Hammer {

    namespace MultiDimensional {

        TEST(SparseTest, ConstructionAccess) {
            auto t = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD, INTEGRATION_INDEX});
            EXPECT_EQ(t->rank(), 3);
            EXPECT_EQ(t->dims()[0], 2);
            EXPECT_EQ(t->dims()[1], 3);
            EXPECT_EQ(t->dims()[2], 1);
        }

        TEST(SparseTest, ElementAccess) {
            auto t = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD, INTEGRATION_INDEX});
            t->element({1,2,0}) = 0.1;
            t->element({0, 1, 0}) = 0.2;
            #ifndef NDEBUG
                ASSERT_DEATH(t->element({1, 2, 1}), "");
            #endif
            EXPECT_DOUBLE_EQ(t->element({1,2,0}).real(), 0.1);
            EXPECT_DOUBLE_EQ(t->element({0,1, 0}).real(), 0.2);
        }

        TEST(SparseTest, Iterators) {
            auto t = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD, INTEGRATION_INDEX});
            unique_ptr<SparseContainer> t2{static_cast<SparseContainer*>(t.release())};
            SparseContainer::iterator it1 = t2->begin();
            EXPECT_EQ(it1, t2->end());
            t2->element({0, 0, 0}) = 0.1;
            t2->element({1,1,0}) = 0.2;
            it1 = t2->begin();
            SparseContainer::iterator it2 = t2->end();
            --it2;
            EXPECT_DOUBLE_EQ(it1->second.real(), 0.1);
            EXPECT_DOUBLE_EQ(it2->second.real(), 0.2);
            ++it1;
            EXPECT_EQ(it1, it2);
        }

        TEST(SparseTest, BracketAccess) {
            auto t = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD, INTEGRATION_INDEX});
            unique_ptr<SparseContainer> t2{static_cast<SparseContainer*>(t.release())};
            (*t2)[(1 <<2) + 2] = 0.1;
            (*t2)[(0 <<2) + 1] = 0.2;
            EXPECT_DOUBLE_EQ(t2->element({1, 2, 0}).real(), 0.1);
            EXPECT_DOUBLE_EQ(t2->element({0, 1, 0}).real(), 0.2);
        }

        TEST(SparseTest, ValueAccess) {
            auto t = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD, INTEGRATION_INDEX});
            unique_ptr<SparseContainer> t2{static_cast<SparseContainer*>(t.release())};
            t2->setValue({1,2,0}, 0.1);
            t2->setValue({0, 1, 0}, 0.2);
            EXPECT_DOUBLE_EQ(t2->value({1, 2, 0}).real(), 0.1);
            EXPECT_DOUBLE_EQ(t2->value({0, 1, 0}).real(), 0.2);
        }

        TEST(SparseTest, Comparison) {
            auto t1 = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD, INTEGRATION_INDEX});
            auto t2 = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD, INTEGRATION_INDEX});
            auto t3 = makeEmptySparse({4, 3, 1}, {WILSON_BCENU, FF_BD, INTEGRATION_INDEX});
            auto t4 = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD_HC, INTEGRATION_INDEX});
            auto t5 = makeEmptyScalar();
            EXPECT_TRUE(t1->compare(*t2));
            EXPECT_FALSE(t1->compare(*t3));
            EXPECT_FALSE(t1->compare(*t4));
            EXPECT_FALSE(t1->compare(*t5));
            t1->element({0,1,0}) = 0.3;
            EXPECT_FALSE(t1->compare(*t2));
        }

        TEST(SparseTest, ComponentMultiply) {
            auto t = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD, INTEGRATION_INDEX});
            t->element({1, 2, 0}) = 0.1;
            t->element({0, 1, 0}) = 0.2;
            EXPECT_DOUBLE_EQ(t->element({1, 2, 0}).real(), 0.1);
            EXPECT_DOUBLE_EQ(t->element({0, 1, 0}).real(), 0.2);
            EXPECT_DOUBLE_EQ(t->element({0, 0, 0}).real(), 0.0);
            (*t) *= 3.;
            EXPECT_DOUBLE_EQ(t->element({1, 2, 0}).real(), 0.3);
            EXPECT_DOUBLE_EQ(t->element({0, 1, 0}).real(), 0.6);
            EXPECT_DOUBLE_EQ(t->element({0, 0, 0}).real(), 0.0);
        }

        TEST(SparseTest, Cloning) {
            auto t = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD, INTEGRATION_INDEX});
            t->element({1, 2, 0}) = 0.1;
            t->element({0, 1, 0}) = 0.2;
            auto t2 = t->clone();
            EXPECT_EQ(t->rank(), t2->rank());
            EXPECT_EQ(t->dims()[0], t2->dims()[0]);
            EXPECT_EQ(t->dims()[1], t2->dims()[1]);
            EXPECT_EQ(t->dims()[2], t2->dims()[2]);
            EXPECT_EQ(t->labels()[0], t2->labels()[0]);
            EXPECT_EQ(t->labels()[1], t2->labels()[1]);
            EXPECT_EQ(t->labels()[2], t2->labels()[2]);
            for (IndexType i1 = 0; i1 < 2; ++i1) {
                for (IndexType i2 = 0; i2 < 3; ++i2) {
                    EXPECT_DOUBLE_EQ(t->element({i1, i2, 0}).real(), t2->element({i1, i2, 0}).real());
                    EXPECT_DOUBLE_EQ(t->element({i1, i2, 0}).imag(), t2->element({i1, i2, 0}).imag());
                }
            }
        }

        TEST(SparseTest, Conjugate) {
            auto t = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD, SPIN_TAUM});
            t->element({1, 2, 0}) = 0.1;
            t->element({0, 1, 0}) = 0.2i;
            t->conjugate();
            EXPECT_DOUBLE_EQ(t->element({1, 2, 0}).real(), 0.1);
            EXPECT_DOUBLE_EQ(t->element({0, 1, 0}).real(), 0.0);
            EXPECT_DOUBLE_EQ(t->element({0, 1, 0}).imag(), -0.2);
            EXPECT_EQ(t->labels()[0], WILSON_BCENU_HC);
            EXPECT_EQ(t->labels()[1], FF_BD_HC);
            EXPECT_EQ(t->labels()[2], SPIN_TAUM_HC);
        }

        TEST(SparseTest, ItAligned) {
            auto t = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD, SPIN_TAUM});
            t->element({1, 2, 0}) = 0.1;
            t->element({0, 1, 0}) = 0.2i;
            auto sp = static_cast<SparseContainer*>(t.get());
            auto itb = sp->firstNonZero();
            auto ite = sp->endNonZero();
            EXPECT_EQ(itb->position(), 1);
            EXPECT_EQ(itb->value().imag(), 0.2);
            EXPECT_EQ(itb->value().real(), 0.);
            itb->next();
            EXPECT_EQ(itb->position(), (1 << 2) + 2);
            EXPECT_EQ(itb->value().imag(), 0.);
            EXPECT_EQ(itb->value().real(), 0.1);
            itb->next();
            EXPECT_EQ(*itb, *ite);
            EXPECT_TRUE(itb->isAligned());
        }

    } // namespace MultiDimensional

} // namespace Hammer
