#include "gtest/gtest.h"
#include "Hammer/FormFactors/BSZ/FFBtoOmegaBSZ.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoOmegaBSZ: public FFBtoOmegaBSZ {

    public:

        TestableFFBtoOmegaBSZ() : FFBtoOmegaBSZ() { }

        using FFBtoOmegaBSZ::evalAtPSPoint;
        using FFBtoOmegaBSZ::initSettings;

    };

    TEST(FFBtoOmegaBSZTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pBmes{17576.6777493606, 0, 0, 16764.8799788388};
        FourMomentum pOmegames{782., 0, 0, 0};
        FourMomentum kNuMu{195.8915304338, 31.769954441031, 97.777865759968, 166.7442672416};
        FourMomentum pMu{16598.7862189268, -31.769954441031, -97.777865759968, 16598.1357115972};

        //Evaluate the FF class
        TestableFFBtoOmegaBSZ ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pBmes, 521), {Particle(pOmegames,223), Particle(kNuMu,14), Particle(pMu,-13)}, {});

        // Compare to direct evaluation
        // Ap,V,A0,A1,A12,T1,T2,T23
        Tensor ffEval{"ffEval",
                      MD::makeVector({8}, {FF_BOMEGA},
                                        {-0.11262003760786,
                                         0.32133945171369,
                                         0.34599698254843,
                                         0.24779174538358,
                                         0.27431431527030,
                                         0.26569423684374,
                                         0.25639515670707,
                                         0.69258666245592})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 8; idx1++) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        };
    }
}
