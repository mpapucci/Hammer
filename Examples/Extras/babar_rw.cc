#include "HepMCParser.hh"
#include "Hammer/Hammer.hh"
#include <iomanip>

using namespace std;

inline void exportHistogram(Hammer::Hammer& ham, vector<string>& schemes, string proc, string filename){
    ofstream file;
    file.open("./data/dat/"+filename+"_"+proc+".dat");
    ham.setWilsonCoefficients("BtoCTauNu", {{"S_qRlL", 0.0},{"S_qLlL", 0.0}});
    double mbmtau = 4.710*1.776;
    // double mcmtau = (4.710-3.400)*1.776;
    //get SM effs
    vector<double> SMeffs{};
    file << 0.0;
    for(auto& scheme : schemes){
        auto histoAcc = ham.getHistogram("Acceptance" + proc, scheme);
        auto histoTot = ham.getHistogram("Total" + proc, scheme);
        SMeffs.push_back(histoAcc[0].sumWi/histoTot[0].sumWi);
        file << "\t" << setprecision(5) << 1.;
    }
    file << endl;

    for (size_t idW = 1; idW <= 60; ++idW) {
        double tanBetaOnMH = (static_cast<double>(idW))*0.05;
        ham.setWilsonCoefficients("BtoCTauNu", {{"S_qRlL", -pow(tanBetaOnMH,2.0)*mbmtau},{"S_qLlL", 0.0}});

        file << tanBetaOnMH;
        for(size_t idx = 0; idx < schemes.size(); ++idx){
            auto histoAcc = ham.getHistogram("Acceptance" + proc, schemes[idx]);
            auto histoTot = ham.getHistogram("Total" + proc, schemes[idx]);
            double effratio = histoAcc[0].sumWi/histoTot[0].sumWi/SMeffs[idx];
            file << "\t" << setprecision(5) << effratio;
        }
        file << endl;
    }

    file.close();
}

inline void tabHistogram1D(Hammer::IOHistogram& histo, size_t dim, pair<double,double>& range, string filename){
    ofstream file;
    file.open("./data/dat/"+filename);
    for(size_t id1 = 0; id1 < dim; ++id1){
        double valx = range.first + (static_cast<double>(id1)+0.5)/static_cast<double>(dim)*(range.second - range.first);
        file << setprecision(5)<< valx << "\t" << histo[id1].sumWi << endl;
    }
    file.close();
}

int main(int, char**) {

    //////////////////////////////////////////////////
    ////  Now reread histograms and reweigh them  ////
    //////////////////////////////////////////////////

    Hammer::Hammer ham{};
    Hammer::IOBuffer buf{Hammer::RecordType::UNDEFINED, 0ul, nullptr};
    ifstream inFile("./data/dat/BaBarHistos.dat", ios::binary);
    // Reload the histograms
    ham.setUnits("GeV");
    ham.initRun();
    inFile >> buf;
    if(!ham.loadRunHeader(buf)) {
        if(buf.start != nullptr) {
            delete[] buf.start;
        }
        inFile.close();
        return EXIT_FAILURE;
    }
    inFile >> buf;
    while(buf.kind == Hammer::RecordType::HISTOGRAM || buf.kind == Hammer::RecordType::HISTOGRAM_DEFINITION) {
        if(buf.kind == Hammer::RecordType::HISTOGRAM) {
            ham.loadHistogram(buf);
        }
        else {
            ham.loadHistogramDefinition(buf);
        }
        inFile >> buf;
    }
    inFile.close();
    vector<string> schemes{"SchemeBLPR", "SchemeCLN", "SchemeBGL", "SchemeISGW2"};

    auto DstarAcc = ham.getHistogram("AcceptanceDstar", schemes[0]);
    auto DstarTot = ham.getHistogram("TotalDstar", schemes[0]);
    auto DAcc = ham.getHistogram("AcceptanceD", schemes[0]);
    auto DTot = ham.getHistogram("TotalD", schemes[0]);
    cout << "D* Events: " << DstarAcc[0].n << "/" << DstarTot[0].n << endl;
    cout << "D Events: " << DAcc[0].n << "/" << DTot[0].n << endl;


    double mbmtau = 4.710*1.776;
    // double mcmtau = (4.710-3.400)*1.776;
    vector<double> vals{0., 0.25, 0.3, 0.5, 1.0};
    pair<double, double> range{0.,2.4};
    pair<double, double> rangeQ2{0.,14.};
    for(auto& tanBetaOnMH : vals){
        ham.setWilsonCoefficients("BtoCTauNu", {{"S_qRlL", -pow(tanBetaOnMH,2.0)*mbmtau},{"S_qLlL", 0.0}});
        auto DstarPEll = ham.getHistogram("PEllDstar", schemes[0]);
        auto DPEll = ham.getHistogram("PEllD", schemes[0]);
        auto DstarQ2 = ham.getHistogram("Q2Dstar", schemes[0]);
        auto DQ2 = ham.getHistogram("Q2D", schemes[0]);

        tabHistogram1D(DstarPEll, 20, range, "Babar_PEllDstar_"+to_string(tanBetaOnMH)+".dat");
        tabHistogram1D(DPEll, 20, range, "Babar_PEllD_"+to_string(tanBetaOnMH)+".dat");

        tabHistogram1D(DstarQ2, 20, rangeQ2, "Babar_Q2Dstar_"+to_string(tanBetaOnMH)+".dat");
        tabHistogram1D(DQ2, 20, rangeQ2, "Babar_Q2D_"+to_string(tanBetaOnMH)+".dat");

    }

    for(size_t idx = 1; idx < 3; ++idx){
        ham.setWilsonCoefficients("BtoCTauNu", {{"S_qRlL", 0.0},{"S_qLlL", 0.0}});
        auto DstarPEll = ham.getHistogram("PEllDstar", schemes[idx]);
        auto DstarQ2 = ham.getHistogram("Q2Dstar", schemes[idx]);
        tabHistogram1D(DstarPEll, 20, range, "Babar_PEllDstar_"+schemes[idx]+".dat");
        tabHistogram1D(DstarQ2, 20, rangeQ2, "Babar_Q2Dstar_"+schemes[idx]+".dat");

    }

    exportHistogram(ham, schemes, "Dstar", "BaBarEffFull");
    exportHistogram(ham, schemes, "D", "BaBarEffFull");

} // int main()
