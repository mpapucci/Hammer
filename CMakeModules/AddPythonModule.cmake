#find_package(PythonInterp)
include(FindPythonModule)

function(find_pip)
    message(STATUS "Looking for pip availability.")
    execute_process(
        COMMAND ${Python3_EXECUTABLE} -m pip -V
        RESULT_VARIABLE result
        OUTPUT_VARIABLE output
        ERROR_VARIABLE error
    )
    if("${result}" STREQUAL "0")
        set(Python3_pip_FOUND TRUE PARENT_SCOPE)
        message(STATUS "Found pip: ${output}.")
    else()
        message(WARNING "Pip not found: unable to install missing packages.")
    endif()
endfunction()

function(install_python_module Package)
    message(STATUS "Trying to install python package '${Package}' with pip.")
    if(${ARGC} GREATER 1)
        set(_${Package}_install_string "${Package}==${ARGV1}")
    else()
        set(_${Package}_install_string "${Package}")
    endif()
    execute_process(
        COMMAND ${Python3_EXECUTABLE} -m pip install --upgrade ${_${Package}_install_string}
        RESULT_VARIABLE result
        OUTPUT_VARIABLE output
        ERROR_VARIABLE error
    )
    if("${result}" STREQUAL "0")
        find_python_module(${Package} QUIET)
        if(Python3_${Package}_FOUND)
            set(Python3_${Package}_INSTALLED TRUE PARENT_SCOPE)
            message(STATUS "Done.")
        else()
            find_cython_module(${Package})
            if(Python3_${Package}_FOUND)
                set(Python3_${Package}_INSTALLED TRUE PARENT_SCOPE)
                message(STATUS "Done.")
            else()
                message(ERROR "Could not find ${Package} after installation with pip")
            endif()
        endif()
    else()
        message(ERROR "Could not install ${Package} -- ${result}")
    endif()
    mark_as_advanced(_${Package}_install_string)
endfunction()
