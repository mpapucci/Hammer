///
/// @file  IOTypes.hh
/// @brief Declarations for Hammer IO structs
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_TOOLS_IOTYPES_HH
#define HAMMER_TOOLS_IOTYPES_HH

#include <vector>
#include <utility>
#include <memory>
#include <iostream>
#include <set>
#include <string>

#include "Hammer/Config/HammerConfig.hh"

#ifdef HAVE_ROOT
#include "Rtypes.h"
#endif

namespace Hammer {

    enum RecordType : char { UNDEFINED = 'u', HEADER = 'b', EVENT = 'e', HISTOGRAM = 'h', RATE = 'r', HISTOGRAM_DEFINITION = 'd' };

    struct IOBuffer {
        RecordType kind;
        uint32_t length;
        uint8_t* start;
    };

    std::ostream& operator<<(std::ostream& os, const IOBuffer& buf);
    std::istream& operator>>(std::istream& is, IOBuffer& buf);

    namespace Serial {

        class DetachedBuffers;

    }

    class IOBuffers {
    public:

        IOBuffers();
        explicit IOBuffers(std::unique_ptr<Serial::DetachedBuffers>&& data);
        IOBuffers(const IOBuffers&) = delete;
        IOBuffers& operator=(const IOBuffers&) = delete;
        IOBuffers(IOBuffers&&);
        IOBuffers& operator=(IOBuffers&&);
        ~IOBuffers();

    public:

        using iterator = std::vector<IOBuffer>::iterator;
        using const_iterator = std::vector<IOBuffer>::const_iterator;
        using reverse_iterator = std::vector<IOBuffer>::reverse_iterator;
        using const_reverse_iterator = std::vector<IOBuffer>::const_reverse_iterator;

        IOBuffer& at(size_t pos);
        const IOBuffer& at(size_t pos) const;

        IOBuffer& operator[](size_t pos);
        const IOBuffer& operator[](size_t pos) const;

        IOBuffer& front();
        const IOBuffer& front() const;

        IOBuffer& back();
        const IOBuffer& back() const;

        size_t size() const;
        bool empty() const;

        iterator begin() noexcept;
        const_iterator begin() const noexcept;
        const_iterator cbegin() const noexcept;

        iterator end() noexcept;
        const_iterator end() const noexcept;
        const_iterator cend() const noexcept;

        reverse_iterator rbegin() noexcept;
        const_reverse_iterator rbegin() const noexcept;
        const_reverse_iterator crbegin() const noexcept;

        reverse_iterator rend() noexcept;
        const_reverse_iterator rend() const noexcept;
        const_reverse_iterator crend() const noexcept;

        void clear();

    private:

        void init();

    private:

        std::vector<IOBuffer> _buffers;
        std::unique_ptr<Serial::DetachedBuffers> _pOwner;

    };

    std::ostream& operator<<(std::ostream& os, const IOBuffers& buf);

#ifdef HAVE_ROOT

    struct RootIOBuffer {
        Char_t kind;
        Int_t length;
        Int_t maxLength;
        UChar_t* start;

        RootIOBuffer& operator=(const IOBuffer& other);
    };

#endif

    ///@brief  contents of a histogram bin after full contraction (real weights)
    ///        to be used to export the histogram outside Hammer
    struct BinContents {
        double sumWi;  ///< sum of weights
        double sumWi2; ///< sum of squared weights
        size_t n;     ////< number of entries in the bin
    };

    using IOHistogram = std::vector<BinContents>;

    struct HistoInfo {
        std::string name;
        std::string scheme;
        std::set<std::set<size_t>> eventGroupId;
    };

}

#endif
