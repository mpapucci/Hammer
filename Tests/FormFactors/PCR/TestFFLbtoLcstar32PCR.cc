#include "gtest/gtest.h"
#include "Hammer/FormFactors/PCR/FFLbtoLcstar32PCR.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFLbtoLcstar32PCR: public FFLbtoLcstar32PCR {

    public:

        TestableFFLbtoLcstar32PCR() : FFLbtoLcstar32PCR() { }

        using FFLbtoLcstar32PCR::initSettings;
        using FFLbtoLcstar32PCR::evalAtPSPoint;
    };

    TEST(FFLbtoLcstar32PCRTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        // Momentum decs
        FourMomentum pLbmes{5620., 0., 0., 0.};
        FourMomentum pLcmes{3067.17304270463, 0, 0, -1586.48210638947};
        FourMomentum kNuTau{103.858273281499, -16.4797557212763, -28.5437742055744, -98.4896519157352};
        FourMomentum pTau{2448.96868401388, 16.4797557212763, 28.5437742055744, 1684.97175830520};

        //Evaluate the FF class
        TestableFFLbtoLcstar32PCR ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pLbmes, -5122), {Particle(pLcmes, -4124), Particle(kNuTau, 16), Particle(pTau, -15)}, {});

        //Compare to direct evaluation
        Tensor ffEval{"ffEval", MD::makeVector({16}, {FF_LBLCSTAR32},
            {0.,0.,-1.01732,0.216278,0.0972496,-0.0444637,-0.781601,0.147147,-0.129211,0.0517703,0.,0.,0.,0.,0.,0.})};


        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 16; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        }
    }


}
