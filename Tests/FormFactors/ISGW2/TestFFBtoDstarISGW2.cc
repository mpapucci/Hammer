#include "gtest/gtest.h"
#include "Hammer/FormFactors/ISGW2/FFBtoDstarISGW2.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoDstarISGW2: public FFBtoDstarISGW2 {

    public:

        TestableFFBtoDstarISGW2() : FFBtoDstarISGW2() { }

        using FFBtoDstarISGW2::initSettings;
        using FFBtoDstarISGW2::evalAtPSPoint;
    };

    TEST(FFBtoDstarISGW2Test, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pBmes{5280.,   0.,   0.,   0.};
        FourMomentum pDstarmes{2258.9451,   -542.95362175877925474, -718.43449427445361377, 513.65086086722812801};
        FourMomentum kNuTau{763.45512706233784082,  59.818100429037400761,  266.26625745054465575,  713.01318784585764875};
        FourMomentum pTau{2257.5997729376621592,    483.13552132974185397,  452.16823682390895801,  -1226.6640487130857768};

        //Evaluate the FF class
        TestableFFBtoDstarISGW2 ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pBmes, 511), {Particle(pDstarmes,-413), Particle(kNuTau,16), Particle(pTau,-15)}, {});
//        //ff.evalAtPSPoint({Sqq}, {pBmes.mass(), pDstarmes.mass()});
//        TestableTensor FFres;
//        dynamic_cast<Tensor&>(FFres) = ff.evalAtPSPoint({7.99026}, {5.280, 2.007});

        //Compare to direct evaluation
        //gammaji=-0.869422 mtx=2.15 r2=0.209807
        //The calculated form factors at t=7.99026 are ff=5.83968 gf=0.164454 af+=-0.133415 af-=0.152319
        //Fs, Ff, Fg, Fm, Fp, Fzt, Fmt, Fpt
        //Switched to eval on momenta (q^2 = 8.05199 GeV^2)
        Tensor ffEval{"ffEval", MD::makeVector({8}, {FF_BDSTAR},
                    {0, 5841.5521663091869, 0.00016466092653151496, 0.00015250556021545483, -0.00013357097257008879,0,0,0})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 1; idx1 < 5; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-3);
        }
    }


}
