///
/// @file  ProcRequirements.hh
/// @brief Container class for required ingredients for the process weight calculation
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_PROCREQUIREMENTS_HH
#define HAMMER_PROCREQUIREMENTS_HH

#include <map>
#include <string>
#include <vector>

#include "Hammer/IndexTypes.hh"
#include "Hammer/ProcGraph.fhh"
#include "Hammer/Math/Tensor.hh"

namespace Hammer {

    class Log;
    class DictionaryManager;
    class AmplitudeBase;
    class RateBase;
    class FormFactorBase;
    class Process;

    /// @brief Decay process class
    ///
    /// Contains the amplitudes, weights and info associated to a decay, ...
    ///
    /// @ingroup Core
    class ProcRequirements {

    public:
        ProcRequirements();

        ProcRequirements(const ProcRequirements& other) = delete;
        ProcRequirements& operator=(const ProcRequirements& other) = delete;
        ProcRequirements(ProcRequirements&& other) = delete;
        ProcRequirements& operator=(ProcRequirements&& other) = delete;

        ~ProcRequirements() noexcept = default;

    public:
        /// @brief
        /// @param[in] dictionaries
        /// @param[in] inputs
        /// @param[in] graph
        size_t initialize(const DictionaryManager* dictionaries, const Process* inputs, const ProcGraph* graph);

        /// @brief
        /// @return
        std::vector<std::tuple<ParticleIndex, ParticleIndex, NumDenPair<AmplEntry>, NumDenPair<double>, NumDenPair<double>>> generatedAmplsMultsPS() const;

        std::pair<double, double> getPSRates() const;

        double calcCorrectionFactor(WTerm what = WTerm::NUMERATOR) const;

        const VertexDict<SelectedAmplEntry>& amplitudes() const;
        const VertexDict<RateBase*>& rates() const;
        const VertexDict<FFIndexDict<FormFactorBase*>> &formFactors() const;
        const std::vector<Tensor>& denominatorWilsonCoeffs() const;
        const std::vector<Tensor>& denominatorFFEigenVectors() const;
        const std::vector<Tensor>& specializedWilsonCoeffs() const;
        const VertexDict<HashId>& rateIds() const;
        const VertexDict<const double*>& partialWidths() const;

    protected:

        /// @brief logging facility
        /// @return   stream to be used for logging
        Log& getLog() const;

        ///@brief
        /// @param[in] descendant
        std::pair<ParticleIndex, bool> getAncestorId(ParticleIndex descendant) const;

        /// @brief
        void getAmplitudes();

        /// @brief
        void getFormFactors();

        /// @brief
        void getRates();

    private:

        VertexDict<VertexUID> _rateIds;

        const DictionaryManager* _dictionaries;
        const ProcGraph* _graph;
        const Process* _inputs;

        VertexDict<SelectedAmplEntry> _requiredAmplitudes;
        VertexDict<RateBase*> _requiredRates;
        VertexDict<FFIndexDict<FormFactorBase*>> _requiredFormFactors;
        VertexDict<const double*> _requiredPWs;
        VertexDict<NumDenPair<double>> _multPSFactors;
        VertexDict<NumDenPair<double>> _massPSFactors;
        std::vector<Tensor> _denominatorWilsonCoeffs;
        std::vector<Tensor> _denominatorFFEigenVectors;
        std::vector<Tensor> _specializedWilsonCoeffs;

    };

} // namespace Hammer

#endif
