///
/// @file  Dot.cc
/// @brief Tensor dot product algorithm
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-

#include <set>
#include <numeric>
#include <type_traits>
#include <tuple>

#include <boost/functional/hash.hpp>


#include "Hammer/Math/MultiDim/Ops/Dot.hh"
#include "Hammer/Math/MultiDim/Ops/Sum.hh"
#include "Hammer/Math/MultiDim/ScalarContainer.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Math/MultiDim/SparseContainer.hh"
#include "Hammer/Math/MultiDim/OuterContainer.hh"
#include "Hammer/Math/MultiDim/BruteForceIterator.hh"
#include "Hammer/Math/MultiDim/BlockIndexing.hh"
#include "Hammer/Exceptions.hh"
#include "Hammer/Tools/Utils.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/OperationDefs.hh"

#include <iostream>

using namespace std;

namespace Hammer {

    namespace MultiDimensional {

        using VTensor = VectorContainer;
        using STensor = SparseContainer;
        using OTensor = OuterContainer;
        using Base = IContainer;

        namespace Ops {

            Dot::Dot(const IndexPairList& indices, pair<bool, bool> shouldHC) : _indices{indices}, _hc{shouldHC} {
                _idxLeft.clear();
                _idxRight.clear();
                for(auto& elem: _indices) {
                    _idxLeft.insert(elem.first);
                    _idxRight.insert(elem.second);
                }

            }

            Base* Dot::operator()(VTensor& a, const VTensor& b) {
                auto newdimlabs = getNewIndexLabels(a, b);
                auto stridesA = a.getIndexing().getInnerOuterStrides(_indices, b.getIndexing().strides());
                if (newdimlabs.first.size() == 0) {
                    auto newscal = makeEmptyScalar();
                    for (size_t i = 0; i < a.numValues(); ++i) {
                        if (isZero(a[i])) {
                            continue;
                        }
                        Base::ElementType firstTerm = _hc.first ? conj(a[i]) : a[i];
                        auto pospairs = a.getIndexing().splitPosition(i, stridesA);
                        Base::ElementType secondTerm = _hc.second ? conj(b[pospairs.second]) : b[pospairs.second];
                        newscal->element({}) += firstTerm * secondTerm;
                    }
                    return newscal.release();
                } else {
                    PositionType reduced = b.getIndexing().reducedNumValues(_indices);
                    auto stridesB = b.getIndexing().getOuterStrides2nd(_indices);
                    auto newvect = makeEmptyVector(newdimlabs.first, newdimlabs.second);
                    VTensor* result = static_cast<VTensor*>(newvect.release());
                    for (size_t i = 0; i < a.numValues(); ++i) {
                        if (isZero(a[i])) {
                            continue;
                        }
                        Base::ElementType firstTerm = _hc.first ? conj(a[i]) : a[i];
                        auto pospairs = a.getIndexing().splitPosition(i, stridesA);
                        for (size_t j = 0; j < reduced; ++j) {
                            auto posB = b.getIndexing().build2ndPosition(j, pospairs.second, stridesB);
                            Base::ElementType secondTerm = _hc.second ? conj(b[posB]) : b[posB];
                            (*result)[pospairs.first * reduced + j] += firstTerm * secondTerm;
                        }
                    }
                    return static_cast<Base*>(result);
                }
            }

            Base* Dot::operator()(STensor& a, const STensor& b) {
                auto newdimlabs = getNewIndexLabels(a, b);
                auto leftinfo = a.getIndexing().processShifts(_indices, IndexPairMember::Left);
                auto rightinfo = b.getIndexing().processShifts(_indices, IndexPairMember::Right);
                IndexList inners((a.dims().size() + b.dims().size() - newdimlabs.first.size()) / 2);
                vector<bool> innerAdds(inners.size(), false);
                if (newdimlabs.first.size() == 0) {
                    auto newscal = makeEmptyScalar();
                    for (auto& elemL : a) {
                        a.getIndexing().splitPosition(elemL.first, get<0>(leftinfo), get<1>(leftinfo),
                                                                     inners, innerAdds);
                        Base::ElementType firstTerm = _hc.first ? conj(elemL.second) : elemL.second;
                        for (auto& elemR : b) {
                            auto tmpRight = b.getIndexing().splitPosition(elemR.first, get<0>(rightinfo),
                                                                          get<1>(rightinfo), inners, innerAdds, true);
                            if (tmpRight == numeric_limits<size_t>::max())
                                continue;
                            Base::ElementType secondTerm = _hc.second ? conj(elemR.second) : elemR.second;
                            newscal->element({}) += firstTerm * secondTerm;
                        }
                    }
                    return newscal.release();
                } else {
                    auto newsparse = makeEmptySparse(newdimlabs.first, newdimlabs.second);
                    STensor* result = static_cast<STensor*>(newsparse.release());
                    for (auto& elemL : a) {
                        auto tmpLeft = a.getIndexing().splitPosition(elemL.first, get<0>(leftinfo), get<1>(leftinfo),
                                                                     inners, innerAdds);
                        Base::ElementType firstTerm = _hc.first ? conj(elemL.second) : elemL.second;
                        for (auto& elemR : b) {
                            auto tmpRight = b.getIndexing().splitPosition(elemR.first, get<0>(rightinfo),
                                                                          get<1>(rightinfo), inners, innerAdds, true);
                            if (tmpRight == numeric_limits<size_t>::max())
                                continue;
                            Base::ElementType secondTerm = _hc.second ? conj(elemR.second) : elemR.second;
                            (*result)[tmpLeft * get<2>(rightinfo) + tmpRight] += firstTerm * secondTerm;
                        }
                    }
                    return static_cast<Base*>(result);
                }
            }

            Base* Dot::operator()(STensor& a, const VTensor& b) {
                auto newdimlabs = getNewIndexLabels(a, b);
                IndexList inners((a.dims().size() + b.dims().size() - newdimlabs.first.size()) / 2);
                vector<bool> innerAdds(inners.size(), false);
                auto leftinfo = a.getIndexing().processShifts(_indices, IndexPairMember::Left);
                if (newdimlabs.first.size() == 0) {
                    auto newscal = makeEmptyScalar();
                    if (b.rank() > 1) {
                        for (auto& elemL : a) {
                            a.getIndexing().splitPosition(elemL.first, get<0>(leftinfo),
                                                                         get<1>(leftinfo), inners, innerAdds);
                            Base::ElementType firstTerm = _hc.first ? conj(elemL.second) : elemL.second;
                            Base::ElementType secondTerm = _hc.second ? conj(b.element(inners)) : b.element(inners);
                            newscal->element({}) += firstTerm * secondTerm;
                        }
                    } else {
                        for (auto& elemL : a) {
                            a.getIndexing().splitPosition(elemL.first, get<0>(leftinfo),
                                                                         get<1>(leftinfo), inners, innerAdds);
                            Base::ElementType firstTerm = _hc.first ? conj(elemL.second) : elemL.second;
                            Base::ElementType secondTerm = _hc.second ? conj(b[inners[0]]) : b[inners[0]];
                            newscal->element({}) += firstTerm * secondTerm;
                        }
                    }
                    return newscal.release();
                } else {
                    auto newsparse = makeEmptySparse(newdimlabs.first, newdimlabs.second);
                    STensor* result = static_cast<STensor*>(newsparse.release());
                    if(b.rank() == _indices.size()) {
                        if(b.rank() > 1) {
                            for (auto& elemL : a) {
                                auto tmpLeft = a.getIndexing().splitPosition(elemL.first, get<0>(leftinfo), get<1>(leftinfo),
                                                                        inners, innerAdds);
                                Base::ElementType firstTerm = _hc.first ? conj(elemL.second) : elemL.second;
                                Base::ElementType secondTerm =
                                    _hc.second ? conj(b.element(inners)) : b.element(inners);
                                (*result)[tmpLeft] += firstTerm * secondTerm;
                            }
                        }
                        else {
                            for (auto& elemL : a) {
                                auto tmpLeft = a.getIndexing().splitPosition(elemL.first, get<0>(leftinfo), get<1>(leftinfo),
                                                                        inners, innerAdds);
                                Base::ElementType firstTerm = _hc.first ? conj(elemL.second) : elemL.second;
                                Base::ElementType secondTerm = _hc.second ? conj(b[inners[0]]) : b[inners[0]];
                                (*result)[tmpLeft] += firstTerm * secondTerm;
                            }
                        }
                    }
                    else {
                        auto itBe = b.endNonZero();
                        LabeledIndexing<AlignedIndexing> fakeB{b.dims(), b.labels()};
                        auto rightinfo = fakeB.processShifts(_indices, IndexPairMember::Right);
                        for (auto& elemL : a) {
                            auto tmpLeft = a.getIndexing().splitPosition(elemL.first, get<0>(leftinfo),
                                                                         get<1>(leftinfo), inners, innerAdds);
                            Base::ElementType firstTerm = _hc.first ? conj(elemL.second) : elemL.second;
                            auto itB = b.firstNonZero();
                            for(; *itB != *itBe; itB->next()) {
                                PositionType alPos = fakeB.posToAlignedPos(itB->position());
                                auto tmpRight = fakeB.splitPosition(
                                    alPos, get<0>(rightinfo), get<1>(rightinfo), inners, innerAdds, true);
                                if (tmpRight == numeric_limits<size_t>::max())
                                    continue;
                                Base::ElementType secondTerm = _hc.second ? conj(itB->value()) : itB->value();
                                (*result)[tmpLeft * get<2>(rightinfo) + tmpRight] += firstTerm * secondTerm;
                            }
                        }
                    }
                    return static_cast<Base*>(result);
                }
            }

            static pair<bool, bool> isSameDot(const OuterElemIterator::EntryType& a, const OuterElemIterator::EntryType& b,
                                  const DotGroupType& info, const DotGroupType& infoOther) {
                UNUSED(a);
                UNUSED(b);
                UNUSED(info);
                UNUSED(infoOther);
                /// @todo IMPLEMENT
                return {false, false};
            }

            Base* Dot::operator()(OTensor& a, const OTensor& b) {
                // get the chunks
                DotGroupList chunks = partitionContractions(a.getIndexing(), b.getIndexing());
                TensorData fullResult;
                OTensor* oResult = nullptr;
                auto leftinfo = a.getIndexing().processShifts(chunks, IndexPairMember::Left);
                auto rightinfo = b.getIndexing().processShifts(chunks, IndexPairMember::Right);
                for(auto& elemA: a) {
                    for(auto& elemB: b) {
                        // check for repetitions
                        vector<pair<size_t,size_t>> multiplicities(leftinfo.size());
                        vector<bool> used(leftinfo.size(), false);
                        for(size_t i=0; i < leftinfo.size(); ++i) {
                            if(used[i]) continue;
                            used[i] = true;
                            size_t count = 1;
                            size_t countHc = 0;
                            for (size_t j = i + 1; j < leftinfo.size(); ++j) {
                                auto tmp = isSameDot(elemA, elemB, chunks[i+1], chunks[j+1]);
                                if(tmp.first) {
                                    if(tmp.second) {
                                        ++countHc;
                                    }
                                    else {
                                        ++count;
                                    }
                                    used[j]=true;
                                }
                            }
                            multiplicities[i] = {count, countHc};
                        }
                        Base::ElementType currentWeight = 1.;
                        OuterElemIterator::EntryType currentTerm;
                        for (size_t i = 0; i < leftinfo.size(); ++i) {
                            if (multiplicities[i].first + multiplicities[i].second == 0)
                                continue;
                            // do the dot
                            OuterElemIterator::EntryType leftTensors;
                            leftTensors.reserve(get<0>(chunks[i + 1]).size());
                            transform(get<0>(chunks[i + 1]).begin(), get<0>(chunks[i + 1]).end(), back_inserter(leftTensors),
                                      [&](IndexType idx) -> const pair<SharedTensorData, bool>& { return elemA[idx]; });
                            OuterElemIterator::EntryType rightTensors;
                            rightTensors.reserve(get<1>(chunks[i + 1]).size());
                            transform(get<1>(chunks[i + 1]).begin(), get<1>(chunks[i + 1]).end(), back_inserter(rightTensors),
                                      [&](IndexType idx) -> const pair<SharedTensorData, bool>& { return elemB[idx]; });
                            IndexList inners(get<2>(chunks[i + 1]).size());
                            vector<bool> innerAdds(inners.size(), false);
                            auto newdimlabs = getNewIndexLabels(a.getIndexing(), b.getIndexing(), chunks[i + 1]);
                            OuterElemIterator itA{leftTensors};
                            OuterElemIterator itAEnd = itA.end();
                            size_t totalRankB = accumulate(rightTensors.begin(), rightTensors.end(), 0ul, [](PositionType tot, const pair<SharedTensorData, bool>& elem) -> PositionType { return tot + elem.first->rank(); });
                            if(totalRankB == inners.size()) {
                                IndexList::iterator itP1, itP2;
                                if (newdimlabs.first.size() == 0) {
                                    Base::ElementType newscal;
                                    for (; itA != itAEnd; ++itA) {
                                        itP1 = inners.begin();
                                        a.getIndexing().splitPosition(itA, chunks[i + 1], get<0>(leftinfo[i]),
                                                                      get<1>(leftinfo[i]), inners, innerAdds);
                                        Base::ElementType firstTerm = _hc.first ? conj(*itA) : *itA;
                                        for(auto& entry: rightTensors) {
                                            itP2 = itP1 + static_cast<ptrdiff_t>(entry.first->rank());
                                            Base::ElementType secondTerm = (!_hc.second != !entry.second)
                                                                                ? conj(entry.first->element(itP1, itP2))
                                                                                : entry.first->element(itP1, itP2);
                                            firstTerm *= secondTerm;
                                            if(isZero(secondTerm)) {
                                                break;
                                            }
                                            itP1 = itP2;
                                        }
                                        newscal += firstTerm;
                                    }
                                    currentWeight *=
                                        pow(newscal, multiplicities[i].first) * pow(conj(newscal), multiplicities[i].second);
                                } else {
                                    auto newsparse = makeEmptySparse(newdimlabs.first, newdimlabs.second);
                                    STensor* result = static_cast<STensor*>(newsparse.get());
                                    for (; itA != itAEnd; ++itA) {
                                        itP1 = inners.begin();
                                        PositionType tmpLeft =
                                            a.getIndexing().splitPosition(itA, chunks[i + 1], get<0>(leftinfo[i]),
                                                                          get<1>(leftinfo[i]), inners, innerAdds);
                                        Base::ElementType firstTerm = _hc.first ? conj(*itA) : *itA;
                                        for (auto& entry : rightTensors) {
                                            itP2 = itP1 + static_cast<ptrdiff_t>(entry.first->rank());
                                            Base::ElementType secondTerm = (!_hc.second != !entry.second)
                                                                               ? conj(entry.first->element(itP1, itP2))
                                                                               : entry.first->element(itP1, itP2);
                                            firstTerm *= secondTerm;
                                            if (isZero(secondTerm)) {
                                                break;
                                            }
                                            itP1 = itP2;
                                        }
                                        (*result)[tmpLeft] += firstTerm;
                                    }
                                    SharedTensorData tmpShared{newsparse.release()};
                                    currentTerm.insert(currentTerm.end(), multiplicities[i].first, {tmpShared, false});
                                    currentTerm.insert(currentTerm.end(), multiplicities[i].second, {tmpShared, true});
                                }
                            }
                            else {
                                OuterElemIterator itBEnd = OuterElemIterator{rightTensors}.end();
                                if (newdimlabs.first.size() == 0) {
                                    Base::ElementType newscal;
                                    for (; itA != itAEnd; ++itA) {
                                        a.getIndexing().splitPosition(itA, chunks[i + 1], get<0>(leftinfo[i]),
                                                                      get<1>(leftinfo[i]), inners, innerAdds);
                                        Base::ElementType firstTerm = _hc.first ? conj(*itA) : *itA;
                                        OuterElemIterator itB{rightTensors};
                                        for (; itB != itBEnd; ++itB) {
                                            PositionType tmpRight = b.getIndexing().splitPosition(
                                                itB, chunks[i + 1], get<0>(rightinfo[i]), get<1>(rightinfo[i]), inners,
                                                innerAdds, true);
                                            if (tmpRight == numeric_limits<size_t>::max())
                                                continue;
                                            Base::ElementType secondTerm = _hc.second ? conj(*itB) : *itB;
                                            newscal += firstTerm * secondTerm;
                                        }
                                    }
                                    currentWeight *= pow(newscal, multiplicities[i].first) *
                                                     pow(conj(newscal), multiplicities[i].second);
                                } else {
                                    auto newsparse = makeEmptySparse(newdimlabs.first, newdimlabs.second);
                                    STensor* result = static_cast<STensor*>(newsparse.get());
                                    for (; itA != itAEnd; ++itA) {
                                        PositionType tmpLeft =
                                            a.getIndexing().splitPosition(itA, chunks[i + 1], get<0>(leftinfo[i]),
                                                                          get<1>(leftinfo[i]), inners, innerAdds);
                                        Base::ElementType firstTerm = _hc.first ? conj(*itA) : *itA;
                                        OuterElemIterator itB{rightTensors};
                                        for (; itB != itBEnd; ++itB) {
                                            PositionType tmpRight = b.getIndexing().splitPosition(
                                                itB, chunks[i + 1], get<0>(rightinfo[i]), get<1>(rightinfo[i]), inners,
                                                innerAdds, true);
                                            if (tmpRight == numeric_limits<size_t>::max())
                                                continue;
                                            Base::ElementType secondTerm = _hc.second ? conj(*itB) : *itB;
                                            (*result)[tmpLeft * get<2>(rightinfo[i]) + tmpRight] +=
                                                firstTerm * secondTerm;
                                        }
                                    }
                                    SharedTensorData tmpShared{newsparse.release()};
                                    currentTerm.insert(currentTerm.end(), multiplicities[i].first, {tmpShared, false});
                                    currentTerm.insert(currentTerm.end(), multiplicities[i].second, {tmpShared, true});
                                }
                            }
                        }
                        // now add those untouched
                        for(auto elem: get<0>(chunks[0])) {
                            currentTerm.insert(currentTerm.end(), {elemA[elem].first, !elemA[elem].second != !_hc.first});
                        }
                        for(auto elem: get<1>(chunks[0])) {
                            currentTerm.insert(currentTerm.end(), {elemB[elem].first, !elemB[elem].second != !_hc.second});
                        }
                        if(currentTerm.size() == 0) {
                            if(fullResult.get() == nullptr) {
                                fullResult = makeScalar(currentWeight.real());
                            }
                            else {
                                fullResult->element({}) += (currentWeight.real());
                            }
                        }
                        else if(currentTerm.size() == 1) {
                            TensorData out = currentTerm[0].first->clone();
                            if(currentTerm[0].second) {
                                out->conjugate();
                            }
                            if (!isZero(currentWeight - 1.)) {
                                out->operator*=(currentWeight);
                            }
                            if(fullResult.get() == nullptr) {
                                fullResult = std::move(out);
                            }
                            else {
                                Ops::Sum summer{};
                                fullResult = calc2(std::move(fullResult), *out, summer, "sum_outerdot");
                            }
                        }
                        else {
                            if (!isZero(currentWeight - 1.)) {
                                auto temp = currentTerm.back();
                                currentTerm.pop_back();
                                auto candNew = temp.first->clone();
                                candNew->operator*=(temp.second ? conj(currentWeight) : currentWeight);
                                currentTerm.push_back({SharedTensorData{candNew.release()}, temp.second});
                            }
                            if (oResult != nullptr) {
                                oResult->addTerm(currentTerm);
                            } else {
                                fullResult = combineSharedTensors(std::move(currentTerm));
                                oResult = static_cast<OTensor*>(fullResult.get());
                            }
                        }
                    }
                }
                return static_cast<Base*>(fullResult.release());
            }

            Base* Dot::operator()(OTensor& a, const STensor& b) {
                DotGroupList chunks = partitionContractions(a.getIndexing(), b.getIndexing());
                ASSERT(get<1>(chunks[0]).size() ==0);
                ASSERT(get<2>(chunks[0]).size() == 0);
                ASSERT(get<1>(chunks[1]).size() == 0);
                TensorData fullResult;
                OTensor* oResult = nullptr;
                auto leftinfo = a.getIndexing().processShifts(chunks, IndexPairMember::Left);
                auto rightinfo = b.getIndexing().processShifts(_indices, IndexPairMember::Right);
                ASSERT(leftinfo.size() == 1);
                for (auto& elemA : a) {
                    Base::ElementType currentWeight = 1.;
                    OuterElemIterator::EntryType currentTerm;
                    OuterElemIterator::EntryType leftTensors;
                    leftTensors.reserve(get<0>(chunks[1]).size());
                    transform(get<0>(chunks[1]).begin(), get<0>(chunks[1]).end(),
                                back_inserter(leftTensors),
                                [&](IndexType idx) -> const pair<SharedTensorData, bool>& { return elemA[idx]; });
                    IndexList inners(get<2>(chunks[1]).size());
                    vector<bool> innerAdds(inners.size(), false);
                    auto newdimlabs = getNewIndexLabels(a.getIndexing(), b.getIndexing(), chunks[1]);
                    OuterElemIterator itA{leftTensors};
                    OuterElemIterator itAEnd = itA.end();
                    if (newdimlabs.first.size() == 0) {
                        Base::ElementType newscal;
                        for (; itA != itAEnd; ++itA) {
                            a.getIndexing().splitPosition(itA, chunks[1], get<0>(leftinfo[0]),
                                                            get<1>(leftinfo[0]), inners, innerAdds);
                            Base::ElementType firstTerm = _hc.first ? conj(*itA) : *itA;
                            for (auto& elemR : b) {
                                auto tmpRight = b.getIndexing().splitPosition(
                                    elemR.first, get<0>(rightinfo), get<1>(rightinfo), inners, innerAdds, true);
                                if (tmpRight == numeric_limits<size_t>::max())
                                    continue;
                                Base::ElementType secondTerm = _hc.second ? conj(elemR.second) : elemR.second;
                                newscal += firstTerm * secondTerm;
                            }
                        }
                        currentWeight *= newscal;
                    } else {
                        auto newsparse = makeEmptySparse(newdimlabs.first, newdimlabs.second);
                        STensor* result = static_cast<STensor*>(newsparse.get());
                        for (; itA != itAEnd; ++itA) {
                            PositionType tmpLeft = a.getIndexing().splitPosition(
                                itA, chunks[1], get<0>(leftinfo[0]), get<1>(leftinfo[0]), inners, innerAdds);
                            Base::ElementType firstTerm = _hc.first ? conj(*itA) : *itA;
                            for (auto& elemR : b) {
                                auto tmpRight = b.getIndexing().splitPosition(
                                    elemR.first, get<0>(rightinfo), get<1>(rightinfo), inners, innerAdds, true);
                                if (tmpRight == numeric_limits<size_t>::max())
                                    continue;
                                Base::ElementType secondTerm = _hc.second ? conj(elemR.second) : elemR.second;
                                (*result)[tmpLeft * get<2>(rightinfo) + tmpRight] += firstTerm * secondTerm;
                            }
                        }
                        SharedTensorData tmpShared{newsparse.release()};
                        currentTerm.push_back({tmpShared, false});
                    }
                    for (auto elem : get<0>(chunks[0])) {
                        currentTerm.insert(currentTerm.end(), {elemA[elem].first, !elemA[elem].second != !_hc.first});
                    }
                    if (currentTerm.size() == 0) {
                        if (fullResult.get() == nullptr) {
                            fullResult = makeScalar(currentWeight.real());
                        } else {
                            fullResult->element({}) += (currentWeight.real());
                        }
                    } else if (currentTerm.size() == 1) {
                        TensorData out = currentTerm[0].first->clone();
                        if (currentTerm[0].second) {
                            out->conjugate();
                        }
                        if (!isZero(currentWeight - 1.)) {
                            out->operator*=(currentWeight);
                        }
                        if (fullResult.get() == nullptr) {
                            fullResult = std::move(out);
                        } else {
                            Ops::Sum summer{};
                            fullResult = calc2(std::move(fullResult), *out, summer, "sum_outerdot");
                        }
                    } else {
                        if (!isZero(currentWeight - 1.)) {
                            auto temp = currentTerm.back();
                            currentTerm.pop_back();
                            auto candNew = temp.first->clone();
                            candNew->operator*=(temp.second ? conj(currentWeight) : currentWeight);
                            currentTerm.push_back({SharedTensorData{candNew.release()}, temp.second});
                        }
                        if (oResult != nullptr) {
                            oResult->addTerm(currentTerm);
                        } else {
                            fullResult = combineSharedTensors(std::move(currentTerm));
                            oResult = static_cast<OTensor*>(fullResult.get());
                        }
                    }
                }
                return static_cast<Base*>(fullResult.release());
            }


            Base* Dot::operator()(OTensor& a, const Base& b) {
                // inverse ordering: start dotting from rightmost ensures compatibility
                //                   with getNewIndexLabels() for fully collapsed outers
                map<IndexType, IndexPairList, greater<IndexType>> edges;
                for(auto& elem: _indices) {
                    auto keyvalA = a.getIndexing().getElementIndex(elem.first);
                    edges[keyvalA.first].push_back({keyvalA.second, elem.second});
                }
                IndexType tmpshift = 0ul;
                for(auto& elem: edges) {
                    for(auto& elem2: elem.second) {
                        elem2.second = static_cast<IndexType>(elem2.second + tmpshift);
                    }
                    tmpshift = static_cast<IndexType>(tmpshift + a.getIndexing().getSubIndexing(elem.first).rank() - elem.second.size());
                }
                if(edges.size() == a.getIndexing().numSubIndexing()) {
                    auto newdimlabs = getNewIndexLabels(a, b);
                    TensorData result;
                    if(newdimlabs.first.size() == 0) {
                        result = makeEmptyScalar();
                    }
                    else {
                        result = makeEmptySparse(newdimlabs.first, newdimlabs.second);
                    }
                    // Outer is going to be collapsed
                    SharedTensorData current_entry;
                    Ops::Sum summer{};
                    for (auto& elem : a) {
                        bool first = true;
                        for (auto& elem2 : edges) {
                            // dot the subtensor
                            Ops::Dot dotter{elem2.second, {elem[elem2.first].second, false}};
                            // cout << "In O dot B" << endl;
                            if (!first) {
                                current_entry = calc2(elem[elem2.first].first, *current_entry, dotter, "dot_outerdot");
                            } else {
                                first = false;
                                current_entry = calc2(elem[elem2.first].first, b, dotter, "dot_outerdot");
                            }
                        }
                        if(result->rank() == 0) {
                            result->element({}) += current_entry->element({});
                        }
                        else {
                            result = calc2(std::move(result), *current_entry, summer, "sum_outerdot");
                        }
                    }
                    return result.release();
                }
                else {
                    OuterContainer::DataType newdata;
                    newdata.reserve(a.numAddends());
                    vector<pair<SharedTensorData, bool>> current_entries(a.getIndexing().numSubIndexing() - edges.size() + 1);
                    for(auto& elem: a) {
                        size_t dotIdx = current_entries.size();
                        size_t curInIdx = elem.size() - 1;
                        size_t curOutIdx = current_entries.size() - 1;
                        for (auto& elem2 : edges) {
                            if(elem2.first != curInIdx) {
                                // copy the pass throughs
                                for(; curInIdx != elem2.first; --curInIdx, --curOutIdx) {
                                    current_entries[curOutIdx] = elem[curInIdx];
                                }
                            }
                            // dot the subtensor
                            // cout << "In O dot B2" << endl;
                            Ops::Dot dotter{elem2.second, {elem[curInIdx].second, false}};
                            if (dotIdx < current_entries.size()) {
                                current_entries[dotIdx].first =
                                    calc2(elem[curInIdx].first, *(current_entries[dotIdx].first), dotter, "dot_outerdot");
                            }
                            else {
                                dotIdx = curOutIdx--;
                                current_entries[dotIdx].first = calc2(elem[curInIdx].first, b, dotter, "dot_outerdot");
                                current_entries[dotIdx].second = false;
                            }
                        }
                        newdata.push_back(current_entries);
                    }
                    vector<IndexList> dimlist;
                    vector<LabelsList> lablist;
                    for (auto elem : newdata[0]) {
                        dimlist.push_back(elem.first->dims());
                        lablist.push_back(elem.first->labels());
                    }
                    a.swap(newdata);
                    a.swapIndexing(BlockIndexing{dimlist, lablist});
                    return static_cast<Base*>(&a);
                }
            }

            Base* Dot::operator()(STensor& a, const OTensor& b) {
                DotGroupList chunks = partitionContractions(a.getIndexing(), b.getIndexing());
                TensorData fullResult;
                OTensor* oResult = nullptr;
                auto leftinfo = a.getIndexing().processShifts(_indices, IndexPairMember::Left);
                auto rightinfo = b.getIndexing().processShifts(chunks, IndexPairMember::Right);
                ASSERT(rightinfo.size() == 1);
                for (auto& elemB : b) {
                    Base::ElementType currentWeight = 1.;
                    OuterElemIterator::EntryType currentTerm;
                    OuterElemIterator::EntryType rightTensors;
                    rightTensors.reserve(get<1>(chunks[1]).size());
                    transform(get<1>(chunks[1]).begin(), get<1>(chunks[1]).end(),
                                back_inserter(rightTensors),
                                [&](IndexType idx) -> const pair<SharedTensorData, bool>& { return elemB[idx]; });
                    IndexList inners(get<2>(chunks[1]).size());
                    vector<bool> innerAdds(inners.size(), false);
                    auto newdimlabs = getNewIndexLabels(a.getIndexing(), b.getIndexing(), chunks[1]);
                    size_t totalRankB = accumulate(rightTensors.begin(), rightTensors.end(), 0ul, [](PositionType tot, const pair<SharedTensorData, bool>& elem) -> PositionType { return tot + elem.first->rank(); });
                    if (totalRankB == inners.size()) {
                        IndexList::iterator itP1, itP2;
                        if (newdimlabs.first.size() == 0) {
                            Base::ElementType newscal;
                            for (auto& elemL : a) {
                                itP1 = inners.begin();
                                a.getIndexing().splitPosition(elemL.first, get<0>(leftinfo), get<1>(leftinfo),
                                                                inners, innerAdds);
                                Base::ElementType firstTerm = _hc.first ? conj(elemL.second) : elemL.second;
                                for (auto& entry : rightTensors) {
                                    itP2 = itP1 + static_cast<ptrdiff_t>(entry.first->rank());
                                    Base::ElementType secondTerm = (!_hc.second != !entry.second)
                                                                        ? conj(entry.first->element(itP1, itP2))
                                                                        : entry.first->element(itP1, itP2);
                                    firstTerm *= secondTerm;
                                    if (isZero(secondTerm)) {
                                        break;
                                    }
                                    itP1 = itP2;
                                }
                                newscal += firstTerm;
                            }
                            currentWeight *= newscal;
                        } else {
                            auto newsparse = makeEmptySparse(newdimlabs.first, newdimlabs.second);
                            STensor* result = static_cast<STensor*>(newsparse.get());
                            for (auto& elemL : a) {
                                itP1 = inners.begin();
                                auto tmpLeft = a.getIndexing().splitPosition(elemL.first, get<0>(leftinfo),
                                                                                get<1>(leftinfo), inners, innerAdds);
                                Base::ElementType firstTerm = _hc.first ? conj(elemL.second) : elemL.second;
                                for (auto& entry : rightTensors) {
                                    itP2 = itP1 + static_cast<ptrdiff_t>(entry.first->rank());
                                    Base::ElementType secondTerm = (!_hc.second != !entry.second)
                                                                        ? conj(entry.first->element(itP1, itP2))
                                                                        : entry.first->element(itP1, itP2);
                                    firstTerm *= secondTerm;
                                    if (isZero(secondTerm)) {
                                        break;
                                    }
                                    itP1 = itP2;
                                }
                                (*result)[tmpLeft] += firstTerm;
                            }
                            SharedTensorData tmpShared{newsparse.release()};
                            currentTerm.push_back({tmpShared, false});
                        }
                    }
                    else {
                        OuterElemIterator itBEnd = OuterElemIterator{rightTensors}.end();
                        if (newdimlabs.first.size() == 0) {
                            Base::ElementType newscal;
                            for (auto& elemL : a) {
                                a.getIndexing().splitPosition(elemL.first, get<0>(leftinfo), get<1>(leftinfo),
                                                                inners, innerAdds);
                                Base::ElementType firstTerm = _hc.first ? conj(elemL.second) : elemL.second;
                                OuterElemIterator itB{rightTensors};
                                for (; itB != itBEnd; ++itB) {
                                    PositionType tmpRight = b.getIndexing().splitPosition(
                                        itB, chunks[1], get<0>(rightinfo[0]), get<1>(rightinfo[0]), inners,
                                        innerAdds, true);
                                    if (tmpRight == numeric_limits<size_t>::max())
                                        continue;
                                    Base::ElementType secondTerm = _hc.second ? conj(*itB) : *itB;
                                    newscal += firstTerm * secondTerm;
                                }
                            }
                            currentWeight *= newscal;
                        } else {
                            auto newsparse = makeEmptySparse(newdimlabs.first, newdimlabs.second);
                            STensor* result = static_cast<STensor*>(newsparse.get());
                            for (auto& elemL : a) {
                                auto tmpLeft = a.getIndexing().splitPosition(elemL.first, get<0>(leftinfo),
                                                                                get<1>(leftinfo), inners, innerAdds);
                                Base::ElementType firstTerm = _hc.first ? conj(elemL.second) : elemL.second;
                                OuterElemIterator itB{rightTensors};
                                for (; itB != itBEnd; ++itB) {
                                    PositionType tmpRight = b.getIndexing().splitPosition(
                                        itB, chunks[1], get<0>(rightinfo[0]), get<1>(rightinfo[0]), inners,
                                        innerAdds, true);
                                    if (tmpRight == numeric_limits<size_t>::max())
                                        continue;
                                    Base::ElementType secondTerm = _hc.second ? conj(*itB) : *itB;
                                    (*result)[tmpLeft * get<2>(rightinfo[0]) + tmpRight] += firstTerm * secondTerm;
                                }
                            }
                            SharedTensorData tmpShared{newsparse.release()};
                            currentTerm.push_back({tmpShared, false});
                        }
                    }
                    for (auto elem : get<1>(chunks[0])) {
                        currentTerm.insert(currentTerm.end(), {elemB[elem].first, !elemB[elem].second != !_hc.second});
                    }
                    if(currentTerm.size() == 0) {
                        if(fullResult.get() == nullptr) {
                            fullResult = makeScalar(currentWeight.real());
                        }
                        else {
                            fullResult->element({}) += (currentWeight.real());
                        }
                    }
                    else if(currentTerm.size() == 1) {
                        TensorData out = currentTerm[0].first->clone();
                        if(currentTerm[0].second) {
                            out->conjugate();
                        }
                        if (!isZero(currentWeight - 1.)) {
                            out->operator*=(currentWeight);
                        }
                        if(fullResult.get() == nullptr) {
                            fullResult = std::move(out);
                        }
                        else {
                            Ops::Sum summer{};
                            fullResult = calc2(std::move(fullResult), *out, summer, "sum_outerdot");
                        }
                    }
                    else {
                        if (!isZero(currentWeight - 1.)) {
                            auto temp = currentTerm.back();
                            currentTerm.pop_back();
                            auto candNew = temp.first->clone();
                            candNew->operator*=(temp.second ? conj(currentWeight) : currentWeight);
                            currentTerm.push_back({SharedTensorData{candNew.release()}, temp.second});
                        }
                        if (oResult != nullptr) {
                            oResult->addTerm(currentTerm);
                        } else {
                            fullResult = combineSharedTensors(std::move(currentTerm));
                            oResult = static_cast<OTensor*>(fullResult.get());
                        }
                    }
                }
                return static_cast<Base*>(fullResult.release());
            }

            Base* Dot::operator()(Base& a, const Base& b) {
                auto newdimlabs = getNewIndexLabels(a, b);
                if(newdimlabs.first.size() == 0) {
                    auto newscalar = makeEmptyScalar();
                    IContainer::ElementType res = 0.;
                    BruteForceIterator bf{a.dims()};
                    for (auto elem : bf) {
                        auto aVal = a.element(elem);
                        if (isZero(aVal))
                            continue;
                        if (_hc.first) {
                            aVal = conj(aVal);
                        }
                        IndexList fixed = b.dims();
                        for (auto idx : _indices) {
                            fixed[idx.second] = elem[idx.first];
                        }
                        auto bVal = b.element(fixed);
                        if (isZero(bVal))
                            continue;
                        if (_hc.second) {
                            bVal = conj(bVal);
                        }
                        res += aVal * bVal;
                    }
                    newscalar->element({}) = res;
                    return newscalar.release();
                }
                else {
                    auto tmp = makeEmptySparse(newdimlabs.first, newdimlabs.second);
                    Base* results = tmp.release();
                    BruteForceIterator bf{a.dims()};
                    for (auto elem : bf) {
                        auto aVal = a.element(elem);
                        if (isZero(aVal))
                            continue;
                        if (_hc.first) {
                            aVal = conj(aVal);
                        }
                        IndexList fixed = b.dims();
                        for (auto idx : _indices) {
                            fixed[idx.second] = elem[idx.first];
                        }
                        BruteForceIterator bf2{b.dims(), fixed};
                        for (auto elem2 : bf2) {
                            auto bVal = b.element(elem2);
                            if (isZero(bVal))
                                continue;
                            if (_hc.second) {
                                bVal = conj(bVal);
                            }
                            auto idxRes = combineIndex(elem, elem2);
                            results->element(idxRes) += aVal * bVal;
                        }
                    }
                    return results;
                }
            }

            Base* Dot::error(Base&, const Base&) {
                throw Error("Invalid data types for tensor Dot");
            }

            IndexList Dot::combineIndex(const IndexList& a, const IndexList& b) const {
                IndexList result = a;
                for (auto elem : reverse_range(_idxLeft)) {
                    result.erase(result.begin() + elem);
                }
                size_t base = result.size();
                result.insert(result.end(),b.begin(), b.end());
                for (auto elem : reverse_range(_idxRight)) {
                    result.erase(result.begin() + static_cast<ptrdiff_t>(base + elem));
                }
                return result;
            }

            pair<IndexList, LabelsList> Dot::getNewIndexLabels(const Base& first, const Base& second) const {
                IndexList resultD = first.dims();
                LabelsList resultL = _hc.first ? flipListOfLabels(first.labels()) : first.labels();
                for (auto elem : reverse_range(_idxLeft)) {
                    resultD.erase(resultD.begin() + elem);
                    resultL.erase(resultL.begin() + elem);
                }
                size_t base = resultD.size();
                const IndexList& dim2 = second.dims();
                const LabelsList& lab2 = _hc.second ? flipListOfLabels(second.labels()) : second.labels();
                resultD.insert(resultD.end(), dim2.begin(), dim2.end());
                resultL.insert(resultL.end(), lab2.begin(), lab2.end());
                for (auto elem : reverse_range(_idxRight)) {
                    resultD.erase(resultD.begin() + static_cast<ptrdiff_t>(base + elem));
                    resultL.erase(resultL.begin() + static_cast<ptrdiff_t>(base + elem));
                }
                return make_pair(resultD, resultL);
            }

            SharedTensorData Dot::calcSharedDot(SharedTensorData origin, const IContainer& other,
                                                     const IndexPairList& indices, pair<bool, bool> shouldHC) {
                Ops::Dot dotter{indices, shouldHC};
                return calc2(std::move(origin), other, dotter, "dot");
            }

            unsigned long long Dot::dotSignature(const IContainer& a, const IContainer& b,
                                                 const std::string& type) const {
                UNUSED(a);
                UNUSED(b);
                UNUSED(type);
                ///@todo IMPLEMENT
                return 0;
            }

            template <size_t N, typename U, typename... Types>
            typename enable_if<is_convertible<vector<U>, typename tuple_element<N, tuple<Types...>>::type>::value, bool>::type
            matchPartitions(const tuple<Types...>& data, U value) {
                return find(get<N>(data).begin(), get<N>(data).end(), value) != get<N>(data).end();
            }

            template <size_t N, typename U, typename... Types>
            typename enable_if<is_convertible<vector<U>, typename tuple_element<N, tuple<Types...>>::type>::value,
                               void>::type
            addPartitionEntry(tuple<Types...>& data, U value) {
                if(find(get<N>(data).begin(), get<N>(data).end(), value) == get<N>(data).end())
                    get<N>(data).push_back(value);
            }

            template <size_t N, typename... Types>
            typename enable_if<(N < sizeof...(Types)), void>::type appendPartitionEntries(const tuple<Types...>& from,
                                                                                          tuple<Types...>& to) {
                get<N>(to).insert(get<N>(to).end(), get<N>(from).begin(), get<N>(from).end());
            }

            DotGroupList Dot::partitionContractions(const BlockIndexing& lhs,
                                                                   const BlockIndexing& rhs) const {
                DotGroupList partitions;
                vector<bool> validPartitions;
                IndexList lfree(lhs.numSubIndexing());
                IndexList rfree(rhs.numSubIndexing());
                iota(lfree.begin(), lfree.end(), 0);
                iota(rfree.begin(), rfree.end(), 0);
                //Placeholder for the untouched
                auto frontelem = make_tuple<IndexList, IndexList, IndexPairList>({}, {}, {});
                partitions.push_back(frontelem);
                validPartitions.push_back(true);
                for (auto& elem : _indices) {
                    auto lloc = lhs.getElementIndex(elem.first).first;
                    auto rloc = rhs.getElementIndex(elem.second).first;
                    lfree.erase(remove(lfree.begin(), lfree.end(), lloc), lfree.end());
                    rfree.erase(remove(rfree.begin(), rfree.end(), rloc), rfree.end());
                    vector<size_t> finds{};
                    auto match = [&](size_t pos, IndexType valL, IndexType valR) -> bool {
                        const auto& data = partitions[pos];
                        return matchPartitions<0>(data, valL) || matchPartitions<1>(data, valR);
                    };
                    for (size_t i = 0; i< partitions.size(); ++i) { // Find all matches on the left or the right
                        if (validPartitions[i] && match(i, lloc, rloc)) {
                            finds.push_back(i);
                        }
                    }
                    auto merge = [&](IndexType lidx, IndexType ridx, IndexPair contraction) -> void {
                        auto& data = partitions[finds[0]];
                        addPartitionEntry<0>(data, lidx);
                        addPartitionEntry<1>(data, ridx);
                        addPartitionEntry<2>(data, contraction);
                    };
                    auto merge_range = [&](size_t other) -> void {
                        auto& from = partitions[finds[other]];
                        auto& to = partitions[finds[0]];
                        appendPartitionEntries<0>(from, to);
                        appendPartitionEntries<1>(from, to);
                        appendPartitionEntries<2>(from, to);
                    };
                    if (finds.size() > 0) { // Insert elem into first found match
                        merge(lloc, rloc, elem);
                        for (size_t idx = 1; idx < finds.size(); ++idx) { // merge in other finds into first found match
                            if(validPartitions[finds[idx]]) {
                                merge_range(idx);
                                validPartitions[finds[idx]] = false;
                            }
                        }
                    }
                    else {
                        auto newelem = make_tuple<IndexList, IndexList, IndexPairList>({lloc}, {rloc}, {elem});
                        partitions.push_back(newelem);
                        validPartitions.push_back(true);
                    }
                }
                // eliminate already merged partitions
                for(size_t i = partitions.size(); 0 < i--;) {
                    if(!validPartitions[i]) partitions.erase(partitions.begin() + static_cast<ptrdiff_t>(i));
                }
                // sort contractions
                for(auto& elem: partitions) {
                    std::sort(get<2>(elem).begin(), get<2>(elem).end(),
                              [](const IndexPair& a, const IndexPair& b) -> bool { return a.second < b.second; });
                }
                //Add in untouched tensors
                partitions[0] = DotGroupType{lfree, rfree, {}};
                return partitions;
            }


            pair<IndexList, LabelsList> Dot::getNewIndexLabels(const BlockIndexing& lhs, const BlockIndexing& rhs,
                                                               const DotGroupType& chunk) const {
                IndexList dims;
                LabelsList labels;
                map<IndexType, IndexType> leftPosMaps;
                map<IndexType, IndexType> rightPosMaps;
                IndexType offset = 0;
                for (auto elem : get<0>(chunk)) {
                    leftPosMaps.insert({elem, offset});
                    dims.insert(dims.end(), lhs.getSubIndexing(elem).dims().begin(), lhs.getSubIndexing(elem).dims().end());
                    labels.insert(labels.end(), lhs.getSubIndexing(elem).labels().begin(), lhs.getSubIndexing(elem).labels().end());
                    offset = static_cast<IndexType>(offset + lhs.getSubIndexing(elem).rank());
                }
                for (auto elem : get<1>(chunk)) {
                    rightPosMaps.insert({elem, offset});
                    dims.insert(dims.end(), rhs.getSubIndexing(elem).dims().begin(), rhs.getSubIndexing(elem).dims().end());
                    labels.insert(labels.end(), rhs.getSubIndexing(elem).labels().begin(),
                                  rhs.getSubIndexing(elem).labels().end());
                    offset = static_cast<IndexType>(offset + rhs.getSubIndexing(elem).rank());
                }
                set<size_t> deletes;
                for(auto& elem: get<2>(chunk)) {
                    auto left = lhs.getElementIndex(elem.first);
                    auto right = rhs.getElementIndex(elem.second);
                    deletes.insert(leftPosMaps[left.first] + left.second);
                    deletes.insert(rightPosMaps[right.first] + right.second);
                }
                for(auto elem: reverse_range(deletes)) {
                    dims.erase(dims.begin() + static_cast<ptrdiff_t>(elem));
                    labels.erase(labels.begin() + static_cast<ptrdiff_t>(elem));
                }
                return {dims, labels};
            }


            DotGroupList Dot::partitionContractions(const LabeledIndexing<AlignedIndexing>&, const BlockIndexing& rhs) const {
                DotGroupList partitions(2);
                IndexList rfree(rhs.numSubIndexing());
                iota(rfree.begin(), rfree.end(), 0);
                get<2>(partitions[1]) = _indices;
                for (auto& elem : _indices) {
                    auto rloc = rhs.getElementIndex(elem.second).first;
                    rfree.erase(remove(rfree.begin(), rfree.end(), rloc), rfree.end());
                    get<1>(partitions[1]).push_back(rloc);
                }
                // sort contractions
                for (auto& elem : partitions) {
                    std::sort(get<2>(elem).begin(), get<2>(elem).end(),
                              [](const IndexPair& a, const IndexPair& b) -> bool { return a.second < b.second; });
                }
                // Add in untouched tensors
                partitions[0] = DotGroupType{IndexList{}, rfree, {}};
                return partitions;
            }

            DotGroupList Dot::partitionContractions(const BlockIndexing& lhs,
                                                    const LabeledIndexing<AlignedIndexing>&) const {
                DotGroupList partitions(2);
                IndexList lfree(lhs.numSubIndexing());
                iota(lfree.begin(), lfree.end(), 0);
                get<2>(partitions[1]) = _indices;
                for (auto& elem : _indices) {
                    auto lloc = lhs.getElementIndex(elem.first).first;
                    lfree.erase(remove(lfree.begin(), lfree.end(), lloc), lfree.end());
                    get<0>(partitions[1]).push_back(lloc);
                }
                // sort contractions
                for (auto& elem : partitions) {
                    std::sort(get<2>(elem).begin(), get<2>(elem).end(),
                              [](const IndexPair& a, const IndexPair& b) -> bool { return a.second < b.second; });
                }
                // Add in untouched tensors
                partitions[0] = DotGroupType{lfree, IndexList{}, {}};
                return partitions;
            }

            pair<IndexList, LabelsList> Dot::getNewIndexLabels(const LabeledIndexing<AlignedIndexing>& lhs,
                                                               const BlockIndexing& rhs,
                                                               const DotGroupType& chunk) const {
                IndexList dims;
                LabelsList labels;
                map<IndexType, IndexType> rightPosMaps;
                IndexType offset = 0;
                dims.insert(dims.end(), lhs.dims().begin(),
                            lhs.dims().end());
                labels.insert(labels.end(), lhs.labels().begin(),
                                lhs.labels().end());
                offset = static_cast<IndexType>(offset + lhs.rank());
                for (auto elem : get<1>(chunk)) {
                    rightPosMaps.insert({elem, offset});
                    dims.insert(dims.end(), rhs.getSubIndexing(elem).dims().begin(),
                                rhs.getSubIndexing(elem).dims().end());
                    labels.insert(labels.end(), rhs.getSubIndexing(elem).labels().begin(),
                                  rhs.getSubIndexing(elem).labels().end());
                    offset = static_cast<IndexType>(offset + rhs.getSubIndexing(elem).rank());
                }
                set<size_t> deletes;
                for (auto& elem : get<2>(chunk)) {
                    auto right = rhs.getElementIndex(elem.second);
                    deletes.insert(elem.first);
                    deletes.insert(rightPosMaps[right.first] + right.second);
                }
                for (auto elem : reverse_range(deletes)) {
                    dims.erase(dims.begin() + static_cast<ptrdiff_t>(elem));
                    labels.erase(labels.begin() + static_cast<ptrdiff_t>(elem));
                }
                return {dims, labels};
            }

            pair<IndexList, LabelsList> Dot::getNewIndexLabels(const BlockIndexing& lhs, const LabeledIndexing<AlignedIndexing>& rhs,
                                                               const DotGroupType& chunk) const {
                IndexList dims;
                LabelsList labels;
                map<IndexType, IndexType> leftPosMaps;
                IndexType offset = 0;
                for (auto elem : get<0>(chunk)) {
                    leftPosMaps.insert({elem, offset});
                    dims.insert(dims.end(), lhs.getSubIndexing(elem).dims().begin(),
                                lhs.getSubIndexing(elem).dims().end());
                    labels.insert(labels.end(), lhs.getSubIndexing(elem).labels().begin(),
                                  lhs.getSubIndexing(elem).labels().end());
                    offset = static_cast<IndexType>(offset + lhs.getSubIndexing(elem).rank());
                }
                dims.insert(dims.end(), rhs.dims().begin(),
                            rhs.dims().end());
                labels.insert(labels.end(), rhs.labels().begin(),
                                rhs.labels().end());
                set<size_t> deletes;
                for (auto& elem : get<2>(chunk)) {
                    auto left = lhs.getElementIndex(elem.first);
                    deletes.insert(leftPosMaps[left.first] + left.second);
                    deletes.insert(offset + elem.second);
                }
                for (auto elem : reverse_range(deletes)) {
                    dims.erase(dims.begin() + static_cast<ptrdiff_t>(elem));
                    labels.erase(labels.begin() + static_cast<ptrdiff_t>(elem));
                }
                return {dims, labels};
            }

        } // namespace Ops


    } // namespace MultiDimensional

} // namespace Hammer
