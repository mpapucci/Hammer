/////////////////////////
//// Histograms demo ////
/////////////////////////
#include "HepMCParser.hh"
#include "Hammer/Hammer.hh"

using namespace std;

//////////////////////////
//// Buffer functions ////
//////////////////////////

inline void printHistogram(Hammer::IOHistogram& histo, bool keepErrors = false) {
    for (size_t idx2 = 0; idx2 < 5; ++idx2){
            for (size_t idx1 = 0; idx1 < 6; ++idx1){
                cout << histo[idx1*5 + idx2].sumWi << "\t\t";
            }
            cout << endl;
            for (size_t idx1 = 0; idx1 < 6; ++idx1){
                cout << histo[idx1*5 + idx2].n << "\t\t";
            }
            cout << endl;
            if(keepErrors){
                for (size_t idx1 = 0; idx1 < 6; ++idx1){
                    cout << sqrt(histo[idx1*5 + idx2].sumWi2) << "\t\t";
                }
                cout << endl << endl;
            }

        }
    cout << endl;
}

int main() {

    //////////////////////////////////////////////////
    //// Now reread histograms and play with them ////
    //////////////////////////////////////////////////

    Hammer::Hammer ham{};
    Hammer::IOBuffer buf{Hammer::RecordType::UNDEFINED, 0ul, nullptr};
    for (size_t idW = 0; idW <= 5; ++idW) {
        ifstream inFile("./DemoHistos.dat", ios::binary);
        // Reload the histograms
        double val = (static_cast<double>(idW))*0.2;
        ham.setUnits("GeV");
        inFile >> buf;
        if(!ham.loadRunHeader(buf)) {
            if(buf.start != nullptr) {
                delete[] buf.start;
            }
            inFile.close();
            return EXIT_FAILURE;
        }
        ham.initRun();
        inFile >> buf;
        while(buf.kind == Hammer::RecordType::HISTOGRAM || buf.kind == Hammer::RecordType::HISTOGRAM_DEFINITION) {
            if(buf.kind == Hammer::RecordType::HISTOGRAM) {
                ham.loadHistogram(buf);
            }
            else {
                ham.loadHistogramDefinition(buf);
            }
            inFile >> buf;
        }
        auto rwgtstart = std::chrono::system_clock::now();
        //Set the WCs
        ham.setWilsonCoefficients("BtoCTauNu", {{"S_qLlL", val*1i}, {"T_qLlL", val/4.}});
        //Note this is an incremental setting; only changes what you specify! One could do
        //ham.resetWilsonCoefficients("BtoCTauNu"); to reset to SM
        //One could also do e.g. ham.setWilsonCoefficients("BtoCMuNu", ...); if one cared about muon mode NP, etc.
        //The output is row major flattened vector, so you need to know the dims
        auto histoT = ham.getHistogram("Total Sum of Weights", "Scheme1");
        auto histoDs = ham.getHistogram("pEllVsQ2:D*", "Scheme1");
        auto histoD = ham.getHistogram("pEllVsQ2:D", "Scheme1");
        cout << endl << "Reweighted histo to S_qLlL = " << val << "i, T_qLlL = " << val/4. << ": " << endl;
        cout << "BD*TauNu: pEllVsQ2" << endl;
        printHistogram(histoDs, true);
        cout << "BDTauNu: pEllVsQ2" << endl;
        printHistogram(histoD, true);
        cout << "Total: " << histoT[0].sumWi << endl;
        cout << "Total: " << sqrt(histoT[0].sumWi2) << endl;
        auto rwgtend = std::chrono::system_clock::now();
        auto durrwgt = rwgtend - rwgtstart;
        typedef std::chrono::duration<float> float_seconds;
        auto secsrwgt = std::chrono::duration_cast<float_seconds>(durrwgt);
        cout << "Reweight Time: " << secsrwgt.count() << endl;
        inFile.close();
    }

} // int main()
