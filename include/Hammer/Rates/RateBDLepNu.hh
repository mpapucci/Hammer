///
/// @file  RateBDLepNu.hh
/// @brief \f$ B \rightarrow D \tau\nu \f$ total rate
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_RATE_BDLEPNU
#define HAMMER_RATE_BDLEPNU

#include "Hammer/RateBase.hh"

namespace Hammer {

    class RateBDLepNu : public RateBase {

    public:
        RateBDLepNu();

        virtual ~RateBDLepNu() override {
        }

    protected:
        virtual Tensor evalAtPSPoint(const std::vector<double>& point) override;
    };

} // namespace Hammer

#endif
