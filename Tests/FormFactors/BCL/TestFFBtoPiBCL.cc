#include "gtest/gtest.h"
#include "Hammer/FormFactors/BCL/FFBtoPiBCL.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoPiBCL: public FFBtoPiBCL {

    public:

        TestableFFBtoPiBCL() : FFBtoPiBCL() { }

        using FFBtoPiBCL::evalAtPSPoint;
        using FFBtoPiBCL::initSettings;

    };

    TEST(FFBtoPiBCLTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs (q^2 = 19.8958 GeV^2)
        FourMomentum pBmes{28783.8884892086, 0, 0, 28295.4737822005};
        FourMomentum pPimes{139., 0, 0, 0};
        FourMomentum kNuMu{483.601218826, 143.20931144041, 440.75294019035, 138.1957947273};
        FourMomentum pMu{28161.2872703827, -143.20931144041, -440.75294019035, 28157.2779874733};

        //Evaluate the FF class
        TestableFFBtoPiBCL ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pBmes, 511), {Particle(pPimes,-211), Particle(kNuMu,14), Particle(pMu,-13)}, {});

        // Compare to direct evaluation
        // Fs, Fz, Fp, Ft
        Tensor ffEval{"ffEval", MD::makeVector({4}, {FF_BPI},
                        {0.,
                         0.50165560937469,
                         1.3961877732509,
                         0.})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 1; idx1 < 3; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        }

    }
    
    TEST(FFBtoPiBCLTest, evalq2cons) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs (q^2 = 19.8958 GeV^2)
        FourMomentum pBmes{28783.8884892086, 0, 0, 28295.4737822005};
        FourMomentum pPimes{139., 0, 0, 0};
        FourMomentum kNuMu{483.601218826, 143.20931144041, 440.75294019035, 138.1957947273};
        FourMomentum pMu{28161.2872703827, -143.20931144041, -440.75294019035, 28157.2779874733};

        //Evaluate the FF class
        TestableFFBtoPiBCL ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        vector<double> apvec={0.415, -0.488, -0.31};
        vector<double> a0vec={0.500, -1.424};
        set.changeSetting<vector<double>>("BtoPiBCL", "ap", apvec);
        set.changeSetting<vector<double>>("BtoPiBCL", "a0", a0vec);
        set.changeSetting<bool>("BtoPiBCL","q2cons",true);
        ff.calcUnits();
        
        ff.eval(Particle(pBmes, 511), {Particle(pPimes,-211), Particle(kNuMu,14), Particle(pMu,-13)}, {});
        
        // Compare to direct evaluation
        // Fs, Fz, Fp, Ft
        Tensor ffEval{"ffEval", MD::makeVector({4}, {FF_BPI},
                        {0.,
                         0.49302579294957,
                         1.3829059390230,
                         0.})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 1; idx1 < 3; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        }
    
    }
    
}
