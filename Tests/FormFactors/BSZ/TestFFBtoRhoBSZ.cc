#include "gtest/gtest.h"
#include "Hammer/FormFactors/BSZ/FFBtoRhoBSZ.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoRhoBSZ: public FFBtoRhoBSZ {

    public:

        TestableFFBtoRhoBSZ() : FFBtoRhoBSZ() { }

        using FFBtoRhoBSZ::evalAtPSPoint;
        using FFBtoRhoBSZ::initSettings;

    };

    TEST(FFBtoRhoBSZTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pBmes{17838.5064935065, 0, 0, 17039.1875956242};
        FourMomentum pRhomes{770., 0, 0, 0};
        FourMomentum kNuMu{198.6188302152, 31.769954441031, 97.777865759968, 169.9399855193};
        FourMomentum pMu{16869.8876632913, -31.769954441031, -97.777865759968, 16869.2476101049};

        //Evaluate the FF class
        TestableFFBtoRhoBSZ ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pBmes, 521), {Particle(pRhomes,113), Particle(kNuMu,14), Particle(pMu,-13)}, {});

        // Compare to direct evaluation
        // Ap,V,A0,A1,A12,T1,T2,T23
        Tensor ffEval{"ffEval",
                      MD::makeVector({8}, {FF_BRHO},
                                        {-0.12031819247473,
                                         0.34558307298139,
                                         0.37540838626044,
                                         0.26721909723634,
                                         0.30077407846314,
                                         0.28712896725280,
                                         0.27709651260590,
                                         0.7574294922472})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 8; idx1++) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        };
    }
}
