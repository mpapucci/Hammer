#include "gtest/gtest.h"
#include "Hammer/Tools/Logging.hh"

using namespace std;

namespace Hammer {

	TEST(LoggingTest, ConstructionDestruction) {
		Log& lg = Log::getLog("TestLogging");
		EXPECT_TRUE(lg.isActive(Log::WARN));
		EXPECT_FALSE(lg.isActive(Log::DEBUG));
	}

	TEST(LoggingTest, Messages) {
		Log& lg = Log::getLog("TestLogging");
		lg.info("Info Message");
		lg.warn("Warn Message");
		lg.error("Error Message");
	}

}
