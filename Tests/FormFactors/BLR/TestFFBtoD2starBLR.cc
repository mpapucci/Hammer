#include "gtest/gtest.h"
#include "Hammer/FormFactors/BLR/FFBtoD2starBLR.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoD2starBLR: public FFBtoD2starBLR {

    public:

        TestableFFBtoD2starBLR() : FFBtoD2starBLR() { }

        using FFBtoD2starBLR::evalAtPSPoint;
        using FFBtoD2starBLR::initSettings; 

    };

    TEST(FFBtoD2starBLRTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pBmes{5280., 0., 0., 0.};
        FourMomentum pD2starmes{3189.85994318182, 0, 0, -2029.45447278719};
        FourMomentum kNuTau{50.20101629360, -24.8428481565870, -43.029075211928, -7.17451174540};
        FourMomentum pTau{2039.93904052459, 24.8428481565870, 43.029075211928, 2036.62898453259};

        //Evaluate the FF class
        TestableFFBtoD2starBLR ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pBmes, 511), {Particle(pD2starmes,-415), Particle(kNuTau,14), Particle(pTau,-13)}, {});

        // Compare to direct evaluation
        Tensor ffEval{"ffEval", MD::makeVector({8}, {FF_BDSSD2STAR},
                                               {0.546490387225108, -0.688578008829105, 0.106563686860457, 0.0388647072104949, 0.0760885649215852, 0.379386626757174, 0.464770127945815, -0.114672154190115})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 8; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        }
    }


}
