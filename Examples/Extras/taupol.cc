////////////////////////////////////////////
//// Comparison of Tau parametrizations ////
////////////////////////////////////////////
#include "HepMCParser.hh"
#include "Hammer/Hammer.hh"
#include <iomanip>
#include <random>

using namespace std;

int main() {

    ////////////////////////////
    //// Run specifications ////
    ////////////////////////////
    auto begin = std::chrono::system_clock::now();
    auto fin = unique_ptr<ifstream>(new ifstream("./data/BCTauNu_Babar_PiPS.hepmc", std::ios::in));
    auto io = unique_ptr<HepMC::IO_GenEvent>(new HepMC::IO_GenEvent(*fin));
    if (io->rdstate() != 0) {
        return EXIT_FAILURE;
    }
    double pi = M_PI;
    //Outfile for Histos. More later!
    ofstream outFileH("./data/TauPolHistos.dat",ios::binary);
    Hammer::Hammer ham{};
    ham.includeDecay("BDTauNu");
    vector<string> include{"BD*TauNu","D*DPi"};
    ham.includeDecay(include);
    ham.includeDecay("TauPiNu");
    ham.addFFScheme("NP", {{"BD*", "BLPR"},{"BD", "BLPR"}});
    ham.setFFInputScheme({{"BD*", "ISGW2"},{"BD", "ISGW2"}});
    //Pure PS in denominator
    set<string> purePS{"BDTauNu", "BD*TauNu", "D*DPi"};
    ham.addPurePSVertices(purePS, Hammer::WTerm::DENOMINATOR);
    //Histograms
    vector<string> names = {"D", "Dstar"};
    for(auto& name : names){
        ham.addHistogram("Q2:"+name, {10}, false, {{0., 12.}});
        ham.addHistogram("CosTheta:"+name, {20}, false, {{-1., 1.}});
        ham.addHistogram("pPi:"+name, {10}, false, {{0., 2.5}});
//        ham.addHistogram("CosThetaVsTrue:"+name, {5,5}, false, {{-1.,1.},{-1.,1.}});
    }
    for(auto& name : names){
        ham.addHistogram("AccQ2:"+name, {10}, false, {{0., 12.}});
        ham.addHistogram("AccCosTheta:"+name, {20}, false, {{-1., 1.}});
        ham.addHistogram("AccpPi:"+name, {10}, false, {{0., 2.5}});
    }
    ham.addTotalSumOfWeights(); //adds "Total Sum of Weights" histo with auto bin filling
    ham.setUnits("GeV");
    ham.initRun();
    outFileH << ham.saveRunHeader();


    //Loop over events
    map<string, set<set<size_t>>> evtIds;
    size_t count = 0;
    HepMC::GenEvent ge;
    for(size_t i =0; i < 200000; ++i) {
        if (io->rdstate() == 0 && io->fill_next_event(&ge)) {
            if ((i + 1) % 10000 == 0) {
                cout<< "processing event " << i + 1 << endl;
            }
            set<size_t> evtId;
            bool first = true;
            bool Dflag = false;
            ham.initEvent();
            auto processes = parseGenEvent(ge, {521, -521, 511, -511});
            for (auto& proc : processes){
                auto procId = ham.addProcess(proc);
                if (procId == 0){ continue; } // Make sure process isn't forbidden
                evtId.insert(proc.getId());
                if(!first) { continue; }//Only allow one decay to be binned.

                Dflag = (proc.getVertexId("BDTauNu") != 0) ? true : false; //Either D or D* only in sample
                if (!Dflag &&  proc.getVertexId("BD*TauNu") == 0 ) { continue; }

                //Collect particles
                pair<Hammer::Particle, vector<Hammer::Particle>> BVer;
                pair<Hammer::Particle, vector<Hammer::Particle>> DsVer;
                if(Dflag){
                    BVer = proc.getParticlesByVertex("BDTauNu");
                } else {
                    BVer = proc.getParticlesByVertex("BD*TauNu");
                    DsVer = proc.getParticlesByVertex("D*DPi");
                }
                auto pB = BVer.first;
                auto pC = *find_if(BVer.second.begin(), BVer.second.end(), [&](Hammer::Particle& p)-> bool {return (abs(p.pdgId()) >= 400);});
                auto TVer = proc.getParticlesByVertex("TauPiNu");
                auto pTau = TVer.first;
                auto pPi = *find_if(TVer.second.begin(), TVer.second.end(), [&](Hammer::Particle& p)-> bool {return (abs(p.pdgId()) == 211);});

                //Construct observables
                auto Pq =  pB.p() - pC.p();
                double q2 = Pq.mass2();
                double PpiB = boostToRestFrameOf(pPi.p(), pB.p()).p();
                //Cosines. Tau and Pion to Q frame
                auto pTauQ = boostToRestFrameOf(pTau.p(), Pq);
                auto pPionQ = boostToRestFrameOf(pPi.p(), Pq);
                auto pPionT = boostToRestFrameOf(pPi.p(), pTau.p());

                double Etau = pTauQ.E();
                double Ptau = pTauQ.p();
                double Epi = pPionQ.E();
                double PpiQ = pPionQ.p();
                double PpiT = pPionT.p();
                double Mtau = pTau.p().mass();
                double Mpi = pPi.p().mass();
                double cosThetaTPi = (2.*Etau*Epi - Mtau*Mtau - Mpi*Mpi)/(2.*Ptau*PpiQ);
                double cosThetaHel = (-Ptau*Epi + Etau*PpiQ*cosThetaTPi)/(Mtau*PpiT);

                vector<Hammer::Particle> visDaughters{};
                //Cuts
                if (Dflag) {
                    visDaughters = {pC, pPi};
                } else {
                    visDaughters = DsVer.second;
                    visDaughters.push_back(pPi);
                }
                bool pass = true;
                default_random_engine generator;
                normal_distribution<double> gaussian(0.0,1.2);
                double q2smear = q2 + gaussian(generator);
                if (q2smear < 4.0) { pass = false; }
                if (pass) {
                    for (auto& vd : visDaughters){
                        auto Pvd = vd.p();
                        if (!(Pvd.theta() > 20.1/180.*pi &&  Pvd.theta() < 150.2/180.*pi)){
                            pass = false;
                            break;
                        }
                        if (vd.pdgId() == 211 && Pvd.E() < 0.2) {
                            pass = false;
                            break;
                        }
                    }
                }

                //Set Histo bins
                string name;
                name = (Dflag) ? "D" : "Dstar";
                ham.fillEventHistogram("Q2:"+name, {q2});
                ham.fillEventHistogram("CosTheta:"+name, {cosThetaHel});
                ham.fillEventHistogram("pPi:"+name, {PpiB});
                if (pass) {
                    ham.fillEventHistogram("AccQ2:"+name, {q2});
                    ham.fillEventHistogram("AccCosTheta:"+name, {cosThetaHel});
                    ham.fillEventHistogram("AccpPi:"+name, {PpiB});
                }

                first = false;
            }
            if (!first){ // Process & store events with at least one tau
                ham.processEvent();
                if (Dflag) {
                    evtIds["D"].insert(evtId);
                } else {
                    evtIds["Dstar"].insert(evtId);
                }
                ++count;
            }
        }
    }
    map<string, size_t> cnt;
    auto total = ham.getHistograms("Total Sum of Weights", "NP");

    //Save histos and count totals
    outFileH << ham.saveHistogram("Total Sum of Weights");
    for(auto& name : names){
        outFileH << ham.saveHistogram("Q2:"+name);
        outFileH << ham.saveHistogram("CosTheta:"+name);
        outFileH << ham.saveHistogram("pPi:"+name);
        outFileH << ham.saveHistogram("AccQ2:"+name);
        outFileH << ham.saveHistogram("AccCosTheta:"+name);
        outFileH << ham.saveHistogram("AccpPi:"+name);
        cnt[name] = 0;
        for (auto& evtId : evtIds[name]){
            auto it = total.find({evtId});
            if (it != total.end()) { cnt[name] += it->second[0].n; }
        }
    }
    outFileH.close();

    auto end = std::chrono::system_clock::now();
    auto dur = end - begin;
    typedef std::chrono::duration<float> float_seconds;
    auto secs = std::chrono::duration_cast<float_seconds>(dur);
    cout << "Histo Time: " << secs.count() << endl;
    cout << "Total Events binned: " << count << endl;

    cout << "B -> D Events: " << cnt["D"] << endl;
    cout << "B -> D* Events: " << cnt["Dstar"] << endl;

    cout << "B -> D Histograms: " << evtIds["D"].size() << endl;
    cout << "B -> D* Histograms: " << evtIds["Dstar"].size() << endl;

} // int main()
