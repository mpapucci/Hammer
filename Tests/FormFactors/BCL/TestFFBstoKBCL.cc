#include "gtest/gtest.h"
#include "Hammer/FormFactors/BCL/FFBstoKBCL.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBstoKBCL: public FFBstoKBCL {

    public:

        TestableFFBstoKBCL() : FFBstoKBCL() { }

        using FFBstoKBCL::evalAtPSPoint;
        using FFBstoKBCL::initSettings;

    };

    TEST(FFBstoKBCLTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs (q^2 = 19.8958 GeV^2)
        FourMomentum pBsmes{9269.42342515698, 0, 0, 7557.61348805621};
        FourMomentum pKmes{493.7, 0, 0, 0};
        FourMomentum kNuMu{691.247703926747, 143.20931144041, 440.75294019035, -512.885296157276};
        FourMomentum pMu{8084.47572123023, -143.20931144041, -440.75294019035, 8070.49878421349};

        //Evaluate the FF class
        TestableFFBstoKBCL ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        double sqtp = 5.280 + 0.139; //GeV t+ = (mB + mPi)^2 branch point
        set.changeSetting<double>("BstoKBCL","tp",sqtp*sqtp);
        ff.calcUnits();
        ff.eval(Particle(pBsmes, 511), {Particle(pKmes,-321), Particle(kNuMu,14), Particle(pMu,-13)}, {});

        // Compare to direct evaluation
        // Fs, Fz, Fp, Ft
        Tensor ffEval{"ffEval", MD::makeVector({4}, {FF_BSK},
                        {0.,
                         0.5601214962546,
                         1.4242702517186,
                         0.})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 1; idx1 < 3; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        }

    }

}
