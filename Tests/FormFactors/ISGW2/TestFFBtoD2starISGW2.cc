#include "gtest/gtest.h"
#include "Hammer/FormFactors/ISGW2/FFBtoD2starISGW2.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoD2starISGW2: public FFBtoD2starISGW2 {

    public:

        TestableFFBtoD2starISGW2() : FFBtoD2starISGW2() { }

        using FFBtoD2starISGW2::evalAtPSPoint;
        using FFBtoD2starISGW2::initSettings;

    };

    TEST(FFBtoD2starISGW2Test, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pBmes{6.0740724185305135e+00, 2.2041876529821600e-01, 2.6809423870568733e-01, 2.9832753503876139e+00};
        FourMomentum pD2starmes{2.8848738200870283e+00, -5.2810511065387389e-01, 1.0249552509378312e+00, 9.7358396705915939e-01};
        FourMomentum kNuTau{6.3239441837503607e-01, 2.3275878019939589e-01, 2.2488816988188981e-01, 5.4329675287006918e-01};
        FourMomentum pTau{2.5568041800684504e+00, 5.1576509575269402e-01, -9.8174918211403384e-01, 1.4663946304583855e+00};

        //Evaluate the FF class
        TestableFFBtoD2starISGW2 ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        //MeV momenta
        ff.eval(Particle(1000*pBmes, -511), {Particle(1000*pD2starmes,415), Particle(1000*kNuTau,-16), Particle(1000*pTau,15)}, {});

        // Compare to direct EvtGen evaluation (also in GeV)
        double hf = 0.0130539;
        double kf = 0.647262;
        double bppbm = 0.000378403;
        double bpmbm = -0.0237689;
        double mb = pBmes.mass(); //GeV
        double mx = pD2starmes.mass(); //GeV
        double sqmbmx = sqrt(mb*mx);
        double Ka1 = kf*mb/sqmbmx;
        double Ka2 = mb*mb*mb*bppbm/sqmbmx;
        double Ka3 = bpmbm*mb*sqmbmx;
        double Kv = 2*hf*mb*sqmbmx;
        Tensor ffEval{"ffEval", MD::makeVector({8}, {FF_BDSSD2STAR},
                                               {0, Ka1, Ka2, Ka3, Kv, 0, 0, 0})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 8; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        }
    }


}
