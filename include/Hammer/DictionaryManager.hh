///
/// @file  DictionaryManager.hh
/// @brief Global container class for amplitudes, rates, FFs, data
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_DICTIONARY_MANAGER
#define HAMMER_DICTIONARY_MANAGER

#include <vector>
#include <memory>

#include "Hammer/IndexTypes.hh"
#include "Hammer/Tools/Pdg.fhh"
#include "Hammer/Tools/HammerSerial.fhh"
#include "Hammer/Tools/SettingsConsumer.hh"

namespace YAML {

    class Node;

} // namespace YAML

namespace Hammer {

    // class RunDefinitions;
    class ProvidersRepo;
    class ExternalData;
    class ProcRates;
    class ProcessDefinitions;
    class SchemeDefinitions;
    class PurePhaseSpaceDefs;
    class Log;

    /// @brief Main class
    ///
    /// Contains ...
    ///
    /// @ingroup Core
    class DictionaryManager : public SettingsConsumer {

    public:

        DictionaryManager();

        DictionaryManager(const DictionaryManager& other) = delete;
        DictionaryManager& operator=(const DictionaryManager& other) = delete;
        DictionaryManager(DictionaryManager&& other) = delete;
        DictionaryManager& operator=(DictionaryManager&& other) = delete;

        virtual ~DictionaryManager() noexcept override;

    public:

        void init();

        virtual const ProvidersRepo& providers() const;

        virtual const ExternalData& externalData() const;
        virtual ExternalData& externalData();

        virtual const ProcRates& rates() const;
        virtual ProcRates& rates();

        virtual const ProcessDefinitions& processDefs() const;
        virtual ProcessDefinitions& processDefs();

        virtual const PurePhaseSpaceDefs& purePSDefs() const;
        virtual PurePhaseSpaceDefs& purePSDefs();

        virtual const SchemeDefinitions& schemeDefs() const;
        virtual SchemeDefinitions& schemeDefs();

        virtual void setSettingsHandler(SettingsHandler& sh) override;

    public:
        void write(flatbuffers::FlatBufferBuilder* msgwriter) const;

        bool read(const Serial::FBHeader* msgreader, bool merge);

    public:
        /// @brief read Hammer settings from a file
        /// @param[in] fileName  the file name
        void readDecays(const std::string& fileName);

        /// @brief read Hammer settings from a string
        /// @param[in] yamlData  the decay options
        void parseDecays(const std::string& yamlData);

        /// @brief write current Hammer settings to a file
        /// @param[in] fileName  the file name
        void saveDecays(const std::string& fileName);

    private:
        /// @brief
        /// @param[in] input
        void processDecays(const YAML::Node& input);

    private:
        /// @brief purely virtual function for a class to define new settings
        virtual void defineSettings() override;

        Log& getLog() const;

    private:
        std::unique_ptr<SchemeDefinitions> _schemeDefs;
        std::unique_ptr<ProvidersRepo> _providers;
        std::unique_ptr<ExternalData> _external;
        std::unique_ptr<ProcRates> _rates;
        std::unique_ptr<PurePhaseSpaceDefs> _purePSDefs;
        std::unique_ptr<ProcessDefinitions> _procDefs;
    };

} // namespace Hammer

#endif
