///
/// @file  FFLbtoLcstar12PCR.hh
/// @brief \f$ \Lambda_b \rightarrow Lambda_c^*(2595) \f$ PCR form factors
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_LBLCSTAR12PCR
#define HAMMER_FF_LBLCSTAR12PCR

#include "Hammer/FormFactors/PCR/FFPCRBase.hh"

namespace Hammer {

    class FFLbtoLcstar12PCR : public FFPCRBase {

    public:
        FFLbtoLcstar12PCR();

        FFLbtoLcstar12PCR(const FFLbtoLcstar12PCR& other) = default;
        FFLbtoLcstar12PCR& operator=(const FFLbtoLcstar12PCR& other) = delete;
        FFLbtoLcstar12PCR(FFLbtoLcstar12PCR&& other) = delete;
        FFLbtoLcstar12PCR& operator=(FFLbtoLcstar12PCR&& other) = delete;
        virtual ~FFLbtoLcstar12PCR() override = default;

    public:
        virtual void eval(const Particle& parent, const ParticleList& daughters,
                          const ParticleList& references) override;

        virtual std::unique_ptr<FormFactorBase> clone(const std::string& label) override;

    protected:
        virtual void evalAtPSPoint(const std::vector<double>& point, const std::vector<double>& masses = {}) override;

        virtual void defineSettings() override;
    private:
        mutable bool initialized;
    };

} // namespace Hammer

#endif
