///
/// @file  Process.hh
/// @brief Hammer process class
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_PROCESS_HH
#define HAMMER_PROCESS_HH

#include <map>
#include <string>
#include <vector>
#include <unordered_map>

#include "Hammer/IndexTypes.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/SettingsConsumer.hh"
#include "Hammer/Tools/HammerSerial.fhh"

namespace Hammer {

    class Log;

    /// @brief Decay process class
    ///
    /// Contains the amplitudes, weights and info associated to a decay, ...
    ///
    /// @ingroup Core
    class Process : public SettingsConsumer {

    public:
        Process();

        Process(const Process& other) = default;
        Process& operator=(const Process& other) = default;
        Process(Process&& other) = default;
        Process& operator=(Process&& other) = default;

        ~Process() noexcept override = default;

    public:

        using TreeMap = std::map<ParticleIndex, ParticleIndices>;
        using const_iterator = TreeMap::const_iterator;

        const_iterator begin() const;

        const_iterator end() const;

        const_iterator find(ParticleIndex particle) const;

    public:

        /// @brief
        /// @param[in] p
        /// @return
        ParticleIndex addParticle(const Particle& p);

        /// @brief
        /// @param[in] parent
        /// @param[in] daughters
        void addVertex(ParticleIndex parent, const ParticleIndices& daughters);

        /// @brief
        /// @param[in] parent
        /// @param[in] prune
        void removeVertex(ParticleIndex parent, bool prune = false);

        /// @brief
        /// @param[in] parent
        /// @return
        const ParticleIndices& getDaughtersIds(ParticleIndex parent = 0) const;

        /// @brief
        /// @param[in] daughter
        /// @return
        ParticleIndex getParentId(ParticleIndex daughter) const;

        /// @brief
        /// @param[in] particle
        /// @return
        ParticleIndices getSiblingsIds(ParticleIndex particle = 0) const;

        /// @brief
        /// @param[in] id
        /// @return
        const Particle& getParticle(ParticleIndex id) const;

        /// @brief
        /// @param[in] parent
        /// @param[in] sorted
        /// @return
        ParticleList getDaughters(ParticleIndex parent = 0, bool sorted = false) const;

        /// @brief
        /// @param[in] particle
        /// @param[in] sorted
        /// @return
        ParticleList getSiblings(ParticleIndex particle = 0, bool sorted = false) const;

        /// @brief
        /// @param[in] daughter
        /// @return
        const Particle& getParent(ParticleIndex daughter) const;

        bool isParent(ParticleIndex particle) const;

        ParticleIndex getFirstVertex() const;

        /// @brief
        /// @return
        HashId getId() const;

        const std::set<HashId>& fullId() const;

        /// @brief
        /// @param[in] parent
        /// @param[in] daughters
        /// @return
        std::pair<Particle, ParticleList> getParticlesByVertex(PdgId parent, std::vector<PdgId> daughters) const;

        /// @brief
        /// @param[in] vertex
        /// @return
        std::pair<Particle, ParticleList> getParticlesByVertex(std::string vertex) const;

        /// @brief
        /// @param[in] vertex
        /// @return
        HashId getVertexId(std::string vertex) const;

        bool initialize();

        void disable();

        /// @brief
        /// @param[in] msgwriter
        /// @param[in] msg
        void write(flatbuffers::FlatBufferBuilder* msgwriter, flatbuffers::Offset<Serial::FBProcIDs>* msg) const;

        /// @brief
        /// @param[in] msgreader
        void read(const Serial::FBProcIDs* msgreader);

        size_t numParticles(bool withoutPhotons = false) const;

    private:
        /// @brief purely virtual function for a class to define new settings
        virtual void defineSettings() override;

        /// @brief logging facility
        /// @return   stream to be used for logging
        Log& getLog() const;

        // ///@brief
        // ///@TODO: implement or remove
        // void expandSoftPhotonVertices();

        ///@brief
        void pruneSoftPhotons();

        /// @brief
        void calcSignatures();

        ParticleIndex findFirstVertex() const;
        void cacheParticleDependencies();

        ParticleList sortByParent(ParticleIndex parent, ParticleList parts) const;

    private:


        ParticleList _particles;
        TreeMap _processTree;

        ProcessUID _hashId;
        VertexUIDSet _fullId;
        ProcIdDict<ParticleIndex> _idTree;

        ParticleIndex _firstVertexIdx = std::numeric_limits<size_t>::max();
        std::unordered_map<ParticleIndex, ParticleIndex> _parentDict;
        UniqueParticleIndices _erasedGammas;
        bool _initialized = false;
    };

    inline ParticleIndex parentId(Process::const_iterator it) {
        return it->first;
    }

    inline const ParticleIndices& daughtersId(Process::const_iterator it) {
        return it->second;
    }

} // namespace Hammer

#endif
