#include "gtest/gtest.h"
#include "Hammer/FormFactors/ISGW2/FFBtoD1ISGW2.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoD1ISGW2: public FFBtoD1ISGW2 {

    public:

        TestableFFBtoD1ISGW2() : FFBtoD1ISGW2() { }

        using FFBtoD1ISGW2::evalAtPSPoint;
        using FFBtoD1ISGW2::initSettings;

    };

    TEST(FFBtoD1ISGW2Test, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pBmes{6.0740724185305135e+00, 2.2041876529821600e-01, 2.6809423870568733e-01, 2.9832753503876139e+00};
        FourMomentum pD1mes{2.8240005676506059e+00, 3.4765542738957983e-01, 1.1098136867676383e+00, 8.7571280698419929e-01};
        FourMomentum kNuTau{1.0517172449734653e+00, -2.6820877777053831e-01, -5.5264138293147658e-01, 8.5367483081849993e-01};
        FourMomentum pTau{2.1983546059064443e+00, 1.4097211567917448e-01, -2.8907806513047413e-01, 1.2538877125849155e+00};

        //Evaluate the FF class
        TestableFFBtoD1ISGW2 ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        set.changeSetting<bool>("BtoD**1ISGW2", "SmearQ2", false);
        //MeV momenta
        ff.eval(Particle(1000*pBmes, -511), {Particle(1000*pD1mes,10413), Particle(1000*kNuTau,-16), Particle(1000*pTau,15)}, {});

        // Compare to direct EvtGen evaluation (also in GeV)
        double vv = -0.062548;
        double rr = -0.991994;
        double sppsm = -0.0721888;
        double spmsm = -0.0421309;
        double mb = pBmes.mass(); //GeV
        double mx = pD1mes.mass(); //GeV
        double sqmbmx = sqrt(mb*mx);
        double Fv1 = rr/sqmbmx;
        double Fv2 = mb*mb*sppsm/sqmbmx;
        double Fv3 = spmsm*sqmbmx;
        double Fa = 2*vv*sqmbmx;
        Tensor ffEval{"ffEval", MD::makeVector({8}, {FF_BDSSD1},
                                               {0, Fv1, Fv2, Fv3, Fa, 0, 0, 0})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 8; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 2e-3); //Account for some EvtGen smearing
        }
    }


}
