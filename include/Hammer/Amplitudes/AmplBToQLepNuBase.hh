///
/// @file  AmplBToQLepNuBase.hh
/// @brief \f$ b -> c \tau\nu \f$ base amplitude
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_AMPL_BTOQLEPNU_BASE
#define HAMMER_AMPL_BTOQLEPNU_BASE

#include "Hammer/AmplitudeBase.hh"

namespace Hammer {

    class AmplBToQLepNuBase : public AmplitudeBase {

    public:
        AmplBToQLepNuBase();

        virtual ~AmplBToQLepNuBase() override {
        }

    public:

        virtual void preProcessWCValues(std::vector<std::complex<double>>& data) const override;

    protected:
        virtual void defineSettings() override;

        virtual void updateWilsonCeffLabelPrefix() override;

    private:

        std::vector<IndexType> _perms;
        std::vector<double> _flips;

    };

} // namespace Hammer

#endif
