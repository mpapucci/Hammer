#include "gtest/gtest.h"
#include "Hammer/FormFactors/Kiselev/FFBctoJpsiKiselev.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBctoJpsiKiselev: public FFBctoJpsiKiselev {

    public:

        TestableFFBctoJpsiKiselev() : FFBctoJpsiKiselev() { }

        using FFBctoJpsiKiselev::evalAtPSPoint;
        using FFBctoJpsiKiselev::initSettings;

    };

    TEST(FFBctoJpsiKiselevTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pBcmes{6275.,  0.,  0.,  0.};
        FourMomentum pJpsimes{3821.5809561753,  -857.35310207508,  0.,  -2069.83348677229};
        FourMomentum kNuTau{131.0024243058,  -0.827618332,  98.867858366606,  -85.9424039919};
        FourMomentum pTau{2322.4166195189,  858.1807204071,  -98.867858366606,  2155.7758907642};

        //Evaluate the FF class
        TestableFFBctoJpsiKiselev ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pBcmes, 541), {Particle(pJpsimes, 443), Particle(kNuTau,12), Particle(pTau,-11)}, {});
        //ff.evalAtPSPoint({Sqq}, {pBmes.mass(), pJpsimes.mass()});

        // Compare to direct evaluation
        // Fs, Ff, Fg, Fm, Fp, Fzt, Fmt, Fpt
        Tensor ffEval{"ffEval",
                      MD::makeVector({8}, {FF_BCJPSI},
                                        {0.,
                                        6206.49,
                                        0.000115714,
                                        0.000126234,
                                        -0.0000778442,
                                        0.,
                                        0.,
                                        0.})};

        auto& t = ff.getTensor();
        EXPECT_TRUE(t.isSameLabelShape(ffEval));
        for (IndexType idx1 = 1; idx1 < 5; idx1++) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        };
    }


}
