#include "Hammer/Math/FourMomentum.hh"
#include "gtest/gtest.h"


namespace Hammer {

    TEST(FourMomentumTest, AssignmentAccess) {
        FourMomentum v = FourMomentum::fromPtEtaPhiM(10.,1.5,0.,91.2);
        EXPECT_DOUBLE_EQ(v.pt(),10.);
        EXPECT_DOUBLE_EQ(v.eta(),1.5);
        EXPECT_DOUBLE_EQ(v.phi(),0.);
        EXPECT_DOUBLE_EQ(v.mass(),91.2);
        FourMomentum v2 = FourMomentum::fromEtaPhiME(1.5,0.,91.2,100.);
        EXPECT_DOUBLE_EQ(v2.E(),100.);
        EXPECT_DOUBLE_EQ(v2.eta(),1.5);
        EXPECT_DOUBLE_EQ(v2.phi(),0.);
        EXPECT_DOUBLE_EQ(v2.mass(),91.2);
        FourMomentum v3{{{20.,2.,3.,5.}}};
        EXPECT_DOUBLE_EQ(v3.E(),20.);
        EXPECT_DOUBLE_EQ(v3.px(),2.);
        EXPECT_DOUBLE_EQ(v3.py(),3.);
        EXPECT_DOUBLE_EQ(v3.pz(),5.);
        FourMomentum v4 = FourMomentum::fromPM(2.,3.,5.,91.2);
        EXPECT_DOUBLE_EQ(v4.px(),2.);
        EXPECT_DOUBLE_EQ(v4.py(),3.);
        EXPECT_DOUBLE_EQ(v4.pz(),5.);
        EXPECT_DOUBLE_EQ(v4.mass(),91.2);
        v4.setE(20.);
        EXPECT_EQ(v4.E(), 20.);
    }

    TEST(FourMomentumTest, Access) {
    }

    TEST(FourMomentumTest, Angles) {
    }

    TEST(FourMomentumTest, Boosts) {
    }

} // namespace Hammer
