///
/// @file  FFBLPRBase.hh
/// @brief Hammer base class for BLPR form factors
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_BLPR_BASE
#define HAMMER_FF_BLPR_BASE

#include<vector>
#include<array>
#include<functional>

#include "Hammer/FormFactorBase.hh"
#include "Hammer/FormFactors/AsCoeffs.hh"

namespace Hammer {

    /// @brief Base class for BLPR form factors
    ///
    /// @ingroup FormFactors
    class FFBLPRBase : public FF1to1Base {

    public:
        FFBLPRBase();

        FFBLPRBase(const FFBLPRBase& other) = default;
        FFBLPRBase& operator=(const FFBLPRBase& other) = delete;
        FFBLPRBase(FFBLPRBase&& other) = delete;
        FFBLPRBase& operator=(FFBLPRBase&& other) = delete;
        virtual ~FFBLPRBase() override = default;

    protected:
        /// @brief
        /// @param[in] point
        /// @param[in] masses
        /// @return
        virtual void evalAtPSPoint(const std::vector<double>& point, const std::vector<double>& masses = {}) override = 0;

        /// @brief
        virtual void defineSettings() override;


    protected:
        virtual void addRefs() const override;

        std::pair<double, double> Xi(double w) const; // numerator and denominator separately

        double ZofW(double w) const;

    private:

        void fillIWs();
        void fillLis();

    protected:

        AsCoeffs _asCorrections;

        enum IwLabel : size_t { ETA = 0, CHI2, CHI3 };

        std::array<std::function<double(double)>, 3> _IWs;
        std::array<std::function<double(double)>, 7> _Li;

        mutable double _a;
        mutable double _zBC;

        mutable double _Cv1z;
        mutable double _Cv2z;
        mutable double _Cv3z;
        mutable double _Cv1zp;
        mutable double _Cv2zp;
        mutable double _Cv3zp;
        mutable double _Cv1zpp;
        mutable double _Cv2zpp;
        mutable double _Cv3zpp;

    private:

        mutable bool _initCs = false;
        mutable double _w0;

    };

} // namespace Hammer

#endif
