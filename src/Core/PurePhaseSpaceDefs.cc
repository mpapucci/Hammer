///
/// @file  PurePhaseSpaceDefs.cc
/// @brief Container class for pure phase space vertices definitions
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#include <fstream>

#include <boost/algorithm/string.hpp>

#include "yaml-cpp/yaml.h"

#include "Hammer/Exceptions.hh"
#include "Hammer/PurePhaseSpaceDefs.hh"
#include "Hammer/Tools/HammerSerial.hh"
#include "Hammer/Tools/Pdg.hh"
#include "Hammer/Tools/Utils.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Tools/ParticleUtils.hh"

using namespace std;

namespace Hammer {


    Log& PurePhaseSpaceDefs::getLog() const {
        return Log::getLog("Hammer.PurePhaseSpaceDefs");
    }

    void PurePhaseSpaceDefs::write(flatbuffers::FlatBufferBuilder* msgwriter,
                                     vector<flatbuffers::Offset<Serial::FBPurePS>>* purePSs) const {
        purePSs->clear();
        auto numlabels = msgwriter->CreateVectorOfStrings(_purePhaseSpaceVerticesNames.numerator);
        vector<uint64_t> ids;
        ids.reserve(_purePhaseSpaceVertices.numerator.size());
        copy(_purePhaseSpaceVertices.numerator.begin(), _purePhaseSpaceVertices.numerator.end(), back_inserter(ids));
        auto numids = msgwriter->CreateVector(ids);
        Serial::FBPurePSBuilder numBuilder{*msgwriter};
        numBuilder.add_labels(numlabels);
        numBuilder.add_ids(numids);
        purePSs->push_back(numBuilder.Finish());
        auto denlabels = msgwriter->CreateVectorOfStrings(_purePhaseSpaceVerticesNames.denominator);
        ids.clear();
        ids.reserve(_purePhaseSpaceVertices.denominator.size());
        copy(_purePhaseSpaceVertices.denominator.begin(), _purePhaseSpaceVertices.denominator.end(), back_inserter(ids));
        auto denids = msgwriter->CreateVector(ids);
        Serial::FBPurePSBuilder denBuilder{*msgwriter};
        denBuilder.add_labels(denlabels);
        denBuilder.add_ids(denids);
        purePSs->push_back(denBuilder.Finish());
    }

    bool PurePhaseSpaceDefs::read(const Serial::FBHeader* msgreader, bool merge) {
        if(msgreader != nullptr) {
            auto pureps = msgreader->pureps();
            ASSERT(pureps->size() != 0);
            auto labels = pureps->Get(0)->labels();
            auto ids = pureps->Get(0)->ids();
            _purePhaseSpaceVerticesNames.numerator.clear();
            _purePhaseSpaceVertices.numerator.clear();
            if (!merge) {
                _purePhaseSpaceVerticesNames.numerator.reserve(labels->size());
                for (unsigned int i = 0; i < labels->size(); ++i) {
                    _purePhaseSpaceVerticesNames.numerator.push_back(labels->Get(i)->c_str());
                }
                for (unsigned int i = 0; i < ids->size(); ++i) {
                    _purePhaseSpaceVertices.numerator.insert(ids->Get(i));
                }
            }
            labels = pureps->Get(1)->labels();
            ids = pureps->Get(1)->ids();
            _purePhaseSpaceVerticesNames.denominator.clear();
            _purePhaseSpaceVertices.denominator.clear();
            if (!merge) {
                _purePhaseSpaceVerticesNames.denominator.reserve(labels->size());
                for (unsigned int i = 0; i < labels->size(); ++i) {
                    _purePhaseSpaceVerticesNames.denominator.push_back(labels->Get(i)->c_str());
                }
                for (unsigned int i = 0; i < ids->size(); ++i) {
                    _purePhaseSpaceVertices.denominator.insert(ids->Get(i));
                }
            }
            return true;
        }
        return false;
    }

    void PurePhaseSpaceDefs::clearPurePhaseSpaceVertices(WTerm what) {
        switch(what) {
        case WTerm::NUMERATOR:
            _purePhaseSpaceVerticesNames.numerator.clear();
            break;
        case WTerm::DENOMINATOR:
            _purePhaseSpaceVerticesNames.denominator.clear();
            break;
        case WTerm::COMMON:
            _purePhaseSpaceVerticesNames.numerator.clear();
            _purePhaseSpaceVerticesNames.denominator.clear();
            break;
        }
    }

    void PurePhaseSpaceDefs::addPurePhaseSpaceVertices(const set<string>& vertices, WTerm what) {
        switch (what) {
        case WTerm::NUMERATOR:
            _purePhaseSpaceVerticesNames.numerator.insert(_purePhaseSpaceVerticesNames.numerator.end(),
                                                          vertices.begin(), vertices.end());
            break;
        case WTerm::DENOMINATOR:
            _purePhaseSpaceVerticesNames.denominator.insert(_purePhaseSpaceVerticesNames.denominator.end(),
                                                            vertices.begin(), vertices.end());
            break;
        case WTerm::COMMON:
            _purePhaseSpaceVerticesNames.numerator.insert(_purePhaseSpaceVerticesNames.numerator.end(),
                                                          vertices.begin(), vertices.end());
            _purePhaseSpaceVerticesNames.denominator.insert(_purePhaseSpaceVerticesNames.denominator.end(),
                                                            vertices.begin(), vertices.end());
            break;
        }
    }

   void PurePhaseSpaceDefs::addPurePhaseSpaceVertex(const string& decay, WTerm what) {
        switch(what) {
        case WTerm::NUMERATOR:
            _purePhaseSpaceVerticesNames.numerator.push_back(decay);
            break;
        case WTerm::DENOMINATOR:
            _purePhaseSpaceVerticesNames.denominator.push_back(decay);
            break;
        case WTerm::COMMON:
            _purePhaseSpaceVerticesNames.numerator.push_back(decay);
            _purePhaseSpaceVerticesNames.denominator.push_back(decay);
            break;
        }
   }

    NumDenPair<set<HashId>> PurePhaseSpaceDefs::purePhaseSpaceVertices() const {
        NumDenPair<set<HashId>> result{{},{}};
        PID& pdg = PID::instance();
        for (auto elem : _purePhaseSpaceVerticesNames.numerator) {
            auto temp = pdg.expandToValidVertexUIDs(elem);
            result.numerator.insert(temp.begin(),temp.end());
        }
        for (auto elem : _purePhaseSpaceVerticesNames.denominator) {
            auto temp = pdg.expandToValidVertexUIDs(elem);
            result.denominator.insert(temp.begin(),temp.end());
        }
        return result;
    }

    NumDenPair<bool> PurePhaseSpaceDefs::isPurePhaseSpace(PdgId parent, const std::vector<PdgId>& daughters) const {
        auto tmp = combineDaughters(daughters, {});
        HashId id = processID(parent, tmp);
        auto itNum = _purePhaseSpaceVertices.numerator.find(id);
        auto itDen = _purePhaseSpaceVertices.denominator.find(id);
        bool foundnum = itNum != _purePhaseSpaceVertices.numerator.end();
        bool foundden = itDen != _purePhaseSpaceVertices.denominator.end();
        return NumDenPair<bool>{foundnum, foundden};
    }

    void PurePhaseSpaceDefs::init() {
        _purePhaseSpaceVertices = purePhaseSpaceVertices();
    }

    YAML::Emitter& operator<<(YAML::Emitter& out, const PurePhaseSpaceDefs& s) {
        out << YAML::convert<PurePhaseSpaceDefs>::encode(s);
        return out;
    }

} // namespace Hammer

namespace YAML {

    Node convert<::Hammer::PurePhaseSpaceDefs>::encode(const ::Hammer::PurePhaseSpaceDefs& value) {
        Node node;
        if (value._purePhaseSpaceVerticesNames.numerator.size() > 0) {
            Node tmpNode;
            for (auto& elem : value._purePhaseSpaceVerticesNames.numerator) {
                tmpNode.push_back(elem);
            }
            node["Numerator"] = tmpNode;
        }
        if (value._purePhaseSpaceVerticesNames.denominator.size() > 0) {
            Node tmpNode;
            for (auto& elem : value._purePhaseSpaceVerticesNames.denominator) {
                tmpNode.push_back(elem);
            }
            node["Denominator"] = tmpNode;
        }
        return node;
    }

    bool convert<::Hammer::PurePhaseSpaceDefs>::decode(const Node& node, ::Hammer::PurePhaseSpaceDefs& value) {
        if (node.IsMap()) {
            for (auto entry2 : node) {
                string what = entry2.first.as<string>();
                if (what == "Numerator") {
                    value._purePhaseSpaceVerticesNames.numerator.clear();
                    YAML::Node processes = entry2.second;
                    if(processes.IsSequence()) {
                        for (auto process : processes) {
                            value._purePhaseSpaceVerticesNames.numerator.push_back(process.as<string>());
                        }
                    } else {
                        return false;
                    }
                } else if (what == "Denominator") {
                    value._purePhaseSpaceVerticesNames.denominator.clear();
                    YAML::Node processes = entry2.second;
                    if(processes.IsSequence()) {
                        for (auto process : processes) {
                            value._purePhaseSpaceVerticesNames.denominator.push_back(process.as<string>());
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }
}
