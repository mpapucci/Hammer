///
/// @file  FFBtoD2starBLR.hh
/// @brief \f$ B \rightarrow D_2^* \f$ BLR form factors
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_BDSSD2STARBLR
#define HAMMER_FF_BDSSD2STARBLR

#include "Hammer/FormFactors/BLR/FFBLRBase.hh"

namespace Hammer {

    class FFBtoD2starBLR : public FFBLRBase {

    public:
        FFBtoD2starBLR();

        FFBtoD2starBLR(const FFBtoD2starBLR& other) = default;
        FFBtoD2starBLR& operator=(const FFBtoD2starBLR& other) = delete;
        FFBtoD2starBLR(FFBtoD2starBLR&& other) = delete;
        FFBtoD2starBLR& operator=(FFBtoD2starBLR&& other) = delete;
        virtual ~FFBtoD2starBLR() override = default;

    public:

        virtual std::unique_ptr<FormFactorBase> clone(const std::string& label) override;

    protected:
        virtual void evalAtPSPoint(const std::vector<double>& point, const std::vector<double>& masses = {}) override;

        virtual void defineSettings() override;
    private:
        mutable bool initialized;
    };

} // namespace Hammer

#endif
