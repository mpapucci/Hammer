///
/// @file  IContainer.hh
/// @brief Interface class for tensor container data structure
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_MATH_IMULTICONTAINER
#define HAMMER_MATH_IMULTICONTAINER

#include <complex>

#include "Hammer/Math/MultiDimensional.fhh"
#include "Hammer/Tools/HammerSerial.fhh"

namespace Hammer {

    namespace MultiDimensional {

        class IContainer {

        public:
            virtual ~IContainer() {}
            IContainer() {}
            IContainer(const IContainer&) = default;
            IContainer(IContainer&&) = default;
            IContainer&  operator=(const IContainer&) = default;
            IContainer& operator=(IContainer&&) = default;

            using ElementType = std::complex<double>;
            using reference = ElementType&;
            using const_reference = const ElementType&;

            virtual size_t rank() const = 0;
            virtual IndexList dims() const = 0;
            virtual LabelsList labels() const = 0;
            virtual size_t numValues() const = 0;
            virtual size_t dataSize() const = 0;
            virtual size_t entrySize() const = 0;
            virtual IndexType labelToIndex(IndexLabel label) const = 0;

            virtual IndexPairList getSameLabelPairs(const IContainer& other,
                                                    const UniqueLabelsList& indices) const = 0;
            virtual IndexPairList getSpinLabelPairs() const = 0;

            virtual bool isSameShape(const IContainer& other) const = 0;
            virtual bool canAddAt(const IContainer& subContainer, IndexLabel coord, IndexType position) const = 0;

            virtual reference element(const IndexList& coords = {}) = 0;
            virtual ElementType element(const IndexList& coords = {}) const = 0;

            virtual reference element(IndexList::const_iterator start, IndexList::const_iterator end) = 0;
            virtual ElementType element(IndexList::const_iterator start, IndexList::const_iterator end) const = 0;

            virtual bool compare(const IContainer& other) const = 0;
            virtual TensorData clone() const = 0;
            virtual void clear() = 0;

            virtual IContainer& operator*=(double value) = 0;
            virtual IContainer& operator*=(const ElementType value) = 0;

            virtual IContainer& conjugate() = 0;

            virtual bool hasNaNs() const = 0;

            using SerialType = std::pair<flatbuffers::Offset<void>, Serial::FBTensorTypes>;

            virtual SerialType write(flatbuffers::FlatBufferBuilder* msgwriter) const = 0;
        };

    }

}

#endif
