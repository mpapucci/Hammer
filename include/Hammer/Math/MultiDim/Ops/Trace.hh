///
/// @file  Trace.hh
/// @brief Tensor trace algorithm
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_MATH_MULTIDIM_OPS_TRACE
#define HAMMER_MATH_MULTIDIM_OPS_TRACE

#include "Hammer/Math/MultiDimensional.fhh"

namespace Hammer {


    namespace MultiDimensional {

        class IContainer;
        class VectorContainer;
        class SparseContainer;
        class OuterContainer;

        namespace Ops {

            class Trace final {
            public:
                Trace(const IndexPairList& indices);

                IContainer* operator()(VectorContainer& first);
                IContainer* operator()(SparseContainer& first);
                IContainer* operator()(OuterContainer& first);

                IContainer* error(IContainer&);

            private:
                std::pair<IndexList, LabelsList> getNewIndexLabels(const IContainer& original) const;
                IndexList reducedIndex(const IndexList& a) const;

                IndexPairList _indices;
                UniqueIndexList _idxSet;
            };

        } // namespace Ops

    } // namespace MultiDimensional

} // namespace Hammer


#endif
