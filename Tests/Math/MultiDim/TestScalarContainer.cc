#include "Hammer/Math/MultiDim/ScalarContainer.hh"
#include "Hammer/Exceptions.hh"

#include "gtest/gtest.h"

using namespace std;

namespace Hammer {

    namespace MultiDimensional {

        TEST(ScalarTest, ConstructionAccess) {
            auto t = makeEmptyScalar();
            EXPECT_EQ(t->rank(), 0);
        }

        TEST(ScalarTest, ElementAccess) {
            auto t = makeEmptyScalar();
            t->element({}) = 0.1;
            EXPECT_THROW(t->element({1, 2, 1}), RangeError);
            EXPECT_DOUBLE_EQ(t->element({}).real(), 0.1);
        }

        TEST(ScalarTest, Comparison) {
            auto t1 = makeEmptyScalar();
            auto t2 = makeEmptyScalar();
            EXPECT_TRUE(t1->compare(*t2));
            t1->element({}) = 0.3;
            EXPECT_FALSE(t1->compare(*t2));
        }

        TEST(ScalarTest, ComponentMultiply) {
            auto t = makeEmptyScalar();
            t->element({}) = 0.1;
            EXPECT_DOUBLE_EQ(t->element({}).real(), 0.1);
            (*t) *= 3.;
            EXPECT_DOUBLE_EQ(t->element({}).real(), 0.3);
        }

        TEST(ScalarTest, Cloning) {
            auto t = makeEmptyScalar();
            t->element({}) = 0.1;
            auto t2 = t->clone();
            EXPECT_EQ(t->rank(), t2->rank());
            EXPECT_DOUBLE_EQ(t->element({}).real(), t2->element({}).real());
            EXPECT_DOUBLE_EQ(t->element({}).imag(), t2->element({}).imag());
        }

        TEST(ScalarTest, Conjugate) {
            auto t = makeEmptyScalar();
            t->element({}) = 1.0 + 0.2i;
            t->conjugate();
            EXPECT_DOUBLE_EQ(t->element({}).real(), 1.0);
            EXPECT_DOUBLE_EQ(t->element({}).imag(), -0.2);
        }


    } // namespace MultiDimensional

} // namespace Hammer
