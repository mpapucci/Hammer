///
/// @file  FFBtoDstarCLNVar.hh
/// @brief \f$ B \rightarrow D^* \f$ CLN form factors with variations
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_BDSTARCLNVAR
#define HAMMER_FF_BDSTARCLNVAR

#include "Hammer/FormFactors/CLN/FFCLNBase.hh"

namespace Hammer {

    class FFBtoDstarCLNVar : public FFCLNBase {

    public:
        FFBtoDstarCLNVar();

        FFBtoDstarCLNVar(const FFBtoDstarCLNVar& other) = default;
        FFBtoDstarCLNVar& operator=(const FFBtoDstarCLNVar& other) = delete;
        FFBtoDstarCLNVar(FFBtoDstarCLNVar&& other) = delete;
        FFBtoDstarCLNVar& operator=(FFBtoDstarCLNVar&& other) = delete;
        virtual ~FFBtoDstarCLNVar() override = default;

    public:

        virtual std::unique_ptr<FormFactorBase> clone(const std::string& label) override;

    protected:
        virtual void evalAtPSPoint(const std::vector<double>& point, const std::vector<double>& masses = {}) override;

        virtual void defineSettings() override;
    private:
        mutable bool initialized;
    };

} // namespace Hammer

#endif
