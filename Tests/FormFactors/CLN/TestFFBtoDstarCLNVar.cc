#include "gtest/gtest.h"
#include "Hammer/FormFactors/CLN/FFBtoDstarCLNVar.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoDstarCLNVar : public FFBtoDstarCLNVar {

    public:

        TestableFFBtoDstarCLNVar() : FFBtoDstarCLNVar() { }

        using FFBtoDstarCLNVar::evalAtPSPoint;
        using FFBtoDstarCLNVar::initSettings;
    };

    TEST(FFBtoDstarCLNVarTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pBmes{5280.,   0.,   0.,   0.};
        FourMomentum pDstarmes{2258.9451,   -542.95362175877925474, -718.43449427445361377, 513.65086086722812801};
        FourMomentum kNuTau{763.45512706233784082,  59.818100429037400761,  266.26625745054465575,  713.01318784585764875};
        FourMomentum pTau{2257.5997729376621592,    483.13552132974185397,  452.16823682390895801,  -1226.6640487130857768};

        //Evaluate the FF class
        TestableFFBtoDstarCLNVar ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pBmes, 511), {Particle(pDstarmes,-413), Particle(kNuTau,16), Particle(pTau,-15)}, {});


        //Compare to direct evaluation
        Tensor ffEval{"ffEval", MD::makeVector({8,5}, {FF_BDSTAR,FF_BDSTAR_VAR},
                        {0.,
                         0.,
                         0.,
                         0.,
                         0.,
                         5426.57,
                         -692.866,
                         0.,
                         0.,
                         0.,
                         0.000167046,
                         -0.0000213285,
                         0.000120461,
                         0.,
                         0.,
                         0.000132622,
                         -0.0000169331,
                         0.,
                         0.000356811,
                         0.000437592,
                         -0.000104423,
                         0.0000133328,
                         0.,
                         -0.000120461,
                         0.,
                         0.,
                         0.,
                         0.,
                         0.,
                         0.,
                         0.,
                         0.,
                         0.,
                         0.,
                         0.,
                         0.,
                         0.,
                         0.,
                         0.,
                         0.})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 1; idx1 < 5; ++idx1) {
            for (IndexType idx2 = 0; idx2 < 5; ++idx2) {
                double comRe = compareVals(t.element({idx1,idx2}).real(),ffEval.element({idx1,idx2}).real());
                EXPECT_NEAR(comRe, 1., 1e-4);
            }
        }
    }


}
