#include "gtest/gtest.h"
#include "Hammer/FormFactors/BLRSXP/FFLbtoLcBLRSXP.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFLbtoLcBLRSXP: public FFLbtoLcBLRSXP {

    public:

        TestableFFLbtoLcBLRSXP() : FFLbtoLcBLRSXP() { }

        using FFLbtoLcBLRSXP::initSettings;
        using FFLbtoLcBLRSXP::evalAtPSPoint;
    };

    TEST(FFLbtoLcBLRSXPTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pLbmes{5620., 0., 0., 0.};
        FourMomentum pLcmes{2514.6000000000004,380.2511160960518,-878.708483534587,-425.0854616098454};
        FourMomentum pTau{2456.13415321897,-581.6942711749232,1418.910848905593,723.6624190525239};
        FourMomentum kNuTau{649.2658467810297,201.44315507887137,-540.2023653710064,-298.5769574426786};

        //Evaluate the FF class
        TestableFFLbtoLcBLRSXP ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pLbmes, -5122), {Particle(pLcmes, -4122), Particle(kNuTau, 16), Particle(pTau, -15)}, {});

        //Compare to direct evaluation
        Tensor ffEval{"ffEval", MD::makeVector({12}, {FF_LBLC},
                                {0.744050262639,
                                 1.18511593540,
                                 1.21407897674,
                                 -0.301188694543,
                                 -0.0826568041477,
                                 0.763812946280,
                                 -0.364472049319,
                                 0.104855753419,
                                 0.830515836674,
                                 -0.352357783478,
                                 0.1066387974459,
                                 0.00453549559603})};


        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 12; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 2e-4);
        }
    }


}
