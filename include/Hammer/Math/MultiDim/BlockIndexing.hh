///
/// @file  BlockIndexing.hh
/// @brief Outer product tensor indexer
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_MATH_MULTIDIM_BLOCKINDEXING
#define HAMMER_MATH_MULTIDIM_BLOCKINDEXING

#include <vector>

#include "Hammer/Math/MultiDimensional.fhh"
#include "Hammer/Math/MultiDim/AlignedIndexing.hh"
#include "Hammer/Math/MultiDim/LabeledIndexing.hh"
#include "Hammer/Math/MultiDim/ISingleContainer.hh"

namespace Hammer {


    namespace MultiDimensional {

        class OuterElemIterator {
        public:
            using EntryType = std::vector<std::pair<SharedTensorData, bool>>;

            OuterElemIterator(const EntryType& entry);

            OuterElemIterator& operator=(const OuterElemIterator&) = delete;
            OuterElemIterator& operator=(OuterElemIterator&&) = delete;
            OuterElemIterator(OuterElemIterator&& other) = default;

        private:
            OuterElemIterator(const OuterElemIterator& other);

        public:
            OuterElemIterator begin() const;

            OuterElemIterator end() const;

            OuterElemIterator& operator++();

            OuterElemIterator operator++(int n);

            IContainer::ElementType operator*();
            PositionType position(IndexType idx) const;
            bool isAligned(IndexType idx) const;

            bool isSame(const OuterElemIterator& other) const;


        private:
            void incrementEntry(size_t position, int n);
            void setInitialState();
            void setFinalState();

            using IteratorList = std::vector<ISingleContainer::NonZeroIt>;
            using ContainerList = std::vector<ISingleContainer*>;

            const EntryType& _entry;
            ContainerList _containers;
            IteratorList _it;
            IndexList _dimensions;
        };

        inline bool operator==(const OuterElemIterator& lhs,
                               const OuterElemIterator& rhs) {
            return lhs.isSame(rhs);
        }

        inline bool operator!=(const OuterElemIterator& lhs,
                               const OuterElemIterator& rhs) {
            return !(lhs == rhs);
        }

        using DotGroupType = std::tuple<IndexList, IndexList, IndexPairList>;
        using DotGroupList = std::vector<DotGroupType>;

        class BlockIndexing {
        public:
            BlockIndexing();
            BlockIndexing(const std::vector<IndexList>& dims, const std::vector<LabelsList>& labels);
            BlockIndexing(LabeledIndexing<AlignedIndexing> left, LabeledIndexing<AlignedIndexing> right);

            BlockIndexing(const BlockIndexing&) = default;
            BlockIndexing(BlockIndexing&&) = default;
            BlockIndexing& operator=(const BlockIndexing&) = default;
            BlockIndexing& operator=(BlockIndexing&&) = default;

            ~BlockIndexing() = default;

            /// @brief  rank of the tensor
            /// @return the number of components
            size_t rank() const;

            /// @brief  dimension of a specific component
            /// @param[in] index   the component index
            /// @return  the dimension
            IndexType dim(IndexType index) const;

            /// @brief  dimension of a specific component by label.
            ///         If multiple components with the same label exists,
            ///         returns the dimension of first (they should all be the same
            ///         by consistency)
            /// @param[in] label   the component label
            /// @return  the dimension
            IndexType dim(IndexLabel label) const;

            /// @brief   get the labels of all the indices at once
            /// @return  the list of labels
            LabelsList labels() const;

            /// @brief  returns only the labels corresponding to spin indices
            /// @return   the set of labels of the spin indices (squashes repetitions if present)
            UniqueLabelsList spinIndices() const;

            /// @brief  get the dimensions of all the indices at once
            /// @return  the list of dimensions
            IndexList dims() const;

        public:
            /// @brief  the number of elements (product of all the dimensions)
            PositionType numValues() const;

            std::vector<IndexList> splitIndices(const IndexList& indices) const;

            std::vector<IndexList::const_iterator> splitIndices(IndexList::const_iterator first, IndexList::const_iterator last) const;

            /// @brief  check that the indices are within range for each component
            /// @param[in] indices the element indices
            /// @return true if all indices are OK
            bool checkValidIndices(const IndexList& indices) const;

            bool checkValidIndices(IndexList::const_iterator first, IndexList::const_iterator last) const;

            bool checkValidIndices(const std::vector<IndexList>& splits) const;

            /// @brief  returns the position of the indices in the two tensor (this and another) that can be
            ///         contracted together, given a set of allowed index labels
            /// @param[in] otherLabels the list of labels of the other tensor
            /// @param[in] indices  the list of labels of the allowed indices to be contracted
            /// @param[in] sortedBySecond whether the result should be sorted according to the second element of the
            /// pair
            /// @return pairs of indices corresponding to coordinates in this and other tensor that needs to be
            /// contracted together
            IndexPairList getSameLabelPairs(const LabelsList& otherLabels, const UniqueLabelsList& indices,
                                            bool sortedBySecond = true) const;

            /// @brief  returns the position of the indices that can be traced together, given a set of allowed index
            ///         labels
            /// @param[in] indices  the list of labels of the allowed indices to be traced
            /// @return pairs of coordinate indices that needs to be traced together
            IndexPairList getOppositeLabelPairs(const UniqueLabelsList& indices) const;

            IndexType labelIndex(IndexLabel label) const;

            bool isSameLabelShape(const LabelsList& otherLabels, const IndexList& otherIndices) const;

            template <typename S>
            bool isSameLabelShape(const LabeledIndexing<S>& other) const;

            bool isSameLabelShape(const BlockIndexing& other, bool includeBlockShapes = false) const;

            void flipLabels();

            IndexPair getElementIndex(IndexType position) const;

            const LabeledIndexing<AlignedIndexing>& getSubIndexing(IndexType position) const;

            size_t numSubIndexing() const;

            size_t maxSubRank() const;

            std::vector<std::tuple<IndexList, std::vector<bool>, PositionType>>
            processShifts(const DotGroupList& chunks, IndexPairMember which) const;

            PositionType splitPosition(const OuterElemIterator& currentPosition, const DotGroupType& chunk,
                                       const IndexList& outerShiftsInnerPositions, const std::vector<bool>& isOuter,
                                       IndexList& innerList, std::vector<bool>& innerAdded,
                                       bool shouldCompare = false) const;

            PositionType buildFullPosition(const OuterElemIterator& current, const IndexList& chunkIndices) const;

        private:
            void calc();

            std::vector<LabeledIndexing<AlignedIndexing>> _subIndexing;
            std::vector<IndexType> _splitIndices;
            std::vector<PositionType> _splitPads;
            LabeledIndexing<AlignedIndexing> _globalIndexing;

        };

        template <typename S>
        bool BlockIndexing::isSameLabelShape(const LabeledIndexing<S>& other) const {
            return isSameLabelShape(other.labels(), other.dims());
        }

    } // namespace MultiDimensional

} // namespace Hammer

#endif
