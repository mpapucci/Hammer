#include "gtest/gtest.h"
#include "Hammer/FormFactors/ISGW2/FFBtoDISGW2.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoDISGW2: public FFBtoDISGW2 {

    public:

        TestableFFBtoDISGW2() : FFBtoDISGW2() { }

        using FFBtoDISGW2::evalAtPSPoint;
        using FFBtoDISGW2::initSettings;

    };

    TEST(FFBtoDISGW2Test, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pBmes{5280., 0., 0., 0.};
        FourMomentum pDmes{2381.3371929065643, 0., 0., -1475.671313782346};
        FourMomentum pTau{2034.777874233763, 553.6990600712329, -92.91177555543321, 819.1030156888944};
        FourMomentum kNuTau{863.8849328596731, -553.6990600712329, 92.91177555543321, 656.5682980934517};


        //Evaluate the FF class
        TestableFFBtoDISGW2 ff;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
//        TestableTensor FFres;
//        dynamic_cast<Tensor&>(FFres)=ff.evalAtPSPoint({7.6762},{5.280,1.86961});
        //Switched to eval on momenta (q^2 = 6.22464 GeV^2)
        ff.eval(Particle(pBmes, 511), {Particle(pDmes,-411), Particle(kNuTau,16), Particle(pTau,-15)}, {});

        // Compare to direct evaluation
        // The calculated form factors at t=7.6762 are f+=1.03227 f0=0.862739
        // Fs, Fz, Fp, Ft
        Tensor ffEval{"ffEval", MD::makeVector({4}, {FF_BD},
                                                {0,0.856145172884, 0.98763713683,0})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 1; idx1 < 3; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        }
    }


}
