#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation-unknown-command"
///
/// @file  LabeledIndexingDefs.hh
/// @brief Tensor labeled indexer template methods definitions
///
#pragma clang diagnostic pop

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-

#include <utility>
#include <algorithm>

#include "Hammer/Exceptions.hh"
#include "Hammer/IndexLabels.hh"
#include "Hammer/Math/MultiDim/IContainer.hh"

namespace Hammer {

    namespace MultiDimensional {

        template<class BasicIndexing>
        IndexType LabeledIndexing<BasicIndexing>::dim(IndexLabel label) const {
            ptrdiff_t pos = - distance(find(_labels.begin(), _labels.end(), label), _labels.begin());
            ptrdiff_t dim = static_cast<ptrdiff_t>(this->rank());
            if (pos < dim && pos >= 0) {
                return this->dim(static_cast<IndexType>(pos));
            }
            throw IndexLabelError("Label " + std::to_string(label) + " not found for multidimensional object" +
                                "': all your base are belong to us.");
        }

        template<class BasicIndexing>
        const LabelsList& LabeledIndexing<BasicIndexing>::labels() const {
            return _labels;
        }

        template<class BasicIndexing>
        UniqueLabelsList LabeledIndexing<BasicIndexing>::spinIndices() const {
            UniqueLabelsList result;
            for(auto elem: _labels) {
                if(abs(elem) > IndexLabel::SPIN_INDEX_START && abs(elem) < IndexLabel::SPIN_INDEX_END) {
                    result.insert(elem);
                }
            }
            return result;
        }

        template <class BasicIndexing>
        LabeledIndexing<BasicIndexing>::LabeledIndexing() : BasicIndexing{}, _labels{} {

        }

        template <class BasicIndexing>
        LabeledIndexing<BasicIndexing>::LabeledIndexing(IndexList dimensions, LabelsList labels) : BasicIndexing{dimensions}, _labels{labels} {
            if (labels.size() != dimensions.size()) {
                throw IndexLabelError("Length of list of labels is different than length of dimensions: " +
                                      std::to_string(labels.size()) + " vs " + std::to_string(dimensions.size()) +
                                      " -> all your base are belong to us.");
            }
        }

        template <class BasicIndexing>
        UniqueLabelsList LabeledIndexing<BasicIndexing>::sameIndices(const LabelsList& otherLabels) const {
            LabelsList tmp1 = _labels;
            LabelsList tmp2 = otherLabels;
            std::sort(tmp1.begin(), tmp1.end());
            std::sort(tmp2.begin(), tmp2.end());
            UniqueLabelsList result;
            std::set_intersection(tmp1.begin(), tmp1.end(), tmp2.begin(), tmp2.end(), inserter(result, result.begin()));
            return result;
        }

        template<class BasicIndexing>
        LabelPairsSet LabeledIndexing<BasicIndexing>::oppositeIndices() const {
            UniqueLabelsList tmp;
            for (auto elem : _labels) {
                tmp.insert(elem);
            }
            auto res = getOppositeLabelPairs(tmp);
            LabelPairsSet out;
            for (auto elem : res) {
                out.insert({_labels[elem.first], _labels[elem.second]});
            }
            return out;
        }

        template <class BasicIndexing>
        IndexPairList LabeledIndexing<BasicIndexing>::getSameLabelPairs(const LabelsList& otherLabels,
                                                                        const UniqueLabelsList& indices,
                                                                        bool sortedBySecond) const {
            IndexPairList result;
            for (auto elem : indices) {
                ptrdiff_t dim = static_cast<ptrdiff_t>(_labels.size());
                ptrdiff_t otherdim = static_cast<ptrdiff_t>(otherLabels.size());
                auto it = _labels.begin();
                auto ito = otherLabels.begin();
                while(it != _labels.end() && ito != otherLabels.end()) {
                    auto it2 = find(it, _labels.end(), elem);
                    auto ito2 = find(ito, otherLabels.end(), elem);
                    ptrdiff_t pos1 = distance(_labels.begin(), it2);
                    ptrdiff_t pos2 = distance(otherLabels.begin(), ito2);
                    if (pos1 < dim && pos2 < otherdim) {
                        result.push_back(std::make_pair(pos1, pos2));
                    }
                    it = it2;
                    ito = ito2;
                    if(it != _labels.end()) {
                        ++it;
                    }
                    if (ito != otherLabels.end()) {
                        ++ito;
                    }
                }
            }
            if(sortedBySecond) {
                std::sort(result.begin(), result.end(), [](const IndexPair& a, const IndexPair& b) -> bool { return a.second < b.second; });
            }
            return result;
        }

        template<class BasicIndexing>
        IndexPairList LabeledIndexing<BasicIndexing>::getOppositeLabelPairs(const UniqueLabelsList& indices) const {
            IndexPairList result;
            for (auto elem : indices) {
                if (elem > 0) {
                    IndexLabel other_elem = static_cast<IndexLabel>(-elem);
                    ptrdiff_t pos1 = - distance(find(_labels.begin(), _labels.end(), elem), _labels.begin());
                    ptrdiff_t pos2 = - distance(find(_labels.begin(), _labels.end(), other_elem), _labels.begin());
                    ptrdiff_t dim = static_cast<ptrdiff_t>(_labels.size());
                    if (pos1 < dim && pos2 < dim) {
                        result.push_back(std::make_pair(pos1, pos2));
                    }
                }
            }
            return result;
        }

        template <class BasicIndexing>
        bool LabeledIndexing<BasicIndexing>::isSameLabelShape(const LabelsList& otherLabels,
                                                              const IndexList& otherIndices) const {
            bool result = this->isSameShape(otherIndices);
            if(!result) return false;
            result &= equal(_labels.begin(), _labels.end(), otherLabels.begin());
            return result;
        }

        template <class BasicIndexing>
        template <typename S>
        bool LabeledIndexing<BasicIndexing>::isSameLabelShape(const LabeledIndexing<S>& other) const {
            bool result = this->isSameShape(other);
            if(!result) return false;
            result &= equal(_labels.begin(), _labels.end(), other._labels.begin());
            return result;
        }

        template <class BasicIndexing>
        IndexType LabeledIndexing<BasicIndexing>::labelIndex(IndexLabel label) const {
            auto itfind = find(_labels.begin(), _labels.end(), label);
            return static_cast<IndexType>(distance(_labels.begin(), itfind));
        }


        template <class BasicIndexing>
        bool LabeledIndexing<BasicIndexing>::canAddAt(const LabelsList& otherLabels, const IndexList& otherIndices,
                                                      IndexLabel coord, IndexType position) const {
            auto pos = labelIndex(coord);
            if (pos >= this->rank())
                return false;
            if (this->dims()[pos] <= position)
                return false;
            auto tmpLabs = _labels;
            tmpLabs.erase(tmpLabs.begin() + pos);
            if(!(tmpLabs == otherLabels)) return false;
            auto tmpDims = this->dims();
            tmpDims.erase(tmpDims.begin() + pos);
            return (tmpDims == otherIndices);
        }

        template <class BasicIndexing>
        void LabeledIndexing<BasicIndexing>::flipLabels() {
            _labels = flipListOfLabels(_labels);
        }

        inline LabelsList flipListOfLabels(LabelsList labels) {
            transform(labels.begin(), labels.end(), labels.begin(),
                      [](IndexLabel l) -> IndexLabel { return static_cast<IndexLabel>(-l); });
            return labels;
        }

    } // namespace MultiDimensional

} // namespace Hammer
