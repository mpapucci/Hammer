///
/// @file  FFBLRSBase.hh
/// @brief Hammer base class for BLRS form factors
//

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_BLRS_BASE
#define HAMMER_FF_BLRS_BASE

#include "Hammer/FormFactorBase.hh"
#include "Hammer/FormFactors/AsCoeffs.hh"

namespace Hammer {

    /// @brief Base class for BLR form factors
    ///
    /// @ingroup FormFactors
    class FFBLRSBase : public FF1to1Base {

    public:
        FFBLRSBase();

        FFBLRSBase(const FFBLRSBase& other) = default;
        FFBLRSBase& operator=(const FFBLRSBase& other) = delete;
        FFBLRSBase(FFBLRSBase&& other) = delete;
        FFBLRSBase& operator=(FFBLRSBase&& other) = delete;
        virtual ~FFBLRSBase() override = default;

    protected:
        /// @brief
        /// @param[in] point
        /// @param[in] masses
        /// @return
        virtual void evalAtPSPoint(const std::vector<double>& point, const std::vector<double>& masses = {}) override = 0;

        /// @brief
        virtual void defineSettings() override;


    protected:
        virtual void addRefs() const override;
        
        void calcConstants(const double& unitres);

        void fillKisMis(const double& unitres);
        
    protected:

        AsCoeffs _asCorrections;
        
        std::array<std::function<double(double)>, 3> _Ki1;
        std::array<std::function<double(double)>, 3> _Ki2;
        //std::array<std::function<double(double)>, 5> _Mi;
        
        double _aS;
        double _zBC;
        double _mb1S;
        double _mc1S;

        double _eB0;
        double _eC0;
        double _eB;
        double _eC;
        
    private:
        
        double _dmbmc;
        double _mLb;
        double _mLc; 
        double _la1S;
        double _b1;
        double _b2;

    };

} // namespace Hammer

#endif
