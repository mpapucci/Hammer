///////////////////////////////////////////////////
//// Comparison of B -> D* FF parametrizations ////
///////////////////////////////////////////////////
#include "HepMCParser.hh"
#include "Hammer/Hammer.hh"
#include <iomanip>

using namespace std;

inline void tabHistogram1D(Hammer::IOHistogram& histo, size_t dim, pair<double,double>& range, string filename){
    ofstream file;
    file.open("./data/dat/"+filename);
    for(size_t id1 = 0; id1 < dim; ++id1){
        double valx = range.first + (static_cast<double>(id1)+0.5)/static_cast<double>(dim)*(range.second - range.first);
        file << setprecision(5)<< valx << "\t" << histo[id1].sumWi << endl;
    }
    file.close();
}

int main() {

    /////////////////////////////////////////
    //// Process and add some histograms ////
    /////////////////////////////////////////
    auto begin = std::chrono::system_clock::now();
    auto fin = unique_ptr<ifstream>(new ifstream("./data/BCTauNu_Babar.hepmc", std::ios::in));
    auto io = unique_ptr<HepMC::IO_GenEvent>(new HepMC::IO_GenEvent(*fin));
    if (io->rdstate() != 0) {
        return EXIT_FAILURE;
    }
    //Outfile for Histos. More later!
    ofstream outFileH("./data/FFcompHistos.dat",ios::binary);
    Hammer::Hammer ham{};
    vector<string> include = {"BD*TauNu","TauEllNuNu"};
    ham.includeDecay(include);
    //ham.includeDecay(string("BD*EllNu"));
    vector<string> schemes{"BLPR", "CLN", "BGL"};
    for (auto& elem: schemes){
        ham.addFFScheme("Scheme"+elem, {{"BD*", elem}});
    }
    ham.setFFInputScheme({{"BD*", "ISGW2"}});
    //Histograms
    ham.addHistogram("Q2", {10}, false, {{0., 12.}});
    ham.addHistogram("pEll", {10}, false, {{0., 2.5}});
    ham.addHistogram("M2miss", {10}, false, {{0., 12.}});
    ham.addTotalSumOfWeights(); //adds "Total Sum of Weights" histo with auto bin filling
    ham.setUnits("GeV");
    ham.initRun();
//    ham.setOptions("BtoD*BGL: {avec: [0.00038,0.026905,0.]}");

    outFileH << ham.saveRunHeader();
    //Container for event Id for D and D* separately
    set<set<size_t>> evtIdD;
    size_t count = 0;
    //Loop over events
    HepMC::GenEvent ge;
    for(size_t i =0; i < 10000; ++i) {
        if (io->rdstate() == 0 && io->fill_next_event(&ge)) {
            if ((i + 1) % 1000 == 0) {
                cout<< "processing event " << i + 1 << endl;
            }
            ham.initEvent();
            auto processes = parseGenEvent(ge, {521, -521, 511, -511});
            set<size_t> evtId;
            bool first = true;
            for (auto& proc : processes){
                auto procId = ham.addProcess(proc);
                if (procId == 0){ // Make sure process isn't forbidden
                    continue;
                }
                evtId.insert(proc.getId());
                if(!first) { //Only allow one decay to be binned.
                    continue;
                }
                //Collect D vertex particles and observables. Extract {Parent Particle, {Daughter Particles}}
//                auto BVer = proc.getParticlesByVertex("BD*EllNu");
                auto BVer = proc.getParticlesByVertex("BD*TauNu");
                auto pB = BVer.first;

                auto pC = *find_if(BVer.second.begin(), BVer.second.end(), [&](Hammer::Particle& p)-> bool {return (abs(p.pdgId()) >= 400);});
                double q2 = (pB.p() - pC.p()).mass2();

                auto TVer = proc.getParticlesByVertex("TauEllNuNu");
                auto pL = *find_if(TVer.second.begin(), TVer.second.end(), [&](Hammer::Particle& p)-> bool {return ((abs(p.pdgId()) == 13) || (abs(p.pdgId()) == 11));});
                double pEll = boostToRestFrameOf(pL.p(), pB.p()).p();

                double m2miss = (pB.p() - pC.p() - pL.p()).mass2();

                //Set bins
                ham.fillEventHistogram("Q2", {q2});
                ham.fillEventHistogram("pEll", {pEll});
                ham.fillEventHistogram("M2miss", {m2miss});
                first = false;
            }
            if (evtId.size() >= 1){ // Process event
                ham.processEvent();
                evtIdD.insert(evtId);
                ++count;
            }
        }
    }
    // outFileH << ham.saveRates();
    outFileH << ham.saveHistogram("Total Sum of Weights");
    outFileH << ham.saveHistogram("Q2");
    outFileH << ham.saveHistogram("pEll");
    outFileH << ham.saveHistogram("M2Miss");
    outFileH.close();

    auto end = std::chrono::system_clock::now();
    auto dur = end - begin;
    typedef std::chrono::duration<float> float_seconds;
    auto secs = std::chrono::duration_cast<float_seconds>(dur);
    cout << "Histo Time: " << secs.count() << endl;
    cout << "Events binned: " << count << endl;
    cout << "B -> D* Histograms: " << evtIdD.size() << endl;


    pair<double, double> rangepEll{0.,2.5};
    pair<double, double> rangeQ2{0.,12.};
    pair<double, double> rangeM2miss{0.,12.};
    for(auto& elem: schemes){
        auto DstarPEll = ham.getHistogram("pEll", "Scheme"+elem);
        auto DstarQ2 = ham.getHistogram("Q2", "Scheme"+elem);
        auto DstarM2miss = ham.getHistogram("M2miss", "Scheme"+elem);
        tabHistogram1D(DstarPEll, 10, rangepEll, "FFcomp_PEllDstar_"+elem+".dat");
        tabHistogram1D(DstarQ2, 10, rangeQ2, "FFcomp_Q2Dstar_"+elem+".dat");
        tabHistogram1D(DstarM2miss, 10, rangeM2miss, "FFcomp_M2Dstar_"+elem+".dat");
    }

} // int main()
