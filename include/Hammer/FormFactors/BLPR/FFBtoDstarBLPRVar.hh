///
/// @file  FFBtoDstarBLPRVar.hh
/// @brief \f$ B \rightarrow D^* \f$ BLPRVar form factors with variations
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_BDSTARBLPRVAR
#define HAMMER_FF_BDSTARBLPRVAR

#include "Hammer/FormFactors/BLPR/FFBLPRVarBase.hh"

namespace Hammer {

    class FFBtoDstarBLPRVar : public FFBLPRVarBase {

    public:
        FFBtoDstarBLPRVar();

        FFBtoDstarBLPRVar(const FFBtoDstarBLPRVar& other) = default;
        FFBtoDstarBLPRVar& operator=(const FFBtoDstarBLPRVar& other) = delete;
        FFBtoDstarBLPRVar(FFBtoDstarBLPRVar&& other) = delete;
        FFBtoDstarBLPRVar& operator=(FFBtoDstarBLPRVar&& other) = delete;
        virtual ~FFBtoDstarBLPRVar() override = default;

    public:

        virtual std::unique_ptr<FormFactorBase> clone(const std::string& label) override;

    protected:
        virtual void evalAtPSPoint(const std::vector<double>& point, const std::vector<double>& masses = {}) override;

        virtual void defineSettings() override;
    private:
        mutable bool initialized;
    };

} // namespace Hammer

#endif
