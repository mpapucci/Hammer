///
/// @file  IndexTypes.hh
/// @brief Hammer data types declarations
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_IndexTypes_HH
#define HAMMER_IndexTypes_HH

#include <vector>
#include <set>
#include <map>
#include <string>

#include "Hammer/Exceptions.hh"
#include "Hammer/Tools/Utils.hh"

namespace Hammer {

    enum class WTerm { COMMON, NUMERATOR, DENOMINATOR };

    using ParticleIndex = size_t;
    using ParticleIndices = std::vector<ParticleIndex>;
    using UniqueParticleIndices = std::set<ParticleIndex>;

    using HashId = size_t;

    using AmplitudeUID = HashId;
    template <typename T>
    using AmplitudeIdDict = std::map<AmplitudeUID, T>;
    using HadronicUID = HashId;
    template <typename T>
    using HadronicIdDict = std::map<HadronicUID, T>;

    using VertexUID = HashId;
    template <typename T>
    using VertexIdDict = std::map<VertexUID, T>;
    using VertexUIDSet = std::set<VertexUID>;
    template <typename T>
    using VertexDict = std::map<ParticleIndex, T>;
    using VertexName = std::string;


    using ProcessUID = HashId;
    template <typename T>
    using ProcIdDict = std::map<ProcessUID, T>;

    using EventUID = std::set<ProcessUID>;
    template <typename T>
    using EventIdDict = UMap<EventUID, T>;
    using EventUIDGroup = std::set<EventUID>;
    template <typename T>
    using EventIdGroupDict = UMap<EventUIDGroup, T>;


    using SchemeName = std::string;
    using SchemeNameList = std::vector<SchemeName>;
    template<typename T>
    using SchemeDict = std::map<SchemeName, T>;

    template <typename T>
    using WCPrefixDict = std::map<std::string, T>;

    using FFIndex = size_t;
    template <typename T>
    using FFIndexDict = std::map<FFIndex, T>;

    struct FFPrefixGroup {
        std::string prefix;
        std::string group;
        std::string get() const {
            return prefix + group;
        }
        bool operator<(const FFPrefixGroup& r) const {
            return (prefix < r.prefix) || ( (prefix == r.prefix) && (group < r.group));
        }
        bool operator==(const FFPrefixGroup& r) const {
            return (prefix == r.prefix) && (group == r.group);
        }
    };
    template <typename T>
    using FFPrefixGroupDict = std::map<FFPrefixGroup, T>;

    template <typename T>
    struct NumDenPair {

        T numerator;
        T denominator;

        T& get(WTerm what) {
            switch(what) {
            case WTerm::NUMERATOR:
                return numerator;
            case WTerm::DENOMINATOR:
                return denominator;
            case WTerm::COMMON:
                throw Error("Invalid option");
            }
            throw Error("Invalid option");
        }

        const T& get(WTerm what) const {
            switch (what) {
            case WTerm::NUMERATOR:
                return numerator;
            case WTerm::DENOMINATOR:
                return denominator;
            case WTerm::COMMON:
                throw Error("Invalid option");
            }
            throw Error("Invalid option");
        }
    };

}

#endif
