// Main page in doxygen

///
/// @mainpage Hammer: Helicity Amplitude Module for Matrix Element Reweighing
///

// Modules definition in doxygen

/// @defgroup Base Base Classes
/// @defgroup Core Hammer Core Classes
/// @defgroup Interfaces Class Interfaces
/// @defgroup Amplitudes Decay Amplitude Classes
/// @defgroup FormFactors Form Factor Classes
/// @defgroup Tools Misc. Helper Classes
/// @defgroup Math Mathematical Classes
/// @defgroup PyExtHammer Hammer Python Package

// Namespaces

/// @namespace Hammer The Hammer namespace contains the library code
/// @namespace Hammer::MultiDimensional The MultiDimensional namespace contains the tensor algebra infrastructure
/// @namespace Hammer::MultiDimensional::Ops The MultiDimensional namespace contains the tensor algebra algorithms
/// @namespace Hammer::Serial The Serial namespace contains the Hammer serialization code based on flatbuffers
/// @namespace Hammer::PS The PS namespace contains the phase space integration infrastructure 
