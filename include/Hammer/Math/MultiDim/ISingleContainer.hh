///
/// @file  ISingleContainer.hh
/// @brief Interface class for single container data structure
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_MATH_ISINGLECONTAINER
#define HAMMER_MATH_ISINGLECONTAINER


#include "Hammer/Math/MultiDim/IContainer.hh"

namespace Hammer {

    namespace MultiDimensional {

        class ISingleContainer : public IContainer {

        public:
            virtual ~ISingleContainer() override {}
            ISingleContainer() {}
            ISingleContainer(const ISingleContainer&) = default;
            ISingleContainer(ISingleContainer&&) = default;
            ISingleContainer&  operator=(const ISingleContainer&) = default;
            ISingleContainer& operator=(ISingleContainer&&) = default;

            class ItBase {
            public:
                virtual ~ItBase() {}
                virtual IContainer::ElementType value() const = 0;
                virtual PositionType position() const = 0;
                virtual void next(int n = 1) = 0;
                virtual bool isSame(const ItBase& other) const = 0;
                virtual bool isAligned() const = 0;
                virtual ptrdiff_t distanceFrom(const ItBase& other) const = 0;
            };

            using NonZeroIt = std::unique_ptr<ItBase>;

            virtual NonZeroIt firstNonZero() const = 0;
            virtual NonZeroIt endNonZero() const = 0;

        };

        inline void next(ISingleContainer::NonZeroIt it) { it->next(); }

        inline bool operator==(const ISingleContainer::ItBase& lhs,
                               const ISingleContainer::ItBase& rhs) {
            return lhs.isSame(rhs);
        }

        inline bool operator!=(const ISingleContainer::ItBase& lhs, const ISingleContainer::ItBase& rhs) {
            return !(lhs == rhs);
        }

    }

}

#endif
