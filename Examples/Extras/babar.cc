#include "HepMCParser.hh"
#include "Hammer/Hammer.hh"
#include <iomanip>
#include <random>

using namespace std;

inline void exportHistogram(Hammer::Hammer& ham, vector<string>& schemes, string proc, string filename){
    ofstream file;
    file.open("./data/dat/"+filename+"_"+proc+".dat");
    ham.setWilsonCoefficients("BtoCTauNu", {{"S_qRlL", 0.0},{"S_qLlL", 0.0}});
    double mbmtau = 4.710*1.776;
    // double mcmtau = (4.710-3.400)*1.776;
    //get SM effs
    vector<double> SMeffs{};
    file << 0.0;
    for(auto& scheme : schemes){
        auto histoAcc = ham.getHistogram("Acceptance" + proc, scheme);
        auto histoTot = ham.getHistogram("Total" + proc, scheme);
        SMeffs.push_back(histoAcc[0].sumWi/histoTot[0].sumWi);
        file << "\t" << setprecision(5) << 1.;
    }
    file << endl;

    for (size_t idW = 1; idW <= 20; ++idW) {
        double tanBetaOnMH = (static_cast<double>(idW))*0.05;
        ham.setWilsonCoefficients("BtoCTauNu", {{"S_qRlL", -pow(tanBetaOnMH,2.0)*mbmtau},{"S_qLlL", 0.0}});

        file << tanBetaOnMH;
        for(size_t idx = 0; idx < schemes.size(); ++idx){
            auto histoAcc = ham.getHistogram("Acceptance" + proc, schemes[idx]);
            auto histoTot = ham.getHistogram("Total" + proc, schemes[idx]);
            double effratio = histoAcc[0].sumWi/histoTot[0].sumWi/SMeffs[idx];
            file << "\t" << setprecision(5) << effratio;
        }
        file << endl;
    }

    file.close();
}


inline void fillVisibleDaughters(vector<Hammer::Particle>& visDaughters, vector<Hammer::Particle>& daughters){
    for (auto elem : daughters) {
        auto id = elem.pdgId();
        if (abs(id) == 111 || abs(id) == 211 || abs(id) == 411 || abs(id) == 421 || abs(id) == 13 || abs(id) == 11){
            visDaughters.push_back(elem);
        }
    }
}

inline double getPionEfficiency(double pLab){ //Takes GeV argument
    if (pLab > 0.04) {
        return 1.54 - 0.790*pLab + 0.421*log(pLab);
    } else {
        return 0.;
    }
}

int main() {
    auto fin = unique_ptr<ifstream>(new ifstream("./data/BCTauNu_BabarPS.hepmc", std::ios::in));
    auto io = unique_ptr<HepMC::IO_GenEvent>(new HepMC::IO_GenEvent(*fin));
    if (io->rdstate() != 0) {
        return EXIT_FAILURE;
    }
    double pi = M_PI;
    ofstream outFileH("./data/dat/BaBarHistos.dat",ios::binary);
    Hammer::Hammer ham{};
    //Included decays
    vector<string> include = {"BD*TauNu","D*DPi"};
    ham.includeDecay(include);
    ham.includeDecay(string("BDTauNu"));
    //Include tag processes
    include = {"BD*EllNu","D*DPi"};
    ham.includeDecay(include);
    ham.includeDecay(string("BDEllNu"));
    //Formfactor schemes
    vector<string> schemes{"SchemeBLPR", "SchemeCLN", "SchemeBGL", "SchemeISGW2"};
    ham.addFFScheme("SchemeBLPR", {{"BD", "BLPR"}, {"BD*", "BLPR"}});
    ham.addFFScheme("SchemeCLN", {{"BD", "CLN"}, {"BD*", "CLN"}});
    ham.addFFScheme("SchemeBGL", {{"BD", "BGL"}, {"BD*", "BGL"}});
    ham.addFFScheme("SchemeISGW2", {{"BD", "ISGW2"}, {"BD*", "ISGW2"}});
    ham.setFFInputScheme({{"BD", "ISGW2"}, {"BD*", "ISGW2"}});
    //Pure PS in denominator
    set<string> purePS{"BDTauNu", "BD*TauNu", "D*DPi", "BD*EllNu", "BDEllNu"};
    ham.addPurePSVertices(purePS, Hammer::WTerm::DENOMINATOR);
    ham.addHistogram("PEllD", {20}, false, {{0.,2.4}});
    ham.addHistogram("PEllDstar", {20}, false, {{0., 2.4}});
    ham.addHistogram("Q2D", {20}, false, {{0., 14.}});
    ham.addHistogram("Q2Dstar", {20}, false, {{0., 14.}});
    ham.addHistogram("AcceptanceD", vector<uint16_t>{1}, false);
    ham.addHistogram("AcceptanceDstar", vector<uint16_t>{1}, false);
    ham.addHistogram("TotalD", vector<uint16_t>{1}, false);
    ham.addHistogram("TotalDstar", vector<uint16_t>{1}, false);
    ham.addTotalSumOfWeights(); //adds "Total Sum of Weights" histo with auto bin filling
    ham.setUnits("GeV");
    ham.initRun();
    outFileH << ham.saveRunHeader();
    HepMC::GenEvent ge;
    for(size_t i = 0; i < 100000; ++i) {
        if (io->rdstate() == 0 && io->fill_next_event(&ge)) {
            if ((i + 1) % 10000 ==0) {
                cout<< "processing event " << i + 1 << endl;
            }
            if ((i + 1) % 20000 == 0) {
                exportHistogram(ham, schemes, "Dstar", "BaBarEff_"+to_string(i+1));
                exportHistogram(ham, schemes, "D", "BaBarEff_"+to_string(i+1));
            }
            ham.initEvent();
            auto processes = parseGenEvent(ge, {521, -521, 511, -511});
            bool tauflag = false;
            bool tagflag = false;
            string procstring = "";
            vector<Hammer::Particle> visDaughters({});
            vector<Hammer::Particle> accVisDaughters({});
            Hammer::Particle pB;
            Hammer::Particle pC;
            uint16_t PEllbin = 0ul;
            uint16_t Q2bin = 0ul;
            for(auto& elem : processes) {
                auto procId = ham.addProcess(elem);
                if (procId != 0){ //process exists
                    bool tagDflag = (elem.getVertexId("BDEllNu") != 0) ? true : false;
                    bool tagDsflag = (elem.getVertexId("BD*EllNu") != 0) ? true : false;
                    bool Dflag = (elem.getVertexId("BDTauNu") != 0) ? true : false;
                    bool Dsflag = (elem.getVertexId("BD*TauNu") != 0) ? true : false;
                    if (Dsflag || Dflag){
                        tauflag = true;
                        auto BVer = Dsflag ?  elem.getParticlesByVertex("BD*TauNu") : elem.getParticlesByVertex("BDTauNu");
                        pB = BVer.first;
                        pC = *find_if(BVer.second.begin(), BVer.second.end(), [&](Hammer::Particle& p)-> bool {return (abs(p.pdgId()) >= 400);});
                        vector<Hammer::Particle> hadDaughters;
                        if(Dsflag){ //Set signal pion efficiency
                            auto DsVer = elem.getParticlesByVertex("D*DPi");
                            auto pPi = *find_if(DsVer.second.begin(), DsVer.second.end(), [&](Hammer::Particle& p)-> bool {return (abs(p.pdgId()) == 211 ||abs(p.pdgId()) == 111 );});
                            ham.setEventBaseWeight(getPionEfficiency(pPi.p().p()));
                            hadDaughters = DsVer.second;
                        } else {
                            hadDaughters = vector<Hammer::Particle>({pC});
                        }
//                        auto hadDaughters = Dsflag ? DsVer.second : vector<Hammer::Particle>({pC});
                        auto tauDaughters = elem.getParticlesByVertex("TauEllNuNu").second;
                        procstring = Dsflag ? "Dstar" : "D";
                        fillVisibleDaughters(visDaughters, tauDaughters);
                        fillVisibleDaughters(visDaughters, hadDaughters);

                    }
                    if (tagDsflag || tagDflag){
                        tagflag = true;
                        auto BVer = tagDsflag ?  elem.getParticlesByVertex("BD*EllNu") : elem.getParticlesByVertex("BDEllNu");
                        auto pCtag = *find_if(BVer.second.begin(), BVer.second.end(), [&](Hammer::Particle& p)-> bool {return (abs(p.pdgId()) >= 400);});
                        auto hadDaughters = tagDsflag ? elem.getParticlesByVertex("D*DPi").second : vector<Hammer::Particle>({pCtag});
                        fillVisibleDaughters(visDaughters, hadDaughters);
                        ham.removeProcess(procId);
                    }
                }
            }
            if (tauflag && tagflag){
                default_random_engine generator;
                normal_distribution<double> gaussian(0.0,1.2);
                double Q2 = (pB.p() - pC.p())*(pB.p() - pC.p()) + gaussian(generator);
                Q2bin = static_cast<uint16_t>(floor(20. * (Q2 - 0.0) / (14. - 0.0)));
                for(auto elem2 : visDaughters) {
                    if (elem2.p().theta() > 20.1/180.*pi && elem2.p().theta() < 150.2/180.*pi){
                        if ((abs(elem2.pdgId()) == 13 && elem2.p().E() > 0.2) || (abs(elem2.pdgId()) == 11 && elem2.p().E() > 0.3)){
                            double PEllBoosted = boostToRestFrameOf(elem2.p(), pB.p()).p();
                            PEllbin = static_cast<uint16_t>(floor(20. * (PEllBoosted - 0.0) / (2.4 - 0.0)));
                            accVisDaughters.push_back(elem2);
                        }
                        if (abs(elem2.pdgId()) != 13 && abs(elem2.pdgId()) != 11) {
                            accVisDaughters.push_back(elem2);
                        }
                    }
                }
                if ( accVisDaughters.size() ==  visDaughters.size() && Q2 >= 4.0){
                    ham.setEventHistogramBin("Acceptance" + procstring, {0});
                    ham.setEventHistogramBin("PEll" + procstring, {PEllbin});
                    ham.setEventHistogramBin("Q2" + procstring, {Q2bin});
                }
                ham.setEventHistogramBin("Total" + procstring, {0});
                ham.processEvent();
            }
        }
    }

    outFileH << ham.saveHistogram("Total Sum of Weights");
    vector<string> modes{"Dstar", "D"};
    for( auto& mode: modes){
        outFileH << ham.saveHistogram("Acceptance"+mode);
        outFileH << ham.saveHistogram("Total"+mode);
        outFileH << ham.saveHistogram("PEll"+mode);
        outFileH << ham.saveHistogram("Q2"+mode);
    }

    auto DstarAcc = ham.getHistogram("AcceptanceDstar", schemes[0]);
    auto DstarTot = ham.getHistogram("TotalDstar", schemes[0]);
    auto DAcc = ham.getHistogram("AcceptanceD", schemes[0]);
    auto DTot = ham.getHistogram("TotalD", schemes[0]);
    cout << "D* Events: " << DstarAcc[0].n << "/" << DstarTot[0].n << endl;
    cout << "D Events: " << DAcc[0].n << "/" << DTot[0].n << endl;

} // int main()



//        cout << scheme << ": {{0, 1}";
//        for (size_t idW = 1; idW <= 20; ++idW) {
//            double tanBetaOnMH = (static_cast<double>(idW))*0.05;
//            ham.setWilsonCoefficients("BtoCTauNu", {{"S_qRlL", -pow(tanBetaOnMH,2.0)*mbmtau},{"S_qLlL", -pow(tanBetaOnMH,2.0)*mcmtau}});
//            auto histoAcc = ham.getHistogram("Acceptance" + proc, scheme);
//            auto histoTot = ham.getHistogram("Total" + proc, scheme);
//            cout << ",{" << tanBetaOnMH << ", " << histoAcc[0].sumWi / histoTot[0].sumWi /SMeff << "}";
//        }
//        cout << "}" << endl << endl;
//    }
//}

//                    auto BVer = elem.getParticlesByVertex("BD*TauNu"); //first try D* mode
//                    if(BVer.second.size() > 0){
//                        if (!tauflag){ //only one tau includeed
//                            pB = BVer.first;
//                            auto tauDaughters = elem.getParticlesByVertex("TauEllNuNu").second;
//                            auto DstarDaughters = elem.getParticlesByVertex("D*DPi").second;
//                            fillVisibleDaughters(visDaughters, tauDaughters);
//                            fillVisibleDaughters(visDaughters, DstarDaughters);
//                            tauflag = true;
//                            procstring = "D*";
//                            continue;
//                        }
//                    }
//                    BVer = elem.getParticlesByVertex("BDTauNu"); //now try D mode
//                    if(BVer.second.size() > 0){
//                        if (!tauflag){ //only one tau includeed
//                            pB = BVer.first;
//                            auto tauDaughters = elem.getParticlesByVertex("TauEllNuNu").second;
//                            fillVisibleDaughters(visDaughters, BVer.second);
//                            fillVisibleDaughters(visDaughters, tauDaughters);
//                            tauflag = true;
//                            procstring = "D";
//                            continue;
//                        }
//                    }
//                    ham.removeProcess(procId);
//                }
//            }
//
