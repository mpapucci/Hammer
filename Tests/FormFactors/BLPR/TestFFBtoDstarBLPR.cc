#include "gtest/gtest.h"
#include "Hammer/FormFactors/BLPR/FFBtoDstarBLPR.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoDstarBLPR: public FFBtoDstarBLPR {

    public:

        TestableFFBtoDstarBLPR() : FFBtoDstarBLPR() { }

        using FFBtoDstarBLPR::evalAtPSPoint;
        using FFBtoDstarBLPR::initSettings;

    };

    TEST(FFBtoDstarBLPRTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pBmes{5280.,   0.,   0.,   0.};
        FourMomentum pDstarmes{2258.9451,   -542.95362175877925474, -718.43449427445361377, 513.65086086722812801};
        FourMomentum kNuTau{763.45512706233784082,  59.818100429037400761,  266.26625745054465575,  713.01318784585764875};
        FourMomentum pTau{2257.5997729376621592,    483.13552132974185397,  452.16823682390895801,  -1226.6640487130857768};

        //Evaluate the FF class
        TestableFFBtoDstarBLPR ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pBmes, 511), {Particle(pDstarmes,-413), Particle(kNuTau,16), Particle(pTau,-15)}, {});
        //ff.evalAtPSPoint({Sqq}, {pBmes.mass(), pDstarmes.mass()});

        // Compare to direct evaluation
        // Fs, Ff, Fg, Fm, Fp, Fzt, Fmt, Fpt
        Tensor ffEval{"ffEval", MD::makeVector({8}, {FF_BDSTAR},
                                               {-0.7138515942, 5753.345778, 0.0001653199311, 0.0001515402387,
                                                -0.0001142943583, -6.155751196e-9, 0.6334797261, -1.061546525})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 8; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        }
    }


}
