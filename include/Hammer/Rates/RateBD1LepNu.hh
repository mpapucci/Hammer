///
/// @file  RateBD1LepNu.hh
/// @brief \f$ B \rightarrow D_1 \tau\nu \f$ total rate
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_RATE_BDSSD1LEPNU
#define HAMMER_RATE_BDSSD1LEPNU

#include "Hammer/RateBase.hh"

namespace Hammer {

    class RateBD1LepNu : public RateBase {

    public:
        RateBD1LepNu();

        virtual ~RateBD1LepNu() override {
        }

    protected:
        virtual Tensor evalAtPSPoint(const std::vector<double>& point) override;
    };

} // namespace Hammer

#endif
