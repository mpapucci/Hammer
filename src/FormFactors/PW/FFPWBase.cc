///
/// @file  FFPWBase.cc
/// @brief Hammer base class for partial wave coefficients
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#include "Hammer/FormFactors/PW/FFPWBase.hh"

using namespace std;

namespace Hammer {

    FFPWBase::FFPWBase() {
        setGroup("PW");
    }
    
}
