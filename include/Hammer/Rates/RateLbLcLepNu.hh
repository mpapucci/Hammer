///
/// @file  RateLbLcLepNu.hh
/// @brief \f$ \Lambda_b \rightarrow \Lambda_c \tau\nu \f$ total rate
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_RATE_LBLCLEPNU
#define HAMMER_RATE_LBLCLEPNU

#include "Hammer/RateBase.hh"

namespace Hammer {

    class RateLbLcLepNu : public RateBase {

    public:
        RateLbLcLepNu();

        virtual ~RateLbLcLepNu() override {
        }

    protected:
        virtual Tensor evalAtPSPoint(const std::vector<double>& point) override;
    };

} // namespace Hammer

#endif
