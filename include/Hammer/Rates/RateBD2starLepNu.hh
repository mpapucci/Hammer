///
/// @file  RateBD2starLepNu.hh
/// @brief \f$ B \rightarrow D_2^* \tau\nu \f$ total rate
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_RATE_BDSSD2STARLEPNU
#define HAMMER_RATE_BDSSD2STARLEPNU

#include "Hammer/RateBase.hh"

namespace Hammer {

    class RateBD2starLepNu : public RateBase {

    public:
        RateBD2starLepNu();

        virtual ~RateBD2starLepNu() override {
        }

    protected:
        virtual Tensor evalAtPSPoint(const std::vector<double>& point) override;
    };

} // namespace Hammer

#endif
