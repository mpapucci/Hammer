#include "gtest/gtest.h"
#include "Hammer/FormFactors/BLRS/FFLbtoLcBLRS.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFLbtoLcBLRS: public FFLbtoLcBLRS {

    public:

        TestableFFLbtoLcBLRS() : FFLbtoLcBLRS() { }

        using FFLbtoLcBLRS::initSettings;
        using FFLbtoLcBLRS::evalAtPSPoint;
    };

    TEST(FFLbtoLcBLRSTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pLbmes{5620., 0., 0., 0.};
        FourMomentum pLcmes{2514.6000000000004,380.2511160960518,-878.708483534587,-425.0854616098454};
        FourMomentum pTau{2456.13415321897,-581.6942711749232,1418.910848905593,723.6624190525239};
        FourMomentum kNuTau{649.2658467810297,201.44315507887137,-540.2023653710064,-298.5769574426786};

        //Evaluate the FF class
        TestableFFLbtoLcBLRS ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pLbmes, -5122), {Particle(pLcmes, -4122), Particle(kNuTau, 16), Particle(pTau, -15)}, {});

        //Compare to direct evaluation
        Tensor ffEval{"ffEval", MD::makeVector({12}, {FF_LBLC},
                                {0.7312555660768754,
                                1.1660722177550018,
                                1.195461806802338,
                                -0.31472386413207737,
                                -0.0851912009508891,
                                0.7527695959975337,
                                -0.3856860143396382,
                                0.11604815859155679,
                                0.805646707782559,
                                -0.37394516263654165,
                                0.11780903740914717,
                                0.013524178395048072})};


        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 12; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        }
    }


}
