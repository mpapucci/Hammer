///
/// @file  ParticleUtils.cc
/// @brief PDG codes to UID functions
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#include "Hammer/Tools/Utils.hh"
#include "Hammer/Tools/ParticleUtils.hh"
#include "Hammer/Tools/Pdg.hh"

#include <algorithm>

using namespace std;

namespace Hammer {

    std::vector<PdgId> flipSigns(const std::vector<PdgId>& list) {
        PID& pdg = PID::instance();
        vector<PdgId> res = list;
        for (auto& elem : res) {
            //check to ensure particle is not self conjugate particle: isMeson return false for -ve pid self conj meson.
            if( pdg.isMeson(elem) == pdg.isMeson(-elem) && elem != PID::PHOTON ){
                elem *= -1;
            }
        }
        return res;
    }

    PdgId flipSign(const PdgId& id) {
        PdgId res = id;
        PID& pdg = PID::instance();
        //check to ensure particle is not self conjugate meson: isMeson return false for -ve pid self conj meson.
        if( pdg.isMeson(res) == pdg.isMeson(-res)  && res != PID::PHOTON ){
            res *= -1;
        }
        return res;
    }

    std::vector<PdgId> combineDaughters(const std::vector<PdgId>& daughters, const std::vector<PdgId>& subDaughters) {
        vector<PdgId> res = daughters;
        sort(res.begin(), res.end(), &pdgSorter);
        if (subDaughters.size() > 0) {
            vector<PdgId> tmp = subDaughters;
            sort(tmp.begin(), tmp.end(), &pdgSorter);
            res.insert(res.end(), tmp.begin(), tmp.end());
        }
        return res;
    }

    HashId processID(PdgId parent, const std::vector<PdgId>& allDaughters) {
        HashId seed = 0ul;
        combine_hash(seed, static_cast<int>(parent));
        for (auto elem : allDaughters) {
            combine_hash(seed, static_cast<int>(elem));
        }
        return seed;
    }

    HashId combineProcessIDs(const std::set<HashId>& allIds) {
        HashId seed = 0ul;
        for(auto elem: allIds) {
            combine_hash(seed, elem);
        }
        return seed;
    }


    bool pdgSorter(PdgId a, PdgId b) {
        if (abs(static_cast<int>(a)) != abs(static_cast<int>(b))) {
            return (abs(static_cast<int>(a)) > abs(static_cast<int>(b)));
        } else {
            return (a > b);
        }
    }

    bool particlesByPdg(const std::function<PdgId(const Particle&)>& pdgGetter, const Particle& a, const Particle& b) {
        PdgId aId = pdgGetter(a);
        PdgId bId = pdgGetter(b);
        return pdgSorter(aId, bId);
    }

} // namespace Hammer
