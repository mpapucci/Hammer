///
/// @file  FFBCLBase.hh
/// @brief Hammer base class for BCL form factors.
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_BCL_BASE
#define HAMMER_FF_BCL_BASE

#include "Hammer/FormFactorBase.hh"

namespace Hammer {

    /// @brief Base class for BCL form factors
    /// Implementation matched to EvtGen
    ///
    /// @ingroup FormFactors
    class FFBCLBase : public FF1to1Base {

    public:
        FFBCLBase();

        FFBCLBase(const FFBCLBase& other) = default;
        FFBCLBase& operator=(const FFBCLBase& other) = delete;
        FFBCLBase(FFBCLBase&& other) = delete;
        FFBCLBase& operator=(FFBCLBase&& other) = delete;
        virtual ~FFBCLBase() override = default;
        
    protected:

        /// @brief
        /// @param[in] point
        /// @param[in] masses
        /// @return
        virtual void evalAtPSPoint(const std::vector<double>& point, const std::vector<double>& masses = {}) override = 0;

        /// @brief
        virtual void defineSettings() override = 0;
    
    protected:
        virtual void addRefs() const override;
    };

} // namespace Hammer

#endif
