///
/// @file  Utils.hh
/// @brief Hammer math utilities class
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_MATH_UTILS
#define HAMMER_MATH_UTILS

#include <complex>
#include <limits>
#include <climits>
#include <cstring>
#include <vector>

namespace Hammer {

    static const double precision = 0.001;

    /// @brief
    /// @param[in] val
    /// @return
    inline bool isZero(const std::complex<double> val) {
        return ((fabs(val.real()) < std::numeric_limits<double>::min()) &&
                (fabs(val.imag()) < std::numeric_limits<double>::min()));

    }

    /// @brief
    /// @param[in] val
    /// @return
    inline bool isZero(const double val) {
        return (fabs(val) < std::numeric_limits<double>::min());
    }

    /// @brief
    /// @param[in] val1
    /// @param[in] val2
    /// @return
    inline bool fuzzyLess(const double val1, const double val2) {
        return (val1-val2 < -1.*std::max(precision, std::numeric_limits<double>::min()));
    }

    /// @brief
    /// @param[in] val1
    /// @param[in] val2
    /// @return
    double compareVals(const double val1, const double val2);

    /// @brief
    /// @param[in] val1
    /// @param[in] val2
    /// @return
    std::complex<double> compareVals(const std::complex<double> val1, const std::complex<double> val2);


    inline double regularize(const double regularVal, const double problematicValue,
                             const double delta = std::numeric_limits<double>::min(), int direction = 0) {
        if (fabs(problematicValue - regularVal) > delta) {
            return regularVal;
        }
        return problematicValue + delta * (direction == 0 ? ((problematicValue > regularVal) ? -1.0 : 1.0) : 1.*direction);
    }

    template <typename T>
    typename std::enable_if<std::is_arithmetic<typename std::remove_reference<T>::type>::value, double>::type
    toDouble(T value) {
        return static_cast<double>(value);
    }

    template <typename T>
    typename std::enable_if<
        std::is_same<typename std::remove_reference<typename std::remove_cv<T>::type>::type, std::string>::value,
        double>::type
    toDouble(T value) {
        return stod(value);
    }

    template <typename T>
    typename std::enable_if<std::is_same<typename std::remove_reference<typename std::remove_cv<T>::type>::type,
                                         std::complex<double>>::value,
                            double>::type
    toDouble(T value) {
        return value.real();
    }

    template <typename T>
    typename std::enable_if<std::is_unsigned<T>::value && std::is_integral<T>::value, T>::type minPadding(T value) {
        if (value == 0u)
            return value;
        if (value == 1u)
            return 0;
        T pad = 0u;
        --value;
        while (value != 0u) {
            value = static_cast<T>(value >> 1);
            ++pad;
        }
        return pad;
    }

    // taken from https://stackoverflow.com/a/42138465
    using Fp_info = std::numeric_limits<double>;

    // taken from https://stackoverflow.com/a/42138465
    inline auto is_ieee754_nan( double const x ) -> bool {
        static constexpr bool   is_claimed_ieee754  = Fp_info::is_iec559;
        static constexpr int    n_bits_per_byte     = CHAR_BIT;
        using Byte = unsigned char;

        static_assert( is_claimed_ieee754, "!" );
        static_assert( n_bits_per_byte == 8, "!" );
        static_assert( sizeof( x ) == sizeof( uint64_t ), "!" );

        #ifdef _MSC_VER
            uint64_t const bits = reinterpret_cast<uint64_t const&>( x );
        #else
            Byte bytes[sizeof(x)];
            memcpy( bytes, &x, sizeof( x ) );
            uint64_t int_value;
            memcpy( &int_value, bytes, sizeof( x ) );
            uint64_t const& bits = int_value;
        #endif

        static constexpr uint64_t   sign_mask       = 0x8000000000000000;
        static constexpr uint64_t   exp_mask        = 0x7FF0000000000000;
        static constexpr uint64_t   mantissa_mask   = 0x000FFFFFFFFFFFFF;

        (void) sign_mask;
        return (bits & exp_mask) == exp_mask and (bits & mantissa_mask) != 0;
    }

    inline auto is_ieee754_nan(std::complex<double> x) -> bool {
        return is_ieee754_nan(x.real()) || is_ieee754_nan(x.imag());
    }

} // namespace Hammer

#endif
