#include "HepMCParser.hh"
#include "Hammer/Hammer.hh"
#include <iomanip>
#include <sstream>

using namespace std;

template <typename T> std::string to_string_prc(const T value, const int n = 6) {
    std::ostringstream out;
    out.precision(n);
    out << std::fixed << value;
    return out.str();
}

inline void tabHistogram1D(Hammer::IOHistogram& histo, size_t dim, pair<double,double>& range, string filename){
    ofstream file;
    file.open("./data/dat/pol/"+filename);
    for(size_t id1 = 0; id1 < dim; ++id1){
        double valx = range.first + (static_cast<double>(id1)+0.5)/static_cast<double>(dim)*(range.second - range.first);
        file << setprecision(5)<< valx << "\t" << histo[id1].sumWi << endl;
    }
    file.close();
}

inline void tabHistogram1Dout(Hammer::IOHistogram& histo, size_t dim, pair<double,double>& range, string filename){
    ofstream file;
    file.open(filename);
    for(size_t id1 = 0; id1 < dim; ++id1){
        double blow = range.first + static_cast<double>(id1)/static_cast<double>(dim)*(range.second - range.first);
        double bhigh = range.first + static_cast<double>(id1 + 1)/static_cast<double>(dim)*(range.second - range.first);
        file << blow << "\t" << bhigh << "\t" << to_string_prc(histo[id1].sumWi,12) << endl;
    }
    file.close();
//    double blow = range.second - (1.)/dim*(range.second - range.first);
//    cout << "{" << setprecision(5) << blow << ", " << range.second << ", " << histo[dim-1].sumWi << "}}" << endl;
}

int main(int, char *argv[]) {

    //////////////////////////////////////////////////
    ////  Now reread histograms and reweigh them  ////
    //////////////////////////////////////////////////

    Hammer::Hammer ham{};
    Hammer::IOBuffer buf{Hammer::RecordType::UNDEFINED, 0ul, nullptr};
    ifstream inFile("./data/TauPolHistos.dat", ios::binary);
    // Reload the histograms
    ham.setUnits("GeV");
    ham.initRun();
    inFile >> buf;
    if(!ham.loadRunHeader(buf)) {
        if(buf.start != nullptr) {
            delete[] buf.start;
        }
        inFile.close();
        return EXIT_FAILURE;
    }
    inFile >> buf;
    while(buf.kind == Hammer::RecordType::HISTOGRAM || buf.kind == Hammer::RecordType::HISTOGRAM_DEFINITION) {
        if(buf.kind == Hammer::RecordType::HISTOGRAM) {
            ham.loadHistogram(buf);
        }
        else {
            ham.loadHistogramDefinition(buf);
        }
        inFile >> buf;
    }
    inFile.close();

    vector<string> names = {"D", "Dstar"};
    vector<string> obs = {"Q2", "CosTheta", "pPi", "AccQ2", "AccCosTheta", "AccpPi"};
    vector<pair<double,double>> ranges({{0.,12.}, {-1.,1.}, {0.,2.5},{0.,12.}, {-1.,1.}, {0.,2.5}});
    vector<size_t> dims({10,20,10,10,20,10});
    map<string, pair<size_t, pair<double,double>>> obsmap;
    for(size_t idx = 0; idx < obs.size(); ++idx){
        obsmap[obs[idx]] = make_pair(dims[idx], ranges[idx]);
    }

//    for(size_t idxv = 0; idxv < 11; ++idxv){
//        double val = -1. + 2.*idxv/10.;
//        ham.setWilsonCoefficients("BtoCTauNu", {{"S_qRlL", val}});
//        for(size_t idx = 0; idx < obs.size(); ++idx){
//            for(auto& name : names){
//                auto histo = ham.getHistogram(obs[idx]+":"+name, "NP");
//                tabHistogram1D(histo, dims[idx], ranges[idx], "TauPol_"+obs[idx]+"_"+name+"_"+to_string_prc(val,2)+".dat");
//            }
//        }
//    }

    string ob = argv[1];
    string name = argv[2];
    vector<complex<double>> WCs;
    for(size_t idx = 0; idx < 11; ++idx){
        WCs.push_back(stod(argv[2*idx+3]) + stod(argv[2*idx+4])*1i);
    }
    ham.setWilsonCoefficients("BtoCTauNu", WCs);


    auto histo = ham.getHistogram(ob+":"+name, "NP");
    tabHistogram1Dout(histo, obsmap[ob].first, obsmap[ob].second, "../HammerProc/temp.dat");


} // int main()
