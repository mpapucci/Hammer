#include "Hammer/Rates/RateD2starDPi.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/ScalarContainer.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
#include "gtest/gtest.h"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableRateD2starDPi : public RateD2starDPi {

    public:
        TestableRateD2starDPi() : RateD2starDPi() {
        }

        using RateD2starDPi::evalAtPSPoint;
    };

    TEST(RateD2starDPiTest, evalAtPSPoint) {

        // Evaluate
        TestableRateD2starDPi rateQ2;
        rateQ2.setSignatureIndex(0); //D1^* -> D*- pi^+
        Tensor rate = rateQ2.evalAtPSPoint({});

        // Compare to direct evaluation
        Tensor rateEval{"", MD::makeVector({1, 1}, {FF_DSSD2STAR, FF_DSSD2STAR_HC},
                                           {0.00333913})};

        // Check TEs.
        for (IndexType idx1 = 0; idx1 < 1; ++idx1) {
            for (IndexType idx2 = 0; idx2 < 1; ++idx2) {
                double comRe = compareVals(rate.element({idx1, idx2}).real(), rateEval.element({idx1, idx2}).real());
                double comIm = compareVals(rate.element({idx1, idx2}).imag(), rateEval.element({idx1, idx2}).imag());
                EXPECT_NEAR(comRe, 1., 1e-4);
                EXPECT_NEAR(comIm, 1., 1e-4);
            }
        }

    }


} // namespace Hammer
