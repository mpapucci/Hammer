set(BoostVersion 1.63.0)
set(BoostSHA256 beae2529f759f6b3bf3f4969a19c2e9d6f0c503edcb2de4a61d1428519fcb3b0)



# Create build folder name derived from version
string(REPLACE "." "_" BoostFolderName ${BoostVersion})
set(BoostFolderName boost_${BoostFolderName})

set(BoostCacheDir ${PROJECT_BINARY_DIR}/ThirdParty/tmp)
set(BoostSourceDir ${PROJECT_BINARY_DIR}/ThirdParty/Boost)
# Check the full path to the source directory is not too long for Windows.  File paths must be less
# than MAX_PATH which is 260.  The current longest relative path Boost tries to create is:
# Build\boost\bin.v2\libs\program_options\build\fd41f4c7d882e24faa6837508d6e5384\libboost_program_options-vc120-mt-gd-1_55.lib.rsp
# which along with a leading separator is 129 chars in length.  This gives a maximum path available
# for 'BoostSourceDir' as 130 chars.
if(WIN32)
  get_filename_component(BoostSourceDirName "${BoostSourceDir}" NAME)
  string(LENGTH "/${BoostSourceDirName}" BoostSourceDirNameLengthWithSeparator)
  math(EXPR AvailableLength 130-${BoostSourceDirNameLengthWithSeparator})
  string(LENGTH "${BoostSourceDir}" BoostSourceDirLength)
  if(BoostSourceDirLength GREATER "130")
    set(Msg "\n\nThe path to boost's source is too long to handle all the files which will ")
    set(Msg "${Msg}be created when boost is built.")
    message(FATAL_ERROR "${Msg}")
  endif()
endif()

# Download boost if required
set(ZipFilePath "${BoostCacheDir}/${BoostFolderName}.tar.bz2")
if(NOT EXISTS ${ZipFilePath})
  message(STATUS "Downloading boost ${BoostVersion} to ${BoostCacheDir}")
endif()
file(DOWNLOAD https://dl.bintray.com/boostorg/release/${BoostVersion}/source/${BoostFolderName}.tar.bz2
     ${ZipFilePath}
     STATUS Status
     SHOW_PROGRESS
     EXPECTED_HASH SHA256=${BoostSHA256}
)

# Extract boost if required
string(FIND "${Status}" "returning early" Found)
if(Found LESS "0" OR NOT IS_DIRECTORY "${BoostSourceDir}")
  message(STATUS "Extracting boost ${BoostVersion} to ${BoostCacheDir}")
  execute_process(COMMAND ${CMAKE_COMMAND} -E tar xfz ${BoostFolderName}.tar.bz2
                  WORKING_DIRECTORY ${BoostCacheDir}
                  RESULT_VARIABLE Result
                  )
  if(NOT Result EQUAL "0")
    message(FATAL_ERROR "Failed extracting boost ${BoostVersion} to ${BoostCacheDir}")
  endif()
  execute_process(COMMAND ${CMAKE_COMMAND} -E rename ${BoostCacheDir}/${BoostFolderName} ${BoostSourceDir})
endif()

# Build b2 (bjam) if required
unset(b2Path CACHE)
find_program(b2Path NAMES b2 PATHS ${BoostSourceDir} NO_DEFAULT_PATH)
if(NOT b2Path)
  message(STATUS "Building b2 (bjam)")
  if(MSVC)
    set(b2Bootstrap "bootstrap.bat")
  else()
    set(b2Bootstrap "./bootstrap.sh")
    if(CMAKE_CXX_COMPILER_ID MATCHES "^(Apple)?Clang$")
      list(APPEND b2Bootstrap --with-toolset=clang)
    elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
      list(APPEND b2Bootstrap --with-toolset=gcc)
    endif()
  endif()
  execute_process(COMMAND ${b2Bootstrap} WORKING_DIRECTORY ${BoostSourceDir}
                  RESULT_VARIABLE Result OUTPUT_VARIABLE Output ERROR_VARIABLE Error)
  if(NOT Result EQUAL "0")
    message(FATAL_ERROR "Failed running ${b2Bootstrap}:\n${Output}\n${Error}\n")
  endif()
endif()
execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory ${BoostSourceDir}/Build)

# Apply patched files
# if(NOT BoostVersion STREQUAL "1.57.0")
#   message(FATAL_ERROR "Remove patched files from the source tree and delete corresponding 'configure_file' commands in this 'add_boost' CMake file.")
# endif()
# configure_file(patches/boost_1_57/boost/thread/pthread/thread_data.hpp ${BoostSourceDir}/boost/thread/pthread/thread_data.hpp COPYONLY)


# Set up general b2 (bjam) command line arguments
set(b2Args <SOURCE_DIR>/b2
           link=static
           threading=multi
           runtime-link=shared
           --build-dir=Build
           stage
           -d+2
           --hash
           )
if(CMAKE_BUILD_TYPE STREQUAL "ReleaseNoInline")
  list(APPEND b2Args "cxxflags=${RELEASENOINLINE_FLAGS}")
endif()
if(CMAKE_BUILD_TYPE STREQUAL "DebugLibStdcxx")
  list(APPEND b2Args define=_GLIBCXX_DEBUG)
endif()

# Set up platform-specific b2 (bjam) command line arguments
if(MSVC)
  if(MSVC11)
    list(APPEND b2Args toolset=msvc-11.0)
  elseif(MSVC12)
    list(APPEND b2Args toolset=msvc-12.0)
  elseif(MSVC14)
    list(APPEND b2Args toolset=msvc-14.0)
  endif()
  list(APPEND b2Args
              define=_BIND_TO_CURRENT_MFC_VERSION=1
              define=_BIND_TO_CURRENT_CRT_VERSION=1
              --layout=versioned
              )
  if(TargetArchitecture STREQUAL "x86_64")
    list(APPEND b2Args address-model=64)
  endif()
elseif(APPLE)
  list(APPEND b2Args variant=release toolset=clang cxxflags=-fPIC cxxflags=-std=c++11 cxxflags=-stdlib=libc++
                     linkflags=-stdlib=libc++ architecture=combined address-model=32_64 --layout=tagged)
elseif(UNIX)
  list(APPEND b2Args --layout=tagged -sNO_BZIP2=1)
  list(APPEND b2Args variant=release cxxflags=-fPIC cxxflags=-std=c++11)
  # Need to configure the toolset based on whatever version CMAKE_CXX_COMPILER is
  string(REGEX MATCH "[0-9]+\\.[0-9]+" ToolsetVer "${CMAKE_CXX_COMPILER_VERSION}")
  if(CMAKE_CXX_COMPILER_ID MATCHES "^(Apple)?Clang$")
    list(APPEND b2Args toolset=clang-${ToolsetVer})
    if(HAVE_LIBC++)
      list(APPEND b2Args cxxflags=-stdlib=libc++ linkflags=-stdlib=libc++)
    endif()
  elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    list(APPEND b2Args toolset=gcc-${ToolsetVer})
  endif()
endif()

# Get list of components
execute_process(COMMAND ./b2 --show-libraries WORKING_DIRECTORY ${BoostSourceDir}
                ERROR_QUIET OUTPUT_VARIABLE Output)
string(REGEX REPLACE "(^[^:]+:|[- ])" "" BoostComponents "${Output}")
string(REGEX REPLACE "\n" ";" BoostComponents "${BoostComponents}")

# Build each required component
add_library(ThirdParty::Boost INTERFACE IMPORTED GLOBAL)
set_property(TARGET ThirdParty::Boost PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${BoostSourceDir})

include(ExternalProject)
include(Utils)
foreach(Component ${BoostComponents})
  list(FIND Boost_BuildComponents ${Component} IdxComponent)
  if(${IdxComponent} GREATER -1)
    ExternalProject_Add(
        boost_${Component}
        INSTALL_DIR ${CMAKE_INSTALL_PREFIX}/packages
        SOURCE_DIR ${BoostSourceDir}
        BINARY_DIR ${BoostSourceDir}
        CONFIGURE_COMMAND ""
        BUILD_COMMAND "${b2Args}" --with-${Component}
        INSTALL_COMMAND ""
        LOG_BUILD ON
    )
    UnderscoreToCapitalized(${Component} CapitalizedComponent)
    add_library(ThirdParty::Boost${CapitalizedComponent} STATIC IMPORTED GLOBAL)
    if(Component STREQUAL "test")
      set(ComponentLibName unit_test_framework)
    else()
      set(ComponentLibName ${Component})
    endif()
    if(MSVC)
      if(MSVC11)
        set(CompilerName vc110)
      elseif(MSVC12)
        set(CompilerName vc120)
      elseif(MSVC14)
        set(CompilerName vc140)
      endif()
      string(REGEX MATCH "[0-9]_[0-9][0-9]" Version "${BoostFolderName}")
      set_target_properties(ThirdParty::Boost${CapitalizedComponent} PROPERTIES
                            IMPORTED_LOCATION_DEBUG ${BoostSourceDir}/stage/lib/libboost_${ComponentLibName}-${CompilerName}-mt-gd-${Version}.lib
                            IMPORTED_LOCATION_MINSIZEREL ${BoostSourceDir}/stage/lib/libboost_${ComponentLibName}-${CompilerName}-mt-${Version}.lib
                            IMPORTED_LOCATION_RELEASE ${BoostSourceDir}/stage/lib/libboost_${ComponentLibName}-${CompilerName}-mt-${Version}.lib
                            IMPORTED_LOCATION_RELWITHDEBINFO ${BoostSourceDir}/stage/lib/libboost_${ComponentLibName}-${CompilerName}-mt-${Version}.lib
                            IMPORTED_LOCATION_RELEASENOINLINE ${BoostSourceDir}/stage/lib/libboost_${ComponentLibName}-${CompilerName}-mt-${Version}.lib
                            LINKER_LANGUAGE CXX)
    else()
      set_target_properties(ThirdParty::Boost${CapitalizedComponent} PROPERTIES
                            IMPORTED_LOCATION ${BoostSourceDir}/stage/lib/libboost_${ComponentLibName}-mt.a
                            LINKER_LANGUAGE CXX)
    endif()
    set_target_properties(boost_${Component} ThirdParty::Boost${CapitalizedComponent} PROPERTIES
                          LABELS Boost FOLDER "ThirdParty/Boost" EXCLUDE_FROM_ALL TRUE)
    add_dependencies(ThirdParty::Boost${CapitalizedComponent} boost_${Component} ThirdParty::Boost)
    set(ThirdParty::Boost${CapitalizedComponent}Libs ThirdParty::Boost${CapitalizedComponent})
    if(Component STREQUAL "locale")
      if(APPLE)
        find_library(IconvLib iconv)
        if(NOT IconvLib)
          message(FATAL_ERROR "libiconv.dylib must be installed to a standard location.")
        endif()
        set(ThirdParty::Boost${CapitalizedComponent}Libs ThirdParty::Boost${CapitalizedComponent} ${IconvLib})
      elseif(UNIX)
        if(BSD)
          find_library(IconvLib libiconv.a)
          if(NOT IconvLib)
            set(Msg "libiconv.a must be installed to a standard location.")
            set(Msg "  For ${Msg} on FreeBSD 10 or later, run\n  pkg install libiconv")
            message(FATAL_ERROR "${Msg}")
          endif()
          set(ThirdParty::Boost${CapitalizedComponent}Libs ThirdParty::Boost${CapitalizedComponent} ${IconvLib})
        else()
          find_library(Icui18nLib libicui18n.a)
          find_library(IcuucLib libicuuc.a)
          find_library(IcudataLib libicudata.a)
          if(NOT Icui18nLib OR NOT IcuucLib OR NOT IcudataLib)
            set(Msg "libicui18n.a, libicuuc.a & licudata.a must be installed to a standard location.")
            set(Msg "  For ${Msg} on Ubuntu/Debian, run\n  sudo apt-get install libicu-dev")
            message(FATAL_ERROR "${Msg}")
          endif()
          set(ThirdParty::Boost${CapitalizedComponent}Libs ThirdParty::Boost${CapitalizedComponent} ${Icui18nLib} ${IcuucLib} ${IcudataLib})
        endif()
      else()
        set(ThirdParty::Boost${CapitalizedComponent}Libs ThirdParty::Boost${CapitalizedComponent})
      endif()
    endif()
    # set(Boost${CapitalizedComponent}Libs ${Boost${CapitalizedComponent}Libs} PARENT_SCOPE)
    list(APPEND ThirdParty::AllBoostLibs ThirdParty::Boost${CapitalizedComponent})
  endif()
endforeach()
# set(AllBoostLibs ${AllBoostLibs} PARENT_SCOPE)
list(FIND Boost_BuildComponents "chrono" indexChrono)
list(FIND Boost_BuildComponents "system" indexSystem)
list(FIND Boost_BuildComponents "coroutine" indexCoroutine)
list(FIND Boost_BuildComponents "context" indexContext)
list(FIND Boost_BuildComponents "filesystem" indexFilesystem)
list(FIND Boost_BuildComponents "graph" indexGraph)
list(FIND Boost_BuildComponents "regex" indexRegex)
list(FIND Boost_BuildComponents "locale" indexLocale)
list(FIND Boost_BuildComponents "log" indexLog)
list(FIND Boost_BuildComponents "date_time" indexDate_time)
list(FIND Boost_BuildComponents "thread" indexThread)
list(FIND Boost_BuildComponents "timer" indexTimer)
list(FIND Boost_BuildComponents "wave" indexWave)
if(${indexChrono} GREATER -1 AND ${indexSystem} GREATER -1)
  add_dependencies(boost_chrono boost_system)
  target_link_libraries(ThirdParty::BoostChrono INTERFACE ThirdParty::BoostSystem)
endif()
if(${indexCoroutine} GREATER -1 AND ${indexContext} GREATER -1 AND ${indexSystem} GREATER -1)
  add_dependencies(boost_coroutine boost_context boost_system)
  target_link_libraries(ThirdParty::BoostCoroutine INTERFACE ThirdParty::BoostContext ThirdParty::BoostSystem)
endif()
if(${indexFilesystem} GREATER -1 AND ${indexSystem} GREATER -1)
  add_dependencies(boost_filesystem boost_system)
  target_link_libraries(ThirdParty::BoostFilesystem INTERFACE ThirdParty::BoostSystem)
endif()
if(${indexGraph} GREATER -1 AND ${indexRegex} GREATER -1)
  add_dependencies(boost_graph boost_regex)
  target_link_libraries(ThirdParty::BoostGraph INTERFACE ThirdParty::BoostRegex)
endif()
if(${indexLocale} GREATER -1 AND ${indexSystem} GREATER -1)
  add_dependencies(boost_locale boost_system)
  target_link_libraries(ThirdParty::BoostLocale INTERFACE ThirdParty::BoostSystem)
endif()
if(${indexThread} GREATER -1 AND ${indexChrono} GREATER -1)
  add_dependencies(boost_thread boost_chrono)
  target_link_libraries(ThirdParty::BoostThread INTERFACE ThirdParty::BoostChrono)
endif()
if(${indexLog} GREATER -1 AND ${indexChrono} GREATER -1 AND ${indexDate_time} GREATER -1 AND ${indexFilesystem} GREATER -1 AND ${indexThread} GREATER -1)
  add_dependencies(boost_log boost_chrono boost_date_time boost_filesystem boost_thread)
  target_link_libraries(ThirdParty::BoostLog INTERFACE ThirdParty::BoostChrono ThirdParty::BoostDateTime ThirdParty::BoostFilesystem ThirdParty::BoostThread)
endif()
if(${indexTimer} GREATER -1 AND ${indexChrono} GREATER -1)
  add_dependencies(boost_timer boost_chrono)
  target_link_libraries(ThirdParty::BoostTimer INTERFACE ThirdParty::BoostChrono)
endif()
if(${indexWave} GREATER -1 AND ${indexChrono} GREATER -1 AND ${indexDate_time} GREATER -1 AND ${indexFilesystem} GREATER -1 AND ${indexThread} GREATER -1)
  add_dependencies(boost_wave boost_chrono boost_date_time boost_filesystem boost_thread)
  target_link_libraries(ThirdParty::BoostWave INTERFACE ThirdParty::BoostChrono ThirdParty::BoostDateTime ThirdParty::BoostFilesystem ThirdParty::BoostThread)
endif()

#==================================================================================================#
# Package                                                                                          #
#==================================================================================================#
if(MSVC)
  foreach(BoostLib BoostChrono BoostDateTime BoostFilesystem BoostLocale BoostProgramOptions BoostRegex BoostSystem BoostThread)
    get_target_property(Location ThirdParty::${BoostLib} IMPORTED_LOCATION_DEBUG)
    install(FILES ${Location} COMPONENT Development CONFIGURATIONS Debug DESTINATION ${CMAKE_INSTALL_PREFIX}/packages/${CMAKE_INSTALL_LIBDIR})
    get_target_property(Location ThirdParty::${BoostLib} IMPORTED_LOCATION_RELEASE)
    install(FILES ${Location} COMPONENT Development CONFIGURATIONS Release DESTINATION ${CMAKE_INSTALL_PREFIX}/packages/${CMAKE_INSTALL_LIBDIR})
  endforeach()
else()
  install(DIRECTORY ${BoostSourceDir}/stage/lib/ COMPONENT Development CONFIGURATIONS Debug Release DESTINATION ${CMAKE_INSTALL_PREFIX}/packages/${CMAKE_INSTALL_LIBDIR})
endif()
