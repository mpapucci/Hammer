set (Hammer_FormFactors_TEST_LIBS FormFactors)

new_test (FFBLPRXPBase "${Hammer_FormFactors_TEST_LIBS}")
new_test (FFBLPRXPVarBase "${Hammer_FormFactors_TEST_LIBS}")
new_test (FFBtoDstarBLPRXP "${Hammer_FormFactors_TEST_LIBS}")
new_test (FFBtoDstarBLPRXPVar "${Hammer_FormFactors_TEST_LIBS}")
new_test (FFBtoDBLPRXP "${Hammer_FormFactors_TEST_LIBS}")
new_test (FFBtoDBLPRXPVar "${Hammer_FormFactors_TEST_LIBS}")
