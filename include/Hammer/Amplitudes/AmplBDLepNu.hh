///
/// @file  AmplBDLepNu.hh
/// @brief \f$ B \rightarrow D \tau\nu \f$ amplitude
/// @brief Also: \f$ B \rightarrow \pi \tau\nu \f$ amplitude
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_AMPL_BDLEPNU
#define HAMMER_AMPL_BDLEPNU

#include "Hammer/Amplitudes/AmplBToQLepNuBase.hh"

namespace Hammer {

    class AmplBDLepNu : public AmplBToQLepNuBase {

    public:
        AmplBDLepNu();

        virtual ~AmplBDLepNu() override {
        }

    public:
        virtual void eval(const Particle& parent, const ParticleList& daughters,
                          const ParticleList& references) override;

    protected:
        virtual void addRefs() const override;

    };

} // namespace Hammer

#endif
