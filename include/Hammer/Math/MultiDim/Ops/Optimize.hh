///
/// @file  Optimize.hh
/// @brief Tensor storage re-optimization algorithm
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_MATH_MULTIDIM_OPS_OPTIMIZE
#define HAMMER_MATH_MULTIDIM_OPS_OPTIMIZE

#include "Hammer/Math/MultiDimensional.fhh"

#include <complex>

namespace Hammer {


    namespace MultiDimensional {

        class IContainer;
        class VectorContainer;
        class SparseContainer;
        class OuterContainer;

        namespace Ops {

            class Optimize final {
            public:
                Optimize();

                IContainer* operator()(VectorContainer& first);
                IContainer* operator()(SparseContainer& first);
                IContainer* operator()(OuterContainer& first);

                IContainer* error(IContainer&);

            };

            inline bool shouldBeSparse(size_t fill, size_t total) {
                return (fill * (sizeof(std::complex<double>) + sizeof(PositionType)) < total * sizeof(std::complex<double>));
            }


        } // namespace Ops

    } // namespace MultiDimensional

} // namespace Hammer


#endif
