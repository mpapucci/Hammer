///
/// @file  Pdg.hh
/// @brief Hammer particle data class
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_TOOLS_PDG
#define HAMMER_TOOLS_PDG

#include <map>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include "Hammer/Tools/Pdg.fhh"
#include "Hammer/IndexTypes.hh"

namespace Hammer {

    /// @brief Hammer class for dealing with particle data
    ///
    /// Provides PDG code information retrieval, particle masses and properties.
    /// Organized as a singleton accessed by the `instance()` method
    ///
    /// @ingroup Tools
    class PID {

    private:
        PID();

        PID(const PID&) = delete;
        PID& operator=(const PID&) = delete;
        PID(PID&&) = delete;
        PID& operator=(PID&&) = delete;
        ~PID();

    public:
        /// @brief convert a particle name to its PDG code.
        ///        if the name corresponds to a set of particles (e.g. "D*"),
        ///        the first is returned
        /// @param[in] name the particle name, see the code for a list of names
        PdgId toPdgCode(const std::string& name) const;

        /// @brief return a list of PDG codes corresponding to a particle name
        ///        E.g. "D*" returns the PDG codes for \f$ D^{*0}, D^{*+} \f$ and their conjugates
        ///        while "Pi0" returns a list with only one PDG code
        /// @param[in] name the particle name, see the code for a list of names
        std::vector<PdgId> toPdgList(const std::string& name) const;

        /// @brief particle mass from a PDG code
        /// @param[in] id the PDG code
        double getMass(PdgId id) const;

        /// @brief particle mass from a particle name
        /// @param[in] name the particle name, see the code for a list of names
        double getMass(const std::string& name) const;

        /// @brief set the particle mass by PDG code
        /// @param[in] id the PDG code
        /// @param[in] value the mass
        void setMass(PdgId id, double value);

        /// @brief set the particle mass by particle name
        /// @param[in] name the particle name, see the code for a list of names
        /// @param[in] value the mass
        void setMass(const std::string& name, double value);

        /// @brief particle width from a PDG code
        /// @param[in] id the PDG code
        double getWidth(PdgId id) const;

        /// @brief particle width from a particle name
        /// @param[in] name the particle name, see the code for a list of names
        double getWidth(const std::string& name) const;

        /// @brief set the particle width by PDG code
        /// @param[in] id the PDG code
        /// @param[in] value the width
        void setWidth(PdgId id, double value);

        /// @brief set the particle width by particle name
        /// @param[in] name the particle name, see the code for a list of names
        /// @param[in] value the width
        void setWidth(const std::string& name, double value);

        /// @brief return \f$ 2s+1 \f$ where s is the particle spin from a PDG code
        /// @param[in] id the PDG code
        size_t getSpinMultiplicity(PdgId id) const;

        /// @brief return \f$ 2s+1 \f$ where s is the particle spin from a particle name
        /// @param[in] name the particle name, see the code for a list of names
        size_t getSpinMultiplicity(const std::string& name) const;

        /// @brief return \f$ \prod_i (2s_i+1) \f$ where \f$ s_i  \f$ is the spin of particle i
        ///       from a list of particle PDG codes
        /// @param[in] ids the PDG codes
        size_t getSpinMultiplicities(const std::vector<PdgId>& ids) const;

        /// @brief return \f$ \prod_i (2s_i+1) \f$ where \f$ s_i  \f$ is the spin of particle i
        ///       from a list of particle names
        /// @param[in] names the names
        size_t getSpinMultiplicities(const std::vector<std::string>& names) const;

        /// @brief return \f$ 3*q \f$ where q is the particle electric charge from a PDG code
        /// @param[in] id the PDG code
        int getThreeCharge(PdgId id) const;

        /// @brief return 3 times the total electric charge from a list of particle PDG codes
        /// @param[in] ids the PDG codes
        int getThreeCharge(const std::vector<PdgId>& ids) const;

        /// @brief the particle lepton number from a PDG code
        /// @param[in] id the PDG code
        int getLeptonNumber(PdgId id) const;

        /// @brief return the total lepton number from a list of particle PDG codes
        /// @param[in] ids the PDG codes
        int getLeptonNumber(const std::vector<PdgId>& ids) const;

        /// @brief the particle baryon number from a PDG code
        /// @param[in] id the PDG code
        int getBaryonNumber(PdgId id) const;

        /// @brief return the total baryon number from a list of particle PDG codes
        /// @param[in] ids the PDG codes
        int getBaryonNumber(const std::vector<PdgId>& ids) const;

        /// @brief the particle lepton numbers for each flavor from a PDG code
        /// @param[in] id the PDG code
        /// @return the triplet \f$ (L_e, L_\mu, L_\tau) \f$
        std::tuple<int, int, int> getLeptonFlavorNumber(PdgId id) const;

        /// @brief return the total lepton numbers for each flabor from a list of particle PDG codes
        /// @param[in] ids the PDG codes
        /// @return the triplet \f$ (L_e, L_\mu, L_\tau) \f$
        std::tuple<int, int, int> getLeptonFlavorNumber(const std::vector<PdgId>& ids) const;

        /// @brief
        /// @param[in] name
        /// @param[in] hadOnly
        /// @return
        std::vector<HashId> expandToValidVertexUIDs(const std::string& name, const bool& hadOnly = false) const;

        /// @brief
        /// @param[in] name
        /// @return
        std::vector<std::pair<PdgId, std::vector<PdgId>>> expandToValidVertices(const std::string& name) const;

        /// @brief get partial Widths
        std::map<HashId, double> getPartialWidths();

        /// @brief
        /// @return
        static PID& instance();

    protected:
        /// @brief
        /// @return
        static PID* getPIDInstance();

        /// @brief initializes the data tables
        void init();

        /// @brief add BRs
        void addBR(PdgId parent, const std::vector<PdgId>& daughters, const double br);

    protected:
        enum location { nj = 1, nq3, nq2, nq1, nl, nr, n, n8, n9, n10 };

        /// @brief check whether a PDG code corresponds to a hadron
        /// @param[in] id the PDG code
        bool isHadron(PdgId id) const;

    public:
        /// @brief check whether a PDG code corresponds to a meson
        /// @param[in] id the PDG code
        bool isMeson(PdgId id) const;

    protected:
        /// @brief check whether a PDG code corresponds to a baryon
        /// @param[in] id the PDG code
        bool isBaryon(PdgId id) const;

        /// @brief check whether a PDG code corresponds to a diquark
        /// @param[in] id the PDG code
        bool isDiQuark(PdgId id) const;

        /// @brief check whether a PDG code corresponds to a pentaquark
        /// @param[in] id the PDG code
        bool isPentaquark(PdgId id) const;

        /// @brief check whether a PDG code corresponds to a charged lepton
        /// @param[in] id the PDG code
        bool isLepton(PdgId id) const;

        /// @brief check whether a PDG code corresponds to a neutrino
        /// @param[in] id the PDG code
        bool isNeutrino(PdgId id) const;

        /// @brief
        /// @param[in] id the PDG code
        /// @return
        PdgId fundamentalID(PdgId id) const;

        /// @brief
        /// @param[in] id the PDG code
        /// @return
        PdgId extraBits(PdgId id) const;

        /// @brief takes the absolute value of a PDG code
        /// @param[in] id the PDG code
        /// @return the code stripped by the sign
        PdgId abspid(PdgId id) const;

        /// @brief extracts a digit from a PDG code
        /// @param[in] loc digit position
        /// @param[in] pid PDG code
        /// @return the digit value
        unsigned short digit(location loc, PdgId pid) const;


    private:
        static PID* _thePID;

        std::map<PdgId, double> _masses;

        std::map<PdgId, double> _widths;

        std::map<HashId, std::pair<double, PdgId>> _brs;

        std::map<std::string, std::vector<PdgId>> _names;

    public:
        /// @name Charged leptons
        //@{
        static const PdgId ELECTRON = 11;
        static const PdgId POSITRON = -ELECTRON;
        static const PdgId EMINUS = ELECTRON;
        static const PdgId EPLUS = POSITRON;
        static const PdgId MUON = 13;
        static const PdgId ANTIMUON = -MUON;
        static const PdgId TAU = 15;
        static const PdgId ANTITAU = -TAU;
        //@}

        /// @name Neutrinos
        //@{
        static const PdgId NU_E = 12;
        static const PdgId NU_EBAR = -NU_E;
        static const PdgId NU_MU = 14;
        static const PdgId NU_MUBAR = -NU_MU;
        static const PdgId NU_TAU = 16;
        static const PdgId NU_TAUBAR = -NU_TAU;
        //@}

        /// @name Bosons
        //@{
        static const PdgId PHOTON = 22;
        static const PdgId GAMMA = PHOTON;
        static const PdgId WBOSON = 24;
        //@}

        /// @name Nucleons
        //@{
        static const PdgId PROTON = 2212;
        static const PdgId ANTIPROTON = -PROTON;
        static const PdgId PBAR = ANTIPROTON;
        static const PdgId NEUTRON = 2112;
        static const PdgId ANTINEUTRON = -NEUTRON;
        //@}

        /// @name Light mesons
        //@{
        static const PdgId PI0 = 111;
        static const PdgId PIPLUS = 211;
        static const PdgId PIMINUS = -PIPLUS;
        static const PdgId RHO0 = 113;
        static const PdgId RHOPLUS = 213;
        static const PdgId RHOMINUS = -RHOPLUS;
        static const PdgId K0L = 130;
        static const PdgId K0S = 310;
        static const PdgId K0 = 311;
        static const PdgId KPLUS = 321;
        static const PdgId KMINUS = -KPLUS;
        static const PdgId ETA = 221;
        static const PdgId ETAPRIME = 331;
        static const PdgId PHI = 333;
        static const PdgId OMEGA = 223;
        //@}

        /// @name Charmonia
        //@{
        static const PdgId ETAC = 441;
        static const PdgId JPSI = 443;
        static const PdgId PSI2S = 100443;
        //@}

        /// @name Charm mesons
        //@{
        static const PdgId D0 = 421;
        static const PdgId DPLUS = 411;
        static const PdgId DMINUS = -DPLUS;
        static const PdgId DSTAR = 423;
        static const PdgId DSTARPLUS = 413;
        static const PdgId DSTARMINUS = -DSTARPLUS;
        static const PdgId DSSD0STAR = 10421;
        static const PdgId DSSD0STARPLUS = 10411;
        static const PdgId DSSD0STARMINUS = -DSSD0STARPLUS;
        static const PdgId DSSD1STAR = 20423;
        static const PdgId DSSD1STARPLUS = 20413;
        static const PdgId DSSD1STARMINUS = -DSSD1STARPLUS;
        static const PdgId DSSD1 = 10423;
        static const PdgId DSSD1PLUS = 10413;
        static const PdgId DSSD1MINUS = -DSSD1PLUS;
        static const PdgId DSSD2STAR = 425;
        static const PdgId DSSD2STARPLUS = 415;
        static const PdgId DSSD2STARMINUS = -DSSD2STARPLUS;
        //@}
        //// @name Charm strange mesons
        //@{
        static const PdgId DSPLUS = 431;
        static const PdgId DSMINUS = -DSPLUS;
        static const PdgId DSSTARPLUS = 433;
        static const PdgId DSSTARMINUS = -DSSTARPLUS;
        static const PdgId DSSDS0STARPLUS = 10431;
        static const PdgId DSSDS0STARMINUS = -DSSDS0STARPLUS;
        static const PdgId DSSDS1STARPLUS = 20433;
        static const PdgId DSSDS1STARMINUS = -DSSDS1STARPLUS;
        static const PdgId DSSDS1PLUS = 10433;
        static const PdgId DSSDS1MINUS = -DSSDS1PLUS;
        static const PdgId DSSDS2STARPLUS = 435;
        static const PdgId DSSDS2STARMINUS = -DSSDS2STARPLUS;
        //@}

        /// @name Bottomonia
        //@{
        static const PdgId ETAB = 551;
        static const PdgId UPSILON1S = 553;
        static const PdgId UPSILON2S = 100553;
        static const PdgId UPSILON3S = 200553;
        static const PdgId UPSILON4S = 300553;
        //@}

        /// @name b mesons
        //@{
        static const PdgId BZERO = 511;
        static const PdgId BPLUS = 521;
        static const PdgId BMINUS = -BPLUS;
        static const PdgId BS = 531;
        static const PdgId BCPLUS = 541;
        static const PdgId BCMINUS = -BCPLUS;
        //@}

        /// @name Baryons
        //@{
        static const PdgId LAMBDA = 3122;
        static const PdgId SIGMA0 = 3212;
        static const PdgId SIGMAPLUS = 3222;
        static const PdgId SIGMAMINUS = 3112;
        static const PdgId LAMBDACPLUS = 4122;
        static const PdgId LAMBDACMINUS = -LAMBDACPLUS;
        static const PdgId LAMBDACSTAR12PLUS = 14122;
        static const PdgId LAMBDACSTAR12MINUS = -LAMBDACSTAR12PLUS;
        static const PdgId LAMBDACSTAR32PLUS = 4124;
        static const PdgId LAMBDACSTAR32MINUS = -LAMBDACSTAR32PLUS;
        static const PdgId LAMBDAB = 5122;
        static const PdgId XI0 = 3322;
        static const PdgId XIMINUS = 3312;
        static const PdgId XIPLUS = -XIMINUS;
        static const PdgId OMEGAMINUS = 3334;
        static const PdgId OMEGAPLUS = -OMEGAMINUS;
        //@}
    };

} // namespace Hammer

#endif
