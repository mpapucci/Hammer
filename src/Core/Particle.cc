///
/// @file  Particle.cc
/// @brief Hammer particle class
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#include "Hammer/Particle.hh"
#include "Hammer/Exceptions.hh"
#include <cmath>
#include <string>

#ifdef HAVE_ROOT
#include "Hammer/Tools/HammerRoot.hh"
#endif


using namespace std;

namespace Hammer {

    Particle::Particle() {
        _code = 0;
        _momentum = FourMomentum(0., 0., 0., 0.);
    }

    Particle::Particle(const FourMomentum& p, PdgId code) {
        _momentum = p;
        _code = code;
    }

#ifdef HAVE_ROOT
    Particle Particle::fromRoot(const TLorentzVector& p, PdgId code) {
        return Particle{{p.E(), p.Px(), p.Py(), p.Pz()}, code};
    }
#endif

    Particle& Particle::setMomentum(const FourMomentum& p) {
        _momentum = p;
        return *this;
    }

    Particle& Particle::setPdgId(PdgId code) {
        _code = code;
        return *this;
    }

    PdgId Particle::pdgId() const {
        return _code;
    }

    const FourMomentum& Particle::momentum() const {
        return _momentum;
    }

    FourMomentum& Particle::momentum() {
        return _momentum;
    }

    const FourMomentum& Particle::p() const {
        return momentum();
    }

    FourMomentum& Particle::p() {
        return _momentum;
    }

} // namespace Hammer
