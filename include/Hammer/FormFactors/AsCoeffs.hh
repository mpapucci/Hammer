///
/// @file  AsCoeffs.hh
/// @brief Hammer class for HQET \f$ \alpha_s \f$ corrections
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_ASCOEFFS
#define HAMMER_FF_ASCOEFFS

namespace Hammer {

    /// @brief Base class for HQET \f$ \alpha_s \f$ corrections
    ///
    /// @ingroup FormFactors
    class AsCoeffs {

    public:
        AsCoeffs() {}

        AsCoeffs(const AsCoeffs& other) = default;
        AsCoeffs& operator=(const AsCoeffs& other) = delete;
        AsCoeffs(AsCoeffs&& other) = delete;
        AsCoeffs& operator=(AsCoeffs&& other) = delete;
        virtual ~AsCoeffs() = default;

    public:
        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double CS(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double CP(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double CV1(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double CV2(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double CV3(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double CA1(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double CA2(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double CA3(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double CT1(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double CT2(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double CT3(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double derCS(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double derCP(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double derCV1(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double derCV2(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double derCV3(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double derCA1(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double derCA2(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double derCA3(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double derCT1(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double derCT2(double w, double z) const;

        /// @brief
        /// @param[in] w
        /// @param[in] z
        /// @return
        double derCT3(double w, double z) const;

    private:

        // Initialization of common variables & functions
        void updateVars(double w, double z) const;

        // DiLog function
        double DiLog(double z) const;

    private:
        mutable double _wSq;
        mutable double _sqrt1wSq;
        mutable double _zSq;
        mutable double _zCu;
        mutable double _zm1Sq;
        mutable double _zp1Sq;
        mutable double _lnz;
        mutable double _wZ;
        mutable double _wP;
        mutable double _wM;
        mutable double _wmwZSq;
        mutable double _lnwP;
        mutable double _rW;
        mutable double _Omega;
        mutable double _w;
        mutable double _z;

    };

} // namespace Hammer

#endif
