///
/// @file  HammerYaml.hh
/// @brief Hammer YaML utility functions
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_TOOLS_HAMMERYAML
#define HAMMER_TOOLS_HAMMERYAML

#include <map>
#include <string>

#include "yaml-cpp/yaml.h"

namespace Hammer {

    template<typename T>
    void writeDict2(YAML::Emitter& emitter, std::map<std::string, std::map<std::string, const T*>> dict2) {
        for(auto& elem: dict2) {
            emitter << YAML::Key << elem.first;
            emitter << YAML::Value << YAML::BeginMap;
            for(auto& elem2: elem.second) {
                emitter << YAML::Key << elem2.first;
                emitter << YAML::Value << *elem2.second;
            }
            emitter << YAML::EndMap;
        }
    }

    template<typename T>
    void writeDict(YAML::Emitter& emitter, std::map<std::string, const T*> dict) {
        for(auto& elem: dict) {
            emitter << YAML::Key << elem.first;
            emitter << YAML::Value << *elem.second;
        }
    }

} // namespace Hammer

#endif
