///
/// @file  AmplitudeBase.hh
/// @brief Hammer base amplitude class
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_AMPLITUDE_BASE
#define HAMMER_AMPLITUDE_BASE

#include <string>
#include <vector>
#include <memory>

#include "Hammer/Particle.fhh"
#include "Hammer/IndexTypes.hh"
#include "Hammer/Math/Tensor.hh"
#include "Hammer/Tools/Logging.hh"
#include "Hammer/Tools/ParticleData.hh"
#include "Hammer/Tools/SettingsConsumer.hh"

namespace Hammer {

    class Log;

    /// @brief Base class for amplitudes
    ///
    /// Provides the amplitude contents as tensor...
    ///
    /// @ingroup Base
    class AmplitudeBase : public ParticleData, public SettingsConsumer {

    public:
        AmplitudeBase();

        AmplitudeBase(const AmplitudeBase& other) = delete;
        AmplitudeBase& operator=(const AmplitudeBase& other) =delete;

        AmplitudeBase(AmplitudeBase&& other) = delete;
        AmplitudeBase& operator=(AmplitudeBase&& other) = delete;
        virtual ~AmplitudeBase() noexcept override = default;

    public:
        /// @brief method to evaluate the object on a specific particle set
        /// @param[in] parent  the parent Particle
        /// @param[in] daughters the daughters (and grand-daughters, if necessary) Particle list
        /// @param[in] references the parent Particle siblings (necessary e.g. for helicity amplitude phase conventions)
        virtual void eval(const Particle& parent, const ParticleList& daughters,
                          const ParticleList& references) override = 0;

    public:

        std::vector<std::complex<double>> getWCVectorFromDict(const std::map<std::string, std::complex<double>>& wcDict) const;
        std::vector<std::complex<double>> getWCVectorFromSettings(WTerm what) const;

        void updateWCSettings(const std::vector<std::complex<double>>& values, WTerm what);
        void updateWCSettings(const std::map<std::string, std::complex<double>>& values, WTerm what);

        /// @brief
        /// @param[in] values
        /// @param[in] data
        /// @return
        void updateWCTensor(std::vector<std::complex<double>> values, MultiDimensional::SharedTensorData& data) const;

        /// @brief
        /// @return
        std::pair<std::string, IndexLabel> getWCInfo() const;

        /// @brief initializes the amplitude (defines settings associated to this amplitude, etc.)
        void init();

        /// @brief returns a reference to itself as a Tensor
        /// @return itself
        Tensor& getTensor();

        /// @brief returns a reference to itself as a Tensor
        /// @return itself
        const Tensor& getTensor() const;

        /// @brief select a specific signature to be the current signature
        /// @param[in] idx  the signature index
        virtual bool setSignatureIndex(size_t idx = 0) override;

        /// @brief
        /// @return
        size_t multiplicityFactor() const;

    protected:

        virtual void preProcessWCValues(std::vector<std::complex<double>>& data) const;

        /// @brief defines new settings for this class
        virtual void defineSettings() override = 0;

        /// @brief adds the index labels for the amplitude tensor for a specific signature to the
        ///        index labels signature list. It will be selected by calling setSignatureIndex
        /// @param[in] tensor  the tensor indices labels
        void addTensor(Tensor&& tensor);

        virtual void updateWilsonCeffLabelPrefix();

    protected:
        /// @brief logging facility
        /// @return   stream to be used for logging
        Log& getLog() const;

    protected:
        std::vector<std::string> _WCNames;
        std::string _WCPrefix;
        IndexLabel _WCLabel;
        std::vector<Tensor> _tensorList;  ///< list of (list of) labels for the tensor indices (one for each signature)
        size_t _multiplicity;

    };

} // namespace Hammer

#endif
