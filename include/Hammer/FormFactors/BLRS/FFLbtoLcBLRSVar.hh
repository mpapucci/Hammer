///
/// @file  FFLbtoLcBLRSVar.hh
/// @brief \f$ \Lambda_b \rightarrow Lambda_c \f$ BLRS form factors
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_LBLCBLRSVAR
#define HAMMER_FF_LBLCBLRSVAR

#include "Hammer/FormFactors/BLRS/FFBLRSBase.hh"

namespace Hammer {

    class FFLbtoLcBLRSVar : public FFBLRSBase {

    public:
        FFLbtoLcBLRSVar();

        FFLbtoLcBLRSVar(const FFLbtoLcBLRSVar& other) = default;
        FFLbtoLcBLRSVar& operator=(const FFLbtoLcBLRSVar& other) = delete;
        FFLbtoLcBLRSVar(FFLbtoLcBLRSVar&& other) = delete;
        FFLbtoLcBLRSVar& operator=(FFLbtoLcBLRSVar&& other) = delete;
        virtual ~FFLbtoLcBLRSVar() override = default;

    public:
        virtual void eval(const Particle& parent, const ParticleList& daughters,
                          const ParticleList& references) override;

        virtual std::unique_ptr<FormFactorBase> clone(const std::string& label) override;

    protected:
        virtual void evalAtPSPoint(const std::vector<double>& point, const std::vector<double>& masses = {}) override;

        virtual void defineSettings() override;
    private:
        mutable bool initialized;
    };

} // namespace Hammer

#endif
