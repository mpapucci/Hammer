///
/// @file  ScalarContainer.hh
/// @brief Order-0 tensor data container
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_MATH_MULTIDIM_SCALARCONTAINER
#define HAMMER_MATH_MULTIDIM_SCALARCONTAINER

#include "Hammer/Math/MultiDim/IContainer.hh"

namespace Hammer {

    class Log;

    namespace Serial {

        struct FBComplex;

    }

    namespace MultiDimensional {

        class ScalarContainer final : public IContainer {
        public:
            ScalarContainer();
            ScalarContainer(const Serial::FBComplex* input);

            size_t rank() const override;
            IndexList dims() const override;
            LabelsList labels() const override;
            size_t numValues() const override;
            size_t dataSize() const override;
            size_t entrySize() const override;
            IndexType labelToIndex(IndexLabel label) const override;

            IndexPairList getSameLabelPairs(const IContainer& other,
                                                    const UniqueLabelsList& indices) const override;
            IndexPairList getSpinLabelPairs() const override;

            bool isSameShape(const IContainer& other) const override;
            bool canAddAt(const IContainer& subContainer, IndexLabel coord, IndexType position) const override;

            reference element(const IndexList& coords = {}) override;
            ElementType element(const IndexList& coords = {}) const override;

            reference element(IndexList::const_iterator start, IndexList::const_iterator end) override;
            ElementType element(IndexList::const_iterator start, IndexList::const_iterator end) const override;

            bool compare(const IContainer& other) const override;
            TensorData clone() const override;
            void clear() override;

            IContainer& operator*=(double value) override;
            IContainer& operator*=(const ElementType value) override;

            IContainer& conjugate() override;

            bool hasNaNs() const override;

            SerialType write(flatbuffers::FlatBufferBuilder* msgwriter) const override;

        private:

            ElementType _data;

        };


        TensorData makeEmptyScalar();
        TensorData makeScalar(std::complex<double> val);

    } // namespace MultiDimensional

} // namespace Hammer


#endif
