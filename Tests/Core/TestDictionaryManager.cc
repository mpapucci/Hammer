#include <fstream>
#include <vector>

#include "gtest/gtest.h"

#include "Hammer/Tools/Pdg.hh"
#include "Hammer/DictionaryManager.hh"
#include "Hammer/ProcessDefinitions.hh"
#include "Hammer/SchemeDefinitions.hh"

using namespace std;

namespace Hammer {

    class TestableProcessDefinitions: public ProcessDefinitions {
    public:
        TestableProcessDefinitions() : ProcessDefinitions() {}
        using ProcessDefinitions::decaySignatures;
        using ProcessDefinitions::forbiddenDecaySignatures;
        using ProcessDefinitions::includedDecaySignatures;
   };

    class TestableSchemeDefinitions: public SchemeDefinitions {
    public:
        TestableSchemeDefinitions() : SchemeDefinitions() {}
        using SchemeDefinitions::getScheme;
   };

    class TestableDictionaryManager: public DictionaryManager {
    public:
        TestableDictionaryManager() : DictionaryManager() {}

        TestableProcessDefinitions& testableProcessDefs() {
            return static_cast<TestableProcessDefinitions&>(this->processDefs());
        }

        TestableSchemeDefinitions& testableSchemeDefs() {
            return static_cast<TestableSchemeDefinitions&>(this->schemeDefs());
        }
   };

    TEST(DictionaryManagerTest, readDecays){
        TestableDictionaryManager seth;
        TestableDictionaryManager evalseth;
        seth.readDecays("test_decsettings.yaml");
        std::vector<std::set<HashId>> va = seth.testableProcessDefs().includedDecaySignatures();
        std::vector<std::set<HashId>> vf = seth.testableProcessDefs().forbiddenDecaySignatures();
        std::vector<std::vector<std::string>> includednames={{ "D*Dpi", "TauPiNu" }, {"B+DTau+Nu"}};
        std::vector<std::set<HashId>> Evalincluded = evalseth.testableProcessDefs().decaySignatures(includednames);
        std::vector<std::vector<std::string>> forbiddennames={{ "B+DTau+Nu", "TauEllNuNu" }};
        std::vector<std::set<HashId>> Evalforbidden = evalseth.testableProcessDefs().decaySignatures(forbiddennames);
        for(size_t i=0; i<va.size();++i)
            EXPECT_EQ(va[i],Evalincluded[i]);
        for(size_t i=0;i<vf.size();++i)
            EXPECT_EQ(vf[i],Evalforbidden[i]);
    }

    TEST(DictionaryManagerTest, loadFFSchemes){
        DictionaryManager seth;
        seth.readDecays("test_decsettings.yaml");
        std::vector<std::string> v = seth.schemeDefs().getFFSchemeNames();
        std::vector<std::string> Eval;
        Eval.push_back("MixedScheme");
        Eval.push_back("AllHQET");
        std::sort(Eval.begin(),Eval.end());
        for(size_t i=0;i<v.size();++i)
            EXPECT_EQ(v[i],Eval[i]);
    }

    TEST(DictionaryManagerTest, saveDecays) {
        TestableDictionaryManager seth;
        seth.readDecays("test_decsettings.yaml");
        seth.saveDecays("testdec.yaml");
        TestableDictionaryManager seth2;
        seth2.readDecays("testdec.yaml");
        std::vector<std::set<HashId>> aldecs=seth2.testableProcessDefs().includedDecaySignatures();
        std::vector<std::set<HashId>> fdecs=seth2.testableProcessDefs().forbiddenDecaySignatures();
        for(size_t i=0;i<aldecs.size();++i)
            EXPECT_EQ(seth.testableProcessDefs().includedDecaySignatures(),seth2.testableProcessDefs().includedDecaySignatures());
        for(size_t j=0;j<fdecs.size();++j)
            EXPECT_EQ(seth.testableProcessDefs().forbiddenDecaySignatures(),seth2.testableProcessDefs().forbiddenDecaySignatures());
        std::vector<std::string> c1=seth.schemeDefs().getFFSchemeNames();
        std::vector<std::string> c2=seth2.schemeDefs().getFFSchemeNames();
        for(size_t k=0; k<c2.size();++k){
            EXPECT_EQ(c1[k],c2[k]);
            EXPECT_EQ(seth.testableSchemeDefs().getScheme(c2[k]),seth2.testableSchemeDefs().getScheme(c2[k]));
        }
    }

    TEST(DictionaryManagerTest, saveAndTestChanges) {
        DictionaryManager seth;
        seth.readDecays("test_decsettings.yaml");
        std::vector<std::string> va = {"B+D*Tau+Nu"};
        seth.processDefs().addIncludedDecay(va);
        std::vector<std::string> vf = {"B+DMu+Nu"};
        seth.processDefs().addForbiddenDecay(vf);
        seth.schemeDefs().removeFFScheme("AllHQET");
        std::map<std::string,std::string> testschemes;
        testschemes.insert(std::pair<std::string,std::string>("BD","ISGW2"));
        testschemes.insert(std::pair<std::string,std::string>("BD*","CLN"));
        seth.schemeDefs().addFFScheme("TestScheme",testschemes);
        std::map<std::string,std::string> testdenom;
        testdenom.insert(std::pair<std::string,std::string>("BD","HQET"));
        testdenom.insert(std::pair<std::string,std::string>("BD*","HQET"));
        seth.schemeDefs().setFFInputScheme(testdenom);
        seth.saveDecays("testdec2.yaml");
        TestableDictionaryManager seth2;
        seth2.readDecays("testdec2.yaml");
        TestableDictionaryManager seth3;
        seth3.readDecays("test_decsettings2.yaml");
        std::vector<std::set<HashId>> aldecs=seth2.testableProcessDefs().includedDecaySignatures();
        std::vector<std::set<HashId>> fdecs=seth2.testableProcessDefs().forbiddenDecaySignatures();
        std::vector<std::string> c1=seth2.schemeDefs().getFFSchemeNames();
        std::vector<std::string> c2=seth3.schemeDefs().getFFSchemeNames();
        std::map<HashId, std::map<std::string, std::vector<std::string>>> m1=seth2.schemeDefs().getFFDuplicates();
        std::map<HashId, std::map<std::string, std::vector<std::string>>> m2=seth3.schemeDefs().getFFDuplicates();
        EXPECT_EQ(seth2.testableProcessDefs().includedDecaySignatures(),seth3.testableProcessDefs().includedDecaySignatures());
        EXPECT_EQ(seth2.testableProcessDefs().forbiddenDecaySignatures(),seth3.testableProcessDefs().forbiddenDecaySignatures());
        for(size_t k=0; k<c2.size();++k){
            EXPECT_EQ(c1[k],c2[k]);
            EXPECT_EQ(seth2.testableSchemeDefs().getScheme(c2[k]),seth3.testableSchemeDefs().getScheme(c2[k]));
        }
        EXPECT_EQ(m1,m2);
    }


} // namespace Hammer
