#include "Hammer/Math/MultiDim/SparseContainer.hh"
#include "Hammer/Math/MultiDim/OuterContainer.hh"
#include "Hammer/Math/MultiDim/ScalarContainer.hh"

#include "gtest/gtest.h"

using namespace std;

namespace Hammer {

    namespace MultiDimensional {

        TEST(OuterTest, ConstructionAccess) {
            auto t = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD, SPIN_DSTAR});
            t->element({1,2,0}) = 0.1;
            t->element({0, 1, 0}) = 0.2;
            const auto t2 = makeOuterSquare(t);
            EXPECT_EQ(t2->rank(), 6);
            EXPECT_EQ(t->rank(), 3);
            EXPECT_EQ(t2->dims()[3],2);
            EXPECT_EQ(t2->dims()[4], 3);
            EXPECT_EQ(t2->dims()[5], 1);
            EXPECT_EQ(t2->labels()[3], WILSON_BCENU_HC);
            EXPECT_EQ(t2->labels()[4], FF_BD_HC);
            EXPECT_EQ(t2->labels()[5], SPIN_DSTAR_HC);
            t->element({0, 0, 0}) = 0.5i;
            const auto t3 = makeOuterSquare(std::move(t));
            EXPECT_EQ(t3->rank(), 6);
            EXPECT_EQ(t3->dims()[3], 2);
            EXPECT_EQ(t3->dims()[4], 3);
            EXPECT_EQ(t3->dims()[5], 1);
            EXPECT_EQ(t3->labels()[3], WILSON_BCENU_HC);
            EXPECT_EQ(t3->labels()[4], FF_BD_HC);
            EXPECT_EQ(t3->labels()[5], SPIN_DSTAR_HC);
            EXPECT_FALSE(t);
            /// @todo fix this const crap
            const OuterContainer* o2 = static_cast<OuterContainer*>(t2.get());
            const OuterContainer* o3 = static_cast<OuterContainer*>(t3.get());
            EXPECT_DOUBLE_EQ(o2->element({0, 0, 0, 0, 0, 0}).real(), 0.);
            EXPECT_DOUBLE_EQ(o3->element({0, 0, 0, 0, 0, 0}).real(),0.5*0.5);
            EXPECT_DOUBLE_EQ(o3->element({0, 0, 0, 0, 0, 0}).imag(), 0.);
        }

        TEST(OuterTest, ConstructionAccess2) {
            auto t = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD, SPIN_DSTAR});
            t->element({1, 2, 0}) = 0.1;
            t->element({0, 1, 0}) = 0.2;
            auto t2 = makeEmptySparse({5, 4, 2}, {WILSON_BCENU, FF_BD, SPIN_DSTAR});
            t2->element({3, 3, 0}) = 0.1;
            t2->element({2, 1, 1}) = 0.2i;
            vector<TensorData> vec;
            vec.push_back(std::move(t));
            vec.push_back(std::move(t2));
            auto res = combineTensors(vec);
            EXPECT_EQ(res->rank(), 6);
            EXPECT_EQ(vec[0]->rank(), 3);
            EXPECT_EQ(vec[1]->rank(), 3);
            EXPECT_EQ(res->dims()[3], 5);
            EXPECT_EQ(res->dims()[4], 4);
            EXPECT_EQ(res->dims()[5], 2);
            EXPECT_EQ(res->labels()[3], WILSON_BCENU);
            EXPECT_EQ(res->labels()[4], FF_BD);
            EXPECT_EQ(res->labels()[5], SPIN_DSTAR);
            vec[0]->element({0, 0, 0}) = 0.5;
            auto res2 = combineTensors(std::move(vec));
            EXPECT_EQ(res2->rank(), 6);
            EXPECT_FALSE(vec[0]);
            EXPECT_FALSE(vec[1]);
            EXPECT_EQ(res2->dims()[3], 5);
            EXPECT_EQ(res2->dims()[4], 4);
            EXPECT_EQ(res2->dims()[5], 2);
            EXPECT_EQ(res2->labels()[3], WILSON_BCENU);
            EXPECT_EQ(res2->labels()[4], FF_BD);
            EXPECT_EQ(res2->labels()[5], SPIN_DSTAR);
            /// @todo fix this const crap
            const OuterContainer* o2 = static_cast<OuterContainer*>(res.get());
            const OuterContainer* o3 = static_cast<OuterContainer*>(res2.get());
            EXPECT_DOUBLE_EQ(o2->element({0, 0, 0, 0, 0, 0}).real(), 0.);
            EXPECT_DOUBLE_EQ(o3->element({0, 0, 0, 0, 0, 0}).real(), 0.);
            EXPECT_DOUBLE_EQ(o2->element({0, 1, 0, 0, 0, 0}).real(), 0.);
            EXPECT_DOUBLE_EQ(o3->element({0, 0, 0, 2, 1, 1}).imag(), 0.5 * 0.2);
        }

        TEST(OuterTest, ValueAccess) {
            auto t = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD, SPIN_DSTAR});
            t->element({1, 2, 0}) = 0.1;
            t->element({0, 1, 0}) = 0.3 + 0.2i;
            auto t2 = makeOuterSquare(t);
            unique_ptr<OuterContainer> o2{static_cast<OuterContainer*>(t2.release())};
            EXPECT_DOUBLE_EQ(o2->value({1, 2, 0, 1, 2, 0}).real(), 0.1 * 0.1);
            EXPECT_DOUBLE_EQ(o2->value({1, 2, 0, 1, 2, 0}).imag(), 0.);
            EXPECT_DOUBLE_EQ(o2->value({0, 1, 0, 0, 1, 0}).real(), 0.3 * 0.3 + 0.2 * 0.2);
            EXPECT_DOUBLE_EQ(o2->value({0, 1, 0, 0, 1, 0}).imag(), 0.);
            EXPECT_DOUBLE_EQ(o2->value({1, 2, 0, 0, 1, 0}).real(), 0.1 * 0.3);
            EXPECT_DOUBLE_EQ(o2->value({1, 2, 0, 0, 1, 0}).imag(), -0.1 * 0.2);
            EXPECT_DOUBLE_EQ(o2->value({0, 1, 0, 1, 2, 0}).real(), 0.3 * 0.1);
            EXPECT_DOUBLE_EQ(o2->value({0, 1, 0, 1, 2, 0}).imag(), 0.2 * 0.1);
        }

        TEST(OuterTest, CopyClone) {
            auto t = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD, SPIN_DSTAR});
            t->element({1, 2, 0}) = 0.1 + 0.2i;
            t->element({0, 1, 0}) = 0.2;
            auto t2 = makeOuterSquare(t);
            auto t4 = t2->clone();
            EXPECT_EQ(t2->rank(), t4->rank());
            EXPECT_EQ(t2->dims()[0], t4->dims()[0]);
            EXPECT_EQ(t2->dims()[1], t4->dims()[1]);
            EXPECT_EQ(t2->dims()[2], t4->dims()[2]);
            EXPECT_EQ(t2->dims()[3], t4->dims()[3]);
            EXPECT_EQ(t2->dims()[4], t4->dims()[4]);
            EXPECT_EQ(t2->dims()[5], t4->dims()[5]);
            EXPECT_EQ(t2->labels()[0], t4->labels()[0]);
            EXPECT_EQ(t2->labels()[1], t4->labels()[1]);
            EXPECT_EQ(t2->labels()[2], t4->labels()[2]);
            EXPECT_EQ(t2->labels()[3], t4->labels()[3]);
            EXPECT_EQ(t2->labels()[4], t4->labels()[4]);
            EXPECT_EQ(t2->labels()[5], t4->labels()[5]);
            /// @todo fix this const crap
            const OuterContainer* o2 = static_cast<OuterContainer*>(t2.get());
            const OuterContainer* o4 = static_cast<OuterContainer*>(t4.get());
            for (IndexType i1 = 0; i1 < 2; ++i1) {
                for (IndexType i2 = 0; i2 < 3; ++i2) {
                    for (IndexType i3 = 0; i3 < 2; ++i3) {
                        for (IndexType i4 = 0; i4 < 3; ++i4) {
                            EXPECT_DOUBLE_EQ(o2->element({i1, i2, 0, i3, i4, 0}).real(),
                                             o4->element({i1, i2, 0, i3, i4, 0}).real());
                            EXPECT_DOUBLE_EQ(o2->element({i1, i2, 0, i3, i4, 0}).imag(),
                                             o4->element({i1, i2, 0, i3, i4, 0}).imag());
                        }
                    }
                }
            }
            t->element({0, 0, 0}) = 0.5;
            auto t3 = makeOuterSquare(std::move(t));
            unique_ptr<const OuterContainer> o3{static_cast<OuterContainer*>(t3.release())};
            const auto t5 = *o3;
            EXPECT_EQ(t5.rank(), t2->rank());
            EXPECT_EQ(t5.dims()[0], o3->dims()[0]);
            EXPECT_EQ(t5.dims()[1], o3->dims()[1]);
            EXPECT_EQ(t5.dims()[2], o3->dims()[2]);
            EXPECT_EQ(t5.labels()[0], o3->labels()[0]);
            EXPECT_EQ(t5.labels()[1], o3->labels()[1]);
            EXPECT_EQ(t5.labels()[2], o3->labels()[2]);
            for (IndexType i1 = 0; i1 < 2; ++i1) {
                for (IndexType i2 = 0; i2 < 3; ++i2) {
                    for (IndexType i3 = 0; i3 < 2; ++i3) {
                        for (IndexType i4 = 0; i4 < 3; ++i4) {
                            EXPECT_DOUBLE_EQ(t5.element({i1, i2, 0, i3, i4, 0}).real(),
                                             o3->element({i1, i2, 0, i3, i4, 0}).real());
                            EXPECT_DOUBLE_EQ(t5.element({i1, i2, 0, i3, i4, 0}).imag(),
                                             o3->element({i1, i2, 0, i3, i4, 0}).imag());
                        }
                    }
                }
            }
        }

        TEST(OuterTest, Comparison) {
            auto t1 = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD, SPIN_DSTAR});
            auto t2 = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD, SPIN_DSTAR});
            auto t3 = makeEmptySparse({4, 3, 1}, {WILSON_BCENU, FF_BD, SPIN_DSTAR});
            auto t4 = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD_HC, SPIN_DSTAR});
            auto t5 = makeEmptyScalar();
            auto o1 = makeOuterSquare(t1);
            auto o2 = makeOuterSquare(t2);
            auto o3 = makeOuterSquare(t3);
            auto o4 = makeOuterSquare(t4);
            auto o5 = makeOuterSquare(t5);
            EXPECT_TRUE(o1->compare(*o2));
            EXPECT_FALSE(o1->compare(*o3));
            EXPECT_FALSE(o1->compare(*o4));
            EXPECT_FALSE(o1->compare(*o5));
            /// @todo fix this const crap
            const OuterContainer* res1 = static_cast<OuterContainer*>(o1.get());
            res1->element({0, 1, 0, 0, 1, 0}) = 0.3;
            EXPECT_FALSE(o1->compare(*o2));
        }

        TEST(OuterTest, ComponentMultiply) {
            auto t = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD, SPIN_DSTAR});
            t->element({1, 2, 0}) = 0.1;
            t->element({0, 1, 0}) = 0.2;
            auto res = makeOuterSquare(t);
            /// @todo fix this const crap
            const OuterContainer* o = static_cast<OuterContainer*>(res.get());
            EXPECT_DOUBLE_EQ(o->element({1, 2, 0, 1, 2, 0}).real(), 0.1 * 0.1);
            EXPECT_DOUBLE_EQ(o->element({0, 1, 0, 0, 1, 0}).real(), 0.2 * 0.2);
            EXPECT_DOUBLE_EQ(o->element({0, 0, 0, 0, 0, 0}).real(), 0.0);
            (*res) *= 3.;
            EXPECT_DOUBLE_EQ(o->element({1, 2, 0, 1, 2, 0}).real(), 3 * 0.1 * 0.1);
            EXPECT_DOUBLE_EQ(o->element({0, 1, 0, 0, 1, 0}).real(), 3 * 0.2 * 0.2);
            EXPECT_DOUBLE_EQ(o->element({0, 0, 0, 0, 0, 0}).real(), 0.0);
        }

        TEST(OuterTest, Conjugate) {
            auto t = makeEmptySparse({2, 3, 1}, {WILSON_BCENU, FF_BD, SPIN_DSTAR});
            t->element({1, 2, 0}) = 0.1 + 0.2i;
            t->element({0, 1, 0}) = 0.2;
            auto t2 = makeOuterSquare(t);
            t2->conjugate();
            unique_ptr<OuterContainer> o2{static_cast<OuterContainer*>(t2.release())};
            EXPECT_DOUBLE_EQ(o2->value({1, 2, 0, 1, 2, 0}).real(), 0.1 * 0.1 + 0.2 * 0.2);
            EXPECT_DOUBLE_EQ(o2->value({1, 2, 0, 1, 2, 0}).imag(), 0.);
            EXPECT_DOUBLE_EQ(o2->value({0, 1, 0, 0, 1, 0}).real(), 0.2 * 0.2);
            EXPECT_DOUBLE_EQ(o2->value({0, 1, 0, 0, 1, 0}).imag(), 0.);
            EXPECT_DOUBLE_EQ(o2->value({1, 2, 0, 0, 1, 0}).real(), 0.1 * 0.2);
            EXPECT_DOUBLE_EQ(o2->value({1, 2, 0, 0, 1, 0}).imag(), -0.2 * 0.2);
            EXPECT_DOUBLE_EQ(o2->value({0, 1, 0, 1, 2, 0}).real(), 0.2 * 0.1);
            EXPECT_DOUBLE_EQ(o2->value({0, 1, 0, 1, 2, 0}).imag(), 0.2 * 0.2);
            EXPECT_EQ(o2->labels()[0], WILSON_BCENU_HC);
            EXPECT_EQ(o2->labels()[1], FF_BD_HC);
            EXPECT_EQ(o2->labels()[2], SPIN_DSTAR_HC);
            EXPECT_EQ(o2->labels()[3], WILSON_BCENU);
            EXPECT_EQ(o2->labels()[4], FF_BD);
            EXPECT_EQ(o2->labels()[5], SPIN_DSTAR);
        }


    } // namespace MultiDimensional

} // namespace Hammer
