///
/// @file  FFBLPRXPBase.hh
/// @brief Hammer base class for BLPR-XP form factors
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_BLPRXP_BASE
#define HAMMER_FF_BLPRXP_BASE

#include<vector>
#include<array>
#include<functional>

#include "Hammer/FormFactorBase.hh"
#include "Hammer/FormFactors/AsCoeffs.hh"

namespace Hammer {

    /// @brief Base class for BLPR-XP form factors
    ///
    /// @ingroup FormFactors
    class FFBLPRXPBase : public FF1to1Base {

    public:
        FFBLPRXPBase();

        FFBLPRXPBase(const FFBLPRXPBase& other) = default;
        FFBLPRXPBase& operator=(const FFBLPRXPBase& other) = delete;
        FFBLPRXPBase(FFBLPRXPBase&& other) = delete;
        FFBLPRXPBase& operator=(FFBLPRXPBase&& other) = delete;
        virtual ~FFBLPRXPBase() override = default;

    protected:
        /// @brief
        /// @param[in] point
        /// @param[in] masses
        /// @return
        virtual void evalAtPSPoint(const std::vector<double>& point, const std::vector<double>& masses = {}) override = 0;

        /// @brief
        virtual void defineSettings() override;


    protected:
        virtual void addRefs() const override;

        double Xi(double w) const; // numerator and denominator separately

        double ZofW(double w) const;

        void calcConstants();

    private:

        void fillIWs();
        void fillLisMis();

    protected:

        AsCoeffs _asCorrections;

        enum IwLabel : size_t { CHI2 = 0, CHI3, ETA, BETA1, BETA2, BETA3, PHI1, PHI1Q };

        std::array<std::function<double(double)>, 8> _IWs;
        std::array<std::function<double(double)>, 7> _Li1;
        std::array<std::function<double(double)>, 7> _Li2;
        std::array<std::function<double(double)>, 25> _Mi;

        double _a;
        double _zBC;

        double _la2OverlaB2;
        double _la1OverlaB2;
        double _eb;
        double _ec;
        double _upsilonc;
        double _upsilonb;
        double _cmagc;
        double _cmagb;

    private:

        double _mb;
        double _mc;
        double _la2;
        double _rho1;
        double _mBBar;
        double _mDBar;
    };

} // namespace Hammer

#endif
