//Validation analysis for Lambda_b to Lambda_c Lep Nu
#include "HepMCParser.hh"
#include "Hammer/Hammer.hh"
#include <iomanip>

using namespace std;

inline void PrintHistogram(Hammer::IOHistogram& histo){
    cout << "{";
    double totwgt = 0.0;
    for(size_t idW = 0; idW < histo.size(); ++idW) {
        totwgt += histo[idW].sumWi;
    }
    for(size_t idW = 0; idW < histo.size() - 1; ++idW) {
        cout << setprecision(5) << histo[idW].sumWi/totwgt << ", ";
    }
    cout << setprecision(5) << histo[histo.size() - 1].sumWi/totwgt << "}";
}

inline double dot(const array<double, 3>& a, const array<double, 3>& b) {
        return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
    }

//First run over events with full tau decay. Trivial (i.e. no) reweighting with histogramming on momenta and opening angles

int main() {
    //Container for histos
    vector<vector<Hammer::IOHistogram>> histocollection{};
    Hammer::Hammer ham{};
    //Do not calculate rates
    ham.setOptions("ProcessCalc: {Rates: false}");
    //Runs and observables
    vector<string> datasets{"Full", "PS", "PSRaw"};
    vector<string> obs{"Q2", "CosThetaLcTau", "PNu", "PMu", "mMuNu", "mMuLc", "CosThetaMuLc", "CosThetaMuNu"};
    vector<uint16_t> dims{20, 10, 20, 20, 20, 20, 10, 10};
    //Included decays
    ham.includeDecay(string("LbLcTauNu"));
    ham.addFFScheme("Scheme", {{"LbLc", "PCR"}});
    ham.setFFInputScheme({{"LbLc", "PCR"}});
    set<string> purePS{"LbLcTauNu"};
    for (auto& elemd : datasets) {
        string elemf = "";
        for(size_t idx = 0; idx < obs.size(); ++idx){
            ham.addHistogram(obs[idx] + elemd, vector<uint16_t>{dims[idx]}, false);
        }
        ham.clearPurePSVertices(Hammer::WTerm::NUMERATOR);
        ham.clearPurePSVertices(Hammer::WTerm::DENOMINATOR);
        if (elemd == "Full"){
            ham.addPurePSVertices(purePS, Hammer::WTerm::NUMERATOR); //PS in numerator and denominator (trivial reweight)
            ham.addPurePSVertices(purePS, Hammer::WTerm::DENOMINATOR);
        } else if (elemd == "PS"){
            elemf = "_PS";
            cout << "Setting input Lb decay to phase space" << endl;
            ham.addPurePSVertices(purePS, Hammer::WTerm::DENOMINATOR); //PS in denominator
        } else if (elemd == "PSRaw"){
            elemf = "_PS";
            cout << "Setting input/output Lb decay to phase space" << endl;
            ham.addPurePSVertices(purePS, Hammer::WTerm::NUMERATOR); //PS in numerator and denominator (trivial reweight)
            ham.addPurePSVertices(purePS, Hammer::WTerm::DENOMINATOR);
        }
        ham.setUnits("GeV");
        ham.initRun();
        auto fin = unique_ptr<ifstream>(new ifstream("./data/LbLcLepNu" + elemf + ".hepmc", std::ios::in));
        auto io = unique_ptr<HepMC::IO_GenEvent>(new HepMC::IO_GenEvent(*fin));
        if (io->rdstate() != 0) {
            return EXIT_FAILURE;
        }
        HepMC::GenEvent ge;
        for(size_t i = 0; i < 25000; ++i) {
            if (io->rdstate() == 0 && io->fill_next_event(&ge)) {
                ham.initEvent();
                auto processes = parseGenEvent(ge, {5122, -5122});
                bool first = true;
                for(auto& elem : processes) {
                    auto procId = ham.addProcess(elem);
                    if (procId != 0 && first){//Process exists and is first one
                        auto BVer = elem.getParticlesByVertex("LbLcTauNu"); //B decay vertex momenta
                        auto pLb = BVer.first;
                        auto pLc = *find_if(BVer.second.begin(), BVer.second.end(), [&](Hammer::Particle& p)-> bool {return (abs(p.pdgId()) > 4000);});
                        auto TVer = elem.getParticlesByVertex("TauEllNuNu"); //Tau decay vertex momenta
                        auto pTau = TVer.first;
                        auto pNu = *find_if(TVer.second.begin(), TVer.second.end(), [&](Hammer::Particle& p)-> bool {return (abs(p.pdgId()) == 16);});
                        auto pMu = *find_if(TVer.second.begin(), TVer.second.end(), [&](Hammer::Particle& p)-> bool {return (abs(p.pdgId()) == 11);});
                        auto pNum = *find_if(TVer.second.begin(), TVer.second.end(), [&](Hammer::Particle& p)-> bool {return (abs(p.pdgId()) == 12);});
                        double q2 = (pLb.p() -  pLc.p())*(pLb.p() -  pLc.p());
                        auto Q2bin = static_cast<uint16_t>(floor(20. * (q2 - 0.0) / (12.0 - 0.0)));
                        double CosThLcTau = dot(pTau.p().pVec(),pLc.p().pVec())/(pTau.p().p() * pLc.p().p());
                        auto CosThLcTaubin = static_cast<uint16_t>(floor(10. * (CosThLcTau + 1.0) / (1.0 + 1.0)));
                        auto PNubin = static_cast<uint16_t>(floor(20. * (pNu.p().p() - 0.0) / (5.0 - 0.0)));
                        auto PMubin = static_cast<uint16_t>(floor(20. * (pMu.p().p() - 0.0) / (5.0 - 0.0)));
                        double mMuNu = (pMu.p() + pNu.p()).mass();
                        auto mMuNubin = static_cast<uint16_t>(floor(20. * (mMuNu + 0.0) / (3.0 - 0.0)));
                        double mMuLc = (pMu.p() + pLc.p()).mass();
                        auto mMuLcbin = static_cast<uint16_t>(floor(20. * (mMuLc + 0.0) / (6.0 - 0.0)));
                        double CosThMuLc = dot(pMu.p().pVec(),pLc.p().pVec())/(pMu.p().p() * pLc.p().p());
                        auto CosThMuLcbin = static_cast<uint16_t>(floor(10. * (CosThMuLc + 1.0) / (1.0 + 1.0)));
                        double CosThMuNu = dot(pMu.p().pVec(),pNum.p().pVec())/(pMu.p().p() * pNum.p().p());
                        auto CosThMuNubin = static_cast<uint16_t>(floor(10. * (CosThMuNu + 1.0) / (1.0 + 1.0)));
                        vector<uint16_t> indices{Q2bin,    CosThLcTaubin, PNubin,       PMubin,
                                                 mMuNubin, mMuLcbin,      CosThMuLcbin, CosThMuNubin};
                        for(size_t idx = 0; idx < obs.size(); ++idx){
                            ham.setEventHistogramBin(obs[idx]+elemd, {indices[idx]});
                        }
                        first = false;
                    } else {
                       ham.removeProcess(procId);
                    }
                }
                ham.processEvent();
                if ((i + 1) % 1000 == 0) {
                    cout << "processed Lb " + elemd + " event " << i + 1 << endl;
//                    cout << ham.getWeight("Scheme") << endl;
                }
            }
        }
        vector<Hammer::IOHistogram> histos{};
        for (size_t idx = 0; idx < obs.size(); ++idx) {
            //WCs set to SM by default
            histos.push_back(ham.getHistogram(obs[idx]+elemd, "Scheme"));
        }
        histocollection.push_back(histos);

    }
    //Print histograms by observable
    for(size_t idx1 = 0; idx1 < obs.size(); ++idx1){
        cout << obs[idx1] << " = {";
        for(size_t idx2 = 0; idx2 < datasets.size() - 1; ++idx2){
            PrintHistogram(histocollection[idx2][idx1]);
            cout << ",";
        }
        PrintHistogram(histocollection[datasets.size() - 1][idx1]);
        cout << "};" << endl << endl;
    }

} // int main()
