//Validation analysis for Tau to Ell Nu Nu
#include "HepMCParser.hh"
#include "Hammer/Hammer.hh"
#include <iomanip>

using namespace std;

inline void PrintHistogram(Hammer::IOHistogram& histo){
    cout << "{";
    double totwgt = 0.0;
    for(size_t idW = 0; idW < histo.size(); ++idW) {
        totwgt += histo[idW].sumWi;
    }
    for(size_t idW = 0; idW < histo.size() - 1; ++idW) {
        cout << setprecision(5) << histo[idW].sumWi/totwgt << ", ";
    }
    cout << setprecision(5) << histo[histo.size() - 1].sumWi/totwgt << "}";
}

inline double dot(const array<double, 3>& a, const array<double, 3>& b) {
        return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
    }

//First run over events with full tau decay. Trivial (i.e. no) reweighting with histogramming on momenta and opening angles

int main() {
    //Container for histos
    vector<vector<Hammer::IOHistogram>> histocollection{};
    Hammer::Hammer ham{};
    //Do not calculate rates
    ham.setOptions("ProcessCalc: {Rates: false}");
    //Runs and observables
    vector<string> datasets{"Full", "PS", "PSRaw"};
    vector<string> obs{"PNu", "PMu", "mMuNu", "mMuD", "CosThetaMuD", "CosThetaMuNu"};
    vector<uint16_t> dims{20, 20, 20, 20, 10, 10};
    //Included decays
    ham.includeDecay(string("TauEllNuNu"));
    ham.addFFScheme("Scheme", {{"BD", "ISGW2"}});
    ham.setFFInputScheme({{"BD", "ISGW2"}});
    set<string> purePS{"TauEllNuNu"};
    for (auto& elemd : datasets) {
        auto elemf = elemd;
        for(size_t idx = 0; idx < obs.size(); ++idx){
            ham.addHistogram(obs[idx] + elemd, vector<uint16_t>{dims[idx]}, false);
        }
        ham.clearPurePSVertices(Hammer::WTerm::NUMERATOR);
        ham.clearPurePSVertices(Hammer::WTerm::DENOMINATOR);
        if (elemd == "PS"){
            cout << "Setting input tau decay to phase space" << endl;
            ham.addPurePSVertices(purePS, Hammer::WTerm::DENOMINATOR); //PS in denominator
        } else if (elemd == "PSRaw") {
            cout << "Setting input and output tau decay to phase space" << endl;
            ham.addPurePSVertices(purePS, Hammer::WTerm::DENOMINATOR); //PS in denominator
            ham.addPurePSVertices(purePS, Hammer::WTerm::NUMERATOR); //PS in denominator
            elemf = "PS";
        }
        ham.setUnits("GeV");
        ham.initRun();
        auto fin = unique_ptr<ifstream>(new ifstream("./data/BTauEll2Nu-" + elemf + ".hepmc", std::ios::in));
        auto io = unique_ptr<HepMC::IO_GenEvent>(new HepMC::IO_GenEvent(*fin));
        if (io->rdstate() != 0) {
            return EXIT_FAILURE;
        }
        HepMC::GenEvent ge;
        for(size_t i = 0; i < 20000; ++i) {
            if (io->rdstate() == 0 && io->fill_next_event(&ge)) {
                ham.initEvent();
                auto processes = parseGenEvent(ge, {511, -511, 521, -521});
                bool first = true;
                for(auto& elem : processes) {
                    auto procId = ham.addProcess(elem);
                    if (procId != 0 && first){//Process exists and is first one
                        auto BVer = elem.getParticlesByVertex("BDTauNu"); //B decay vertex momenta
                        auto pD = *find_if(BVer.second.begin(), BVer.second.end(), [&](Hammer::Particle& p)-> bool {return (abs(p.pdgId()) > 400);});
                        auto TVer = elem.getParticlesByVertex("TauEllNuNu"); //Tau decay vertex momenta
                        auto pNu = *find_if(TVer.second.begin(), TVer.second.end(), [&](Hammer::Particle& p)-> bool {return (abs(p.pdgId()) == 16);});
                        auto pMu = *find_if(TVer.second.begin(), TVer.second.end(), [&](Hammer::Particle& p)-> bool {return (abs(p.pdgId()) == 13);});
                        auto pNum = *find_if(TVer.second.begin(), TVer.second.end(), [&](Hammer::Particle& p)-> bool {return (abs(p.pdgId()) == 14);});

                        auto PNubin = static_cast<uint16_t>(floor(20. * (pNu.p().p() - 0.0) / (5.0 - 0.0)));
                        auto PMubin = static_cast<uint16_t>(floor(20. * (pMu.p().p() - 0.0) / (5.0 - 0.0)));
                        double mMuNu = (pMu.p() + pNu.p()).mass();
                        auto mMuNubin = static_cast<uint16_t>(floor(20. * (mMuNu + 0.0) / (3.0 - 0.0)));
                        double mMuD = (pMu.p() + pD.p()).mass();
                        auto mMuDbin = static_cast<uint16_t>(floor(20. * (mMuD + 0.0) / (6.0 - 0.0)));
                        double CosThMuD = dot(pMu.p().pVec(),pD.p().pVec())/(pMu.p().p() * pD.p().p());
                        auto CosThMuDbin = static_cast<uint16_t>(floor(10. * (CosThMuD + 1.0) / (1.0 + 1.0)));
                        double CosThMuNu = dot(pMu.p().pVec(),pNum.p().pVec())/(pMu.p().p() * pNum.p().p());
                        auto CosThMuNubin = static_cast<uint16_t>(floor(10. * (CosThMuNu + 1.0) / (1.0 + 1.0)));
                        vector<uint16_t> indices{PNubin, PMubin, mMuNubin, mMuDbin, CosThMuDbin, CosThMuNubin};
                        for(size_t idx = 0; idx < obs.size(); ++idx){
                            ham.setEventHistogramBin(obs[idx]+elemd, {indices[idx]});
                        }
                        first = false;
                    } else {
                       ham.removeProcess(procId);
                    }
                }
                ham.processEvent();
                if ((i + 1) % 5000 == 0) {
                    cout << "processed tau"+elemd + " event " << i + 1 << endl;
//                    cout << ham.getWeight("Scheme") << endl;
                }
            }
        }
        vector<Hammer::IOHistogram> histos{};
        for (size_t idx = 0; idx < obs.size(); ++idx) {
            //WCs set to SM by default
            histos.push_back(ham.getHistogram(obs[idx]+elemd, "Scheme"));
        }
        histocollection.push_back(histos);

    }
    //Print histograms by observable
    for(size_t idx1 = 0; idx1 < obs.size(); ++idx1){
        cout << obs[idx1] << " = {";
        for(size_t idx2 = 0; idx2 < datasets.size() - 1; ++idx2){
            PrintHistogram(histocollection[idx2][idx1]);
            cout << ",";
        }
        PrintHistogram(histocollection[datasets.size() - 1][idx1]);
        cout << "};" << endl << endl;
    }

} // int main()
