
include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG(-std=c++14 COMPILER_SUPPORTS_CXX14)
CHECK_CXX_COMPILER_FLAG(-std=c++17 COMPILER_SUPPORTS_CXX17)
CHECK_CXX_COMPILER_FLAG(-std=c++1y COMPILER_SUPPORTS_CXX1Y)
CHECK_CXX_COMPILER_FLAG(-std=c++1z COMPILER_SUPPORTS_CXX1Z)
CHECK_CXX_COMPILER_FLAG(-std=c++20 COMPILER_SUPPORTS_CXX20)
CHECK_CXX_COMPILER_FLAG(-std=c++2a COMPILER_SUPPORTS_CXX2A)

mark_as_advanced(COMPILER_SUPPORTS_CXX14 COMPILER_SUPPORTS_CXX17 COMPILER_SUPPORTS_CXX20 COMPILER_SUPPORTS_CXX1Y COMPILER_SUPPORTS_CXX1Z COMPILER_SUPPORTS_CXX2A)

# use at least C++11, later standards if available
SET(CMAKE_CXX_STANDARD_REQUIRED ON)
SET(CMAKE_CXX_EXTENSIONS ON)

# Require C++11 support, prefer ISO C++ over GNU variants,
# as relying solely on ISO C++ is more portable.
if((COMPILER_SUPPORTS_CXX20 OR COMPILER_SUPPORTS_CXX2A) AND NOT(MAX_CXX_STD LESS 20))
    if(NOT (CMAKE_VERSION VERSION_LESS 3.12.0))
        SET(CMAKE_CXX_STANDARD 20)
    else()
        if(COMPILER_SUPPORTS_CXX20)
            SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++20")
        else()
            SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++2a")
        endif()
    endif()
elseif((COMPILER_SUPPORTS_CXX17 OR COMPILER_SUPPORTS_CXX1Z) AND NOT(MAX_CXX_STD LESS 17))
    if(NOT (CMAKE_VERSION VERSION_LESS 3.8.0))
        SET(CMAKE_CXX_STANDARD 17)
    else()
        if(COMPILER_SUPPORTS_CXX17)
            SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17")
        else()
            SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1z")
        endif()
    endif()
elseif((COMPILER_SUPPORTS_CXX14 OR COMPILER_SUPPORTS_CXX1Y) AND NOT(MAX_CXX_STD LESS 14))
    SET(CMAKE_CXX_STANDARD 14)
else()
    SET(CMAKE_CXX_STANDARD 11)
endif()

if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    SET(CMAKE_CXX_EXTENSIONS OFF)
endif()

# setup verbosity level for compile flags
set(Hammer_CompileOptions "")
set(Hammer_VerboseOptions "")
set(Hammer_SanitizeOptions "")
if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang" OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "AppleClang")
    set(Hammer_VerboseOptions "${Hammer_VerboseOptions} -O0 -fno-optimize-sibling-calls -fno-omit-frame-pointer")
    set(Hammer_VerboseOptions "${Hammer_VerboseOptions} -Weverything -Wextra -Wpedantic -Wno-c++98-compat")
    set(Hammer_VerboseOptions "${Hammer_VerboseOptions} -Wno-c++98-compat-pedantic -Wno-padded -Wno-documentation-deprecated-sync")
    set(Hammer_VerboseOptions "${Hammer_VerboseOptions} -Wno-unknown-pragmas -Wno-weak-vtables")
    set(Hammer_VerboseOptions "${Hammer_VerboseOptions} -fstack-protector-strong -Wstack-protector")
    set(Hammer_VerboseOptions "${Hammer_VerboseOptions} -Wno-documentation -Wno-documentation-unknown-command -Wno-poison-system-directories")    
    set(Hammer_SanitizeOptions "${Hammer_SanitizeOptions} -fsanitize=address -fsanitize=undefined -fno-sanitize=vptr")
elseif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
	message(STATUS "Detected GNU compiler.")
	set(Hammer_CompileOptions "${Hammer_CompileOptions} -ansi -D_FILE_OFFSET_BITS=64 -Wno-unknown-pragmas")
    set(Hammer_VerboseOptions "${Hammer_VerboseOptions} -O0 -Wextra -fno-optimize-sibling-calls -fno-omit-frame-pointer")
    set(Hammer_VerboseOptions "${Hammer_VerboseOptions} -fno-sanitize-recover -fstack-protector")
    set(Hammer_VerboseOptions "${Hammer_VerboseOptions} -pedantic -Wall -Wextra -Wshadow -Wformat=2 -Wfloat-equal -Wconversion -Wlogical-op")
    set(Hammer_VerboseOptions "${Hammer_VerboseOptions} -Wcast-qual -Wcast-align")
    if(CMAKE_CXX_COMPILER_VERSION VERSION_GREATER 6.0.0)
        set(Hammer_VerboseOptions "${Hammer_VerboseOptions} -Wshift-overflow=2 -Wduplicated-cond")
    endif()
    set(Hammer_SanitizeOptions "${Hammer_SanitizeOptions} -fsanitize=address -fsanitize=undefined")
elseif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
  # using Intel C++
else()
    set(Hammer_VerboseOptions "${Hammer_VerboseOptions} -O0")
    set(Hammer_SanitizeOptions "${Hammer_SanitizeOptions}")
endif()
if(NOT (Hammer_CompileOptions STREQUAL ""))
    string(REPLACE " " ";" Hammer_CompileOptions ${Hammer_CompileOptions})
endif()
if(NOT (Hammer_VerboseOptions STREQUAL ""))
    string(REPLACE " " ";" Hammer_VerboseOptions ${Hammer_VerboseOptions})
endif()
if(NOT (Hammer_SanitizeOptions STREQUAL ""))
    string(REPLACE " " ";" Hammer_SanitizeOptions ${Hammer_SanitizeOptions})
endif()

#setting fPIC
IF(CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
	set(Hammer_PIC TRUE)
ELSE()
    set(Hammer_PIC ${CMAKE_POSITION_INDEPENDENT_CODE})
ENDIF()
message(STATUS "Process independent code flag is ${Hammer_PIC}.")

IF(NOT CMAKE_CONFIGURATION_TYPES AND NOT CMAKE_BUILD_TYPE)
	SET(CMAKE_BUILD_TYPE Release CACHE STRING
	  "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel."
	  FORCE)
ENDIF()

#setting correct rpaths
SET(CMAKE_MACOSX_RPATH 1)
SET(CMAKE_SKIP_BUILD_RPATH FALSE)
SET(CMAKE_BUILD_WITH_INSTALL_RPATH TRUE)
SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

#platform information
set(L2_Cache_Width 64)
if(UNIX)
	if(APPLE)
		execute_process(COMMAND sysctl -a machdep.cpu.cache.linesize
						COMMAND sed "s/machdep.cpu.cache.linesize: //"
						OUTPUT_VARIABLE L2_Cache_Width)
	else()
	execute_process(COMMAND getconf LEVEL1_DCACHE_LINESIZE
		            OUTPUT_VARIABLE L2_Cache_Width)
	endif()
endif(UNIX)

include(TestBigEndianCXX)
TEST_BIG_ENDIAN_CXX(IS_BIG_ENDIAN)

include(CheckIncludeFileCXX)
# IF(COMPILER_SUPPORTS_CXX17 OR COMPILER_SUPPORTS_CXX1Z)
# 	check_include_file_cxx(experimental/filesystem HAVE_FILESYSTEM_TS)
# 	IF(NOT HAVE_FILESYSTEM_TS)
# 		UNSET(HAVE_FILESYSTEM_TS CACHE)
# 		check_include_file_cxx(filesystem HAVE_FILESYSTEM_TS)
# 	ENDIF()
# ENDIF()
# IF(UNIX)
# 	check_include_file_cxx("sys/stat.h" HAVE_SYS_STAT_H)
# 	check_include_file_cxx("sys/types.h" HAVE_SYS_TYPES_H)
# 	check_include_file_cxx("sys/sysctl.h" HAVE_SYS_SYSCTL_H)
# ENDIF()
# check_include_file_cxx("rpc/types.h" HAVE_RPC_TYPES_H)
# check_include_file_cxx("rpc/xdr.h" HAVE_RPC_XDR_H)
