#include "gtest/gtest.h"
#include "Hammer/FormFactors/ISGW2/FFBtoRhoOmegaISGW2.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoRhoOmegaISGW2: public FFBtoRhoOmegaISGW2 {

    public:

        TestableFFBtoRhoOmegaISGW2() : FFBtoRhoOmegaISGW2() { }

        using FFBtoRhoOmegaISGW2::initSettings;
        using FFBtoRhoOmegaISGW2::evalAtPSPoint;
    };

    TEST(FFBtoRhoOmegaISGW2Test, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        
        //Momentum decs
        FourMomentum pBmes{5474.7423876404975e+00, 2844.8361598961946e-01, 1776.6650823948621e-01, 1409.3767837987914e+00};
        FourMomentum pRhomes{1613.2517093046810e+00, -7871.9937114550276e-01, 3761.4870333161349e-01, 1084.6451460327347e+00};
        FourMomentum kNuTau{1413.3906670454139e+00, 3690.9499116728806e-01, 1331.7693582553813e+00, 2963.6538529043410e-01};
        FourMomentum pTau{2448.1000112904030e+00, 7025.8799596783428e-01, -1530.2515533475087e+00, 2836.6252475622744e-02};

        //Evaluate the FF class
        TestableFFBtoRhoOmegaISGW2 ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pBmes, -511), {Particle(pRhomes, 213), Particle(kNuTau,-16), Particle(pTau,15)}, {});

        //Compare to direct evaluation
        //Ap,V,A0,A1,A12,T1,T2,T23
        Tensor ffEval{"ffEval", MD::makeVector({8}, {FF_BRHO},
                    {0, 1.15547, 0.487348, 0.384063, 0.22558, 0, 0, 0})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 1; idx1 < 5; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        }
    }


}
