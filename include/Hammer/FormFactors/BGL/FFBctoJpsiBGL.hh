///
/// @file  FFBctoJpsiBGL.hh
/// @brief \f$ B_c \rightarrow J/\psi \f$ BGL form factors
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_BCJPSIBGL
#define HAMMER_FF_BCJPSIBGL

#include "Hammer/FormFactors/BGL/FFBGLBase.hh"

namespace Hammer {

    class FFBctoJpsiBGL : public FFBGLBase {

    public:
        FFBctoJpsiBGL();

        FFBctoJpsiBGL(const FFBctoJpsiBGL& other) = default;
        FFBctoJpsiBGL& operator=(const FFBctoJpsiBGL& other) = delete;
        FFBctoJpsiBGL(FFBctoJpsiBGL&& other) = delete;
        FFBctoJpsiBGL& operator=(FFBctoJpsiBGL&& other) = delete;
        virtual ~FFBctoJpsiBGL() override = default;

    public:

        virtual std::unique_ptr<FormFactorBase> clone(const std::string& label) override;

    protected:
        virtual void evalAtPSPoint(const std::vector<double>& point, const std::vector<double>& masses = {}) override;

        virtual void defineSettings() override;
    private:
        mutable bool initialized;
        
    protected:
        virtual void addRefs() const override;    
    };

} // namespace Hammer

#endif
