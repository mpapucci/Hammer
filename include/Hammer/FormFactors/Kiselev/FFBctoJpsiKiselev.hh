///
/// @file  FFBctoJpsiKiselev.hh
/// @brief \f$ B_c \rightarrow J/\psi \f$ Kiselev form factors
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_BCJPSIKiselev
#define HAMMER_FF_BCJPSIKiselev

#include "Hammer/FormFactors/Kiselev/FFKiselevBase.hh"

namespace Hammer {

    class FFBctoJpsiKiselev : public FFKiselevBase {

    public:
        FFBctoJpsiKiselev();

        FFBctoJpsiKiselev(const FFBctoJpsiKiselev& other) = default;
        FFBctoJpsiKiselev& operator=(const FFBctoJpsiKiselev& other) = delete;
        FFBctoJpsiKiselev(FFBctoJpsiKiselev&& other) = delete;
        FFBctoJpsiKiselev& operator=(FFBctoJpsiKiselev&& other) = delete;
        virtual ~FFBctoJpsiKiselev() override = default;

    public:
        virtual void eval(const Particle& parent, const ParticleList& daughters,
                          const ParticleList& references) override;

        virtual std::unique_ptr<FormFactorBase> clone(const std::string& label) override;

    protected:
        virtual void evalAtPSPoint(const std::vector<double>& point, const std::vector<double>& masses = {}) override;

        virtual void defineSettings() override;
    private:
        mutable bool initialized;
    };

} // namespace Hammer

#endif
