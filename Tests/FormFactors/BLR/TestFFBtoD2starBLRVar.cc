#include "gtest/gtest.h"
#include "Hammer/FormFactors/BLR/FFBtoD2starBLRVar.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoD2starBLRVar: public FFBtoD2starBLRVar {

    public:

        TestableFFBtoD2starBLRVar() : FFBtoD2starBLRVar() { }

        using FFBtoD2starBLRVar::evalAtPSPoint;
        using FFBtoD2starBLRVar::initSettings;

    };

    TEST(FFBtoD2starBLRVarTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pBmes{5280., 0., 0., 0.};
        FourMomentum pD2starmes{3189.85994318182, 0, 0, -2029.45447278719};
        FourMomentum kNuTau{50.20101629360, -24.8428481565870, -43.029075211928, -7.17451174540};
        FourMomentum pTau{2039.93904052459, 24.8428481565870, 43.029075211928, 2036.62898453259};

        //Evaluate the FF class
        TestableFFBtoD2starBLRVar ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pBmes, 511), {Particle(pD2starmes,-415), Particle(kNuTau,14), Particle(pTau,-13)}, {});

        // Compare to direct evaluation
        Tensor ffEval{"ffEval", MD::makeVector({8, 8}, {FF_BDSSD2STAR, FF_BDSSD2STAR_VAR},
                                               {0.546490387225108, 0.780700553178726, 0.307620900305189, 0.364527194110234, 0.101473798088436, -0.281142111174432, -0.0832642109616469, 0.140571055587216, -0.688578008829105, -0.983682869755864, -0.387602402453807, -0.0000357979440126166, 0.0532113110391841, 0.645548433310511, 0, -0.322774216655256, 0.106563686860457, 0.152233838372081, 0.0599849842891214, -0.281142111174432, 0, 0, -0.281142111174432, 0, 0.0388647072104949, 0.0555210103007071, 0.0218770476144996, -0.281021239200277, -0.179668313085996, -0.281142111174432, 0.281142111174432, 0.140571055587216, 0.0760885649215852, 0.108697949887979, 0.0428304566580900, -0.000120871974155222, 0.179668313085996, 0.281142111174432, 0, -0.140571055587216, 0.379386626757174, 0.541980895367392, 0.213557746695949, 0, 0, -0.281142111174432, 0, 0.140571055587216, 0.464770127945815, 0.663957325636879, 0.261620347833779, -0.281021239200277, 0.101473798088436, 0, 0, 0, -0.114672154190115, -0.163817363128736, -0.0645492622313376, 0.281142111174432, 0, 0, -0.281142111174432, 0})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 8; ++idx1) {
            for (IndexType idx2 = 0; idx2 < 8; ++idx2) {
                double comRe = compareVals(t.element({idx1, idx2}).real(),ffEval.element({idx1, idx2}).real());
                EXPECT_NEAR(comRe, 1., 1e-4);
            }
        }
    }


}
