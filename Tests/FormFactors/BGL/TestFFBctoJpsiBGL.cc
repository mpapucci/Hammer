#include "gtest/gtest.h"
#include "Hammer/FormFactors/BGL/FFBctoJpsiBGL.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBctoJpsiBGL: public FFBctoJpsiBGL {

    public:

        TestableFFBctoJpsiBGL() : FFBctoJpsiBGL() { }

        using FFBctoJpsiBGL::evalAtPSPoint;
        using FFBctoJpsiBGL::initSettings;

    };

    TEST(FFBctoJpsiBGLTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pBcmes{6275.,  0.,  0.,  0.};
        FourMomentum pJpsimes{3821.5809561753,  -857.35310207508,  0.,  -2069.83348677229};
        FourMomentum kNuTau{131.0024243058,  -0.827618332,  98.867858366606,  -85.9424039919};
        FourMomentum pTau{2322.4166195189,  858.1807204071,  -98.867858366606,  2155.7758907642};

        //Evaluate the FF class
        TestableFFBctoJpsiBGL ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pBcmes, 541), {Particle(pJpsimes, 443), Particle(kNuTau,12), Particle(pTau,-11)}, {});
        //ff.evalAtPSPoint({Sqq}, {pBmes.mass(), pJpsimes.mass()});

        // Compare to direct evaluation
        // Fs, Ff, Fg, Fm, Fp, Fzt, Fmt, Fpt
        Tensor ffEval{"ffEval",
                      MD::makeVector({8}, {FF_BCJPSI},
                                        {0.,
                                         4427.03290506073,
                                         0.0000826899544190131,
                                         0.000250864518586240,
                                         -0.0000481165205547890,
                                         0.,
                                         0.,
                                         0.})};

        auto& t = ff.getTensor();
        EXPECT_TRUE(t.isSameLabelShape(ffEval));
        for (IndexType idx1 = 1; idx1 < 5; idx1++) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        };
    }


}
