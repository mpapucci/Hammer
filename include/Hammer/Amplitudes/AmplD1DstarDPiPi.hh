///
/// @file  AmplD1DstarDPiPi.hh
/// @brief \f$ D_1 \rightarrow D^* \pi, D^* \rightarrow D \pi \f$ amplitude
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_AMPL_D1DSTARDPIPI
#define HAMMER_AMPL_D1DSTARDPIPI

#include "Hammer/AmplitudeBase.hh"

namespace Hammer {

    class AmplD1DstarDPiPi : public AmplitudeBase {

    public:
        AmplD1DstarDPiPi();

        virtual ~AmplD1DstarDPiPi() override {
        }

    public:
        virtual void eval(const Particle& parent, const ParticleList& daughters,
                          const ParticleList& references) override;
    
    protected:
        virtual void defineSettings() override;
        
    };     
    
} // namespace Hammer

#endif
