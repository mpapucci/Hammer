#include "gtest/gtest.h"
#include "Hammer/FormFactors/BLR/FFBtoD0starBLRVar.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFBtoD0starBLRVar: public FFBtoD0starBLRVar {

    public:

        TestableFFBtoD0starBLRVar() : FFBtoD0starBLRVar() { }

        using FFBtoD0starBLRVar::evalAtPSPoint;
        using FFBtoD0starBLRVar::initSettings;

    };

    TEST(FFBtoD0starBLRVarTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        //Momentum decs
        FourMomentum pBmes{5280., 0., 0., 0.};
        FourMomentum pD0starmes{3117.27272727273, 0, 0, -2104.13622567512};
        FourMomentum kNuTau{49.97993502618, -24.8428481565870, -43.029075211928, -5.41530120089};
        FourMomentum pTau{2112.74733770109, 24.8428481565870, 43.029075211928, 2109.55152687601};

        //Evaluate the FF class
        TestableFFBtoD0starBLRVar ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pBmes, 511), {Particle(pD0starmes,-10411), Particle(kNuTau,14), Particle(pTau,-13)}, {});

        // Compare to direct evaluation
        Tensor ffEval{"ffEval", MD::makeVector({4, 6}, {FF_BDSSD0STAR, FF_BDSSD0STAR_VAR},
                                               {0.495949833064184, 0.708499761520263, 0.164535722165732, -0.345774933396686, 0.610104020438920, -0.478999981264232, -0.131663927760578, -0.188091325372254, -0.0436806668598272, 0.259931083017855, 0, 0, 0.720459193477596, 1.02922741925371, 0.239018678476773, 0, 1.71697794406059, -1.34801996952003, 0.877081840967645, 1.25297405852521, 0.290979620278217, -0.146804930608091, 1.71697794406059, -1.34801996952003})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 4; ++idx1) {
            for (IndexType idx2 = 0; idx2 < 6; ++idx2) {
                double comRe = compareVals(t.element({idx1, idx2}).real(),ffEval.element({idx1, idx2}).real());
                EXPECT_NEAR(comRe, 1., 1e-4);
            }
        }
    }


   TEST(FFBtoD0starBLRVarTest, rename) {

        SettingsHandler seth{};
        seth.addSetting<string>("Hammer", "Units", "MeV");
        seth.addSetting<vector<string>>("BtoD**0*BLRVar","ErrNames", {"name1", "", "name2", "", ""});

        //Evaluate the FF class
        TestableFFBtoD0starBLRVar ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(seth);
        ff.initSettings();

        vector<string>* names = seth.getNamedSettingValue<vector<string>>("BtoD**0*BLRVar", "ErrNames");
        EXPECT_EQ(names->size(), 5);
        EXPECT_EQ(names->at(0), "name1");
        EXPECT_EQ(names->at(1), "delta_ztp");
        EXPECT_EQ(names->at(2), "name2");
        EXPECT_EQ(names->at(3), "delta_chi1");
        EXPECT_EQ(names->at(4), "delta_chi2");
        vector<string>* defaultNames = seth.getEntry("BtoD**0*BLRVar", "ErrNames", WTerm::COMMON)->getDefault<vector<string>>();
        EXPECT_EQ(defaultNames->size(), 5);
        EXPECT_EQ(defaultNames->at(0), "delta_zt1");
        EXPECT_EQ(defaultNames->at(1), "delta_ztp");
        EXPECT_EQ(defaultNames->at(2), "delta_zeta1");
        EXPECT_EQ(defaultNames->at(3), "delta_chi1");
        EXPECT_EQ(defaultNames->at(4), "delta_chi2");
        

        EXPECT_EQ(seth.getNamedSettingValue<double>("BtoD**0*BLRVar", "delta_zt1"), nullptr);
        EXPECT_EQ(*seth.getNamedSettingValue<double>("BtoD**0*BLRVar", "name1"), 0.0);
        EXPECT_EQ(*seth.getNamedSettingValue<double>("BtoD**0*BLRVar", "name2"), 0.0);

    }
}
