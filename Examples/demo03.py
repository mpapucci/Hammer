from math import floor, sqrt
from sys import path

path.append("@CONFIG_PY_DIR@")

from hammer import hepmc, pdg
from hammer.hammerlib import (BinSizes, FourMomentum, Hammer, IOBuffer,
                              Particle, Process)

HAMMER = Hammer()
PARSER = hepmc.HepMCParser("./data/BCLepNu.hepmc")
BUFFER = IOBuffer()
PROC = Process()

def add_particle(part):
    return PROC.add_particle(Particle(FourMomentum(part.p.e, part.p.px, part.p.py, part.p.pz), part.pdg))

def add_decay_filter(base, code, gen_event):
    ret = [None, None, False, None]
    abs_pdg = abs(gen_event.particles[code].pdg)
    if abs_pdg in [pdg.DSTARPLUS, pdg.DSTAR, pdg.DPLUS, pdg.D0]:
        is_dstar = abs_pdg in [pdg.DSTARPLUS, pdg.DSTAR]
        ret[1] = [gen_event.particles[code].p, is_dstar]
    if abs_pdg in [pdg.MUON, pdg.ELECTRON]:
        ret[3] = gen_event.particles[code].p.p()
    if abs_pdg == pdg.TAU:
        ret[2] = True
    if abs_pdg in [pdg.BPLUS, pdg.BZERO]:
        ret[0] = gen_event.particles[code].p
    daughters = {key: add_particle(gen_event.particles[key])
                      for key in gen_event.particles[code].end_vertex(gen_event).children}
    if len(daughters) > 0:
        PROC.add_vertex(base, list(daughters.values()))
    for key,value in daughters.items():
        subret = add_decay_filter(value, key, gen_event)
        if subret != [None, None, False, None]:
            if ret[0] is not None and subret[1] is not None:
                ret[1] = subret[1]
            if ret[2] and ret[3] is None:
                ret[3] = subret[3]
            if not ret[2] and subret[2] and subret[3] is not None:
                ret[2] = subret[2]
                ret[3] = subret[3]
    return ret

def parseGenEvent(gen_event, pdg_list):
    global PROC
    selected = [ key  for key, value in gen_event.particles.items() if value.pdg in pdg_list and value.status_code == 2]
    first = True
    proc_ids = set()
    suffix = "D*"
    is_dstar = True
    for elem in selected:
        PROC = Process()
        suffix = "D*"
        base = add_particle(gen_event.particles[elem])
        ret = add_decay_filter(base, elem, gen_event)
        pid = HAMMER.add_process(PROC)
        if pid != 0:
            proc_ids.add(pid)
            if first:
                if ret[0] is not None and ret[1] is not None:
                    q2 = (ret[0] - ret[1][0]).mass2()
                    is_dstar = ret[1][1]
                    if not is_dstar:
                        suffix = "D"
                    if ret[2] and ret[3] is not None:
                        pl = ret[3]
                        HAMMER.fill_event_histogram("pEllVsQ2:"+suffix, [pl, q2])
                first = False
    return proc_ids, is_dstar


#HAMMER.includeDecay(["BD*TauNu", "TauEllNuNu"])
#HAMMER.includeDecay(["BDTauNu", "TauEllNuNu"])
HAMMER.include_decay(["BD*TauNu", "TauEllNuNu"])
HAMMER.include_decay(["BDTauNu", "TauEllNuNu"])
HAMMER.add_ff_scheme("Scheme1", {"BD": "BLPR", "BD*": "BLPR"})
HAMMER.set_ff_input_scheme({"BD": "ISGW2", "BD*": "ISGW2"})
HAMMER.add_histogram("pEllVsQ2:D*", BinSizes([6,5]), False, [[0,2.5],[3.,12.]])
HAMMER.add_histogram("pEllVsQ2:D", BinSizes([6,5]), False, [[0,2.5],[3.,12.]])
HAMMER.keep_errors_in_histogram("pEllVsQ2:D*", True)
HAMMER.keep_errors_in_histogram("pEllVsQ2:D", True)
HAMMER.collapse_processes_in_histogram("pEllVsQ2:D*")
HAMMER.collapse_processes_in_histogram("pEllVsQ2:D")
HAMMER.add_total_sum_of_weights()
HAMMER.set_units("GeV")
HAMMER.init_run()

count = 0
with open("./DemoHistosPY.dat", 'wb') as fout:
    BUFFER = HAMMER.save_run_header()
    BUFFER.save(fout)
    event = PARSER.read_event()
    evts_dstar = set()
    evts_d = set()
    idx = 0
    count = 0
    while event:
        HAMMER.init_event()
        ids, is_dstar = parseGenEvent(event, [pdg.BPLUS, pdg.BMINUS, pdg.BZERO, -pdg.BZERO])
        if len(ids) > 0:
            HAMMER.process_event()
            count += 1
#            if is_dstar:
#                evts_dstar.add(ids)
#            else:
#                evts_d.add(ids)
        idx += 1
        if idx % 1000 == 0:
            print("Processing event " + str(idx))
        event = PARSER.read_event()
    histo = HAMMER.get_histogram("Total Sum of Weights", "Scheme1")
    print("Events binned: " + str(histo[0].n))
    BUFFER = HAMMER.save_histogram("Total Sum of Weights")
    BUFFER.save(fout)
    BUFFER = HAMMER.save_histogram("pEllVsQ2:D*")
    BUFFER.save(fout)
    BUFFER = HAMMER.save_histogram("pEllVsQ2:D")
    BUFFER.save(fout)
