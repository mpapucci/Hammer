#include "gtest/gtest.h"
#include "Hammer/FormFactors/LSPR/FFLbtoLcstar12LSPR.hh"
#include "Hammer/Math/Constants.hh"
#include "Hammer/Math/FourMomentum.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Math/MultiDim/VectorContainer.hh"
#include "Hammer/Particle.hh"
#include "Hammer/Tools/Pdg.hh"
// May need other stuff here

using namespace std;
using namespace complex_literals;

namespace Hammer {

    namespace MD = MultiDimensional;

    class TestableFFLbtoLcstar12LSPR: public FFLbtoLcstar12LSPR {

    public:

        TestableFFLbtoLcstar12LSPR() : FFLbtoLcstar12LSPR() { }

        using FFLbtoLcstar12LSPR::initSettings;
        using FFLbtoLcstar12LSPR::evalAtPSPoint;
    };

    TEST(FFLbtoLcstar12LSPRTest, eval) {

        SettingsHandler set{};
        set.addSetting<string>("Hammer", "Units", "MeV");
        // Momentum decs
        FourMomentum pLbmes{5620., 0., 0., 0.};
        FourMomentum pLcmes{3053.24065836299, 0, 0, -1608.80499684731};
        FourMomentum kNuTau{103.003318909325, -16.4797557212763, -28.5437742055744, -97.5876749994806};
        FourMomentum pTau{2463.75602272769, 16.4797557212763, 28.5437742055744, 1706.39267184679};

        //Evaluate the FF class
        TestableFFLbtoLcstar12LSPR ff;
        //_FFErrNames = ;
        ff.setSettingsHandler(set);
        ff.initSettings();
        ff.calcUnits();
        ff.eval(Particle(pLbmes, -5122), {Particle(pLcmes, -14122), Particle(kNuTau, 16), Particle(pTau, -15)}, {});

        //Compare to direct evaluation
        Tensor ffEval{"ffEval", MD::makeVector({12}, {FF_LBLCSTAR12},
                                               {0.800689720050897,
                                                0.15180473106173,
                                                0.164627941225841,
                                                -0.699710351366009,
                                                0.00860587827688889,
                                                0.811514188436004,
                                                -0.63695621301262,
                                                0.00834708275883007,
                                                0.854156249543776,
                                                -0.687821574518799,
                                                0.00839946552333471,
                                                -0.0300044513272222})};

        auto& t = ff.getTensor();
        for (IndexType idx1 = 0; idx1 < 12; ++idx1) {
            double comRe = compareVals(t.element({idx1}).real(),ffEval.element({idx1}).real());
            EXPECT_NEAR(comRe, 1., 1e-4);
        }
    }


}
