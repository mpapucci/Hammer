///
/// @file  ExternalData.hh
/// @brief Container class for values of WC and FF vectors
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_EXTERNALDATA
#define HAMMER_EXTERNALDATA

#include <map>
#include <set>
#include <string>
#include <vector>
#include <memory>
#include <unordered_map>

#include <boost/thread/tss.hpp>

#include "Hammer/IndexTypes.hh"
#include "Hammer/Math/MultiDimensional.fhh"
#include "Hammer/Tools/Pdg.fhh"
#include "Hammer/Tools/HammerSerial.fhh"
#include "Hammer/Tools/SettingsConsumer.hh"
#include "Hammer/Tools/Utils.hh"

namespace Hammer {

    class AmplitudeBase;
    class FormFactorBase;
    class Tensor;
    class RunDefinitions;
    class ProvidersRepo;

    /// @brief Main class
    ///
    /// Contains ...
    ///
    /// @ingroup Core
    class ExternalData : public SettingsConsumer {

    public:

        ExternalData(const ProvidersRepo* provs);

        ExternalData(const ExternalData& other) = delete;
        ExternalData& operator=(const ExternalData& other) = delete;
        ExternalData(ExternalData&& other) = delete;
        ExternalData& operator=(ExternalData&& other) = delete;

        virtual ~ExternalData() noexcept override;

    public:

        /// @brief
        /// @param[in] schemeName
        /// @param[in] labels
        /// @return
        virtual const Tensor& getExternalVectors(std::string schemeName, LabelsList labels) const;

        /// @brief
        /// @param[in] parent
        /// @param[in] daughters
        /// @param[in] granddaughters
        /// @param[in] what
        /// @return
        virtual MultiDimensional::SharedTensorData getWilsonCoefficients(PdgId parent, const std::vector<PdgId>& daughters,
                                    const std::vector<PdgId>& granddaughters = {}, WTerm what = WTerm::NUMERATOR) const;

        /// @brief
        /// @param[in] amp
        /// @param[in] what
        /// @return
        virtual MultiDimensional::SharedTensorData getWilsonCoefficients(AmplitudeBase* amp, WTerm what = WTerm::NUMERATOR) const;

        /// @brief
        /// @param[in] amp
        /// @return
        virtual MultiDimensional::SharedTensorData getSpecializedWilsonCoefficients(AmplitudeBase* amp) const;

        /// @brief
        /// @param[in] prefixName
        /// @param[in] settings
        void specializeWCInWeights(const std::string& prefixName, const std::map<std::string, std::complex<double>>& settings);

        /// @brief
        /// @param[in] prefixName
        /// @param[in] values
        void specializeWCInWeights(const std::string& prefixName, const std::vector<std::complex<double>>& values);

        /// @brief
        /// @param[in] prefixName
        void resetSpecializeWCInWeights(const std::string& prefixName);

        /// @brief
        /// @param[in] ampl
        /// @return bool
        bool isWCSpecialized(AmplitudeBase* ampl) const;

        /// @brief
        /// @param[in] prefixName
        /// @param[in] values
        /// @param[in] what
        void setWilsonCoefficients(const std::string& prefixName, const std::vector<std::complex<double>>& values, WTerm what = WTerm::NUMERATOR);

        void setWilsonCoefficients(const std::string& prefixName,
                                   const std::map<std::string, std::complex<double>>& values, WTerm what = WTerm::NUMERATOR);

        /// @brief
        /// @param[in] prefixName
        /// @param[in] values
        void setWilsonCoefficientsLocal(const std::string& prefixName, const std::vector<std::complex<double>>& values);

        void setWilsonCoefficientsLocal(const std::string& prefixName,
                                        const std::map<std::string, std::complex<double>>& values);

        void resetWilsonCoefficients(const std::string& prefixName, WTerm what = WTerm::NUMERATOR);

        MultiDimensional::SharedTensorData getTempWilsonCoefficients(const std::string& prefixName,
                                         const std::vector<std::complex<double>>& values) const;
        MultiDimensional::SharedTensorData getTempWilsonCoefficients(const std::string& prefixName,
                                         const std::map<std::string, std::complex<double>>& values) const;


        /// @brief
        /// @param[in] ff
        /// @param[in] schemeName
        /// @return
        virtual MultiDimensional::SharedTensorData getFFEigenVectors(FormFactorBase* ff, const std::string& schemeName) const;


        // /// @brief
        // /// @param[in] amp
        // /// @param[in] numerator
        // /// @param[in] hConj
        // /// @return
        // virtual const Tensor* getFFEigenVectors(FormFactorBase* ff, WTerm what = WTerm::NUMERATOR) const;

        /// @brief
        /// @param[in] process
        /// @param[in] values
        void setFFEigenVectors(const FFPrefixGroup& process,
                               const std::vector<double>& values);

        /// @brief
        /// @param[in] process
        /// @param[in] values
        void setFFEigenVectors(const FFPrefixGroup& process, const std::map<std::string, double>& values);

        /// @brief
        /// @param[in] process
        /// @param[in] values
        void setFFEigenVectorsLocal(const FFPrefixGroup& process, const std::vector<double>& values);

        /// @brief
        /// @param[in] process
        /// @param[in] values
        void setFFEigenVectorsLocal(const FFPrefixGroup& process, const std::map<std::string, double>& values);

        void resetFFEigenVectors(const FFPrefixGroup& process);

        MultiDimensional::SharedTensorData getTempFFEigenVectors(const FFPrefixGroup& process,
                                     const std::vector<double>& values) const;
        MultiDimensional::SharedTensorData getTempFFEigenVectors(const FFPrefixGroup& process,
                                     const std::map<std::string, double>& values) const;


        void init(std::vector<std::string> schemeNames);

        void reInitFormFactorErrors();

    private:

        void initWilsonCoefficients();
        void initFormFactorErrors();
        void initExternalVectors();

        virtual void defineSettings() override;

        Log& getLog() const;

    private:

        using externalVectorsType = std::unordered_map<std::string, UMap<LabelsList, Tensor>>;
        using processWilsonCoefficientsType = std::map<IndexLabel, std::array<MultiDimensional::SharedTensorData, 2>>;
        using processFFEigenVectorsType = std::map<IndexLabel, SchemeDict<MultiDimensional::SharedTensorData>>;


        mutable boost::thread_specific_ptr<processWilsonCoefficientsType> _processWilsonCoefficients;
        mutable boost::thread_specific_ptr<processFFEigenVectorsType> _processFFEigenVectors;
        mutable boost::thread_specific_ptr<externalVectorsType> _externalVectors;

        std::vector<std::string> _schemes;
        std::map<IndexLabel, MultiDimensional::SharedTensorData> _processSpecializedWilsonCoefficients;
        std::map<std::string, bool> _isWCSpecializedDict{};
        const ProvidersRepo* _providers;

    };

} // namespace Hammer

#endif
