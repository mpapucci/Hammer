///
/// @file  AmplBOmega3PiLepNu.hh
/// @brief \f$ B \rightarrow \omega \tau\nu, \omega \rightarrow 3\pi \f$ amplitude
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_AMPL_BROMEGA3PILEPNU
#define HAMMER_AMPL_BROMEGA3PILEPNU

#include "Hammer/Amplitudes/AmplBToQLepNuBase.hh"

namespace Hammer {

    class AmplBOmega3PiLepNu : public AmplBToQLepNuBase {

    public:
        AmplBOmega3PiLepNu();

        virtual ~AmplBOmega3PiLepNu() override {
        }

    public:
        virtual void eval(const Particle& parent, const ParticleList& daughters,
                          const ParticleList& references) override;
        
    }; 
    
} // namespace Hammer

#endif
