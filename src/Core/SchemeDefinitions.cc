///
/// @file  SchemeDefinitions.cc
/// @brief Container class for Scheme Definitions
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#include <fstream>

#include <boost/algorithm/string.hpp>

#include "yaml-cpp/yaml.h"

#include "Hammer/Exceptions.hh"
#include "Hammer/SchemeDefinitions.hh"
#include "Hammer/Tools/HammerSerial.hh"
#include "Hammer/Tools/Pdg.hh"
#include "Hammer/Tools/Utils.hh"
#include "Hammer/Math/Utils.hh"
#include "Hammer/Tools/ParticleUtils.hh"

using namespace std;

namespace Hammer {


    Log& SchemeDefinitions::getLog() const {
        return Log::getLog("Hammer.SchemeDefinitions");
    }

    void SchemeDefinitions::write(flatbuffers::FlatBufferBuilder* msgwriter,
                                     vector<flatbuffers::Offset<Serial::FBFFScheme>>* schemes) const {
        schemes->reserve(_formFactorSchemes.size());
        for (auto& elem : _formFactorSchemes) {
            vector<uint16_t> chs;
            vector<uint64_t> ids;
            vector<string> keys;
            vector<string> vals;
            for (auto& elem2 : elem.second) {
                chs.push_back(static_cast<uint16_t>(elem2.second));
                ids.push_back(elem2.first);
            }
            if(elem.first == "Denominator") {
                for(auto& elem2: _formFactorBase) {
                    keys.push_back(elem2.first);
                    vals.push_back(elem2.second);
                }
            }
            else {
                auto it = _formFactorSchemeNames.find(elem.first);
                for(auto& elem2: it->second) {
                    keys.push_back(elem2.first);
                    vals.push_back(elem2.second);
                }
            }
            auto serialChs = msgwriter->CreateVector(chs);
            auto serialIds = msgwriter->CreateVector(ids);
            auto serialProcs = msgwriter->CreateVectorOfStrings(keys);
            auto serialGroups = msgwriter->CreateVectorOfStrings(vals);
            auto name = msgwriter->CreateString(elem.first);
            Serial::FBFFSchemeBuilder serialFF{*msgwriter};
            serialFF.add_name(name);
            serialFF.add_ffids(serialIds);
            serialFF.add_ffschemes(serialChs);
            serialFF.add_ffproc(serialProcs);
            serialFF.add_ffgroup(serialGroups);
            auto resFF = serialFF.Finish();
            schemes->push_back(resFF);
        }
    }

    bool SchemeDefinitions::read(const Serial::FBHeader* msgreader, bool merge) {
        auto schemes = msgreader->ffschemes();
        if(!merge) {
            _formFactorSchemes.clear();
            _formFactorSchemeNames.clear();
            _formFactorBase.clear();
        }
        for (unsigned int i = 0; i < schemes->size(); ++i) {
            string name = schemes->Get(i)->name()->c_str();
            auto it = _formFactorSchemes.find(name);
            //if(merge && it != _formFactorSchemes.end()) {
            if(it != _formFactorSchemes.end()) {
                bool result = true;
                auto ids = schemes->Get(i)->ffids();
                auto chs = schemes->Get(i)->ffschemes();
                for (unsigned int j = 0; j < ids->size(); ++j) {
                    auto it2 = it->second.find(ids->Get(j));
                    if(it2 == it->second.end() || it2->second != chs->Get(j)) {
                        result = false;
                    }
                }
                if(name == "Denominator") {
                    auto procs = schemes->Get(i)->ffproc();
                    auto groups = schemes->Get(i)->ffgroup();
                    for (unsigned int j = 0; j < procs->size(); ++j) {
                        auto itd = _formFactorBase.find(procs->Get(j)->c_str());
                        if(itd != _formFactorBase.end()) {
                            if(itd->second != groups->Get(j)->c_str()) {
                                result = false;
                                break;
                            }
                        }
                        _formFactorBase.emplace(procs->Get(j)->c_str(), groups->Get(j)->c_str());
                    }
                }
                else {
                    auto it2 = _formFactorSchemeNames.find(name);
                    if(it2 == _formFactorSchemeNames.end()) {
                        it2 = _formFactorSchemeNames.insert(make_pair(name, map<string, string>{})).first;
                    }
                    auto procs = schemes->Get(i)->ffproc();
                    auto groups = schemes->Get(i)->ffgroup();
                    for (unsigned int j = 0; j < procs->size(); ++j) {
                        auto itn = it2->second.find(procs->Get(j)->c_str());
                        if (itn != it2->second.end()) {
                            if (itn->second != groups->Get(j)->c_str()) {
                                result = false;
                                break;
                            }
                        }
                        it2->second.emplace(procs->Get(j)->c_str(), groups->Get(j)->c_str());
                    }
                }
                if(!result) return false;
            }
            else {
                auto res = _formFactorSchemes.insert({name, map<HashId, FFIndex>{}});
                auto ids = schemes->Get(i)->ffids();
                auto chs = schemes->Get(i)->ffschemes();
                for (unsigned int j = 0; j < ids->size(); ++j) {
                    res.first->second.insert({ids->Get(j), chs->Get(j)});
                }
                if (name == "Denominator") {
                    auto procs = schemes->Get(i)->ffproc();
                    auto groups = schemes->Get(i)->ffgroup();
                    for (unsigned int j = 0; j < procs->size(); ++j) {
                        _formFactorBase.insert({procs->Get(j)->c_str(), groups->Get(j)->c_str()});
                    }
                } else {
                    auto res2 = _formFactorSchemeNames.insert({name, map<string, string>{}});
                    auto procs = schemes->Get(i)->ffproc();
                    auto groups = schemes->Get(i)->ffgroup();
                    for (unsigned int j = 0; j < procs->size(); ++j) {
                        res2.first->second.insert({procs->Get(j)->c_str(), groups->Get(j)->c_str()});
                    }
                }
            }
        }
        return true;
    }

    void SchemeDefinitions::addFFScheme(const string& name, const map<string, string>& schemes) {
        auto it = _formFactorSchemeNames.find(name);
        if (it != _formFactorSchemeNames.end()) {
            it->second = schemes;
        }
        else {
            _formFactorSchemeNames.insert({name, schemes});
        }
    }

    void SchemeDefinitions::removeFFScheme(const string& name) {
        auto it = _formFactorSchemeNames.find(name);
        if (it != _formFactorSchemeNames.end()) {
            _formFactorSchemeNames.erase(it);
        }
    }

    void SchemeDefinitions::setFFInputScheme(const map<string, string>& schemes) {
        _formFactorBase = schemes;
    }

    vector<string> SchemeDefinitions::getFFSchemeNames() const {
        vector<string> names;
        for (auto& elem : _formFactorSchemeNames) {
            names.push_back(elem.first);
        }
        return names;
    }

    const map<HashId, string> SchemeDefinitions::getScheme(const string& name) const {
        const map<string, string>* toProcess;
        if(name.size() == 0) {
            toProcess = &_formFactorBase;
        }
        else {
            auto it = _formFactorSchemeNames.find(name);
            if (it != _formFactorSchemeNames.end()) {
                toProcess = &(it->second);
            }
            else {
                return map<HashId, string>{};
            }
        }
        map<HashId, string> result;
        PID& pdg = PID::instance();
        for(auto& elem: *toProcess) {
            vector<HashId> tmp = pdg.expandToValidVertexUIDs(elem.first, true);
            for(auto elem2: tmp) {
                result[elem2] = elem.second;
            }
        }
        return result;
    }

    map<HashId, map<string, vector<string>>> SchemeDefinitions::getFFDuplicates() const {
        map<HashId, map<string, vector<string>>> result;
        PID& pdg = PID::instance();
        for(auto& elem: _formFactorSchemeNames) {
            for(auto& elem2: elem.second) {
                vector<string> chunks;
                boost::algorithm::split(chunks, elem2.second, boost::algorithm::is_any_of("_"));
                if(chunks.size() >= 2) {
                    string token = chunks[1];
                    for (size_t idx = 2; idx < chunks.size(); ++idx) {token += "_" + chunks[idx];}
                    vector<HashId> tmp = pdg.expandToValidVertexUIDs(elem2.first, true);
                    for(auto id: tmp) {
                        result[id][chunks[0]].push_back(token);
                    }
                }
            }
        }
        for (auto& elem2 : _formFactorBase) {
            vector<string> chunks;
            boost::algorithm::split(chunks, elem2.second, boost::algorithm::is_any_of("_"));
            if (chunks.size() >= 2) {
                string token = chunks[1];
                for (size_t idx = 2; idx < chunks.size(); ++idx) {
                    token += "_" + chunks[idx];
                }
                vector<HashId> tmp = pdg.expandToValidVertexUIDs(elem2.first, true);
                for (auto id : tmp) {
                    result[id][chunks[0]].push_back(token);
                }
            }
        }
        return result;
    }

    const SchemeDict<map<HashId, FFIndex>>& SchemeDefinitions::getSchemeDefs() const {
        return _formFactorSchemes;
    }

    FFIndex SchemeDefinitions::getDenominatorFormFactor(HashId processId) const {
        auto& den = _formFactorSchemes.find("Denominator")->second;
        auto it = den.find(processId);
        if (it != den.end()) {
            return it->second;
        } else {
            return 0;
        }
    }

    set<FFIndex> SchemeDefinitions::getFormFactorIndices(HashId processId) const {
        set<FFIndex> result;
        for (auto& elem : _formFactorSchemes) {
            auto it = elem.second.find(processId);
            if (it != elem.second.end()) {
                result.insert(it->second);
            }
        }
        return result;
    }

    SchemeDict<FFIndex> SchemeDefinitions::getFFSchemesForProcess(HashId id) const {
        SchemeDict<FFIndex> result;
        for (auto& elem : _formFactorSchemes) {
            auto it = elem.second.find(id);
            if (it != elem.second.end()) {
                result.insert({elem.first, it->second});
            } else {
                MSG_ERROR("Process not found for scheme '" + elem.first + "', hash Id: " + to_string(id));
            }
        }
        return result;
    }

    void SchemeDefinitions::init(map<HashId, vector<string>> formFactGroups) {
        _formFactorSchemes.clear();
        auto names = getFFSchemeNames();
        for (auto& elem : names) {
            auto res = _formFactorSchemes.insert({elem, map<HashId, FFIndex>{}});
            if (res.second) {
                auto dict = getScheme(elem);
                map<HashId, string>::iterator ite;
                for (auto& elem2 : dict) {
                    auto it = formFactGroups.find(elem2.first);
                    if (it != formFactGroups.end()) {
                        auto it2 = find(it->second.begin(), it->second.end(), elem2.second);
                        if(it2 == it->second.end()){
                            MSG_ERROR("The parametrization '" + elem2.second + "' in scheme '" + elem +"' is unknown or misassigned. You're gonna need a bigger boat.");
                        }
                        ptrdiff_t pos =
                            distance(it->second.begin(), find(it->second.begin(), it->second.end(), elem2.second));
                        ptrdiff_t dim = static_cast<ptrdiff_t>(it->second.size());
                        if (pos >= 0 && pos < dim) {
                            res.first->second.insert({elem2.first, static_cast<size_t>(pos)});
                        }
                    }
                }
            }
        }
        auto dict = getScheme();
        if(dict.size() != 0){
            auto res = _formFactorSchemes.insert({"Denominator", map<HashId, FFIndex>{}});
            if (res.second) {
                for (auto& elem2 : dict) {
                    auto it = formFactGroups.find(elem2.first);
                    if (it != formFactGroups.end()) {
                        ptrdiff_t pos =
                            distance(it->second.begin(), find(it->second.begin(), it->second.end(), elem2.second));
                        ptrdiff_t dim = static_cast<ptrdiff_t>(it->second.size());
                        if (pos >= 0 && pos < dim) {
                            res.first->second.insert({elem2.first, static_cast<size_t>(pos)});
                        }
                    }
                }
            }
        }
    }

    YAML::Emitter& operator<<(YAML::Emitter& out, const SchemeDefinitions& s) {
        out << YAML::convert<SchemeDefinitions>::encode(s);
        return out;
    }

} // namespace Hammer

namespace YAML {

    Node convert<::Hammer::SchemeDefinitions>::encode(const ::Hammer::SchemeDefinitions& value) {
        YAML::Node node;
        if (value._formFactorSchemeNames.size() > 0) {
            YAML::Node numNode;
            for(auto& elem: value._formFactorSchemeNames) {
                YAML::Node tmpAssoc;
                for(auto& elem2: elem.second) {
                    tmpAssoc[elem2.first] = elem2.second;
                }
                numNode[elem.first] = tmpAssoc;
            }
            node["NumeratorSchemes"] = numNode;
        }
        YAML::Node tmpAssoc;
        for (auto& elem : value._formFactorBase) {
            tmpAssoc[elem.first] = elem.second;
        }
        node["Denominator"] = tmpAssoc;
        return node;
    }

    bool convert<::Hammer::SchemeDefinitions>::decode(const Node& node, ::Hammer::SchemeDefinitions& value) {
        if (node.IsMap()) {
            for (const auto& entry2 : node) {
                string what = entry2.first.as<string>();
                if (what == "NumeratorSchemes") {
                    YAML::Node schemes = entry2.second;
                    if (schemes.IsMap()) {
                        value._formFactorSchemeNames.clear();
                        for (const auto& scheme : schemes) {
                            string name = scheme.first.as<string>();
                            auto res = value._formFactorSchemeNames.insert({name, map<string, string>{}});
                            if (scheme.second.IsMap()) {
                                YAML::Node processes = scheme.second;
                                for (const auto& process : processes) {
                                    string procname = process.first.as<string>();
                                    string proctype = process.second.as<string>();
                                    res.first->second.insert({procname, proctype});
                                }
                            } else {
                                return false;
                            }
                        }
                    } else {
                        return false;
                    }
                } else if (what == "Denominator") {
                    value._formFactorBase.clear();
                    if (entry2.second.IsMap()) {
                        YAML::Node processes = entry2.second;
                        for (const auto& process : processes) {
                            string procname = process.first.as<string>();
                            string proctype = process.second.as<string>();
                            value._formFactorBase.insert({procname, proctype});
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }

}
