///
/// @file  ProcResults.cc
/// @brief Container for process-related results of weight calculation
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#include "Hammer/Tools/HammerSerial.hh"
#include "Hammer/ProcResults.hh"
#include "Hammer/Tools/Logging.hh"
#include "Hammer/Tools/Utils.hh"

#include <iostream>

using namespace std;

namespace Hammer {

    ProcResults::ProcResults() {
        // initSettings();
    }

    ProcResults::ProcResults(const Serial::FBProcData* msgreader) {
        // initSettings();
        read(msgreader, false);
    }

    // ProcResults::~ProcResults() {
    // }

    const Tensor& ProcResults::processAmplitude(WTerm what) const {
        return _processAmplitude.get(what);
    }

    Tensor& ProcResults::processAmplitude(WTerm what) {
        return _processAmplitude.get(what);
    }

    const Tensor& ProcResults::processAmplitudeSquared(WTerm what) const {
        return _processSquaredAmplitude.get(what);
    }

    Tensor& ProcResults::processAmplitudeSquared(WTerm what) {
        return _processSquaredAmplitude.get(what);
    }

    vector<reference_wrapper<const Tensor>> ProcResults::processFormFactors(const string& name) const {
        vector<reference_wrapper<const Tensor>> result;
        auto it = _processFormFactors.find(name);
        if (it != _processFormFactors.end()) {
            transform(it->second.begin(), it->second.end(), back_inserter(result),
                      [](const Tensor& elem) -> reference_wrapper<const Tensor> { return cref(elem); });
        }
        return result;
    }

    vector<reference_wrapper<Tensor>> ProcResults::processFormFactors(const string& name) {
        vector<reference_wrapper<Tensor>> result;
        auto it = _processFormFactors.find(name);
        if (it != _processFormFactors.end()) {
            transform(it->second.begin(), it->second.end(), back_inserter(result),
                      [](Tensor& elem) -> reference_wrapper<Tensor> { return ref(elem); });
        }
        return result;
    }

    void ProcResults::appendFormFactor(const std::string& name, const Tensor& amp) {
        _processFormFactors[name].push_back(amp);
    }

    void ProcResults::appendFormFactor(const std::string& name, Tensor&& amp) {
        _processFormFactors[name].push_back(std::move(amp));
    }

    void ProcResults::clearFormFactors() {
        _processFormFactors.clear();
    }

    bool ProcResults::haveFormFactors() const {
        return _processFormFactors.size() > 0;
    }

    vector<string> ProcResults::availableSchemes() const {
        vector<string> result;
        result.reserve(_processFormFactors.size());
        transform(_processFormFactors.begin(), _processFormFactors.end(), back_inserter(result), [](const pair<string, vector<Tensor>>& val) -> string { return val.first; });
        return result;
    }

    const Tensor& ProcResults::processWeight(const string& name) const {
        return getOrThrow(_processWeights, name, RangeError("Invalid scheme name"));
    }

    Tensor& ProcResults::processWeight(const string& name) {
        return getOrThrow(_processWeights, name, RangeError("Invalid scheme name"));
    }

    void ProcResults::setProcessWeight(const string& name, const Tensor& amp) {
        _processWeights[name] = amp;
    }

    void ProcResults::setProcessWeight(const string& name, Tensor&& amp) {
        _processWeights[name] = std::move(amp);
    }

    void ProcResults::clearWeights() {
        _processWeights.clear();
    }

    bool ProcResults::haveWeights() const {
        return _processWeights.size() > 0;
    }


    void ProcResults::defineSettings() {
        setPath("ProcessWrite");
        addSetting<bool>("Amplitudes", true);
        addSetting<bool>("SquaredAmplitudes", true);
        addSetting<bool>("FormFactors", true);
    }

    void ProcResults::write(flatbuffers::FlatBufferBuilder* msgwriter, flatbuffers::Offset<Serial::FBProcData>* msg) const {
        vector<flatbuffers::Offset<Serial::FBFormFactor>> ffs;
        vector<flatbuffers::Offset<Serial::FBTensor>> wgtsVals;
        vector<string> wgtsNames;
        ffs.reserve(_processFormFactors.size());
        wgtsVals.reserve(_processWeights.size());
        wgtsNames.reserve(_processWeights.size());
        if(isOn("FormFactors")) {
            for (auto& elem : _processFormFactors) {
                auto name = msgwriter->CreateString(elem.first);
                vector<flatbuffers::Offset<Serial::FBTensor>> ffVec;
                ffVec.reserve(elem.second.size());
                for (auto& elem2 : elem.second) {
                    flatbuffers::Offset<Serial::FBTensor> val;
                    elem2.write(msgwriter, &val);
                    ffVec.push_back(val);
                }
                auto serialFFVec = msgwriter->CreateVector(ffVec);
                Serial::FBFormFactorBuilder serialFF{*msgwriter};
                serialFF.add_id(name);
                serialFF.add_ffsvals(serialFFVec);
                auto resFF = serialFF.Finish();
                ffs.push_back(resFF);
            }
        }
        for (auto& elem : _processWeights) {
            wgtsNames.push_back(elem.first);
            flatbuffers::Offset<Serial::FBTensor> val;
            elem.second.write(msgwriter, &val);
            wgtsVals.push_back(val);
        }
        flatbuffers::Offset<Serial::FBTensor> ampnum;
        flatbuffers::Offset<Serial::FBTensor> amp2num;
        flatbuffers::Offset<Serial::FBTensor> ampden;
        flatbuffers::Offset<Serial::FBTensor> amp2den;
        if(isOn("Amplitudes")) {
            _processAmplitude.numerator.write(msgwriter, &ampnum);
            _processAmplitude.denominator.write(msgwriter, &ampden);
        }
        if(isOn("SquaredAmplitudes")) {
            _processSquaredAmplitude.numerator.write(msgwriter, &amp2num);
            _processSquaredAmplitude.denominator.write(msgwriter, &amp2den);
        }
        flatbuffers::Offset<flatbuffers::Vector<flatbuffers::Offset<Serial::FBFormFactor>>> serialFF;
        if(isOn("FormFactors")) {
            serialFF = msgwriter->CreateVector(ffs);
        }
        auto serialWgtN = msgwriter->CreateVectorOfStrings(wgtsNames);
        auto serialWgtV = msgwriter->CreateVector(wgtsVals);
        Serial::FBProcDataBuilder serialprocess{*msgwriter};
        if(isOn("Amplitudes")) {
            serialprocess.add_ampnum(ampnum);
            serialprocess.add_ampden(ampden);
        }
        if(isOn("SquaredAmplitudes")) {
            serialprocess.add_amp2num(amp2num);
            serialprocess.add_amp2den(amp2den);
        }
        if(isOn("FormFactors")) {
            serialprocess.add_formfacts(serialFF);
        }
        serialprocess.add_weightames(serialWgtN);
        serialprocess.add_weighvalues(serialWgtV);
        *msg = serialprocess.Finish();
    }

    bool ProcResults::read(const Serial::FBProcData* msgreader, bool merge) {
        if (msgreader != nullptr) {
            bool result = true;
            if(!merge && flatbuffers::IsFieldPresent(msgreader, Serial::FBProcData::VT_AMPNUM)) {
                _processAmplitude.numerator.read(msgreader->ampnum());
                _processAmplitude.denominator.read(msgreader->ampden());
            }
            if(!merge && flatbuffers::IsFieldPresent(msgreader, Serial::FBProcData::VT_AMP2NUM)) {
                _processSquaredAmplitude.numerator.read(msgreader->amp2num());
                _processSquaredAmplitude.denominator.read(msgreader->amp2den());
            }
            if(flatbuffers::IsFieldPresent(msgreader, Serial::FBProcData::VT_FORMFACTS)) {
                auto formfacts = msgreader->formfacts();
                if(!merge) {
                    _processFormFactors.clear();
                }
                for (unsigned int i = 0; i < formfacts->size() && result; ++i) {
                    auto elem = formfacts->Get(i);
                    auto it = _processFormFactors.find(elem->id()->c_str());
                    if(it != _processFormFactors.end() && strcmp(elem->id()->c_str(), "Denominator") != 0) {
                        MSG_ERROR("Try to merge two process form factor schemes with same name '" + string(elem->id()->c_str()) +
                                  "'!");
                        result = false;
                    } else {
                        auto res = _processFormFactors.insert({elem->id()->c_str(), vector<Tensor>{}});
                        auto ffVec = elem->ffsvals();
                        for (unsigned int j = 0; j < ffVec->size(); ++j) {
                            auto elem2 = ffVec->Get(j);
                            Tensor t;
                            t.read(elem2);
                            res.first->second.push_back(std::move(t));
                        }
                    }
                }
            }
            auto wName = msgreader->weightames();
            auto wVal = msgreader->weighvalues();
            if(!merge) {
                _processWeights.clear();
            }
            for (unsigned int i = 0; i < wName->size() && result; ++i) {
                auto it = _processWeights.find(wName->Get(i)->c_str());
                if(it != _processWeights.end()) {
                    MSG_ERROR("Try to merge two process weights with same name '" + string(wName->Get(i)->c_str()) + "'!");
                    result = false;
                }
                else {
                    Tensor t;
                    t.read(wVal->Get(i));
                    _processWeights.insert({wName->Get(i)->c_str(), t});
                }
            }
            return result;
        }
        return false;
    }

    Log& ProcResults::getLog() const {
        return Log::getLog("Hammer.ProcResults");
    }

} // namespace Hammer
