///
/// @file  FFLbtoLcBLRS.hh
/// @brief \f$ \Lambda_b \rightarrow Lambda_c \f$ BLRS form factors
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_FF_LBLCBLRS
#define HAMMER_FF_LBLCBLRS

#include "Hammer/FormFactors/BLRS/FFBLRSBase.hh"

namespace Hammer {

    class FFLbtoLcBLRS : public FFBLRSBase {

    public:
        FFLbtoLcBLRS();

        FFLbtoLcBLRS(const FFLbtoLcBLRS& other) = default;
        FFLbtoLcBLRS& operator=(const FFLbtoLcBLRS& other) = delete;
        FFLbtoLcBLRS(FFLbtoLcBLRS&& other) = delete;
        FFLbtoLcBLRS& operator=(FFLbtoLcBLRS&& other) = delete;
        virtual ~FFLbtoLcBLRS() override = default;

    public:
        virtual void eval(const Particle& parent, const ParticleList& daughters,
                          const ParticleList& references) override;

        virtual std::unique_ptr<FormFactorBase> clone(const std::string& label) override;

    protected:
        virtual void evalAtPSPoint(const std::vector<double>& point, const std::vector<double>& masses = {}) override;

        virtual void defineSettings() override;
    private:
        mutable bool initialized;
    };

} // namespace Hammer

#endif
