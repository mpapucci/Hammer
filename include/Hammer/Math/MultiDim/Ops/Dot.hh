///
/// @file  Dot.hh
/// @brief Tensor dot product algorithm
///

//**** This file is a part of the HAMMER library
//**** Copyright (C) 2016 - 2024 The HAMMER Collaboration
//**** HAMMER is licensed under version 3 of the GPL; see COPYING for details
//**** Please note the MCnet academic guidelines; see GUIDELINES for details

// -*- C++ -*-
#ifndef HAMMER_MATH_MULTIDIM_OPS_DOT
#define HAMMER_MATH_MULTIDIM_OPS_DOT

#include "Hammer/Math/MultiDimensional.fhh"
#include "Hammer/Math/MultiDim/BlockIndexing.hh"

namespace Hammer {


    namespace MultiDimensional {

        class IContainer;
        class VectorContainer;
        class SparseContainer;
        class OuterContainer;

        namespace Ops {

            class Dot final {
            public:
                Dot(const IndexPairList& indices, std::pair<bool, bool> shouldHC = {false, false});

                IContainer* operator()(VectorContainer& first, const VectorContainer& second);
                IContainer* operator()(SparseContainer& first, const SparseContainer& second);

                IContainer* operator()(SparseContainer& first, const VectorContainer& second);

                IContainer* operator()(OuterContainer& first, const OuterContainer& second);
                IContainer* operator()(OuterContainer& first, const SparseContainer& second);
                IContainer* operator()(SparseContainer& first, const OuterContainer& second);
                IContainer* operator()(OuterContainer& first, const IContainer& second);

                IContainer* operator()(IContainer& first, const IContainer& second);

                IContainer* error(IContainer&, const IContainer&);

            private:
                std::pair<IndexList, LabelsList> getNewIndexLabels(const IContainer& first,
                                                                   const IContainer& second) const;
                std::pair<IndexList, LabelsList> getNewIndexLabels(const BlockIndexing& lhs, const BlockIndexing& rhs,
                                                                    const DotGroupType& chunk) const;
                std::pair<IndexList, LabelsList> getNewIndexLabels(const LabeledIndexing<AlignedIndexing>& lhs, const BlockIndexing& rhs,
                                                                   const DotGroupType& chunk) const;
                std::pair<IndexList, LabelsList> getNewIndexLabels(const BlockIndexing& lhs,
                                                                   const LabeledIndexing<AlignedIndexing>& rhs,
                                                                   const DotGroupType& chunk) const;

                IndexList combineIndex(const IndexList& a, const IndexList& b) const;

                SharedTensorData calcSharedDot(SharedTensorData origin, const IContainer& other,
                                               const IndexPairList& indices, std::pair<bool, bool> shouldHC = {false, false});

                unsigned long long dotSignature(const IContainer& a, const IContainer& b,
                                                const std::string& type) const;

            public:
                DotGroupList partitionContractions(const BlockIndexing& lhs, const BlockIndexing& rhs) const;
                DotGroupList partitionContractions(const LabeledIndexing<AlignedIndexing>& lhs, const BlockIndexing& rhs) const;
                DotGroupList partitionContractions(const BlockIndexing& lhs, const LabeledIndexing<AlignedIndexing>& rhs) const;

                IndexPairList _indices;
                std::pair<bool, bool> _hc;
                UniqueIndexList _idxLeft;
                UniqueIndexList _idxRight;

            };

        } // namespace Ops

    } // namespace MultiDimensional

} // namespace Hammer


#endif
