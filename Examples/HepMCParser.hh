#include <vector>
#include <set>

#include "HepMC/IO_GenEvent.h"
#include "HepMC/GenEvent.h"
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"

#include "Hammer/Process.hh"
#include <chrono>
#include <fstream>


inline Hammer::Particle toParticle(const HepMC::GenParticle* p) {
    return Hammer::Particle{{p->momentum().e(), p->momentum().px(),
                             p->momentum().py(), p->momentum().pz()},
                             p->pdg_id()};
}

static inline void addDecayAndProducts(Hammer::Process& proc, const HepMC::GenParticle* p, size_t parent) {
    std::vector<size_t> daughters;
    auto endv = p->end_vertex();
    for(auto it = endv->particles_out_const_begin(); it != endv->particles_out_const_end(); ++it) {
        daughters.push_back(proc.addParticle(toParticle(*it)));
        if((*it)->status() == 2 && (*it)->end_vertex() != nullptr && (*it)->pdg_id() != 111) {
            addDecayAndProducts(proc, *it, daughters.back());
        }
    }
    proc.addVertex(parent, daughters);
}

static inline std::vector<Hammer::Process> parseGenEvent(const HepMC::GenEvent& evt, std::set<int> pdgCodes) {
    std::vector<Hammer::Process> results;
    for (HepMC::GenEvent::particle_const_iterator pi = evt.particles_begin(); pi != evt.particles_end(); ++pi) {
        if((*pi)->status() == 2 && pdgCodes.find((*pi)->pdg_id()) != pdgCodes.end()) {
            Hammer::Process p;
            addDecayAndProducts(p, *pi, p.addParticle(toParticle(*pi)));
            results.push_back(p);
        }
    }
    return results;
}
