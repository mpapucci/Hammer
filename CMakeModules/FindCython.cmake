# Find the Cython compiler.
#
# This code sets the following variables:
#
#  Cython_EXECUTABLE
#
# See also UseCython.cmake

#=============================================================================
# Copyright 2011 Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#=============================================================================

# Use the Cython executable that lives next to the Python executable
# if it is a local installation.
get_filename_component( _python_path ${Python3_EXECUTABLE} DIRECTORY )
execute_process(COMMAND "${Python3_EXECUTABLE}" "-c"
  "import re, cython; print(re.compile('/__init__.py.*').sub('',cython.__file__))"
  RESULT_VARIABLE _cython_status
  OUTPUT_VARIABLE _cython_path
  ERROR_QUIET
  OUTPUT_STRIP_TRAILING_WHITESPACE)
if(NOT _cython_status)
  get_filename_component(_cython_path ${_cython_path} DIRECTORY)
  string(FIND "${_cython_path}" "lib" _cython_path_lib_pos REVERSE)
  string(SUBSTRING ${_cython_path} 0 ${_cython_path_lib_pos} _cython_path)
  set(_cython_path "${_cython_path}bin")
endif()

IF(EXISTS "${_cython_path}/cython")
  SET(Cython_EXECUTABLE "${_cython_path}/cython")
ELSE()
  find_program(Cython_EXECUTABLE
    NAMES cython cython.bat
    HINTS ${_cython_path} ${_python_path}
  )
ENDIF()

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Cython FOUND_VAR Cython_FOUND REQUIRED_VARS Cython_EXECUTABLE)

mark_as_advanced(Cython_EXECUTABLE)
