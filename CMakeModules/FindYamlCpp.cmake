# Locate yaml-cpp
#
# This module defines
#  YamlCpp_FOUND, if false, do not try to link to yaml-cpp
#  YamlCpp_LIBRARY, where to find yaml-cpp
#  YamlCpp_INCLUDE_DIR, where to find yaml.h
#
# By default, the dynamic libraries of yaml-cpp will be found. To find the static ones instead,
# you must set the YamlCpp_STATIC_LIBRARY variable to TRUE before calling find_package(YamlCpp ...).
#
# If yaml-cpp is not installed in a standard path, you can use the YamlCpp_DIR CMake variable
# to tell CMake where yaml-cpp is.

# attempt to find static library first if this is set
find_package(PkgConfig)
pkg_check_modules(PC_YamlCpp QUIET yaml-cpp)
set(YamlCpp_DEFINITIONS ${PC_YamlCpp_CFLAGS_OTHER})

if(YamlCpp_STATIC_LIBRARY)
    set(YamlCpp_STATIC libyaml-cpp.a)
endif()


# find the yaml-cpp include directory
find_path(YamlCpp_INCLUDE_DIR yaml-cpp/yaml.h
          HINTS ${YamlCpp_DIR}/include
          ${PC_YamlCpp_INCLUDEDIR} ${PC_YamlCpp_INCLUDE_DIRS}
          PATH_SUFFIXES include
          PATHS
          ${YamlCpp_DIR}/include/
          /usr/local/include/
          /usr/include/
          /sw/yaml-cpp/         # Fink
          /opt/local/yaml-cpp/  # DarwinPorts
          /opt/csw/yaml-cpp/    # Blastwave
          /opt/yaml-cpp/
          )
mark_as_advanced(YamlCpp_INCLUDE_DIR)

# find the yaml-cpp library
find_library(YamlCpp_LIBRARY
             NAMES ${YamlCpp_STATIC} yaml-cpp
             HINTS ${YamlCpp_DIR}/lib ${YamlCpp_DIR}/lib64
             ${PC_YamlCpp_LIBDIR} ${PC_YamlCpp_LIBRARY_DIRS}
             PATH_SUFFIXES lib64 lib
             PATHS
                   ${YamlCpp_DIR}/lib
                   /usr/local
                    /usr
                    /sw
                    /opt/local
                    /opt/csw
                    /opt)
mark_as_advanced(YamlCpp_LIBRARY)

if(PC_YamlCpp_VERSION)
   set(YamlCpp_VERSION_STRING ${PC_YamlCpp_VERSION})
else()
  if(CMAKE_C_COMPILER_LOADED)
    include(CheckIncludeFile)
    check_include_file("${YamlCpp_INCLUDE_DIR}/yaml-cpp/aliasmanager.h" YAMLCPP_V03)
    check_include_file("${YamlCpp_INCLUDE_DIR}/yaml-cpp/node/detail/iterator_fwd.h" YAMLCPP_V05)
  else()
    include(CheckIncludeFileCXX)
    check_include_file_cxx("${YamlCpp_INCLUDE_DIR}/yaml-cpp/aliasmanager.h" YAMLCPP_V03)
    check_include_file_cxx("${YamlCpp_INCLUDE_DIR}/yaml-cpp/node/detail/iterator_fwd.h" YAMLCPP_V05)
  endif()
  if(YAMLCPP_V03)
    set(YamlCpp_VERSION_STRING "0.3.0")
  elseif(YAMLCPP_V05)
    set(YamlCpp_VERSION_STRING "0.5.0")
  endif()
  UNSET(YAMLCPP_V03 CACHE)
  UNSET(YAMLCPP_V05 CACHE)
endif()


if(YamlCpp_VERSION_STRING MATCHES "([0-9]+)\\.([0-9]+)\\.([0-9]+)")
     set(YamlCpp_VERSION_MAJOR "${CMAKE_MATCH_1}")
     set(YamlCpp_VERSION_MINOR "${CMAKE_MATCH_2}")
     set(YamlCpp_VERSION_PATCH "${CMAKE_MATCH_3}")
endif()

# handle the QUIETLY and REQUIRED arguments and set YamlCpp_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(YamlCpp
                                  REQUIRED_VARS YamlCpp_INCLUDE_DIR YamlCpp_LIBRARY
                                  VERSION_VAR YamlCpp_VERSION_STRING
                                 )

if(YAMLCPP_FOUND)
  set(YamlCpp_LIBRARIES ${YamlCpp_LIBRARY} )
  set(YamlCpp_INCLUDE_DIRS ${YamlCpp_INCLUDE_DIR} )
endif(YAMLCPP_FOUND)
